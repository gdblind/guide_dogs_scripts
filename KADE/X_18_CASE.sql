USE GDB_Final_migration
GO

BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblVetAuthorization 
			WHERE   SF_Object LIKE '%case%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblVetAuthorization 
			WHERE  SF_Object_2 LIKE '%allow%'
END 


BEGIN --record type
	SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE='case'
END 


BEGIN--DROP CASE

	DROP TABLE GDB_Final_migration.DBO.IMP_CASE_parent
	DROP TABLE GDB_Final_migration.DBO.IMP_CASE
END


BEGIN-- CREATE CASE (PARENT) 

		SELECT DISTINCT 
				X1.ID OwnerId
				 ,'Pfa-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS Legacy_Id__c
				 ,NULL [Contact:Legacy_ID__c]
				 ,T1.pfa_DogID AS [Dog__r:Legacy_Id__c]
				 ,'0123D0000004cplQAA' AS  RecordTypeId				-- Pratt Fund
				 ,'tblPrattFundAuthorization' AS Source_Data_Table__c
				 ,NULL AS CloseDate
				 ,T1.pfa_Descision  AS  [Status]
				 ,T1.pfa_CaseSummary  AS  [Description]	
				 ,NULL AS [Type]
				 ,NULL AS Reason__c
				 ,NULL AS Reason_Detail__c
				 ,NULL AS [Subject]
				,CAST(T1.pfa_SubmitDate AS DATE ) AS  CreatedDate
				,NULL AS Alert_Date__c
				,NULL AS Alert_Sent__c
				,NULL AS Alert_Recipient__C
				,NULL AS Ingestion_Type__c
				,NULL AS Require_Vet__c
				,NULL AS Survey_Summary__c

					
				--UPDATE FB-00151 adn FB-00448
				,T1.pfa_DxCode AS [Pre_Existing_Diagnosis__r:Legacy_ID__c]
 				--,T2.DiagnosisText AS  Diagnosis__c	
				--,T3.CategoryDesc  AS  Diagnosis_Category__c			-- migrate value from [trefVetDiagCategory].[CategoryDesc]
				--,T4.ReleaseCategory AS  Dog_Release_Category__c		-- migrate value from  	-- BEH-Behavior; COS-Cosmetic; HST-History; MED-Medical; OTH-Other; PEO-People; TMP-Temperament; WRK-Work
				
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN T1.pfa_Amount1 
					WHEN T1.pfa_AmountType2= '19' THEN T1.pfa_Amount2 
					ELSE NULL
					END AS One_Time_Amount__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN T1.pfa_Amount1 
					WHEN T1.pfa_AmountType2= '39' THEN T1.pfa_Amount2 
					ELSE NULL
					END AS Annual_Amount__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN T1.pfa_ExpirationDate1 
					WHEN T1.pfa_AmountType2= '19' THEN T1.pfa_ExpirationDate2 
					ELSE NULL
					END AS One_Time_Expiration_Date__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN T1.pfa_ExpirationDate1 
					WHEN T1.pfa_AmountType2= '39' THEN T1.pfa_ExpirationDate2 
					ELSE NULL
					END AS Annual_Expiration_Date__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN T1.pfa_AmountForLife1
					WHEN T1.pfa_AmountType2= '19' THEN T1.pfa_AmountForLife2
					ELSE NULL
					END AS One_Time_Amount_for_Life__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN T1.pfa_AmountForLife1
					WHEN T1.pfa_AmountType2= '39' THEN T1.pfa_AmountForLife2
					ELSE NULL
					END AS Annual_Amount_For_Life__c
			
		--	INTO GDB_Final_migration.DBO.IMP_CASE_parent
			FROM  GDB_KADE_Final.dbo.tblPrattFundAuthorization AS T1 
			LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T2 ON T2.DiagnosisCode=T1.pfa_DxCode
			LEFT JOIN GDB_KADE_Final.dbo.trefVetDiagCategory AS T3 ON T2.DiagnosisCategory=T3.CategoryCode
			LEFT JOIN GDB_KADE_Final.dbo.trefDogReleaseCategories AS T4 ON T2.DogReleaseCategory=T4.Code
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T5 ON T5.FileNum=T1.pfa_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T5.FileNum
 
/*END --eof create case parent --TC1: 49


BEGIN --AUDIT CASE (PARENT) 

	SELECT * FROM GDB_Final_migration.dbo.IMP_CASE_parent
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CASE_parent GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

END 


BEGIN-- CREATE CASE */
	UNION
 			SELECT DISTINCT 
			   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'not-'+CAST(T1.not_Key  AS varchar(30)) AS  Legacy_ID__c	
				,T3.psn_PersonID  AS  [Contact:Legacy_ID__c]
				,NULL AS  [Dog__r:Legacy_Id__c]	
			--	,NULL AS [Parent:Legacy_Id__c]
				,'0123D0000004awyQAA' AS  RecordTypeId	-- Support Center
				,'tblPhoneNote'  AS  Source_Data_Table__c	-- tblPhoneNote
				,CAST(T1.not_Date  AS DATE) AS  ClosedDate	
				,CASE WHEN T1.not_Date > GETDATE() THEN 'Open' ELSE 'Close' END AS  [Status]	-- if > Today then 'Closed' else 'Open'
	 			,CAST(T1.not_Memo AS NVARCHAR(MAX)) AS  [Description]	
				,T5.[CASE#Type] AS  [Type]	
				,T5.[CASE#Reason]  AS  Reason__c	
				,T5.[Case#ReasonDetail]  AS  Reason_Detail__c
				,T1.not_Summary  AS  [Subject]	
				,CAST(T1.not_CreatedDate AS DATE)   AS  CreatedDate	
				,CAST(T6.pra_AlertDate AS DATE)   AS Alert_Date__c
 				,CASE T6.pra_AlertSent WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS Alert_Sent__c
 				,T6.pra_EmailAdditions AS Alert_Recipient__C
				,NULL AS Ingestion_Type__c
				,NULL AS Require_Vet__c
 				,CAST(CAST(T7.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T7.scs_SurveyOffered AS Survey_Summary__c
				,NULL AS Main_Category__c --filler
				,NULL AS Category__c --filler
				,NULL AS Sub_Category__c --filler
				--fillers
				,NULL AS  Start_Date__c	
				,NULL AS  Expiration_Date__c	
				,NULL AS  Allowance_Amount__c	
  				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
				,NULL AS  Pre_existing_Diagnosis_Release_Status__c
		     INTO GDB_Final_migration.dbo.IMP_CASE
	 		FROM GDB_KADE_Final.dbo.tblPhoneNote AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.not_PersonRelID
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T4 ON T4.FileNum=t1.not_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T4.FileNum
			LEFT JOIN GDB_Final_migration.dbo.CHART_PhoneNote_NatureofCall AS T5 ON T5.not_NatureOfCall = T1.not_NatureOfCall
			LEFT JOIN GDB_KADE_Final.dbo.tblPhoneRptAlerts AS T6 ON T6.pra_PhoneRptID=T1.not_Key
			LEFT JOIN (SELECT * FROM GDB_KADE_Final.DBO.tblSupportCtrSurvey WHERE scs_FormID=1) AS T7 ON T7.scs_FormRecID=T1.not_Key
		
			UNION 
			
			SELECT DISTINCT  
				CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'di-'+CAST(T1.di_DogIngestID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate 'di-'+[di_DogIngestID]
				,NULL AS  [Contact:Legacy_ID__c]
				,T1.di_DogID  AS  [Dog__r:Legacy_Id__c]	
			--	,NULL AS [Parent:Legacy_Id__c]
				,'0123D0000004cpqQAA'  AS  RecordTypeId	-- Dog Ingestion
				,'tblDogIngest'  AS  Source_Data_Table__c	-- tblDogIngest
				,CAST(T1.di_IngestDate AS DATE) AS  ClosedDate	
				,CASE WHEN T1.di_IngestDate < GETDATE() THEN 'Closed' ELSE 'New' END  AS  [Status]	-- if < Today then 'Closed' else 'New'
				,T1.di_Description  AS  [Description]
				,'Dog Ingestion'  AS  [Type]	-- Dog Ingestion
				,NULL AS Reason__c
				,NULL AS Reason_Detail__c
				,'Dog Insgestion ' + CAST(CAST(T1.di_IngestDate AS DATE) AS NVARCHAR(10)) AS  [Subject]	-- concatenate 'Dog Insgestion ' +[di_IngestDate]
				,CAST(T1.di_CreatedDate  AS DATE) AS  CreatedDate	
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,T1.di_IngestType  AS  Ingestion_Type__c	
				,T1.di_RequireVet  AS  Require_Vet__c	-- Default nulls to 'No'
	 			,NULL AS Survey_Summary__c --filler
				,NULL AS Main_Category__c --filler
				,NULL AS Category__c --filler
				,NULL AS Sub_Category__c --filler
 				--fillers
				,NULL AS  Start_Date__c	
				,NULL AS  Expiration_Date__c	
				,NULL AS  Allowance_Amount__c	
  				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
				,NULL AS  Pre_existing_Diagnosis_Release_Status__c
 	 		FROM GDB_KADE_Final.dbo.tblDogIngest AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T2 ON T2.FileNum=t1.di_EntryBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T2.FileNum

			UNION 

			SELECT  DISTINCT 
				 CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Pfps-'+ CAST(T.pfps_ProcSelID AS NVARCHAR(30)) AS Legacy_Id__c
	 			,NULL AS  [Contact:Legacy_ID__c]
				,NULL AS  [Dog__r:Legacy_Id__c]	
			--	,'Pfa-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS [Parent:Legacy_Id__c]
				,'0123D0000004cplQAA' AS  RecordTypeId		 	-- Vet Authorization
				,'tblPrattFundProcSelect'  AS  Source_Data_Table__c	-- tblPrattFundProcSelect
				,NULL AS ClosedDate	
				,T1.pfa_Descision  AS  [Status]	
				,NULL AS [Description]
 				,NULL AS [Type]	-- Dog Ingestion
				,NULL AS Reason__c  --filler
				,NULL AS Reason_Detail__c--filler
				,NULL AS [Subject]--filler
				,NULL AS CreatedDate--filler
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,NULL AS Ingestion_Type__c --filler
				,NULL AS Require_Vet__c --filler
				,NULL AS Survey_Summary__c --filler
				,T5.pf_MainCategory AS Main_Category__c
				,T5.pf_Category AS Category__c
				,T5.pf_SubCategory AS Sub_Category__c
				--fillers
				,NULL AS  Start_Date__c	
				,NULL AS  Expiration_Date__c	
				,NULL AS  Allowance_Amount__c	
  				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
			    ,NULL AS  Pre_existing_Diagnosis_Release_Status__c
 			FROM GDB_KADE_Final.DBO.tblPrattFundProcSelect AS T 
			LEFT JOIN GDB_KADE_Final.dbo.tblPrattFundAuthorization AS T1 ON T1.pfa_AuthID = T.pfps_AuthID
			LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T2 ON T2.DiagnosisCode=T1.pfa_DxCode
			LEFT JOIN GDB_KADE_Final.dbo.trefVetDiagCategory AS T3 ON T2.DiagnosisCategory=T3.CategoryCode
			LEFT JOIN GDB_KADE_Final.dbo.trefDogReleaseCategories AS T4 ON T2.DogReleaseCategory=T4.Code
			LEFT JOIN GDB_KADE_Final.DBO.trefPrattFundAuthProc AS T5 ON T.pfps_ProcID=T5.pf_CatID
	 		 LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T6 ON T6.FileNum=T1.pfa_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T6.FileNum

			UNION 

	/*		SELECT   DISTINCT
				 CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Scn-'+CAST(T1.scn_ID AS NVARCHAR(30))  AS  Legacy_Id__c	-- concatenate 'Scn-'+[scn_ID]
				,NULL AS  [Contact:Legacy_ID__c]
				,NULL AS  [Dog__r:Legacy_Id__c]	
			--	,NULL AS [Parent:Legacy_Id__c]
				,'0123D0000004awyQAA'  AS  RecordTypeID	-- Support Center
				,'tblSupportCenterNote' AS  Source_Data_Table__c
				,CAST(T1.scn_Date  AS DATE) AS  ClosedDate	
				,CASE WHEN T1.scn_Date < GETDATE() THEN 'Completed' ELSE 'New' END AS  [Status]	-- is <Today then 'Completed' else 'New'
				,CAST(T1.scn_Memo  AS NVARCHAR(MAX)) AS  [Description]	
				,T2.[CASE#Type]  AS  [Type]	
				,T2.[CASE#Reason] AS  Reason__c	
				,T2.[CASE#ReasonDetail] AS  Reason_Detail__c	
				,CAST(CASE WHEN T1.scn_Summary IS NULL THEN LEFT(CAST(T1.scn_Memo AS NVARCHAR(MAX)), 255) ELSE T1.scn_Summary END AS NVARCHAR(MAX)) AS  [Subject]	-- concatenate [scn_Summary] line break [scn_Memo]
				,NULL AS CreatedDate--filler
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,NULL AS Ingestion_Type__c --filler
				,NULL AS Require_Vet__c --filler
 				,CAST(CAST(T3.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T3.scs_SurveyOffered AS Survey_Summary__c
				,NULL AS Main_Category__c
				,NULL AS Category__c
				,NULL AS Sub_Category__c
				--fillers
				,NULL AS  Start_Date__c	
				,NULL AS  Expiration_Date__c	
				,NULL AS  Allowance_Amount__c	
  				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  [Pre_existing_Diagnosis_Code__r:Legacy_Id__c]	 
			FROM GDB_KADE_Final.dbo.tblSupportCenterNote AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T4 ON T4.FileNum=t1.scn_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T4.FileNum
			LEFT JOIN GDB_Final_migration.DBO.CHART_SupportCenter_NaOfCall AS T2 ON T1.scn_NatureOfCall=T2.scn_NatureOfCall
			LEFT JOIN (SELECT * FROM GDB_KADE_Final.DBO.tblSupportCtrSurvey WHERE scs_FormID=2) AS T3 ON T3.scs_FormRecID=T1.scn_ID
			 
			UNION
            
			SELECT DISTINCT
				 CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Vta-'+CAST(T1.vta_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Vta-'+[vta_ID]
			 	,NULL AS  [Contact:Legacy_ID__c]
				,T8.dog_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to Dog through Vet Record
 				--,NULL AS [Parent:Legacy_Id__c]
			    ,'0123D0000004cplQAA' AS  RecordTypeId		 	-- Vet Authorization
				,'tblVetAuthorization' AS Source_Data_Table__c
				,NULL AS ClosedDate
				,NULL   AS [Status]
				,NULL AS [Description]
				,T4.aut_Type  AS  [Type]	-- migrate value from [aut_Type]
				,NULL AS Reason__c  --filler
				,NULL AS Reason_Detail__c--filler
				,NULL AS [Subject]--filler
				,NULL AS CreatedDate--filler
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,NULL AS Ingestion_Type__c --filler
				,NULL AS Require_Vet__c --filler
				,NULL AS Survey_Summary__c --filler
				,NULL AS Main_Category__c
				,T3.CategoryDesc   AS  Category__c	
				,NULL AS Sub_Category__c
				
				,CAST(T1.vta_StartDate  AS DATE) AS  Start_Date__c	
				,CAST(T1.vta_ExpDate  AS DATE)  AS  Expiration_Date__c	
				,T1.vta_AuthAmount  AS  Allowance_Amount__c	
  				,CASE T1.vta_Paid WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS  Paid__c	
				,CASE T1.vta_life WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Life__c	
				,CASE T1.vta_Expired WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END   AS  Expired__c	
				,T1.vta_PreExstDiagCode  AS  [Pre_existing_Diagnosis_Code__r:Legacy_Id__c]	-- migrate as is 
		 		
			FROM GDB_KADE_Final.dbo.tblVetAuthorization AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T2 ON T2.FileNum=t1.vta_AuthBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USERS AS X1 ON X1.ADP__c = T2.FileNum
			LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T ON T.vdl_VetEntryID=T1.vta_VetEntryID
			LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T8 ON T8.dog_DogID=T.vdl_DogID
			LEFT JOIN GDB_KADE_Final.DBO.trefVetDiagCategory AS T3 ON T1.vta_Category=T3.CategoryCode
			LEFT JOIN GDB_KADE_Final.dbo.trefVetAuthorizationType AS T4 ON T4.aut_ID=T1.vta_AuthType
			--LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T5 ON T5.DiagnosisCode=T1.vta_PreExstDiagCode
			--LEFT JOIN GDB_KADE_Final.dbo.trefVetDiagCategory AS T6 ON T6.CategoryCode=T5.DiagnosisCategory
			--LEFT JOIN GDB_KADE_Final.dbo.trefDogReleaseCategories AS T7 ON T7.ReleaseCategory=T5.DogReleaseCategory
			*/

END --- tc1: 228,279

  

BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_CASE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CASE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT Source_Data_Table__c, COUNT(*) c
	FROM GDB_Final_migration.dbo.IMP_CASE
	GROUP BY Source_Data_Table__c

END 



