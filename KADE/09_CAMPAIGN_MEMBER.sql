USE GDB_Final_migration
GO


BEGIN-- 
 	 		SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblAGSTravelInfo
			WHERE   SF_Object LIKE '%memb%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVolCalWebTourRequest 
			WHERE  SF_Object_2 LIKE '%cam%'
END 

BEGIN --DROP CAMP MEMBER

	DROP TABLE GDB_Final_migration.dbo.IMP_CAMPAIGN_MEMBER

END


BEGIN ---create	IMP CAMPAIGN MEMBER

	
			SELECT DISTINCT
					T3.cls_FirstString  +' '+ T3.cls_Campus +'-'+CAST(T5.psn_PersonID AS NVARCHAR(30)) AS Legacy_Id__c
					,T3.cls_FirstString  +' '+ T3.cls_Campus  AS  [Campaign:Legacy_id__c]	-- link through [tblClient].[cli_ClassCode] to Campaign	-- Link [tblClient].[cli_ClientID]
					,NULL AS [Lead:Legacy_Id__c]
					,CAST(T5.psn_PersonID AS VARCHAR(50))  AS  [Contact:Legacy_id__c]	-- link through [tblPersonRel].[prl_PersonID] to Contact	-- Link [tblPersonRel].[prl_ClientInstanceID]
					,CASE T1.ByCar WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  By_Car__c		
					,CASE T1.ByBus WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  By_Bus__c	
					,CASE T1.ByTrain WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  By_Train__c		
					,CAST(T1.ArrivalDAte AS DATE) AS  Arrival_Date__c		
					,T1.ArrivalFlight1  AS  Arrival_Flight_1__c		
					,T1.LVPlace1  AS  LV_Place_1__c		
					,T1.LVTime1  AS  LV_Time_1__c		
					,T1.ARPlace1  AS  AR_Place_1__c		
					,T1.ARTime1  AS  AR_Time_1__c		
					,T1.ArrivalFlight2  AS  Arrival_Flight_2__c		
					,T1.LVPlace2  AS  LV_Place_2__c		
					,T1.LVTime2  AS  LV_Time_2__c		
					,T1.ARPlace2  AS  AR_Place_2__c		
					,T1.ARTime2  AS  AR_Time_2__c		
					,T1.ArrivalFlight3  AS  Arrival_Flight_3__c		
					,T1.LVPlace3  AS  LV_Place_3__c		
					,T1.LVTime3  AS  LV_Time_3__c		
					,T1.ARPlace3  AS  AR_Place_3__c		
					,T1.ARTime3  AS  AR_Time_3__c		
					,CAST(T1.DepartDate AS DATE) AS  Departing_Date__c		
					,T1.DepartFlight1  AS  Departing_Flight_1__c		
					,T1.DLVPlace1  AS  DLV_Place_1__c		
					,T1.DLVTime1  AS  DLV_Time_1__c		
					,T1.DARPlace1  AS  DAR_Place_1__c		
					,T1.DARTime1  AS  DAR_Time_1__c		
					,T1.DepartFlight2  AS  Departing_Flight_2__c		
					,T1.DLVPlace2  AS  DLV_Place_2__c		
					,T1.DLVTime2  AS  DLV_Time_2__c		
					,T1.DARPlace2  AS  DAR_Place_2__c		
					,T1.DARTime2  AS  DAR_Time_2__c		
					,T1.DepartFlight3  AS  Departing_Flight_3__c		
					,T1.DLVPlace3  AS  DLV_Place_3__c		
					,T1.DLVTime3  AS  DLV_Time_3__c		
					,T1.DARPlace3  AS  DAR_Place_3__c		
					,T1.DARTime3  AS  DAR_Time_3__c	
					,'Attended' AS [Status]  --FB-00685
						,[Type] = NULL --FB-01818
						 --	,ActivityDate =NULL
							,FSM_Notes__c = NULL
							,Attempt_1__c = NULL	
							,Attempt_2__c = NULL	
							,Attempt_3__c = NULL	
							,Comments__c = NULL		
							,Visit_Date__c = NULL
							,Visit_Time__c = NULL
							,Visit_Location__C = NULL
							,CreatedDate = NULL
					,'tblAGSTravelInfo' AS zrefsrc
		   INTO GDB_Final_migration.dbo.IMP_CAMPAIGN_MEMBER			
			FROM GDB_KADE_Final.dbo.tblAGSTravelInfo AS T1 
			INNER JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON CAST(T2.cli_ClientID AS NVARCHAR(30)) =CAST(T1.ClientID AS NVARCHAR(30)) 
			INNER JOIN GDB_KADE_Final.dbo.tblClass AS T3 ON CAST(T3.cls_ClassID AS NVARCHAR(30)) =CAST(T2.cli_ClassCode AS NVARCHAR(30)) 
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T4 ON T4.prl_ClientInstanceID = T1.ClientID
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T5 ON T5.psn_PersonID=T4.prl_PersonID
			--715
			UNION 
		
		    SELECT DISTINCT
 					'LWTR-'+CAST(T1.ID AS NVARCHAR(30)) AS Legacy_id__c
					,'LWTR'  AS  [Campaign:Legacy_id__c]   ---Campaign Name: "Legacy Web Tour Request"
					,T1.ID AS   [Lead:Legacy_Id__c]
					,NULL AS  [Contact:Legacy_id__c]
					,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c	
					,NULL AS [Status]
						,[Type] = NULL --FB-01818
						 --	,ActivityDate =NULL
							,FSM_Notes__c = NULL
							,Attempt_1__c = NULL	
							,Attempt_2__c = NULL	
							,Attempt_3__c = NULL	
							,Comments__c = NULL		
							,Visit_Date__c = NULL
							,Visit_Time__c = NULL
							,Visit_Location__C = NULL
							,CreatedDate = NULL
					,'tblVolCalWebTourRequest' AS zrefsrc
 		 	FROM GDB_KADE_Final.dbo.tblVolCalWebTourRequest AS T1
			--TC2: 798

			UNION 
		
		    SELECT DISTINCT --fb-00685

 					'CLI-'+CAST(T1.cli_ClientID AS NVARCHAR(30)) AS Legacy_id__c
					,'cls-'+ CAST(T1.cli_ClassCode AS VARCHAR(30)) AS  [Campaign:Legacy_id__c]   
					,NULL AS   [Lead:Legacy_Id__c]
					,CAST(T2.prl_PersonID AS VARCHAR(50)) AS  [Contact:Legacy_id__c]
					,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c	
					,T1.cli_Status AS [Status]
							,[Type] = NULL --FB-01818
						 --	,ActivityDate =NULL
							,FSM_Notes__c = NULL
							,Attempt_1__c = NULL	
							,Attempt_2__c = NULL	
							,Attempt_3__c = NULL	
							,Comments__c = NULL		
							,Visit_Date__c = NULL
							,Visit_Time__c = NULL
							,Visit_Location__C = NULL
							,CreatedDate = NULL
					,'tblClient' AS zrefsrc
 		 	FROM GDB_KADE_Final.dbo.tblClient AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T1.cli_ClientID=T2.prl_ClientInstanceID
           WHERE t1.cli_ClassCode IS NOT NULL
           
		   --TC2: 10398
		   --TC2: 12010
UNION

					SELECT   DISTINCT
						--	 OwnerId = GDB_Final_migration.[dbo].[fnc_OwnerId]()
 							Legacy_ID__c = 'PSL-'+CAST(T1.psl_ClientListID	 AS NVARCHAR(30)) -- Concatenate 'psl-'+[psl_ClientListID]	
							,'psd-'+CAST(T3.psd_DetailID  AS NVARCHAR(30)) AS  [Campaign:Legacy_id__c] 
							,NULL AS   [Lead:Legacy_Id__c]
							,CAST(T2.prl_PersonID AS NVARCHAR(50)) AS  [Contact:Legacy_id__c]
											,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c
							,[Status] = T1.psl_FUStatus	
							,[Type] = T1.psl_FUType
						 --	,ActivityDate =CAST( T1.psl_CreatedDate	AS DATE)
							,FSM_Notes__c = T1.psl_FSMComments
							,Attempt_1__c = CASE T1.psl_Attempt1 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,Attempt_2__c = CASE T1.psl_Attempt2	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,Attempt_3__c = CASE T1.psl_Attempt3	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,Comments__c = T1.psl_SCComments		
							,Visit_Date__c = CAST(T1.psl_VistiDate	AS DATE)
							,Visit_Time__c = CONVERT(NVARCHAR(15),CAST(T1.psl_VisitTime  AS TIME), 100)
							,Visit_Location__C = T1.psl_VistLocation		
			
							,CreatedDate = CAST(T1.psl_CreatedDate AS DATE)
							,zrefSrc = 'tblAGSPrescreenClientList'
					FROM GDB_KADE_Final.dbo.tblAGSPrescreenClientList AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.psl_ClientID
 				--	LEFT JOIN GDB_Final_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_KADE_Final.dbo.tblAGSPrescreenDetail AS T3 ON T3.psd_DetailID = T1.psl_DetailID

UNION --FB-02086
		
		    SELECT DISTINCT
 					'cls-'+T1.cls_ClassID AS Legacy_id__c
					,'cls-'+T1.cls_ClassID  AS   [Campaign:Legacy_id__c]   
					,NULL AS   [Lead:Legacy_Id__c]
					,'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(50)) AS  [Contact:Legacy_id__c]
					,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c	
					,'Instructor' AS [Status]
						,[Type] = NULL --FB-01818
					--	 	,ActivityDate =NULL
							,FSM_Notes__c = NULL
							,Attempt_1__c = NULL	
							,Attempt_2__c = NULL	
							,Attempt_3__c = NULL	
							,Comments__c = NULL		
							,Visit_Date__c = NULL
							,Visit_Time__c = NULL
							,Visit_Location__C = NULL
							,CreatedDate = NULL
					,'tblClass_Class' AS zrefsrc
 		 	FROM GDB_KADE_Final.dbo.tblClass AS T1
			INNER JOIN GDB_KADE_Final.DBO.tblStaff AS T2 ON T1.cls_HeadInstructor=T2.FileNum

UNION --FB-02087
			
		    SELECT DISTINCT
 					'Str-'+T1.cls_FirstString  +' '+ T1.cls_Campus+'_'+CAST(T2.FileNum AS varchar(30)) AS Legacy_id__c
					,T1.cls_FirstString  +' '+ T1.cls_Campus  AS   [Campaign:Legacy_id__c]   
					,NULL AS   [Lead:Legacy_Id__c]
					,'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(50)) AS  [Contact:Legacy_id__c]
					,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c	
					,'Instructor' AS [Status]
						,[Type] = NULL --FB-01818
						 --	,ActivityDate =NULL
							,FSM_Notes__c = NULL
							,Attempt_1__c = NULL	
							,Attempt_2__c = NULL	
							,Attempt_3__c = NULL	
							,Comments__c = NULL		
							,Visit_Date__c = NULL
							,Visit_Time__c = NULL
							,Visit_Location__C = NULL
							,CreatedDate = NULL
					,'tblClass_String' AS zrefsrc
 		 	FROM GDB_KADE_Final.dbo.tblClass AS T1
			INNER JOIN GDB_KADE_Final.DBO.tblStaff AS T2 ON T1.cls_HeadInstructor=T2.FileNum
					

SELECT * FROM GDB_Final_migration.dbo.IMP_CAMPAIGN_MEMBER	
WHERE zrefsrc='tblclient'
 
 END


 --Audit

 SELECT zrefsrc, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_CAMPAIGN_MEMBER	
			GROUP BY zrefsrc
			ORDER BY zrefsrc