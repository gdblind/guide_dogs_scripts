
--FB-00020	
		SELECT * FROM GDB_Final_migration.dbo.IMP_VET_RECORD

--FB-00005

		SELECT * FROM GDB_Final_migration.dbo.IMP_BREEDING_ACTION_dbg WHERE Legacy_Id__c='DBG-4082'

--FB-FB-00004
 
		SELECT * FROM GDB_Final_migration.dbo.IMP_BREEDING_ACTION_dbg WHERE Legacy_Id__c='DBG-10048'

--FB-00012

		SELECT Sex__c, COUNT(*) c 
		FROM GDB_Final_migration.dbo.IMP_DOG
		GROUP BY Sex__c


--FB_00024

	SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_ROLE WHERE [Dog__r:Legacy_Id__c]='44146'
	SELECT	DISTINCT 
						GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'pd-'+CAST(T1.pd_RelationID  AS NVARCHAR(30)) AS  Legacy_Id__c		-- concatenate 'pd-'+[drl_relationsID]
						,CAST(T3.psn_PersonID  AS NVARCHAR(30)) AS  [Contact__r:Legacy_Id__c]					-- link through [tblPersonRel].[prl_PersonID] to contact	-- Link [tblPersonRel].[prl_RelationID] 
						,CAST(T4.drl_DogID  AS NVARCHAR(30)) AS  [Dog__r:Legacy_Id__c]						-- Link [tblDogRel].[drl_DogID]
						,NULL AS Breed__c	--filler	
						,NULL AS Archive__c	--filler
						,NULL AS Boarding_Only__c	--filler
 						,CASE T1.pd_CoRel WHEN 'SEC' THEN  'Secondary' WHEN  'PRI' THEN 'Primary' WHEN 'RRI' THEN 'Primary' END  AS  Co_Person_Relationship__c	-- SEC = Secondary PRI=Primary RRI=Primary	
						,T1.pd_RelCode  AS  Person_Role__c		
						,T1.pd_RelCode  AS  Dog_Role__c		
						,CAST(T1.pd_StartDate  AS DATE) AS  Start_Date__c		
						,CAST(T1.pd_EndDate  AS DATE) AS  End_Date__c		
						,T1.pd_EndReason  AS  End_Reason__c
						
						--ref
						,T1.pd_RelationID AS zrefID
						,'tblPersonDog' AS zrefSrc
		 		FROM GDB_KADE_Final.dbo.tblPersonDog AS T1	
				--to person
				INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_RelationID=T1.pd_PersonRelID
				INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
				--to dog
				INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T4 ON T4.drl_RelationID=T1.pd_DogRelID
				INNER JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T4.drl_DogID
					WHERE t5.dog_DogID='44146'
				UNION 
 
				 
				SELECT	  
					GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'drl-'+CAST(T1.drl_RelationID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'drl'+[drl_relationsID]	
					,CAST(T4.psn_PersonID  AS NVARCHAR(30)) AS   [Contact__r:Legacy_Id__c]						-- link through [tblPersonDog].[pd_PersonRelID] to [tblPersonRel].[prl_PersonID]	-- Link  [tblPersonDog].[pd_PersonRelID] 
					,CAST(T5.dog_DogID  AS NVARCHAR(30)) AS   [Dog__r:Legacy_Id__c]							-- Link  [tblDog].[dog_DogID] 
					,NULL AS Breed__c	--filler	
					,NULL AS Archive__c	--filler
					,NULL AS Boarding_Only__c	--filler
 					,NULL AS Co_Person_Relationship__c --filler
					,T1.drl_RelationCode  AS  Person_Role__c		
					,T1.drl_RelationCode  AS  Dog_Role__c		
 					,CAST(T1.drl_StartDate   AS DATE)  AS  Start_Date__c		
					,CAST(T1.drl_EndDate   AS DATE)  AS  End_Date__c		
					,T1.drl_EndReason  AS  End_Reason__c			
					
					--ref
					,T1.drl_RelationID AS zrefID
					,'tblDogRel' AS zrefSrc
 				FROM  GDB_KADE_Final.dbo.tblDogRel AS T1   
				--to person
				INNER JOIN GDB_KADE_Final.dbo.tblPersonDog AS T2 ON T2.pd_DogRelID = T1.drl_RelationID
				INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_RelationID = T2.pd_PersonRelID
				INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				--to dog
				INNER JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T1.drl_DogID
				WHERE T5.dog_DogID='44146'
--MK2017.06.13.  
	--MIGRATION ISSUES WITH GROUPS AND PARSING OF FIRSTNAME
	 --FOR THE MIGRATION WE IGNORED THE GROUP NUMBER. 
	SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, 
		CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
		,T2.* 
		FROM GDB_KADE_Final.dbo.tblPerson AS T1
		INNER JOIN GDB_KADE_Final.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
		 WHERE     T2.pg_GroupID='5677'
				OR T2.pg_GroupID='14760'
				OR T2.pg_GroupID='15256'
				OR T2.pg_GroupID='35035'
				OR T2.pg_GroupID='67878'
				OR T2.pg_GroupID='79709'
		ORDER BY T2.pg_GroupID, T1.psn_PersonID


-- missing names
		SELECT * 
	--	INTO GDB_Final_migration.dbo.imp_account_excp
		FROM GDB_Final_migration.dbo.imp_account

		WHERE name IS NULL OR name =''

		SELECT t.*
	 --	INTO 
		FROM GDB_Final_migration.dbo.imp_account t
		INNER JOIN GDB_Final_migration.dbo.imp_account_excp t2 ON t.legacy_id__c=t2.legacy_id__c

--invalid foreign key
	ACCOUNT:LEGACY_ID__C	LEGACY_ID__C
	iv-220	iv-277
	iv-225	iv-280
	iv-225	iv-281
	iv-225	iv-282
	iv-227	iv-283
	iv-231	iv-287
	iv-232	iv-288
	iv-232	iv-289

	 SELECT * FROM GDB_KADE_Final.dbo.tblGDBVendorInfo WHERE ivi_VendorInfoID='277'
	
	SELECT * FROM GDB_KADE_Final.dbo.tblGDBVendor WHERE iv_VendorID='220'


-- duplicate dog id on table that is a DOG(EXT) 
		SELECT * FROM GDB_KADE_Final.dbo.tblBreedingConformation
		WHERE bc_DogID IN (SELECT bc_DogID FROM GDB_KADE_Final.dbo.tblBreedingConformation GROUP BY bc_DogID HAVING COUNT(*)>1)
		ORDER BY bc_DogID


		-- dupe tattoo value. 
		SELECT * FROM GDB_Final_migration.dbo.imp_dog 
		WHERE GDB_TATTOO__C IN (SELECT GDB_TATTOO__C FROM GDB_Final_migration.dbo.imp_dog  GROUP BY GDB_TATTOO__C HAVING COUNT(*)>1)
		OR legacy_id__c='22665'
		SELECT * FROM 
 

