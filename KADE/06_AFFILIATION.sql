
 
BEGIN--Map: tblPuppyClubDetail   
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblAgencyContactRel 
			WHERE   SF_Object LIKE '%aff%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblAgencyContactRel
			WHERE  SF_Object_2 LIKE '%affil%'
END 

BEGIN-- DROP AFFILIATION

DROP TABLE GDB_Final_migration.DBO.IMP_AFFILIATION

END 

BEGIN--CREATE AFFILIATION

			--tblPuppyClubDetail
				SELECT DISTINCT
				 'pcd-'+CAST(T1.pcd_ID  AS    NVARCHAR(30))   AS  Legacy_ID__c	
				,'pcd-'+CAST(T1.pcd_ClubID  AS NVARCHAR(30))   AS  [npe5__Organization__r:Legacy_ID__c]
				,T1.pcd_PersonID  AS  [npe5__Contact__r:Legacy_ID__c]	
				,T1.pcd_RelationCode  AS  Role__c	-- Migrate "code" as is. --FB-00436
				,NULL AS npe5__Primary__c --filler
				,NULL AS npe5__Status__c --filler 
 				
				INTO GDB_Final_migration.dbo.IMP_AFFILIATION

				FROM GDB_KADE_Final.dbo.tblPuppyClubDetail AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON T2.psn_PersonID=T1.pcd_PersonID
 				INNER JOIN GDB_KADE_Final.dbo.trefPuppyClub AS T3 ON T1.pcd_ClubID=T3.clb_ClubID
			 UNION 
			 --tblPuppyClubDetail-PRP --FB-00297
				SELECT DISTINCT
				'pcd-'+CAST(T1.pcd_ClubID  AS VARCHAR(30)) +'_'+ CAST(T5.prl_PersonID AS VARCHAR(20)) AS Legacy_ID__c
				--'prpcd-'+CAST(T1.pcd_ID  AS NVARCHAR(30)) AS Legacy_ID__c
				-- 'prpcd-'+CAST(T1.pcd_ID  AS    NVARCHAR(30)) + '_'+ CAST(t5.prl_PersonID AS NVARCHAR(30))   AS  Legacy_ID__c	
				,'pcd-'+CAST(T1.pcd_ClubID  AS NVARCHAR(30))   AS  [npe5__Organization__r:Legacy_ID__c]
				,T5.prl_PersonID  AS  [npe5__Contact__r:Legacy_ID__c]	
				,'PRP'  AS  Role__c	
				,NULL AS npe5__Primary__c --filler
				,NULL AS npe5__Status__c --filler 
 				
				FROM GDB_KADE_Final.dbo.tblPuppyClubDetail AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplication AS T2 ON T1.pcd_ClubID=T2.pra_PuppyClubID
				INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplicationDetail AS T3 ON T3.pad_ApplicationID=T2.pra_ApplicationID
				INNER JOIN GDB_KADE_Final.dbo.tblPersonDog AS T4 ON T4.pd_RelationID=T3.pad_PlacedPDID
				INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T5 ON T5.prl_RelationID=T4.pd_PersonRelID
				WHERE T4.pd_EndDate IS NULL AND T4.pd_CoRel='pri'
				
 				--tc2: 16557 Follow-up 852  fina 838
				
				UNION
			
			--stg_tblVolEquipment
				SELECT DISTINCT 
				 'vps-'+CAST(T1.vps_PathScarfID AS NVARCHAR(30)) AS Legacy_ID__c
				,'vps-'+CAST(T2.vps_PathScarfID AS NVARCHAR(30)) AS  [npe5__Organization__r:Legacy_ID__c] 
				,T1.vps_PersonID  AS  [npe5__Contact__r:Legacy_ID__c]
				,NULL AS  Role__c	
				,NULL AS npe5__Primary__c --filler
				,NULL AS npe5__Status__c --filler 
				FROM GDB_Final_migration.DBO.stg_tblVolEquipment  T1
				INNER JOIN GDB_Final_migration.dbo.stg_tblVolEquipment_1 AS T2 ON T1.vps_OrgName=T2.vps_OrgName
				INNER JOIN GDB_KADE_Final.DBO.tblPerson AS T3 ON T3.psn_PersonID=T1.vps_PersonID

			 UNION
			--tblAgencyContactRel
				SELECT
				'cre-'+CAST(T1.cre_RelationshipID AS varchar(30))  AS  Legacy_ID__c	
				,'acy-' +CAST(T2.acy_AgencyID AS NVARCHAR(20))   AS  [npe5__Organization__r:Legacy_Id__c]	
				,T1.cre_PersonID  AS [npe5__Contact__r:Legacy_Id__c]
				,NULL AS  Role__c	
				,CASE T1.cre_Rank WHEN 'Primary' THEN 'TRUE' ELSE 'FALSE' END  AS  npe5__Primary__c	-- If = Primary, then TRUE
				,CASE T1.cre_Active WHEN 'Yes'  THEN 'Current' ELSE 'Former' END   AS  npe5__Status__c	-- If = TRUE then 'Current' else 'Former'
 				FROM GDB_KADE_Final.dbo.tblAgencyContactRel AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblAgency AS T2 ON T2.acy_AgencyID = T1.cre_AgencyID
				INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID = T1.cre_PersonID
END --EOF TC1: 4531  TC2:  Final 5622


BEGIN--AUDIT

	 SELECT * FROM GDB_Final_migration.dbo.IMP_AFFILIATION
	 SELECT DISTINCT Role__c FROM GDB_Final_migration.dbo.IMP_AFFILIATION

	 SELECT * 
	 FROM GDB_Final_migration.dbo.IMP_AFFILIATION
	 WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM  GDB_Final_migration.dbo.IMP_AFFILIATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 ORDER BY Legacy_ID__c

END 

--TEST 
	SELECT * FROM GDB_KADE_Final.DBO.tblPerson
	WHERE psn_PersonID='55987'