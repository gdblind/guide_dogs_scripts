USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblPersonAlerts
			WHERE   SF_Object LIKE '%ale%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

BEGIN --DROP ALERT

	DROP TABLE GDB_Final_migration.dbo.IMP_ALERT

END 

BEGIN--CREATE IMP ALERT

			SELECT    
			CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblStaff].[FileNum]
			,'Pa-'+CAST(T1.pa_AlertID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Pa-'+[pa_AlertID]	
			,T1.pa_PersonID  AS  [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
			,CAST(T1.pa_AlertDate  AS DATE) AS  Date__c		
 			,T1.pa_AlertTxt  AS  Alert__c		
			,CASE T1.pa_AlertDeactivated WHEN 1 THEN 'TRUE' ELSE 'FALSE' end AS  Deactivated__c		
			INTO GDB_Final_migration.dbo.IMP_ALERT
			FROM GDB_KADE_Final.dbo.tblPersonAlerts AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPerson T2 ON T2.psn_PersonID = T1.pa_PersonID
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.pa_ActivatedBy


END --tc1: 377 TC2:426


		SELECT * FROM GDB_Final_migration.dbo.IMP_ALERT


























