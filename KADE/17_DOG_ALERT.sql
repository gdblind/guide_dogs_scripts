USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVetReminder_
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


BEGIN --drop DOG ALERT

	DROP TABLE GDB_Final_migration.dbo.IMP_DOG_ALERT

END 

BEGIN --create DOG ALERT

 
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,CASE WHEN (ROW_NUMBER() OVER(PARTITION BY T1.flgDogID ORDER BY T1.flgDogID) >1)
							  THEN ('Flg-'+CAST(T1.flgDogID  AS NVARCHAR(30))+'-'+CAST(ROW_NUMBER() OVER(PARTITION BY T1.flgDogID ORDER BY T1.flgDogID) AS NVARCHAR(30))) 
							  ELSE ('Flg-'+CAST(T1.flgDogID  AS NVARCHAR(30))) 
							  END  AS Legacy_ID__c
					--	,NULL AS [Procedure__r:Legacy_ID__c] --filler
						,IIF(T1.flgDateEnd IS NULL,T.dog_DogID,NULL) AS  DogLink		-- Link tblDog.dog_DogID
						,IIF(T1.flgDateEnd IS NOT NULL,T.dog_DogID,NULL) AS  InActiveDogLink		-- Link tblDog.dog_DogID
						,CAST(T1.flgDateStart AS date)  AS  Start_Date__c		
						,CAST(T1.flgDateEnd AS date)  AS  End_Date__c	
						,T2.Flag  AS  Note__c	        -- migrate value from [trefDogFlags].[flag]; DNC if [flag]=null	-- Yes/Link [trefDogFlags].[FlagCode]
					--	,NULL AS Recheck_Procedure__c --filler
					--	,NULL AS Recheck_Date__c --filler
					--	,NULL AS Reminder_Sent__c --filler
						,IIF((T1.flgDateEnd) IS NULL,'TRUE','FALSE') AS Active__c
						--,NULL AS Note__c --filler
						
						,T1.flgDogID AS zrefId
						,'tblDogFlags' AS zrefSrc
						
					INTO GDB_Final_migration.dbo.IMP_DOG_ALERT
					FROM GDB_KADE_Final.dbo.tblDogFlags AS T1
					INNER JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.flgDogID
					LEFT JOIN GDB_KADE_Final.dbo.trefDogFlags AS T2 ON T2.FlagCode=T1.flgFlagCode
					WHERE T1.flgFlagCode IS NOT NULL 
				 
				 
				 	UNION 

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'Ale-'+CAST(T1.ale_ID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Ale-'+[ale_ID]	
						--,NULL AS [Procedure__r:Legacy_ID__c] --filler
						,IIF(T1.ale_OnOff ='yes',T.dog_DogID, NULL) AS  DogLink		 -- Link [tblDog].[dog_DogID]
						,IIF(T1.ale_OnOff ='no',T.dog_DogID,NULL) AS  InActiveDogLink		-- Link tblDog.dog_DogID
						,CAST(T1.ale_AlertDate AS DATE) AS  Start_Date__c	
						,NULL AS End_Date__c  --filler	
						,T1.ale_AlertText  AS  Note__c
					--	,NULL AS Recheck_Procedure__c --filler
					--	,NULL AS Recheck_Date__c --filler
					--	,NULL AS Reminder_Sent__c --filler
 						,T1.ale_OnOff  AS  Active__c	
						
						
						,T1.ale_ID AS zrefID
						,'tblVetAlert' AS zrefSrc
					FROM GDB_KADE_Final.dbo.tblVetAlert AS T1
					INNER JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.ale_DogID

	UNION -- Per Kate email 8/18/2018
		   SELECT  
				GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				,'dnk-'+ CAST(t1.dkn_DogKennelNotesID AS VARCHAR(30)) AS Legacy_id__c
				,T1.dkn_DogID AS doglink  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				--,Title = 'Canine Welfare Notes: '+  CAST(CAST(T1.dkn_CreatedDate AS  DATE) AS NVARCHAR(30))   
				--,RecordTypeID='0123D0000005PZlQAM'
				,NULL AS InActiveDogLink
				,Start_Date__c = T1.dkn_createdDate
				, NULL AS End_Date__c
				,Note__c = CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))
				,NULL AS Active__c
				,T1.dkn_DogKennelNotesID AS zrefID
				,'tblDogKennelNotes' zrefSrc
			
			FROM GDB_KADE_Final.dbo.tblDogKennelNotes AS T1
			WHERE LEN(CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))) >0

				--	UNION 
/* FB-00479
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 						,'REM-'+CAST(T1.rem_VetRemID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Rem-'+[rem_VetRemID]	
						,'VDP-'+ CAST(T2.vdp_ProcID AS NVARCHAR(30)) AS  [Procedure__r:Legacy_ID__c]			-- Link [tblVetProc].[vdp_ProcID]
						,T.dog_DogID  AS  [Dog__r:Legacy_ID__c]		-- Link [tblDog].[dog_DogID]
						,CAST(T1.rem_ReminderDate AS DATE)  AS  Start_Date__c	
						,NULL AS End_Date__c  --filler	
						,NULL AS  Flag__c
 						,T3.VetProcedureText  AS  Recheck_Procedure__c	-- migrate value from [VetProcedureText]	-- Yes/Link trefVetProc.VetProcedureCode
						,CAST(T1.rem_RecheckDate  AS DATE) AS  Recheck_Date__c		
 						,CASE T1.rem_ReminderSent WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Reminder_Sent__c		
						,NULL  AS  Active__c
						,CASE WHEN T1.rem_notes IS NOT NULL THEN T1.rem_NonProcReminder  +CHAR(10)+ T1.rem_notes ELSE T1.rem_NonProcReminder END AS  Note__c	-- Concatenate with "rem_Notes"	
 					
						,T1.rem_VetRemID AS zrefID
						,'tblVetReminder_' AS zrefSrc

					FROM GDB_KADE_Final.dbo.[tblVetReminder_] AS T1
					INNER JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.rem_DogID
					LEFT JOIN GDB_KADE_Final.dbo.tblVetProc AS T2 ON T2.vdp_ProcID=T1.rem_ProcID
					LEFT JOIN GDB_KADE_Final.dbo.trefVetProc AS T3 ON T3.VetProcedureCode = T1.rem_RecheckProcedure
					*/
					 
END --tc1: 11746 TC2: 8540 final 9152

					 

BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_ALERT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_DOG_ALERT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_DOG_ALERT 
			GROUP BY zrefSrc
		 
END 
