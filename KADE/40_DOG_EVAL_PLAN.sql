USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblPRADogEvalTrends
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


BEGIN -- drop
	DROP TABLE GDB_Final_migration.dbo.IMP_DOG_EVAL_PLAN
END

BEGIN -- IMP_DOG_EVAL_PLAN

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'DEP-'+CAST(T1.dep_PlanID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "DEP-" with dep_PlanID	
					,'PRE-'+CAST(T2.pre_EvalID AS NVARCHAR(30)) AS  [Puppy_Report__r:Legacy_Id__c]		-- Link [tblPRADogEval].[pre_EvalID]
					,CAST(T1.dep_CreatedDate AS DATE) AS  CreatedDate		
					,CAST(T1.dep_Issues AS NVARCHAR(MAX)) AS  Issues__c		
					,CAST(T1.dep_Plan  AS NVARCHAR(MAX)) AS  Plan__c		
					,CAST(T1.dep_Results AS NVARCHAR(MAX)) AS  Results__c		
					,NULL  AS  [Diagnosis_Code__r:Legacy_Id__c]	--filler

					--reference
					,T1.dep_PlanID AS zrefID
 					,'tblPRADogEvalPlan' AS zrefSrc
					 INTO GDB_Final_migration.dbo.IMP_DOG_EVAL_PLAN
					FROM GDB_KADE_Final.dbo.tblPRADogEvalPlan AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPRADogEval as T2 on T2.pre_EvalID=T1.dep_EvalID

		 	 	UNION

					SELECT  DISTINCT 
					GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'Det-'+CAST(T1.det_TrendID AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Det-'+[det_TrendID]	
					,'PRE-'+CAST(T2.pre_EvalID AS NVARCHAR(30)) AS  [Puppy_Report__r:Legacy_Id__c]				-- Link [tblPRADogEval].[pre_EvalID]
					,CAST(T1.det_CreatedDate AS NVARCHAR(30)) AS CreatedDate
					,NULL  AS  Issues__c	--filler
					,NULL  AS  Plan__c		--filler
					,NULL  AS  Results__c	--filler
					,T1.det_TrendsRRID  AS  [Diagnosis_Code__r:Legacy_Id__c]	  -- migrate as is 	-- Link [trefVetPetChamp].[DiagnosisCode]
					
					--reference
					,T1.det_TrendID AS zrefID
					,'tblPRADogEvalTrends' AS zrefSrc
					FROM GDB_KADE_Final.dbo.tblPRADogEvalTrends AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPRADogEval as T2 on T2.pre_EvalID=T1.det_EvalID
					--LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T3 ON T3.DiagnosisCode=T1.det_TrendsRRID
					--LEFT JOIN GDB_KADE_Final.dbo.trefVetDiagCategory AS T4 ON T4.CategoryCode=T3.DiagnosisCategory



END --tc1: 11,101  TC2: 11,129
 
BEGIN --audit
 
	SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_EVAL_PLAN
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_DOG_EVAL_PLAN GROUP BY Legacy_ID__c HAVING COUNT(*)>1)



END 
