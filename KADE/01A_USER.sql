USE GDB_Final_migration
GO

SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblStaff
SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblPerson

BEGIN--Map: USER  
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblStaff
			WHERE  SF_Object LIKE '%use%'
END 


BEGIN--DROP USERS
	
	DROP TABLE GDB_Final_migration.dbo.IMP_USER

END 

BEGIN--USERS
			SELECT 
				 T1.FileNum  AS  ADP__c
				,'Staff-'+ CAST(T1.FileNum AS NVARCHAR(30)) AS  Legacy_ID__c	
				,T1.LastName  AS  LastName	
				,T1.FirstName  AS  FirstName	
				--,X.ID AS [CONTACTID]
	 			,T1.Position  AS  Title
				,CASE WHEN T1.[Status] ='Active' THEN 'TRUE' ELSE 'FALSE' END AS IsActive	
				,T1.DeptNum AS  Department_Number__c	
				,T1.DeptNumLong  AS  Department_Number_Long__c	
				,T1.LoginID +'@guidedogs.com' AS  Username	-- concatenate [loginID] + '@guidedogs.com'
				,T1.LoginID +'@guidedogs.com' AS  Email	-- concatenate [loginID] + '@guidedogs.com'
				,LEFT(T1.LoginID,8) AS Alias
				,CAST(T1.HireDate  AS DATE) AS  Hire_Date__c	
				,CAST(T1.TermDate  AS DATE) AS  Termination_Date__c	
				,CAST(T1.RehireDate  AS DATE) AS  Rehire_Date__c	
			 	,T1.[Location]  AS  Location	
				,T1.DogStatusChangeBy  AS  Dog_Status_Changed_By__c	
				,T1.DogNameChange  AS  Dog_Name_Change__c	
				,T1.PuppyConcerns  AS  Puppy_Concerns__c	
				,T1.PrattCommittee  AS  Pratt_Committee__c	
				,'ISO-8859-1'  AS EMAILENCODINGKEY	-- ISO-8859-1
				,'en_US' AS LANGUAGELOCALEKEY	-- en_US
				,'en_US' AS LOCALESIDKEY	-- en_US
				,'America/Los_Angeles'  AS TIMEZONESIDKEY	-- America/Los_Angeles 
				,'00e1L000001MhRpQAK'  AS ProfileId	-- "GDB Read Only"
				--, 'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30)) zrefFineNum
				, CASE WHEN T1.[Status] ='Active' THEN 'TRUE' ELSE 'FALSE' END AS zrefStatus
				,T1.LoginID zrefLoginID
				, T1.FileNum  AS  zrefLegacyNum
				

		INTO GDB_Final_migration.dbo.IMP_USER
			FROM GDB_KADE_Final.dbo.tblStaff AS T1
		--	LEFT JOIN GDB_Final_migration.dbo.XTR_User AS X1 ON T1.FileNum =  X1.ADP__C
		--	WHERE x1.ADP__C IS NULL
		--	LEFT JOIN GDB_Final_migration.dbo.XTR_CONTACT AS X ON 'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30))=X.Legacy_Id__c

			UNION

			 SELECT DISTINCT 
				 NULL  AS  ADP__c	
				,'Psn-'+ CAST(T1.psn_PersonID AS NVARCHAR(30)) AS  Legacy_ID__c
				,T1.psn_Last  AS  LastName	
				,T1.psn_First  AS  FirstName	
			--	,X.ID AS [CONTACTID]
	 			,NULL  AS  Title
				,'False' IsActive	
				,NULL AS  Department_Number__c	
				,NULL  AS  Department_Number_Long__c	
				,CAST(T1.psn_PersonID AS NVARCHAR(30)) +T1.psn_Last +'@guidedogs.com' AS  Username	-- concatenate [loginID] + '@guidedogs.com'
				,CAST(T1.psn_PersonID AS NVARCHAR(30)) +T1.psn_Last +'@guidedogs.com' AS  Email	-- concatenate [loginID] + '@guidedogs.com'
				,LEFT(T1.psn_PersonID,8) AS Alias
				,NULL AS  Hire_Date__c	
				,NULL AS  Termination_Date__c	
				,NULL AS  Rehire_Date__c	
			 	,NULL AS  [Location]	
				,NULL  AS  Dog_Status_Changed_By__c	
				,'NO'  AS  Dog_Name_Change__c	
				,NULL  AS  Puppy_Concerns__c	
				,NULL  AS  Pratt_Committee__c	
				,'ISO-8859-1'  AS EMAILENCODINGKEY	-- ISO-8859-1
				,'en_US' AS LANGUAGELOCALEKEY	-- en_US
				,'en_US' AS LOCALESIDKEY	-- en_US
				,'America/Los_Angeles'  AS TIMEZONESIDKEY	-- America/Los_Angeles 
				,'00e1L000001MhRpQAK'  AS ProfileId	-- "Read Only"
				--, 'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30)) zrefFineNum
				, 'False' AS zrefStatus
				,NULL AS zrefLoginID
				,T1.psn_PersonID AS zrefLegacyNum

				FROM GDB_KADE_Final.dbo.[tblPerson] AS T1
             --   LEFT JOIN GDB_Final_migration.dbo.XTR_CONTACT AS X ON CAST(T1.psn_PersonID AS NVARCHAR(30))=X.Legacy_Id__c
				INNER JOIN GDB_KADE_Final.dbo.tblPuppyClubDetail AS T2 ON T2.pcd_PersonID=T1.psn_PersonID
				WHERE T2.pcd_RelationCode='cfr' AND T1.psn_PersonID NOT IN (SELECT DISTINCT FileNum FROM GDB_KADE_Final.dbo.tblStaff)
				

END

SELECT * FROM GDB_Final_migration.dbo.IMP_USER
 
BEGIN -- audit

		--checkc dupe Alias
			SELECT * 
			FROM GDB_Final_migration.dbo.IMP_USER  
			WHERE Alias IN (SELECT Alias FROM GDB_Final_migration.dbo.IMP_USER  GROUP BY Alias HAVING COUNT(*)>1)
			ORDER BY LastName, FirstName

END

--Drop table GDB_Final_migration.dbo.Xtr_User

BEGIN--USERS ---update managerID 
			
			SELECT 
			X1.ID
			,X2.ID AS ManagerID
			INTO GDB_Final_migration.dbo.IMP_USERS_MANAGER_UPDATE -- DROP TABLE
			FROM GDB_KADE_Final.dbo.tblStaff AS T1
			LEFT JOIN GDB_Final_migration.dbo.XTR_User AS X1 ON T1.FileNum = X1.ADP__C
			LEFT JOIN GDB_Final_migration.dbo.XTR_User AS X2 ON t1.Manager = X2.ADP__C
			WHERE T1.Manager IS NOT NULL 

END 

SELECT COUNT(*) FROM GDB_Final_migration.dbo.XTR_User

SELECT * FROM GDB_Final_migration.dbo.IMP_USERS_MANAGER_UPDATE