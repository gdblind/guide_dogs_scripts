USE GDB_Final_migration
GO

BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblVetAuthorization 
			WHERE   SF_Object LIKE '%case%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblVetAuthorization 
			WHERE  SF_Object_2 LIKE '%allow%'
END 
/* record types
0121L000001UQCgQAO	Canine Welfare
0121L000001UQChQAO	Dog Ingestion
0121L000001UQCiQAO	Fund Committee Review
0121L000001UQCjQAO	Support Center Case
0121L000001UQCkQAO	DELETE Veterinary Financial Assistance Claim

*/


BEGIN--DROP CASE

	DROP TABLE GDB_Final_migration.DBO.IMP_CASE
END

BEGIN-- CREATE CASE --UPDATE FB-00151 adn FB-00448

SELECT DISTINCT 
				X1.ID OwnerId
				,'Pfa-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS Legacy_Id__c
				,NULL [Contact:Legacy_ID__c]
				,T1.pfa_DogID AS [Dog__r:Legacy_Id__c]
				,'0121L000001UQCiQAO' AS  RecordTypeId				-- Pratt Fund  --Fund Committee Review
				,'tblPrattFundAuthorization' AS Source_Data_Table__c
				,NULL AS CloseDate
				,T1.pfa_Descision  AS  [Status]
				,T1.pfa_CaseSummary  AS  [Description]	
				,NULL AS [Type]
			--	,NULL AS Reason__c
			--	,NULL AS Reason_Detail__c
				,'Pratt Fund'+T6.dog_Name+' '+CONVERT(VARCHAR(10),T1.pfa_SubmitDate,101) AS [Subject] --FB-00933
				,CAST(T1.pfa_SubmitDate AS DATE ) AS  CreatedDate
				,NULL AS Alert_Date__c
				,NULL AS Alert_Sent__c
				,NULL AS Alert_Recipient__C
				,NULL AS Ingestion_Type__c
				,NULL AS Require_Vet__c
				,NULL AS Survey_Summary__c
				,T1.pfa_DxCode AS [Pre_Existing_Diagnosis__r:Legacy_ID__c]
 				
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN T1.pfa_Amount1 
					WHEN T1.pfa_AmountType2= '19' THEN T1.pfa_Amount2 
					ELSE NULL
					END AS One_Time_Amount__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN T1.pfa_Amount1 
					WHEN T1.pfa_AmountType2= '39' THEN T1.pfa_Amount2 
					ELSE NULL
					END AS Annual_Amount__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN T1.pfa_ExpirationDate1 
					WHEN T1.pfa_AmountType2= '19' THEN T1.pfa_ExpirationDate2 
					ELSE NULL
					END AS One_Time_Expiration_Date__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN T1.pfa_ExpirationDate1 
					WHEN T1.pfa_AmountType2= '39' THEN T1.pfa_ExpirationDate2 
					ELSE NULL
					END AS Annual_Expiration_Date__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '19' THEN IIF(T1.pfa_AmountForLife1 = 'True', 'Yes','No')
					WHEN T1.pfa_AmountType2= '19' THEN IIF(T1.pfa_AmountForLife2 = 'True', 'Yes','No')
					ELSE NULL
					END AS One_Time_Amount_for_Life__c
				,CASE  
					WHEN T1.pfa_AmountType1 = '39' THEN IIF(T1.pfa_AmountForLife1 = 'True', 'Yes','No')
					WHEN T1.pfa_AmountType2= '39' THEN IIF(T1.pfa_AmountForLife2= 'True', 'Yes','No')
					ELSE NULL
					END AS Annual_Amount_For_Life__c
				,'Pratt Fund' AS Fund_Type__c  --FB-00933
		--		,T6.dog_Name +

			INTO GDB_Final_migration.DBO.IMP_CASE
			FROM  GDB_KADE_Final.dbo.tblPrattFundAuthorization AS T1 
			LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T2 ON T2.DiagnosisCode=T1.pfa_DxCode
			LEFT JOIN GDB_KADE_Final.dbo.trefVetDiagCategory AS T3 ON T2.DiagnosisCategory=T3.CategoryCode
			LEFT JOIN GDB_KADE_Final.dbo.trefDogReleaseCategories AS T4 ON T2.DogReleaseCategory=T4.Code
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T5 ON T5.FileNum=T1.pfa_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T5.FileNum
			LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T6 ON T6.dog_DogID=T1.pfa_DogID

UNION

SELECT DISTINCT 
			   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'not-'+CAST(T1.not_Key  AS varchar(30)) AS  Legacy_ID__c	
				,T3.psn_PersonID  AS  [Contact:Legacy_ID__c]
				,NULL AS  [Dog__r:Legacy_Id__c]	
				,'0121L000001UQCjQAO' AS  RecordTypeId	-- Support Center
				,'tblPhoneNote'  AS  Source_Data_Table__c	-- tblPhoneNote
				,CAST(T1.not_Date  AS DATE) AS  ClosedDate	
				,CASE WHEN T1.not_Date > GETDATE() THEN 'Open' ELSE 'Closed' END AS  [Status]	-- if > Today then 'Closed' else 'Open'
	 			,CAST(T1.not_Memo AS NVARCHAR(MAX)) AS  [Description]	
				,T8.erc_ERCode AS  [Type]	--fb-00045
			--	,T5.[CASE#Reason]  AS  Reason__c	--fb-00045
			--	,T5.[Case#ReasonDetail]  AS  Reason_Detail__c  --fb-00045
				,IIF(T1.not_Summary IS NULL, 'Phone Note ' +CONVERT(VARCHAR(20), T1.not_Date,101),T1.not_Summary)  AS  [Subject]	--FB-00933
				,IIF(CAST(T1.not_Date  AS DATE)<= CAST(T1.not_CreatedDate AS DATE),CAST(T1.not_Date  AS DATE),CAST(T1.not_CreatedDate AS DATE))  AS  CreatedDate	
				,CAST(T6.pra_AlertDate AS DATE)   AS Alert_Date__c
 				,CASE T6.pra_AlertSent WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS Alert_Sent__c
 				,T6.pra_EmailAdditions AS Alert_Recipient__C
				,NULL AS Ingestion_Type__c
				,NULL AS Require_Vet__c
 				,CAST(CAST(T7.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T7.scs_SurveyOffered AS Survey_Summary__c
				--FILLERS
				,NULL AS [Pre_Existing_Diagnosis__r:Legacy_ID__c] 
				,NULL AS One_Time_Amount__c 
				,NULL AS Annual_Amount__c 
				,NULL AS  One_Time_Expiration_Date__c	
				,NULL AS  Annual_Expiration_Date__c	
				,NULL AS  One_Time_Amount_for_Life__c	
  				,NULL AS  Annual_Amount_For_Life__c	
				,NULL AS Fund_Type__c  --FB-00933
		    
	 		FROM GDB_KADE_Final.dbo.tblPhoneNote AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.not_PersonRelID
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T4 ON T4.FileNum=t1.not_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T4.FileNum
			--LEFT JOIN GDB_Final_migration.dbo.CHART_PhoneNote_NatureofCall AS T5 ON T5.not_NatureOfCall = T1.not_NatureOfCall  --fb-00045
			LEFT JOIN GDB_KADE_Final.dbo.tblPhoneRptAlerts AS T6 ON T6.pra_PhoneRptID=T1.not_Key
			LEFT JOIN (SELECT * FROM GDB_KADE_Final.DBO.tblSupportCtrSurvey WHERE scs_FormID=1) AS T7 ON T7.scs_FormRecID=T1.not_Key
			LEFT JOIN  GDB_KADE_Final.dbo.trefAGSNatureOfCallCodes AS T8 ON T8.erc_ID=T1.not_NatureOfCall
			
	UNION 
			
			SELECT DISTINCT  
				CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'di-'+CAST(T1.di_DogIngestID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate 'di-'+[di_DogIngestID]
				,NULL AS  [Contact:Legacy_ID__c]
				,T1.di_DogID  AS  [Dog__r:Legacy_Id__c]	
				,'0121L000001UQChQAO'  AS  RecordTypeId	-- Dog Ingestion
				,'tblDogIngest'  AS  Source_Data_Table__c	-- tblDogIngest
				,CAST(T1.di_IngestDate AS DATE) AS  ClosedDate	
				,CASE WHEN T1.di_IngestDate < GETDATE() THEN 'Closed' ELSE 'New' END  AS  [Status]	-- if < Today then 'Closed' else 'New'
				,T1.di_Description  AS  [Description]
				,'Dog Ingestion'  AS  [Type]	-- Dog Ingestion
			--	,NULL AS Reason__c
			--	,NULL AS Reason_Detail__c
				,IIF(t1.di_IngestDate IS NOT NULL,'Dog Ingestion ' + CONVERT(VARCHAR(10),T1.di_IngestDate,101), 'Dog Ingestion ' + CONVERT(VARCHAR(10),T1.di_CreatedDate,101)) AS  [Subject]	-- concatenate 'Dog Insgestion ' +[di_IngestDate] --FB-00933
				,IIF(CAST(T1.di_IngestDate AS DATE)<= CAST(T1.di_CreatedDate  AS DATE), CAST(T1.di_IngestDate AS DATE), CAST(T1.di_CreatedDate  AS DATE)) AS  CreatedDate	
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,T1.di_IngestType  AS  Ingestion_Type__c	
				,T1.di_RequireVet  AS  Require_Vet__c	-- Default nulls to 'No'
	 			,NULL AS Survey_Summary__c --filler
				,NULL AS [Pre_Existing_Diagnosis__r:Legacy_ID__c] 
				,NULL AS One_Time_Amount__c 
				,NULL AS Annual_Amount__c 
				,NULL AS  One_Time_Expiration_Date__c	
				,NULL AS  Annual_Expiration_Date__c	
				,NULL AS  One_Time_Amount_for_Life__c	
  				,NULL AS  Annual_Amount_For_Life__c
				,NULL AS Fund_Type__c  --FB-00933
 				
 	 		FROM GDB_KADE_Final.dbo.tblDogIngest AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T2 ON T2.FileNum=t1.di_EntryBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T2.FileNum

		UNION 

			SELECT   DISTINCT
				 CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Scn-'+CAST(T1.scn_ID AS NVARCHAR(30))  AS  Legacy_Id__c	-- concatenate 'Scn-'+[scn_ID]
				,NULL AS  [Contact:Legacy_ID__c]
				,NULL AS  [Dog__r:Legacy_Id__c]	
				,'0121L000001UQCjQAO'  AS  RecordTypeID	-- Support Center
				,'tblSupportCenterNote' AS  Source_Data_Table__c
				,CAST(T1.scn_Date  AS DATE) AS  ClosedDate	
				,CASE WHEN T1.scn_Date < GETDATE() THEN 'Completed' ELSE 'New' END AS  [Status]	-- is <Today then 'Completed' else 'New'
				,CAST(T1.scn_Memo  AS NVARCHAR(MAX)) AS  [Description]	
				,T5.noc_Description  AS  [Type]	
			--	,T2.[CASE#Reason] AS  Reason__c	  --fb-00045
			--	,T2.[CASE#ReasonDetail] AS  Reason_Detail__c	--fb-00045
				--,CAST(CASE WHEN T1.scn_Summary IS NULL THEN LEFT(CAST(T1.scn_Memo AS NVARCHAR(MAX)), 255) ELSE T1.scn_Summary END AS NVARCHAR(MAX)) AS  [Subject]
				,IIF(T1.scn_Summary IS NULL, 'Support Center Note ' +CONVERT(VARCHAR(20), T1.scn_Date,101),T1.scn_Summary)  AS  [Subject]	--FB-00933
				,CAST(T1.scn_Date  AS DATE) AS CreatedDate--filler
				,NULL AS Alert_Date__c  --filler
				,NULL AS Alert_Sent__c  --filler
				,NULL AS Alert_Recipient__c   --filler
				,NULL AS Ingestion_Type__c --filler
				,NULL AS Require_Vet__c --filler
 				,CAST(CAST(T3.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T3.scs_SurveyOffered AS Survey_Summary__c
					--FILLERS
				,NULL AS [Pre_Existing_Diagnosis__r:Legacy_ID__c] 
				,NULL AS One_Time_Amount__c 
				,NULL AS Annual_Amount__c 
				,NULL AS  One_Time_Expiration_Date__c	
				,NULL AS  Annual_Expiration_Date__c	
				,NULL AS  One_Time_Amount_for_Life__c	
  				,NULL AS  Annual_Amount_For_Life__c	
				,NULL AS Fund_Type__c  --FB-00933

			FROM GDB_KADE_Final.dbo.tblSupportCenterNote AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T4 ON T4.FileNum=t1.scn_WhoBy
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T4.FileNum
			--LEFT JOIN GDB_Final_migration.DBO.CHART_SupportCenter_NaOfCall AS T2 ON T1.scn_NatureOfCall=T2.scn_NatureOfCall
			LEFT JOIN (SELECT * FROM GDB_KADE_Final.DBO.tblSupportCtrSurvey WHERE scs_FormID=2) AS T3 ON T3.scs_FormRecID=T1.scn_ID
			LEFT JOIN  GDB_KADE_Final.dbo.[trefSCNatureofCall] AS T5 ON T5.noc_ID=T1.scn_NatureOfCall

-- TC2: 183,306 Final  193,104

--Update Field Values

UPDATE GDB_Final_migration.dbo.IMP_CASE
SET [Status]='Closed'
	WHERE [Status]='Close'

UPDATE GDB_Final_migration.dbo.IMP_CASE
SET [Status]='Closed'
	WHERE [Status]='Completed'



SELECT DISTINCT [STATUS], COUNT(*) FROM GDB_Final_migration.DBO.IMP_CASE
GROUP BY [Status]




BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_CASE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CASE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT Source_Data_Table__c, COUNT(*) c
	FROM GDB_Final_migration.dbo.IMP_CASE
	GROUP BY Source_Data_Table__c

END 

SELECT * FROM GDB_Final_migration.dbo.IMP_CASE
	ORDER BY [Contact:Legacy_ID__c],[Dog__r:Legacy_Id__c]

--exceptions

	DROP TABLE GDB_Final_migration.[dbo].[XTR_CASE]
SELECT T1.*
FROM GDB_Final_migration.dbo.IMP_CASE AS T1
LEFT JOIN GDB_Final_migration.[dbo].[XTR_CASE] AS T2 ON T1.Legacy_Id__c=T2.Legacy_ID__c
WHERE T2.Legacy_ID__c IS NULL