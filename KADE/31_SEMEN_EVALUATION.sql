USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDogSemenEvaluation
			WHERE   SF_Object LIKE '%se%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_SEMEN_EVALUATION

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DSE-'+CAST(T1.dse_SemenEvalID  AS NVARCHAR(30)) AS  Legacy_ID__c		
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]				-- Link [tblDog].[dog_DogID]
							,CAST(T1.dse_DateCollected  AS DATE) AS  Date_Collected__c		
							,T1.dse_Clinician  AS  Clinician__c		
							,T1.dse_EvalReason  AS  Evaluation_Reason__c		
							,T1.dse_CollectedWith  AS  Collected_With__c		
							,T1.dse_Libido  AS  Libido__c		
							,T1.dse_TesticularSize  AS  Testicular_Size__c		
							,T1.dse_FractionsCollected  AS  Fractions_Collected__c		
							,T1.dse_VolumeML  AS  Volume_per_ML__c		
							,(T1.dse_MotilityPct *100)  AS  Total_Motility__c			--fb-02486
							,T1.dse_Quality  AS  Quality__c		
							,T1.dse_Speed  AS  Speed__c		
							,T1.dse_SpermCount  AS  Sperm_Count_per_ML__c		
							,(T1.dse_MorphNormal *100)  AS  Total_Normal_Morphology__c	--fb-02486		
							,(T1.dse_MorphPrimaryAb  *100) AS  Morph_Primary_Abnormalities__c		--fb-02486
							,(T1.dse_MorphSecondAb  *100) AS  Secondary_Morphological_Abnormalities__c		--fb-02486
							,CASE T1.dse_PrimHeads WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END    AS  Historic_Prim_Heads__c		
							,T1.dse_iPrimHeads  AS  Prim_Heads__c		
							,CASE T1.dse_PrimMembraneDestroyed  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Membrane_Destroyed__c		
							,T1.dse_iPrimMembraneDestroyed  AS  Prim_Membrane_Destroyed__c		
							,CASE T1.dse_PrimMidAbaxial  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Mid_Abaxial__c		
							,T1.dse_iPrimMidAbaxial  AS  Abaxial_Attachments__c		
							,CASE T1.dse_PrimMidDouble  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Mid_Double__c		
							,T1.dse_iPrimMidDouble  AS  Double_Midpieces__c		
							,CASE T1.dse_PrimMidCoiled  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Med_Coiled__c		
							,T1.dse_iPrimMidCoiled  AS  Coiled_Midpieces__c		
							,CASE T1.dse_PrimMidFrayedThin  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Mid_Frayed_Thin__c		
							,T1.dse_iPrimMidFrayedThin  AS  Frayed_Thin_Midpieces__c		
							,CASE T1.dse_PrimSwollen  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Swollen__c		
							,T1.dse_iPrimSwollen  AS  Prim_Swollen__c		
							,CASE T1.dse_PrimProximalDrops  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Proximal_Drops__c		
							,T1.dse_iPrimProximalDrops  AS  Proximal_Droplets__c		
							,CASE T1.dse_PrimTailCoiled  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Tail_Coiled__c		
							,T1.dse_iPrimTailCoiled  AS  Coiled_Tails__c		
							,CASE T1.dse_PrimTailDouble  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_Prim_Tail_Double__c		
							,T1.dse_iPrimTailDouble  AS  Double_Tails__c		
							,CASE T1.dse_2ndDetachedHeads  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_2nd_Detached_Heads__c		
							,T1.dse_i2ndDetachedHeads  AS  Detached_Heads__c		
							,CASE T1.dse_2ndDetachedGalea  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_2nd_Detached_Galea__c		
							,CASE T1.dse_2ndBentMid  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_2nd_Bent_Mid__c		
							,T1.dse_i2ndBentMid  AS  Bent_Midpieces		
							,CASE T1.dse_2ndBentTail  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_2nd_Bent_Tail__c		
							,T1.dse_i2ndBentTail  AS  Bent_Tails__c		
							,CASE T1.dse_2ndDistalDrops  WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Historic_2nd_Distal_Drops__c		
							,T1.dse_i2ndDistalDrops  AS  Distal_Droplets__c		
							,CASE T1.dse_2ndSeparatingHead WHEN -1 THEN 'TRUE' WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Historic_2nd_Separating_Head__c		
							,T1.dse_i2ndSeparatingHead  AS  Separating_Head__c		
							,CAST(T1.dse_DateNextDue  AS DATE) AS  Historic_Date_Next_Due__c		
							,CAST(T1.dse_LastEvalDate AS DATE) AS  Last_Eval_Date__c		
							,T1.dse_Comments  AS  Comments__c		
							,T1.dse_Recommendations  AS  Recommendations__c		


					 INTO GDB_Final_migration.DBO.IMP_SEMEN_EVALUATION
					FROM GDB_KADE_Final.dbo.tblDogSemenEvaluation AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.dse_DogID


					--FINAL  2886


END 

BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_SEMEN_EVALUATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_SEMEN_EVALUATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_SEMEN_EVALUATION 
			GROUP BY zrefSrc
		 
END 