USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API
					,','+SF_Field_API+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblKennelColorEval
			WHERE   SF_Object LIKE '%ob%'
			
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API  
					,SF_Field_API+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDogEarlyDevelopment
			WHERE   SF_Object LIKE '%ob%'

END 

/*
SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%ob%'

0121L000001UQD9QAO	Dog
0121L000001UQDAQA4	Early Development
0121L000001UQDBQA4	Kennel Color Eval
0121L000001UQDCQA4	Student

*/


BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_OBSERVATION

END 

BEGIN -- CREATE IMP 
/*
					SELECT  DISTINCT
						 OwnerId= GDB_Final_migration.[dbo].[fnc_OwnerId]() 
						,RecordTypeID = '0123D0000004axLQAQ'     	-- Student	
						,Legacy_Id__c = 'MEE-'+CAST(T1.mee_ID  AS NVARCHAR(30))    	-- concatenate 'Mee-'+[mee_ID]	
						,[Class__r:Legacy_ID__c] = T2I.Legacy_Id__c 				-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
 						,[Contact__r:Legacy_ID__c] = T4.psn_PersonID   				-- Link [tblPersonRel].[prl_ClientInstanceID]
						--,[Application__r:Legacy_Id__c] = NULL   --filler
						,[Dog__r:Legacy_ID__c] = NULL			--filler
						--,Study__c = NULL
						--,Comments__c = NULL
						,Active__c = NULL
 					--	,Day_Held__c   FB_00002 		
						,Type__c = T5.cme_Type   								-- migrate value form [cme_type]	-- Yes/Link [trefClassMeetings].[cle_TypeID]
 						--,Summary__c = CAST(T1.mee_Summary AS NVARCHAR(MAX))  	--FB-00002	
			 			,Supervisor_Review__c = NULL 
						,Supervisor__c = NULL 
						,Tell_Student__c = NULL 
						,History__c = NULL 
						,History_Level__c = NULL 
						,Issues__c = NULL 
						,Observation_Plan__c = CAST(T1.mee_Summary AS NVARCHAR(MAX)) 
						,Outcome__c = NULL 
						,Status__c = NULL 
						,Day_Began__c = T1.mee_DayHeld	--FB-00002
						,Day_Resolved__c =NULL		
						,Kade_Created_By__c = NULL	
						--,Color_Eval__c = NULL	--FB-00002
						,Date_Eval__c = NULL	
						,Date_Assigned__c = NULL	
						,Date_Left__c = NULL	
						,Excessive_Jumping__c = NULL	
						,Excessive_Barking__c = NULL	
						,Heavy_Panting__c = NULL	
						,Mouthy__c = NULL	
						,Restlessness_Pacing__c = NULL	
						,Excessive_Drooling__c = NULL	
						,Resistant_Inside__c = NULL	
						,Trembling__c = NULL	
						,Balking__c = NULL	
						,Skittish__c = NULL 
						,Other__c = NULL	
						,Other_Comments__c  = NULL	
						,Singled_Double__c = NULL	
						,Roommate__c = NULL	
						,Location_Kennel__c  = NULL	
						,Roommate_Eval__c = NULL	
						,Crate_Schedule__c = NULL	
						,Tie_Down__c = NULL	
						,Extra_Walks__c = NULL	
						,Time_in_Office__c = NULL	
						,Training_Sessions__c = NULL	
						,One_on_One__c = NULL	
						,Community_Run__c = NULL	
						,Socialization_Walks__c = NULL	
						,PVC_feeder__c = NULL	
						,Puzzle_Bowl__c = NULL						

						,CreatedDate = CAST(T1.mee_CreatedDate AS DATE) 	
						,zrefID = T1.mee_ID
						,zrefSrc = 'tblClassMeetings'
					--INTO GDB_Final_migration.dbo.IMP_OBSERVATION
					FROM GDB_KADE_Final.dbo.tblClassMeetings AS T1
 					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.mee_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.mee_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID --fixed FB-01123
					LEFT JOIN GDB_KADE_Final.dbo.trefClassMeetings AS T5 ON T5.cme_TypeID=T1.mee_Type

				UNION	
*/					
					SELECT  DISTINCT
						 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END   -- Link [tblStaff].[Filenum]
						,RecordTypeID = '0121L000001UQD9QAO'     	-- Dog
						,Legacy_Id__c = 'DCO-'+CAST(T1.dco_ID AS NVARCHAR(30))    	-- Concatenate "DCO-" with dco_ID	
						,[Class__r:Legacy_ID__c] = 'cls-'+CAST(T2.cli_ClassCode AS NVARCHAR(30))
						,[Contact__r:Legacy_ID__c] = T4.prl_PersonID 
						--,[Application__r:Legacy_Id__c] = 'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))     	
						,[Dog__r:Legacy_ID__c] = T3.dog_DogID 
						--,Study__c = NULL
						--,Comments__c = NULL
						,Active__c = NULL
 						--,Day_Held__c = NULL 
						,Type__c = T1.dco_Type   		
						--,Summary__c = NULL 
 			 			,Supervisor_Review__c = CASE T1.dco_SupReview  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END    		
						,Supervisor__c = X2.ID     		-- Link [tblStaff].[Filenum]
						,Tell_Student__c = CASE T1.dco_TellStdnt  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  		
						,History__c = CASE T1.dco_HistoryYesNo  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END     		
						,History_Level__c = T1.dco_HistoryLevel     		
						,Issues__c = CAST(T1.dco_Issue AS NVARCHAR(MAX))   		
						,Observation_Plan__c = CAST(T1.dco_Plan  AS NVARCHAR(MAX))     		
						,Outcome__c = CAST(T1.dco_Outcome  AS NVARCHAR(MAX))    		
						,Status__c = T1.dco_Status    		
						,Day_Began__c = T1.dco_DayBegan	
						,Day_Resolved__c = T1.dco_DayResolved  	
						,Kade_Created_By__c = NULL	
					--	,Color_Eval__c = NULL	--FB-00002
						,Date_Eval__c = NULL	
						,Date_Assigned__c = NULL	
						,Date_Left__c = NULL	
						,Excessive_Jumping__c = NULL	
						,Excessive_Barking__c = NULL	
						,Heavy_Panting__c = NULL	
						,Mouthy__c = NULL	
						,Restlessness_Pacing__c = NULL	
						,Excessive_Drooling__c = NULL	
						,Resistant_Inside__c = NULL	
						,Trembling__c = NULL	
						,Balking__c = NULL	
						,Skittish__c = NULL 
						,Other__c = NULL	
						,Other_Comments__c  = NULL	
						,Singled_Double__c = NULL	
						,Roommate__c = NULL	
						,Location_Kennel__c  = NULL	
						,Roommate_Eval__c = NULL	
						,Crate_Schedule__c = NULL	
						,Tie_Down__c = NULL	
						,Extra_Walks__c = NULL	
						,Time_in_Office__c = NULL	
						,Training_Sessions__c = NULL	
						,One_on_One__c = NULL	
						,Community_Run__c = NULL	
						,Socialization_Walks__c = NULL	
						,PVC_feeder__c = NULL	
						,Puzzle_Bowl__c = NULL						

						,CreatedDate = CAST(T1.dco_CreatedDate AS DATE)   		
 						,zrefID = T1.dco_ID  
						,zrefSrc = 'tblClassDogConcerns' 
				--INTO GDB_Final_migration.dbo.IMP_OBSERVATION 
					FROM GDB_KADE_Final.dbo.tblClassDogConcerns AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.dco_ClientID
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T3 ON T3.dog_DogID=T1.dco_DogID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=t1.dco_WhoBy
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=t1.dco_Supervisor
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T4 ON T2.cli_ClientID=T4.prl_ClientInstanceID

				UNION

					SELECT  DISTINCT
						 OwnerId = GDB_Final_migration.[dbo].[fnc_OwnerId]() 
		       	        ,RecordTypeID = '0121L000001UQDAQA4'     	-- Early Developement - FB-000298
						,Legacy_ID__c = 'EDS-'+CAST(T1.eds_ID AS NVARCHAR(30))    	-- concatenate 'Eds='+[eds_ID]	
						,[Class__r:Legacy_ID__c] = NULL--filler
						,[Contact__r:Legacy_ID__c] = NULL--filler
						--,[Application__r:Legacy_Id__c] = NULL   --filler
						,[Dog__r:Legacy_ID__c] = T3.dog_DogID 		-- Link [tblDog].[dog_DogID]
					
					--	,Study__c = T2.Study -- migrate value from [Study]	-- Yes/Link [trefDogEarlyDevelopmentStudy].[StudyId]
					--	,Comments__c = T1.eds_Comments   		
						,Active__c = CASE T1.eds_Active   WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END    	
						
						--,Day_Held__c = NULL     		
						,Type__c = T2.Study  --FB-00002  							
 						--,Summary__c = NULL 
		 
			 			,Supervisor_Review__c = NULL 
						,Supervisor__c = NULL 
						,Tell_Student__c = NULL 
						,History__c = NULL 
						,History_Level__c = NULL 
						,Issues__c = NULL 
						,Observation_Plan__c = CAST(T1.eds_Comments	AS NVARCHAR(MAX)) --FB-00002 
						,Outcome__c = NULL 
						,Status__c = NULL 
						,Day_Began__c = NULL	
						,Day_Resolved__c =NULL		
						,Kade_Created_By__c = NULL	
 						--,Color_Eval__c = NULL	--FB-00002
						,Date_Eval__c = NULL	
						,Date_Assigned__c = NULL	
						,Date_Left__c = NULL	
						,Excessive_Jumping__c = NULL	
						,Excessive_Barking__c = NULL	
						,Heavy_Panting__c = NULL	
						,Mouthy__c = NULL	
						,Restlessness_Pacing__c = NULL	
						,Excessive_Drooling__c = NULL	
						,Resistant_Inside__c = NULL	
						,Trembling__c = NULL	
						,Balking__c = NULL	
						,Skittish__c = NULL 
						,Other__c = NULL	
						,Other_Comments__c  = NULL	
						,Singled_Double__c = NULL	
						,Roommate__c = NULL	
						,Location_Kennel__c  = NULL	
						,Roommate_Eval__c = NULL	
						,Crate_Schedule__c = NULL	
						,Tie_Down__c = NULL	
						,Extra_Walks__c = NULL	
						,Time_in_Office__c = NULL	
						,Training_Sessions__c = NULL	
						,One_on_One__c = NULL	
						,Community_Run__c = NULL	
						,Socialization_Walks__c = NULL	
						,PVC_feeder__c = NULL	
						,Puzzle_Bowl__c = NULL						

 						,CreatedDate = CAST(T1.eds_CreatedDate AS DATE)
 						,zrefID = T1.eds_ID  
						,zrefSrc = 'tblDogEarlyDevelopment' 	
					FROM GDB_KADE_Final.dbo.tblDogEarlyDevelopment AS T1
					LEFT JOIN GDB_KADE_Final.dbo.trefDogEarlyDevelopmentStudy AS T2 ON T2.StudyID=T1.eds_Study
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T3 ON T3.dog_DogID = T1.eds_DogID

	/*			UNION
				SELECT  DISTINCT
						 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END   -- Link [tblStaff].[Filenum] 
						,RecordTypeID = '0123D0000004axLQAQ'	-- Record type name ="Student"							
						,Legacy_Id__c = 'SCO-'+CAST(T1.sco_ID	AS NVARCHAR(30)) -- Concatenate "SCO-" with sco_ID	
						,[Class__r:Legacy_ID__c] = T2I.Legacy_Id__c 				-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
 						,[Contact__r:Legacy_ID__c] = T3.prl_PersonID  				-- Link [tblPersonRel].[prl_ClientInstanceID] --FB-01435
 						--,[Application__r:Legacy_Id__c] = NULL   --filler
						,[Dog__r:Legacy_ID__c] = T.dog_DogID  	-- Link [tblDog].[dog_DogID]
						--,Study__c = NULL 
						--,Comments__c = NULL
						,Active__c = NULL 
						--,Day_Held__c = NULL 
						,Type__c = T1.sco_Type		
 						--,Summary__c = NULL   
			 			,Supervisor_Review__c  = CASE T1.sco_SupReview	WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE'	END 
						,Supervisor__c = X2.ID		-- Link [tblStaff].[FileNum]
						,Tell_Student__c = CASE T1.sco_TellStdnt 	WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE'	END 
						,History__c = NULL 
						,History_Level__c = NULL 
						,Issues__c = CAST(T1.sco_Issues	AS NVARCHAR(MAX))	
						,Observation_Plan__c = CAST(T1.sco_Plan	AS NVARCHAR(MAX))	
						,Outcome__c = CAST(T1.sco_Outcome AS NVARCHAR(MAX))
						,Status__c = T1.sco_Status 
						,Day_Began__c = T1.sco_DayBegan		
						,Day_Resolved__c = T1.sco_DayResolved		
						,Kade_Created_By__c = T1.sco_CreatedBy		
						--,Color_Eval__c = NULL	--FB-00002
						,Date_Eval__c = NULL	
						,Date_Assigned__c = NULL	
						,Date_Left__c = NULL	
						,Excessive_Jumping__c = NULL	
						,Excessive_Barking__c = NULL	
						,Heavy_Panting__c = NULL	
						,Mouthy__c = NULL	
						,Restlessness_Pacing__c = NULL	
						,Excessive_Drooling__c = NULL	
						,Resistant_Inside__c = NULL	
						,Trembling__c = NULL	
						,Balking__c = NULL	
						,Skittish__c = NULL 
						,Other__c = NULL	
						,Other_Comments__c  = NULL	
						,Singled_Double__c = NULL	
						,Roommate__c = NULL	
						,Location_Kennel__c  = NULL	
						,Roommate_Eval__c = NULL	
						,Crate_Schedule__c = NULL	
						,Tie_Down__c = NULL	
						,Extra_Walks__c = NULL	
						,Time_in_Office__c = NULL	
						,Training_Sessions__c = NULL	
						,One_on_One__c = NULL	
						,Community_Run__c = NULL	
						,Socialization_Walks__c = NULL	
						,PVC_feeder__c = NULL	
						,Puzzle_Bowl__c = NULL						
						,CreatedDate = CAST(T1.sco_CreatedDate	AS DATE)	
						,zrefID = T1.sco_ID   
						,zrefSrc = 'tblClassStdntConcerns'   

					FROM GDB_KADE_Final.dbo.tblClassStdntConcerns AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.sco_DogID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=t1.sco_WhoBy
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=t1.sco_Supervisor
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.sco_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.sco_ClientID  --fixed FB-01123
						--	LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
 */
				UNION
		
					SELECT  DISTINCT
						 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END 
						,RecordTypeID = '0121L000001UQDBQA4'	-- Record type name = "Kennel Color Eval"	
						,Legacy_Id__c = 'KCE-'+CAST(T1.kce_ID AS NVARCHAR(30))	-- Concatenate "KCE-" with kce_ID	
						,[Class__r:Legacy_ID__c] = NULL--filler
						,[Contact__r:Legacy_ID__c]= NULL--filler
						--,[Application__r:Legacy_Id__c] = NULL   --filler
						,[Dog__r:Legacy_ID__c] = T.dog_DogID  	-- Link [tblDog].[dog_DogID]
						--,Study__c = NULL
						--,Comments__c = NULL   		
						,Active__c = NULL    	
						--,Day_Held__c = NULL     		
						,Type__c = T1.kce_ColorEval    -- FB-00002							
 						--,Summary__c = NULL   
			 			,Supervisor_Review__c = NULL 
						,Supervisor__c = NULL 
						,Tell_Student__c = NULL 
						,History__c = NULL 
						,History_Level__c = NULL 
						,Issues__c = NULL 
						,Observation_Plan__c = T1.kce_OngoingPlan
						,Outcome__c = NULL 
						,Status__c = NULL 
						,Day_Began__c = NULL	
						,Day_Resolved__c =NULL		
						,Kade_Created_By__c = NULL	
							
						--,Color_Eval__c =  T1.kce_ColorEval	--FB-00002
						,Date_Eval__c = CAST(T1.kce_DateEval AS DATE)		
						,Date_Assigned__c = CAST(T1.kce_DateAssigned AS DATE) 		
						,Date_Left__c = CAST(T1.kce_DateLeft	AS DATE)		
						,Excessive_Jumping__c = CASE T1.kce_ExcessiveJumping WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Excessive_Barking__c = CASE T1.kce_ExcessiveBarking  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Heavy_Panting__c = CASE T1.kce_HeavyPanting  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Mouthy__c = CASE T1.kce_Mouthly		 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Restlessness_Pacing__c = CASE T1.[kce_Restlessness/Pacing]		 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Excessive_Drooling__c = CASE T1.kce_ExcessiveDrooling	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Resistant_Inside__c =CASE  T1.kce_ResistantInside	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Trembling__c = CASE T1.kce_Trembling  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Balking__c = CASE T1.kce_Bulking  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Skittish__c = CASE T1.kce_Skittish	  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Other__c = CASE T1.kce_Other	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Other_Comments__c = T1.kce_OtherComments		
						,Singled_Double__c = T1.kce_SingledDouble		
						,Roommate__c = T1.kce_Roommate		
						,Location_Kennel__c = T1.kce_LocationKennel		
						,Roommate_Eval__c = T1.kce_RoommateEval		
						,Crate_Schedule__c =CASE T1.kce_CrateSchedule WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 		
						,Tie_Down__c =CASE T1.[kce_Tie-Down] WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Extra_Walks__c =CASE T1.kce_ExtraWalks	  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Time_in_Office__c =CASE T1.kce_TimeinOffice WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 		
						,Training_Sessions__c =CASE T1.kce_TrainingSessions	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,One_on_One__c = CASE T1.kce_OneonOne  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Community_Run__c =CASE T1.kce_CommunityRun	  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 
						,Socialization_Walks__c =CASE T1.kce_SocializationWalks	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,PVC_feeder__c =CASE T1.kce_PVCfeeder	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
						,Puzzle_Bowl__c =CASE T1.kce_PuzzleBowl	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
			 			
						,CreatedDate = CAST(T1.kce_CreatedDate  AS DATE) 
						,zrefID = T1.kce_ID  
						,zrefSrc = 'tblKennelColorEval' 

					FROM GDB_KADE_Final.dbo.tblKennelColorEval AS T1
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=t1.kce_CWTID
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.kce_DogID
					

					
								
					 

END 

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_OBSERVATION 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_OBSERVATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_OBSERVATION 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_OBSERVATION ORDER BY zrefSrc, zrefID
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_OBSERVATION
	SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblClassMeetings  --9515

END 