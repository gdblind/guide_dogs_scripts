USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblTRNTestingCategories
			WHERE   SF_Object LIKE '%test%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_TEST_CATEGORY

END 

BEGIN -- CREATE IMP 

					SELECT  -- GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					'TTT-'+CAST(T1.ttt_TestTrendsID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Ttt-'+[ttt_TestTrendsID]	
					,'TDT-'+CAST(T2.tdt_TestingID AS NVARCHAR(30))  AS  [Training__r:Legacy_ID__c]		-- Link [tblTRNTesting].[tdt_TestingID]
					--,T4.tc_CategoryText AS  Category__c	-- migrate value from [trefTRNTestingCategories].[tc_CateogryText]	-- Ref    [trefTRNTestingTrends].[tt_TrendsID] Link  [trefTRNTestingTrends].[tt_CategoryTypeID] to [trefTRNTestingCategories].[tc_CategoryTypeID] 
					--,T3.tt_TrendsText  AS  Type__c	-- migrate value from [tt_TrendsText]	-- Ref [trefTRNTestingTrends].[tt_TrendsID]
					, T3.tt_TrendsText AS [Name]
					,T3.tt_TrendsText AS Trend__c
					,CAST(T1.ttt_CreatedDate AS date) AS CreatedDate
					
					,'tblTRNTestingTrends' AS zrefSrc
					
					INTO GDB_Final_migration.DBO.IMP_TEST_CATEGORY
					FROM GDB_KADE_Final.dbo.tblTRNTestingTrends AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblTRNTesting AS T2 ON T2.tdt_TestingID=T1.ttt_TestingID
					LEFT JOIN GDB_KADE_Final.dbo.trefTRNTestingTrends AS T3 ON T3.tt_TrendsID=T1.ttt_TrendsID
					LEFT JOIN GDB_KADE_Final.dbo.trefTRNTestingCategories AS T4 ON T4.tc_CategoryTypeID=T3.tt_CategoryTypeID
		/*		UNION 
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'TTC-'+CAST(T1.ttc_TestCatID  AS NVARCHAR(30)) AS  Legacy_ID__c		
					,'TDT-'+CAST(T2.tdt_TestingID AS NVARCHAR(30))  AS  [Training__r:Legacy_ID__c]		-- Link [tblTRNTesting].[tdt_TestingID]
					,NULL AS Category__c
					,T3.tc_CategoryText  AS  Type__c	-- migrate value from [tc_CategoryText]	-- Yes Link [trefTRNTestingCategories].[tc_CategoryTypeID]
					,T1.ttc_CatScore  AS  Category_Score__c		

					,CAST(T1.ttc_CreatedDate AS DATE) AS CreatedDate
					,'tblTRNTestingCategories' AS zrefSrc
					FROM GDB_KADE_Final.dbo.tblTRNTestingCategories AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblTRNTesting AS T2 ON T2.tdt_TestingID=T1.ttc_TestingID
					LEFT JOIN GDB_KADE_Final.dbo.trefTRNTestingCategories AS T3 ON T3.tc_CategoryTypeID=T1.ttc_CatIDType
					*/

END --Tc1: 500412  tc2: 518,036

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_TEST_CATEGORY 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_TEST_CATEGORY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c


	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_TEST_CATEGORY 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_TEST_CATEGORY ORDER BY [Training__r:Legacy_ID__c]
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_TEST_CATEGORY
	 

END 