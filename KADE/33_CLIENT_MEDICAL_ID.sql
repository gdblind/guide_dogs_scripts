USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblNursingMedicalNotes
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 
 
 SELECT * FROM GDB_Final_migration.DBO.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE

BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_CLIENT_MEDICAL_ID
	 

END 


BEGIN -- CREATE IMP_CLIENT_MEDICAL_ID 
					 SELECT DISTINCT
					  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					 ,T1.Client_Medical_Id__c AS [Name]
					 ,T2.psn_First AS CLIENT_First_Name__c
					 ,T2.psn_Last AS Client_Last_Name__c  --FB-01075
					 ,CONVERT(VARCHAR(11),T2.psn_DOB, 101) AS CLIENT_DOB__c
					 ,T1.psn_PersonId AS zrefPersonId
				 	  INTO GDB_Final_migration.DBO.IMP_CLIENT_MEDICAL_ID
					 FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds AS T1
					 INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON t1.psn_PersonID = T2.psn_PersonID
					 ORDER BY Client_Medical_Id__c

END  --TC1 1005 TC2: final: 1359
			--Create Update to Contact import file
					SELECT DISTINCT psn_PersonID AS [Contact:Legacy_ID__c], Client_Medical_ID__c 
					INTO GDB_Final_migration.dbo.IMP_Contact_tblMedicalChartId
					FROM  GDB_Final_migration.dbo.stg_tblMedicalChartIds
					ORDER BY psn_PersonID, Client_Medical_ID__c


		--check dupes
					SELECT * FROM GDB_Final_migration.dbo.IMP_CLIENT_MEDICAL_ID
					WHERE [Name] IN (SELECT [Name] FROM GDB_Final_migration.dbo.IMP_CLIENT_MEDICAL_ID GROUP BY [Name] HAVING COUNT(*)>1)
					ORDER BY [Name]

  


  SELECT * FROM GDB_Final_migration.dbo.IMP_CLIENT_MEDICAL_ID

  SELECT * FROM GDB_Final_migration.dbo.IMP_Contact_tblMedicalChartId



