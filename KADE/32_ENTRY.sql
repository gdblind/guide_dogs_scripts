USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGDBInvDispenseTrans
			WHERE   SF_Object LIKE '%en%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_ENTRY

END 

BEGIN -- CREATE IMP 

					
			SELECT DISTINCT --GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					'Idt-'+ CAST(T1.idt_DispTransID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'Idt-'+[idt_DispTransID]	
				 	,'id-'+  CAST(T1.idt_DispenseID AS NVARCHAR(30))  AS  [Order__r:Legacy_Id__c]			 -- Link [tblGDBInvDispense].[id_DispenseID]
					,T2.Legacy_ID__c  AS  [Inventory__r:Legacy_Id__c]		-- Ref [trefGDBInvItemsDetail].[iid_ItemDetailID]
			 	 	 ,T1.idt_OpenTrans  AS  Open__c		
					,T1.idt_Quantity  AS  Quantity__c		
					,T5.iuom_Type  AS  Sell_UOM__c	-- migrate value in [iuom_Type]	-- Ref [trefGDBInvUOM].[iuom_ID]
					,T1.idt_SellUnitCost  AS  Sell_Unit_Cost__c		
					,CAST(T1.idt_DateProcessed AS DATE) AS  Date_Processed__c		
 					,CAST(T1.idt_CreatedDate AS DATE) AS CreatedDate
					,T1.idt_ItemDetailID
					,T2.Campus__c AS zref_InvCampus
					,t2.Department_Code__c AS zref_InvDept
					,T10.Campus__c AS zref_OrderCampus
					,T10.Department__c AS zref_OrderDept


			--INTO GDB_Final_migration.dbo.IMP_ENTRY
			FROM GDB_KADE_Final.dbo.tblGDBInvDispenseTrans AS T1
			LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvUOM AS T5 ON T5.iuom_ID=T1.idt_SellUOM
			INNER JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS T2 ON T1.idt_ItemDetailID=T2.zrefItemDetailID
			LEFT JOIN GDB_FINAL_Migration.dbo.IMP_GDB_ORDER AS T10 ON 'id-'+CAST(T1.idt_DispenseID AS VARCHAR(50)) = T10.Legacy_ID__c

	
END --tc1: 4623  tC2: 4940 final 5418
SELECT COUNT(*) 
FROM GDB_KADE_Final.dbo.tblGDBInvDispenseTrans --4940
 
BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_ENTRY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_ENTRY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		
END 



