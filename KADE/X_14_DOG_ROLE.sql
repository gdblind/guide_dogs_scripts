USE GDB_Final_migration
GO


BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblStaffDog
			WHERE   SF_Object LIKE '%dog%'
			
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'



END 

BEGIN --drop DOG ROLE

	DROP TABLE GDB_Final_migration.dbo.IMP_DOG_ROLE

END

BEGIN --CREATE IMP DOG ROLE
				SELECT	DISTINCT 
						GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'pd-'+CAST(T1.pd_RelationID  AS NVARCHAR(30)) AS  Legacy_Id__c		-- concatenate 'pd-'+[drl_relationsID]
						,CAST(T3.psn_PersonID  AS NVARCHAR(30)) AS  [Contact__r:Legacy_Id__c]					-- link through [tblPersonRel].[prl_PersonID] to contact	-- Link [tblPersonRel].[prl_RelationID] 
						,CAST(T4.drl_DogID  AS NVARCHAR(30)) AS  [Dog__r:Legacy_Id__c]						-- Link [tblDogRel].[drl_DogID]
						,NULL AS Breed__c	--filler	
						,NULL AS Archive__c	--filler
						,NULL AS Boarding_Only__c	--filler
 						,CASE T1.pd_CoRel WHEN 'SEC' THEN  'Secondary' WHEN  'PRI' THEN 'Primary' WHEN 'RRI' THEN 'Primary' END  AS  Co_Person_Relationship__c	-- SEC = Secondary PRI=Primary RRI=Primary	
						,T1.pd_RelCode  AS  Person_Role__c		
						,T1.pd_RelCode  AS  Dog_Role__c		
						,CAST(T1.pd_StartDate  AS DATE) AS  Start_Date__c		
						,CAST(T1.pd_EndDate  AS DATE) AS  End_Date__c		
						,T1.pd_EndReason  AS  End_Reason__c
						
						--ref
						,T1.pd_RelationID AS zrefID
						,'tblPersonDog' AS zrefSrc
				
			 	INTO GDB_Final_migration.dbo.IMP_DOG_ROLE
				
				FROM GDB_KADE_Final.dbo.tblPersonDog AS T1	
				--to person
				LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_RelationID=T1.pd_PersonRelID
				LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
				--to dog
				LEFT JOIN GDB_KADE_Final.dbo.tblDogRel AS T4 ON T4.drl_RelationID=T1.pd_DogRelID
				LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T4.drl_DogID

				UNION 

				SELECT	DISTINCT 
					GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'stf-'+CAST(T1.stf_DogID AS NVARCHAR(30))  AS  Legacy_Id__c	-- concatenate 'Stf-'+[stf_DogID]	-- Link tblDog_DogID
					,'Staff-'+ CAST(T3.FileNum AS NVARCHAR(30)) AS [Contact__r:Legacy_Id__c]	-- link to contact created from [tblStaff]	-- Yes/[tblStaff].[File_Num]
					,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]	-- concatenate 'Stf-'+[stf_DogID]	-- Link tblDog_DogID
 					,T1.stf_Breed  AS  Breed__c		
					,T1.stf_Archive  AS  Archive__c	-- Only migrate record when [sft_Archive]=No	
					,T1.stf_BoardingOnly  AS  Boarding_Only__c		
			  
			  		,NULL AS Co_Person_Relationship__c --filler
					,NULL AS Person_Role__c		
					,NULL AS Dog_Role__c		
 					,NULL AS Start_Date__c		
					,NULL AS End_Date__c		
					,NULL AS End_Reason__c			 

					--ref
					,T1.stf_DogID AS zrefID
					,'tblStaffDog' AS zrefSrc
				FROM GDB_KADE_Final.dbo.tblStaffDog AS T1
				LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID = T1.stf_DogID
				LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.stf_StaffID
				WHERE T1.stf_Archive='No'

				UNION 
				 
				SELECT	  
					GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'drl-'+CAST(T1.drl_RelationID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'drl'+[drl_relationsID]	
					,CAST(T4.psn_PersonID  AS NVARCHAR(30)) AS   [Contact__r:Legacy_Id__c]						-- link through [tblPersonDog].[pd_PersonRelID] to [tblPersonRel].[prl_PersonID]	-- Link  [tblPersonDog].[pd_PersonRelID] 
					,CAST(T5.dog_DogID  AS NVARCHAR(30)) AS   [Dog__r:Legacy_Id__c]							-- Link  [tblDog].[dog_DogID] 
					,NULL AS Breed__c	--filler	
					,NULL AS Archive__c	--filler
					,NULL AS Boarding_Only__c	--filler
 					,NULL AS Co_Person_Relationship__c --filler
					,T1.drl_RelationCode  AS  Person_Role__c		
					,T1.drl_RelationCode  AS  Dog_Role__c		
 					,CAST(T1.drl_StartDate   AS DATE)  AS  Start_Date__c		
					,CAST(T1.drl_EndDate   AS DATE)  AS  End_Date__c		
					,T1.drl_EndReason  AS  End_Reason__c			
					
					--ref
					,T1.drl_RelationID AS zrefID
					,'tblDogRel' AS zrefSrc
 				FROM  GDB_KADE_Final.dbo.tblDogRel AS T1   
				--to person
				INNER JOIN GDB_KADE_Final.dbo.tblPersonDog AS T2 ON T2.pd_DogRelID = T1.drl_RelationID
				LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_RelationID = T2.pd_PersonRelID
				LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				--to dog
				LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T1.drl_DogID
			 
				
	 

	END --TC1: 
		 
BEGIN-- audits

			SELECT zrefSrc, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_DOG_ROLE
			GROUP BY zrefSrc

			SELECT Dog_Role__c, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_DOG_ROLE
			GROUP BY  Dog_Role__c
			ORDER BY Dog_Role__c

			
			SELECT Person_Role__c, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_DOG_ROLE
			GROUP BY  Person_Role__c
			ORDER BY Person_Role__c

			SELECT * 
			FROM GDB_Final_migration.dbo.IMP_DOG_ROLE
			WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_Final_migration.dbo.IMP_DOG_ROLE GROUP BY Legacy_Id__c HAVING COUNT(*)>1)
		--	AND Legacy__c='drl-230595'
			ORDER BY Legacy_Id__c


			SELECT * FROM GDB_KADE_Final.dbo.tblDogRel WHERE drl_RelationID='230595'

			SELECT   DISTINCT     T1.drl_RelationID, T1.drl_DogID, T4.psn_PersonID
			FROM     GDB_KADE_Final.dbo.tblDogRel AS T1 
			INNER JOIN GDB_KADE_Final.dbo.tblPersonDog AS T2 ON T1.drl_RelationID = T2.pd_DogRelID 
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T2.pd_PersonRelID = T3.prl_RelationID 
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T3.prl_PersonID = T4.psn_PersonID
			WHERE T1.drl_RelationID='230595'
				ORDER BY t1.drl_RelationID

			
			SELECT T1.*
			FROM GDB_Final_migration.dbo.IMP_DOG_ROLE AS T1
			LEFT JOIN GDB_Final_migration.dbo.XTR_DOG_ROLE AS X1 ON X1.Legacy_Id__c = T1.Legacy_Id__c
			WHERE X1.Legacy_Id__c IS NULL 
			ORDER BY [Contact__r:Legacy_Id__c], [Dog__r:Legacy_Id__c], Legacy_Id__c



END 			 	