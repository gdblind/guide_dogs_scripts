SELECT distinct T5.Legacy_ID__c,T.idt_ItemDetailID, T2.iid_DepartmentCode, T2.iid_ItemID FROM  GDB_KADE_Final.dbo.tblGDBInvDispenseTrans AS T
INNER JOIN GDB_KADE_Final.dbo.trefGDBInvItemsDetail AS T2 ON T.idt_ItemDetailID=T2.iid_ItemDetailID
INNER JOIN GDB_Final_migration.dbo.stg_trefInvItems AS T3 ON t3.zrefItemDetailID =T.idt_ItemDetailID
--INNER JOIN GDB_Final_migration.dbo.stg_trefInvItems2 AS T4 ON t4.zrefItemDetailID = t.idt_ItemDetailID
INNER JOIN  GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems AS T5 ON t5.zrefItemDetailID = t.idt_ItemDetailID
ORDER BY T.idt_ItemDetailID


SELECT * FROM	GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems 
--WHERE GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems.zrefItemDetailID IN ('2892', '2891', '2908', '3208') 
WHERE  GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems.zrefItemID ='2886' --WHERE Legacy_ID__c ='Ii-2774_Iid-2774SR'
