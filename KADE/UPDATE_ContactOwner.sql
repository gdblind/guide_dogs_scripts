

	SELECT DISTINCT  t1.Legacy_ID__c, X1.ID AS OwnerID, T3.FieldRep  AS zrefFieldRep, t2.add_Country AS refCountry, T2.zrefCountryCode AS zrefCountryCode, T3.Country AS zrefCountryAbbr
	FROM GDB_Final_migration.dbo.IMP_CONTACT AS T1
	INNER JOIN GDB_Final_migration.dbo.stg_Address AS T2 ON T1.LEGACY_ID__C  = CAST(T2.ad_PersonID AS VARCHAR(50))
	INNER JOIN GDB_KADE_Final.dbo.trefZipCodes AS T3 ON T3.ZipCode = T2.add_ZipCode 
	INNER JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON x1.ADP__C = T3.FieldRep
	LEFT JOIN GDB_Kade_FINAL.DBO.tblPersonRel AS T4 ON T1.Legacy_ID__c=CAST(T4.prl_PersonID AS VARCHAR(50))
	WHERE T2.npsp__Default_Address__c = 'true' AND T4.prl_ClientInstanceID <>'0'


	--testing

	SELECT t1.ID, X1.ID AS OwnerID, T4.FieldRep, t3.add_Country, T3.zrefCountryCode, t4.Country
	FROM [GDB_Final_migration].dbo.xtr_contact AS T1
	INNER JOIN GDB_Final_migration.dbo.XTR_ACCOUNT AS T2 ON T1.ACCOUNTID=T2.ID
	INNER JOIN GDB_Final_migration.dbo.stg_Address AS T3 ON T2.LEGACY_ID__C  = CAST(T3.ad_PersonID AS VARCHAR(50))
	INNER JOIN GDB_KADE_Final.dbo.trefZipCodes AS T4 ON T4.ZipCode = T3.add_ZipCode 
	INNER JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON x1.ADP__C = T4.FieldRep
	WHERE T3.npsp__Default_Address__c = 'true' 


	SELECT T.psn_PersonID, t.psn_First, T.psn_Last, t.zrefAccountId
	FROM GDB_KADE_Final.dbo.tblPerson AS T
	WHERE T.psn_PersonID<> T.zrefAccountId