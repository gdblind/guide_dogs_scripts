
USE GDB_Final_migration
GO

	--ACCOUNT: UPDATE PRIMARY CONTACT

		SELECT  CAST(T1.zrefAccountId AS VARCHAR(50)) AS [Legacy_Id__c] 
			   ,T1.psn_PersonID  AS [npe01__One2OneContact__r:Legacy_Id__c]
			  
			   ,T1.psn_First 
			   ,T1.psn_Last
			   ,NULL AS [Primary_Leader__r:Legacy_ID__c]  
		INTO GDB_Final_migration.dbo.IMP_ACCOUNT_UPDATE --drop table GDB_Final_migration.dbo.IMP_ACCOUNT_UPDATE 
		FROM GDB_KADE_Final.DBO.tblPerson T1
	 	WHERE T1.zrefRecordType ='HHD'   AND (T1.zrefAccountID=T1.psn_PersonID) 
		

		UNION --FB-1808

		SELECT 
		'pcd-'+ CAST([pcd_ClubID] AS VARCHAR(20)) AS [Legacy_ID__c]
		,NULL AS [npe01__One2OneContact__r:Legacy_Id__c]
		,NULL AS psn_First 
		,NULL AS psn_Last
		,T.pcd_PersonID AS [Primary_Leader__r:Legacy_ID__c] 
		FROM GDB_KADE_Final.dbo.tblPuppyClubDetail AS T
		WHERE T.pcd_RelationCode='PRL'
		ORDER BY Legacy_ID__c

	--	SELECT * FROM GDB_Final_migration.dbo.IMP_ACCOUNT_UPDATE
