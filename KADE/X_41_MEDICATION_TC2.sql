USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVetMedication
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_MEDICATION
	SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblVetMedication  --TC2: 777,923
END 

BEGIN -- CREATE IMP 


--Step 1
					SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'VDM-'+CAST(T1.vdm_MedID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'vdm'+[vdm_MedID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,T2.vdl_DogID AS [Dog__r:Legacy_ID__c]
							--,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) AS  [Medicine__r:Legacy_Id__c]		-- trefGDBInventroyItems.ii_ItemID
							--,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) + '_Iid-'+CAST(T3.ii_ItemID  AS NVARCHAR(30))+ IIF(t4.iid_Campus='0','SR', 'OR') AS  [Inventory__r:Legacy_ID__c] -- Yes/Link trefGDBInvItemsDetail.iid_ItemDetailID
							,T1.vdm_Quantity  AS  Quantity__c		
							,T1.vdm_Directions  AS  Direction__c		
							,CAST(T1.vdm_CreatedDate AS DATE) AS CreatedDate
							,T1.vdm_MedID AS zrefMedID
							,T2.vdl_Facility
							,T1.vdm_MedCode AS zrefGDBInvItemID
							,T2.vdl_TreatmentDate AS Date_Prescribed__c
				 	INTO GDB_Final_migration.dbo.stg_MEDICATION  -- drop table GDB_Final_migration.dbo.stg_MEDICATION
					FROM GDB_KADE_Final.dbo.tblVetMedication AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdm_VetEntryID

					--COUNT TC2: 777,923
				
--Step2

				SELECT DISTINCT  
					T1.OwnerId
					,T1.Legacy_ID__c
					,T1.[Vet_Record__r:Legacy_Id__c]
					,T1.[Dog__r:Legacy_ID__c]
					,t1.Quantity__c
					,t1.Direction__c
					,T1.CreatedDate
					,T1.zrefMedID
					,T1.zrefGDBInvItemID
					,T2.Legacy_ID__c AS [Inventory__r:Legacy_ID__c]
					,T2.[Name]
					,T1.Date_Prescribed__c		
				 	INTO GDB_Final_migration.dbo.stg_MEDICATION2  -- drop table GDB_Final_migration.dbo.stg_MEDICATION2
					FROM GDB_Final_migration.dbo.stg_MEDICATION  AS T1
					INNER JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS T2 ON T1.zrefGDBInvItemID = T2.zrefItemID AND t1.vdl_Facility=t2.zrefCampus
					LEFT JOIN GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry AS T3 ON T3.ItemDetailID=T2.zrefItemDetailID
					--WHERE T3.ItemDetailID IS NULL 

					--COUNT  454,464


			SELECT DISTINCT  
					T1.OwnerId
					,T1.Legacy_ID__c
					,T1.[Vet_Record__r:Legacy_Id__c]
					,T1.[Dog__r:Legacy_ID__c]
					,t1.Quantity__c
					,t1.Direction__c
					,T1.CreatedDate
					,T1.zrefMedID
					,T1.zrefGDBInvItemID
					,T2.Legacy_ID__c AS [Inventory__r:Legacy_ID__c]
					,T2.[Name]
							
				 	INTO GDB_Final_migration.dbo.stg_MEDICATION3  -- drop table GDB_Final_migration.dbo.IMP_MEDICATION
					FROM GDB_Final_migration.dbo.stg_MEDICATION  AS T1
					INNER JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS T2 ON T1.zrefGDBInvItemID = T2.zrefItemID --AND t1.vdl_Facility=t2.zrefCampus
					LEFT JOIN GDB_Final_migration.dbo.stg_MEDICATION2 AS T3 ON T1.Legacy_ID__c=t3.Legacy_ID__c
					WHERE T3.Legacy_ID__c IS NULL AND t2.zrefCampus='0'

					UNION
					SELECT 
							T1.OwnerId
							,T1.Legacy_ID__c
							,T1.[Vet_Record__r:Legacy_Id__c]
							,T1.[Dog__r:Legacy_ID__c]
							,t1.Quantity__c
							,t1.Direction__c
							,T1.CreatedDate
							,T1.zrefMedID
							,T1.zrefGDBInvItemID
							,T1.Legacy_ID__c AS [Inventory__r:Legacy_ID__c]
							,T1.[Name]

					FROM GDB_Final_migration.dbo.stg_MEDICATION2  AS T1
					







END 
-- CHECK DUPS
			SELECT * 
			FROM GDB_Final_migration.dbo.IMP_MEDICATION
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_MEDICATION
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 


			SELECT * 
			FROM GDB_Final_migration.dbo.IMP_MEDICATION
		--	ORDER BY [Inventory__r:Legacy_ID__c] DESC
			WHERE [Inventory__r:Legacy_ID__c] LIKE '%_%'


					
					                   
					