USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGuideStatus
			WHERE   SF_Object LIKE '%fi%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 

/*
SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%fie%'
0121L000001UQD2QAO	Grad Report			Field_Report__c
0123D0000004axCQAQ	Guide Evaluation	Field_Report__c
*/


BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_FIELD_REPORT

END 

BEGIN -- CREATE IMP 

					SELECT   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblInstr].[db_InstrID]
 							,[Name] = 'Foster Care'
							,'GDS-'+CAST(T1.gds_GuideStatusID AS VARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'GDS-'+[gds_GuideStatusID]	
							,'0121L000001UQEPQA4' AS   RecordTypeID	-- FosterCare
						  	,CAST(T1.gds_CreatedDate AS DATE) AS CreatedDate
					 
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
 							,NULL AS ContactLink
							,CAST(T1.gds_Date AS DATE) AS  Date__c	
						 	,CAST(T1.gds_TrainNotes AS VARCHAR(MAX)) AS  Training_Notes__c	
							/*,NULL AS  Issue_Date__c		--fb-00046
							,NULL AS  Float__c		
							,NULL AS  Reason__c		
							,NULL AS  Graduation_Notes__c		
							,NULL AS  PRP_Name__c		
							,NULL AS  Name__c		
							,NULL AS  PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  [Instructor__r:Legacy_Id__c]		 		 
							,NULL AS  Supervisor_Summary__c		
							,NULL AS  Disposition__c		
							,NULL AS  Director_of_training__c	*/				
							,NULL AS Report_By__c --		FB-00847
							,NULL AS Visit_Type__c
							,'Foster Care' AS Visit_Category__c
							,NULL AS Apprentice_Training__c
							,NULL AS Visit_Summary__c
							,NULL AS Recommendation__c
							,NULL AS Recommendation_Detail__c
							,NULL AS Update_Information__c
							,NULL AS Overall_satisfaction_with_dog__c
							,NULL AS Overall_satisfaction_with_GW_mechanics__c
							,NULL AS Overall_Satisfaction_Behavior__c
							,NULL AS Overall_Satisfaction_with_House_Behavior__c
							,NULL AS Overall_Satisfaction_with_Relieving__c
							,NULL AS Brand_of_Food_and_formula__c
							,NULL AS Ration_of_dog_food__c
							,NULL AS Current_weight__c
							,NULL AS Current_Health_Concerns__c
							,NULL AS Ingested_any_items_in_last_year__c
							,NULL AS Food_Reward_Use_on_Routes__c
							,NULL AS Management_Techniques_Used__c
							,NULL AS House_Behavior_Challenges__c
							,NULL AS Environmental_Sensitivities__c
							,NULL AS Vison_Status__c
							,NULL AS Influencing_Line_Negatively__c
							,NULL AS Inluencing_Clearances_Negatively__c
							,NULL AS Decreasing_Dog_s_Effectiveness__c
							,NULL AS Neutral_to_Positive_Influence__c
							,NULL AS Overall_Health__c
							,NULL AS Hearing__c
							,NULL AS Emotional__c
							,NULL AS Learnig_Skills_Problem_Solving__c
							,NULL AS Financial_Stability__c
							,NULL AS Employment__c
							,NULL AS Home_Condition__c
							,NULL AS General_O_M_Skills__c
							,NULL AS Rapport_with_Dog__c
							,NULL AS Ability_to_Manage_Dog__c
							,NULL AS Reinforcement_Sufficient_for_Dog__c
							,NULL AS Observed_Food_Reward__c
							,NULL AS Dog_s_House_Behavior_Approprate__c
							,NULL AS General_Condition_of_Dog__c
							,NULL AS BCS__c
							,NULL AS Relieving_Issues__c
							,NULL AS Observe_Perform_Guide_Work__c
							,NULL AS R__Primary_Reinforcement__c
							,NULL AS R__2ndry_Reinforcement__c
							,NULL AS P__Aversive__c
							,NULL AS P__Timeout__c
							,NULL AS Focus_Around_Distractions__c
							,NULL AS Guide_Work_Mechanics__c
							,NULL AS Observed_Pace__c
							,NULL AS Observed_Lead__c
							,NULL AS Guide_Work_Behavior_Overall__c
							,NULL AS Appropriate_Pace_for_Client__c
							,NULL AS Appropriate_Lead_for_Client__c
							,NULL AS Stress_Release_Opportunities__c
							,NULL AS Team_Compatible__c
							,NULL AS Home_Interview_Completed__c
							,NULL AS Comply_with_Recommendations__c
							,NULL AS Time_Spent_with_Client__c
							,NULL AS Conclusion_Recommendation__c
 							,NULL AS Narrative__c		--FB-00847
							,NULL AS Narrative_2__c		--FB-00847
							,NULL AS Survey_Emailed__c		
							,T1.gds_GuideStatusID AS zrefId
							,'tblGuideStatus' AS zrefSrc
				 INTO GDB_Final_migration.DBO.IMP_FIELD_REPORT
					FROM GDB_KADE_Final.dbo.tblGuideStatus AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.gds_DogID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.gds_InstrID
					WHERE T1.gds_Type='FC'
			/*	UNION 

					SELECT  CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId   
 							,[Name] = CASE WHEN T1.ev_EvalType IS NOT NULL THEN 'Evaluation ' + T1.ev_EvalType WHEN t1.ev_EvalType IS NULL THEN 'Evaluation' END 
							,'EV-'+CAST(T1.ev_Count AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "EV-" with ev_Count	
							,'0123D0000004axCQAQ' AS   RecordTypeID	-- Guide Evaluation	
							,CAST(T1.ev_CreatedDate AS DATE) AS CreatedDate
 							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,NULL AS ContactLink
							,CAST(T1.ev_Date  AS DATE) AS  Date__c
							,NULL AS Training_Notes__c
 							,CAST(T1.ev_IssueDate AS DATE) AS  Issue_Date__c		
							,T1.ev_Float  AS  Float__c		
	 						,CAST(T1.ev_Reason   AS NVARCHAR(MAX))   AS  Reason__c		
							,CAST(T1.ev_Gradnotes  AS NVARCHAR(MAX)) AS  Graduation_Notes__c		
							,T1.ev_PRPName  AS  PRP_Name__c		
							,T1.ev_Name  AS  Name__c		
							,CAST(T1.ev_PRPDate AS DATE)  AS  PRP_Date__c		
							,CAST(T1.ev_PRPnotes  AS NVARCHAR(MAX))   AS  PRP_Notes__c		
							,CAST(T1.ev_WorkOut  AS NVARCHAR(MAX))  AS  Workout__c		
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))AS  [Instructor__r:Legacy_Id__c]		 		-- Link [tblStaff].[FileNum]
								
							,CAST(T1.ev_SupervisorSummary AS NVARCHAR(MAX)) AS  Supervisor_Summary__c		
							,CASE  T1.ev_Disp
							    WHEN '1' THEN 'Return to Grad'  
								WHEN '2' THEN 'Reissue'  
								WHEN '3' THEN 'Retire'  
								END AS  Disposition__c		
							,CAST(T1.ev_DOT AS DATE) AS  Director_of_training__c		
							
							,NULL AS Int_Position__c
							,T1.ev_EvalType   AS Visit_Type__c
							,'Evaluation' AS Visit_Category__c
							,NULL AS Apprentice_Training__c
							,NULL AS Visit_Summary__c
							,NULL AS Recommendation__c
							,NULL AS Update_Information__c
							,NULL AS Overall_satisfaction_with_dog__c
							,NULL AS Overall_satisfaction_with_GW_mechanics__c
							,NULL AS Overall_Satisfaction_Behavior__c
							,NULL AS Overall_Satisfaction_with_House_Behavior__c
							,NULL AS Overall_Satisfaction_with_Relieving__c
							,NULL AS Brand_of_Food_and_formula__c
							,NULL AS Ration_of_dog_food__c
							,NULL AS Current_weight__c
							,NULL AS Current_Health_Concerns__c
							,NULL AS Ingested_any_items_in_last_year__c
							,NULL AS Food_Reward_Use_on_Routes__c
							,NULL AS Management_Techniques_Used__c
							,NULL AS House_Behavior_Challenges__c
							,NULL AS Environmental_Sensitivities__c
							,NULL AS Vison_Status__c
							,NULL AS Influencing_Line_Negatively__c
							,NULL AS Inluencing_Clearances_Negatively__c
							,NULL AS Decreasing_Dog_s_Effectiveness__c
							,NULL AS Neutral_to_Positive_Influence__c
							,NULL AS Overall_Health__c
							,NULL AS Hearing__c
							,NULL AS Emotional__c
							,NULL AS Learnig_Skills_Problem_Solving__c
							,NULL AS Financial_Stability__c
							,NULL AS Employment__c
							,NULL AS Home_Condition__c
							,NULL AS General_O_M_Skills__c
							,NULL AS Rapport_with_Dog__c
							,NULL AS Ability_to_Manage_Dog__c
							,NULL AS Reinforcement_Sufficient_for_Dog__c
							,NULL AS Observed_Food_Reward__c
							,NULL AS Dog_s_House_Behavior_Approprate__c
							,NULL AS General_Condition_of_Dog__c
							,NULL AS BCS__c
							,NULL AS Relieving_Issues__c
							,NULL AS Observe_Perform_Guide_Work__c
							,NULL AS R__Primary_Reinforcement__c
							,NULL AS R__2ndry_Reinforcement__c
							,NULL AS P__Aversive__c
							,NULL AS P__Timeout__c
							,NULL AS Focus_Around_Distractions__c
							,NULL AS Guide_Work_Mechanics__c
							,NULL AS Observed_Pace__c
							,NULL AS Observed_Lead__c
							,NULL AS Guide_Work_Behavior_Overall__c
							,NULL AS Appropriate_Pace_for_Client__c
							,NULL AS Appropriate_Lead_for_Client__c
							,NULL AS Stress_Release_Opportunities__c
							,NULL AS Team_Compatible__c
							,NULL AS Home_Interview_Completed__c
							,NULL AS Comply_with_Recommendations__c
							,NULL AS Time_Spent_with_Client__c
							,NULL AS Conclusion_Recommendation__c
 							,NULL AS Conclusion_Narrative__c
							,NULL AS Conclusion_Narrative_2__c
							,NULL AS Survey_Emailed__c	
							 ,T1.ev_Count AS zrefId
							,'tbl_GuideEval' AS zrefSrc

					FROM GDB_KADE_Final.dbo.tbl_GuideEval AS T1
					LEFT JOIN GDB_Final_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.ev_FileNum
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.ev_DogId
					LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.ev_Instructor*/

				UNION 

					SELECT   IIF(T1.gfu_ByWhom='' OR T1.gfu_ByWhom IS NULL, GDB_Final_migration.[dbo].[fnc_OwnerId](),X1.ID) AS OwnerID
							,[Name] = T5.Visit_Category + ' ' + T5.VisitType
							,'GFU-'+CAST(T1.gfu_ID  AS VARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Gfu-'+[gfu_ID]	
							,'0121L000001UQD2QAO' AS RecordTypeID-- Graduate Field Report
							,CAST(T1.gfu_CreatedDate AS DATE) AS CreatedDate
							,NULL AS  [Dog__r:Legacy_Id__c]
  							,t3.prl_PersonID AS ContactLink	-- Link to Contact	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,CAST(T1.gfu_Date AS DATE) AS  Date__c		
								,NULL AS  Training_Notes__c	
							/*	,NULL AS  Issue_Date__c		--fb-00046
								,NULL AS  Float__c		
								,NULL AS  Evaluation_Type__c		
								,NULL AS  Reason__c		
								,NULL AS  Graduation_Notes__c		
								,NULL AS  PRP_Name__c		
								,NULL AS  Name__c		
								,NULL AS  PRP_Date__c		
								,NULL AS  PRP_Notes__c		
								,NULL AS  Workout__c		
								,NULL AS  [Instructor__r:Legacy_Id__c]		 		 
								,NULL AS  Supervisor_Summary__c		
								,NULL AS  Disposition__c		
								,NULL AS  Director_of_training__c	*/
							,T1.gfu_IntPosition  AS  Report_By__c		
							,T5.VisitType  AS  Visit_Type__c			-- migrate value from [VisitType]	-- Ref [trefAGSVisitTypes].[VisitTypeID]
							,T5.Visit_Category AS  Visit_Category__c	-- migrate value from [Visit_Category]	-- Ref [trefAGSVisitTypes].[VisitTypeID]
							,T1.gfu_ApprTraining  AS  Apprentice_Training__c		
							,CAST(T1.gfu_Comments  AS VARCHAR(MAX)) AS  Visit_Summary__c	
							,CASE  --FB-00427
								WHEN T1.gfu_DogRecommend LIKE 'return%' THEN 'Return' 
								WHEN T1.gfu_DogRecommend LIKE 'Keep in Service%' THEN 'Keep in Service'
								WHEN T1.gfu_DogRecommend LIKE 'retire%' THEN 'Retire'
								WHEN T1.gfu_DogRecommend LIKE '%follow-up%' THEN 'Follow-up'
								ELSE 'Other' END AS Recommendation__c	
							,T1.gfu_DogRecommend  AS  Recommendation_Detail__c		
							,T1.gfu_UpdateInfo  AS  Update_Information__c		
							,CASE T1.gfu_CS_Dog  
								WHEN 1 THEN 'Lowest Satisfaction' 
								WHEN 2 THEN 'Marginal'
								WHEN 3 THEN 'Adequate'
								WHEN 4 THEN 'Very Good'
								WHEN 5 THEN 'Highest Satisfaction'
								WHEN 0 THEN 'Not Applicable' END
								AS  Overall_satisfaction_with_dog__c		
							,CASE T1.gfu_CS_Mechanics 
								WHEN 1 THEN 'Lowest Satisfaction' 
								WHEN 2 THEN 'Marginal'
								WHEN 3 THEN 'Adequate'
								WHEN 4 THEN 'Very Good'
								WHEN 5 THEN 'Highest Satisfaction'
								WHEN 0 THEN 'Not Applicable' END AS  Overall_satisfaction_with_GW_mechanics__c		
							,CASE T1.gfu_CS_Behavior 
								WHEN 1 THEN 'Lowest Satisfaction' 
								WHEN 2 THEN 'Marginal'
								WHEN 3 THEN 'Adequate'
								WHEN 4 THEN 'Very Good'
								WHEN 5 THEN 'Highest Satisfaction'
								WHEN 0 THEN 'Not Applicable' END
								 AS  Overall_Satisfaction_Behavior__c		
							,CASE T1.gfu_CS_HomeBehavior
								WHEN 1 THEN 'Lowest Satisfaction' 
								WHEN 2 THEN 'Marginal'
								WHEN 3 THEN 'Adequate'
								WHEN 4 THEN 'Very Good'
								WHEN 5 THEN 'Highest Satisfaction'
								WHEN 0 THEN 'Not Applicable' END
								 AS  Overall_Satisfaction_with_House_Behavior__c		
							,CASE T1.gfu_CS_Relieving 
								WHEN 1 THEN 'Lowest Satisfaction' 
								WHEN 2 THEN 'Marginal'
								WHEN 3 THEN 'Adequate'
								WHEN 4 THEN 'Very Good'
								WHEN 5 THEN 'Highest Satisfaction'
								WHEN 0 THEN 'Not Applicable' END
							 AS  Overall_Satisfaction_with_Relieving__c		
							,T1.gfu_IG_FoodType  AS  Brand_of_Food_and_formula__c		
							,T1.gfu_IG_FoodRation  AS  Ration_of_dog_food__c		
							,T1.gfu_IG_DogWeight  AS  Current_weight__c		
							,T1.gfu_IG_HealthConcerns  AS  Current_Health_Concerns__c		
							,T1.gfu_IG_IngestedItems  AS  Ingested_any_items_in_last_year__c		
							,T1.gfu_IG_FoodRewardUse  AS  Food_Reward_Use_on_Routes__c		
							,T1.zrefManagemetnTechUsed  AS  Management_Techniques_Used__c		
							,T1.zrefHouseBehaviorChallenges  AS  House_Behavior_Challenges__c		
							,T1.zrefEnvironmentalSensitivites  AS  Environmental_Sensitivities__c		
							,T1.gfu_OC_VisionStatus  AS  Vison_Status__c		
							,T1.gfu_OC_VWD_LineNeg  AS  Influencing_Line_Negatively__c		
							,T1.gfu_OC_VWD_ClearanceNeg  AS  Inluencing_Clearances_Negatively__c		
							,T1.gfu_OC_VWD_DecreasingEffect  AS  Decreasing_Dog_s_Effectiveness__c		
							,T1.gfu_OC_VWD_PositiveInfluence  AS  Neutral_to_Positive_Influence__c		
							,T1.gfu_OC_OverallHealth  AS  Overall_Health__c		
							,T1.gfu_OC_Hearing  AS  Hearing__c		
							,T1.gfu_OC_Emotional  AS  Emotional__c		
							,T1.gfu_OC_LearningSkills  AS  Learnig_Skills_Problem_Solving__c		
							,T1.gfu_OC_FinancialStubility  AS  Financial_Stability__c		
							,T1.gfu_OC_Employment  AS  Employment__c		
							,T1.gfu_OC_HomeCondition  AS  Home_Condition__c		
							,T1.gfu_OC_MobilitySkills  AS  General_O_M_Skills__c		
							,T1.gfu_OC_RapportWDog  AS  Rapport_with_Dog__c		
							,T1.gfu_OC_ManageDog  AS  Ability_to_Manage_Dog__c		
							,T1.gfu_OC_Reinforcement  AS  Reinforcement_Sufficient_for_Dog__c		
							,T1.gfu_OC_ObservedFoodReward  AS  Observed_Food_Reward__c		
							,T1.gfu_OD_HouseBehavior  AS  Dog_s_House_Behavior_Approprate__c		
							,T1.gfu_OD_Condition  AS  General_Condition_of_Dog__c		
							,BCS__c = IIF(T1.gfu_OD_BCS = '0', 'N/A', CONVERT(VARCHAR(10),T1.gfu_OD_BCS)) 	
							,T1.gfu_OD_RelievingIssues  AS  Relieving_Issues__c		
							,T1.gfu_OD_PerformGuideWork  AS  Observe_Perform_Guide_Work__c		
							,T1.gfu_OD_UMT_RPrim  AS  R__Primary_Reinforcement__c		
							,T1.gfu_OD_UMT_R2nd  AS  R__2ndry_Reinforcement__c		
							,T1.gfu_OD_UMT_PAverage  AS  P__Aversive__c		
							,T1.gfu_OD_UMT_PTimeout  AS  P__Timeout__c		
							,T1.gfu_OD_DistractionFocus  AS  Focus_Around_Distractions__c		
							,T1.gfu_OD_GuideMachanics  AS  Guide_Work_Mechanics__c		
							,T1.gfu_OD_ObservedPace  AS  Observed_Pace__c		
							,T1.gfu_OD_ObservedLead  AS  Observed_Lead__c		
							,T1.gfu_IP_BehaviorOverall  AS  Guide_Work_Behavior_Overall__c		
							,T1.gfu_IP_PaceForClient  AS  Appropriate_Pace_for_Client__c		
							,T1.gfu_IP_LeadForClient  AS  Appropriate_Lead_for_Client__c		
							,T1.gfu_IP_StressRelief  AS  Stress_Release_Opportunities__c		
							,T1.gfu_IP_TeamCompatible  AS  Team_Compatible__c		
							,T1.gfu_IP_HomeInterview  AS  Home_Interview_Completed__c		
							,T1.gfu_IP_ComplyWRecom  AS  Comply_with_Recommendations__c		
							,T1.gfu_IP_TimeWithClient  AS  Time_Spent_with_Client__c		
							,T1.gfu_Conclusions_Recomm  AS  Conclusion_Recommendation__c		
							,CAST(T1.gfu_Conclusions_Narrative AS VARCHAR(MAX)) AS  Narrative__c		
							,CAST(T1.gfu_Conclusions_Narrative2 AS VARCHAR(MAX))   AS  Narrative_2__c		
							,T2.hve_Email AS Survey_Emailed__c	

							,T1.gfu_ID AS zrefId
							,'tblGradFieldReport' AS zrefSrc
					FROM GDB_KADE_Final.dbo.tblGradFieldReport AS T1 
					LEFT JOIN GDB_KADE_Final.dbo.tblGFRSurveyEmail AS T2 ON T2.hve_GFRID=T1.gfu_ID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.gfu_ByWhom
				    LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.gfu_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 	LEFT JOIN GDB_KADE_Final.dbo.trefAGSVisitTypes AS T5 ON T5.VisitTypeID=T1.gfu_VisitType
					--WHERE t3.prl_PersonID='3203'


				 
END ---tc1: 58439  TC2: 54472  final 56343

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT 
	GROUP BY zrefSrc


	--update for multi-select picklist
	SELECT * FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT ORDER BY [Dog__r:Legacy_Id__c]
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT

	
	UPDATE GDB_FINAL_Migration.dbo.IMP_FIELD_REPORT
	SET Narrative__c=REPLACE(Narrative__c,'"',''') 
	WHERE Narrative__c like '%"%'

	UPDATE GDB_Final_migration.dbo.IMP_FIELD_REPORT SET Body=REPLACE(Body,'"','&quot;') where Body LIKE '%"%'
	 

END 