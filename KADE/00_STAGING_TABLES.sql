
 

 -- Move charts that have no updates 
	--SELECT * INTO GDB_Final_migration.[dbo].[CHART_Contact_Prefix] FROM [GDB_Final_migration].[dbo].[CHART_Contact_Prefix]
		--GO

	 SELECT * INTO GDB_Final_migration.[dbo].[CHART_DOG] FROM GDB_TC2_migration.[dbo].CHART_DOG
		GO

		SELECT * INTO GDB_Final_migration.[dbo].[CHART_DogComments_Category] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_DogComments_Category]
		GO

		--SELECT * INTO GDB_Final_migration.[dbo].[CHART_DogRel_RelCode] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_DogRel_RelCode]
		--GO
		--SELECT * INTO GDB_Final_migration.[dbo].[CHART_EquipFood] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_EquipFood]
		--GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_Person_Prefix] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_Person_Prefix]
		GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_PersonNote_Category] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_PersonNote_Category]
		GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_PhoneNote_NatureofCall] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_PhoneNote_NatureofCall]
		GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_PhoneType] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_PhoneType]
		GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_SupportCenter_NaOfCall] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_SupportCenter_NaOfCall]
		GO
	--	SELECT * INTO GDB_Final_migration.[dbo].[CHART_trefVet] FROM [GDB_TC2_MAPS_Delivery].[dbo].[CHART_trefVet]
	--	GO
		SELECT * INTO GDB_Final_migration.[dbo].[CHART_CFR] FROM [GDB_TC2_migration].[dbo].[CHART_CFR]
		GO


--TBLPERSON and related tbl. 
	--ACCOUNTS/CONTACTS
	--UPDATE: add zrefRecordType field
	
	BEGIN 
		ALTER TABLE GDB_KADE_Final.dbo.tblPerson
		ADD zrefRecordType NVARCHAR(3)

 		--check trans rule
		SELECT psn_PersonID, psn_First, psn_Last, zrefRecordType
		,CASE WHEN psn_First IS NULL AND psn_Last NOT LIKE '%family%' THEN 'ORG' ELSE 'HHD' END AS test
		FROM GDB_KADE_Final.dbo.tblPerson
		ORDER BY zrefRecordType 

			--update
			UPDATE GDB_KADE_Final.dbo.tblPerson
			SET zrefRecordType ='ORG'
			WHERE psn_First IS NULL AND psn_Last NOT LIKE '%family%' 

			UPDATE GDB_KADE_Final.dbo.tblPerson
			SET zrefRecordType ='HHD'
			WHERE zrefRecordType IS NULL 
	END 

	BEGIN -- ACCOUNTS. UPDATE add zrefFirstName and zrefSecFirstName  fields for prim and sec contacts when firstname like ' & ' or ' and '

		ALTER TABLE GDB_KADE_Final.dbo.tblPerson
		ADD zrefFirstName NVARCHAR(50)

		ALTER TABLE GDB_KADE_Final.dbo.tblPerson
		ADD zrefSecFirstName NVARCHAR(50)

			--UPDATE GDB_KADE_Final.dbo.tblPerson 
			--SET zrefFirstName =NULL, zrefSecFirstName=NULL 

			--&..  first and sec.
			UPDATE GDB_KADE_Final.dbo.tblPerson 
			SET zrefFirstName = SUBSTRING(psn_First, 1, CHARINDEX('&', psn_First)-2 ) 
			WHERE psn_First LIKE  '% & %' 
		
			UPDATE GDB_KADE_Final.dbo.tblPerson 
			SET zrefSecFirstName = SUBSTRING(psn_First, CHARINDEX('&', psn_First)+2, 255)
			WHERE psn_First LIKE  '% & %' 
		
			--and... first and sec.
			UPDATE GDB_KADE_Final.dbo.tblPerson 
			SET zrefFirstName = SUBSTRING(psn_First, 1, CHARINDEX(' and ', psn_First)-1)
			WHERE psn_First LIKE  '% and %' 
		
			UPDATE GDB_KADE_Final.dbo.tblPerson 
			SET zrefSecFirstName = SUBSTRING(psn_First, CHARINDEX(' and ', psn_First)+5, 255) 
			WHERE psn_First LIKE  '% and %' 

				SELECT psn_First, psn_Last, zreffirstname, zrefsecfirstname 
				FROM GDB_KADE_Final.dbo.tblPerson
				WHERE psn_First LIKE  '% & %' OR psn_First LIKE '% and %'
	END 
		  
	BEGIN--ACCOUNTS. UPDATE: add zrefSalutation
		ALTER TABLE GDB_KADE_Final.DBO.tblPerson
		ADD zrefSalutation NVARCHAR(20)

			SELECT T1.psn_Prefix, T2.[Convert], T2.[New Value], T1.zrefSalutation
			FROM GDB_KADE_Final.DBO.tblPerson AS T1
			INNER JOIN GDB_Final_migration.DBO.CHART_Person_Prefix AS T2 ON T1.psn_Prefix=T2.psn_Prefix
			WHERE T2.[New Value] IS NOT NULL 
			ORDER BY T1.psn_Prefix

			UPDATE GDB_KADE_Final.DBO.tblPerson
			SET zrefSalutation = T2.[New Value]
			FROM GDB_KADE_Final.DBO.tblPerson AS T1
			INNER JOIN GDB_Final_migration.DBO.CHART_Person_Prefix AS T2 ON T1.psn_Prefix=T2.psn_Prefix
			WHERE T2.[New Value] IS NOT NULL 
	END 
	
	BEGIN-- STG (STAGING TABLE): stg_tblPersonGroup: table that has all GroupId that exist in the tblPerson. 
			SELECT  T1.* 
			INTO GDB_Final_migration.dbo.stg_tblPersonGroup
			FROM GDB_KADE_Final.dbo.tblPersonGroup AS T1
			LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T2 ON T1.pg_GroupID=T2.psn_PersonID
			WHERE t2.psn_PersonID IS NOT NULL  
			ORDER BY t1.pg_GroupID, t1.pg_PersonID
	END --10,823  Final 11,767
		
	BEGIN--ACCOUNTS. UPDATE: add zrefAccountID -- this is to indicate the legacy ID of the household account or organization account.

		ALTER TABLE GDB_KADE_Final.dbo.tblPerson
		ADD zrefAccountId INT
      
		UPDATE GDB_KADE_Final.DBO.tblPerson
		SET zrefAccountId = CASE WHEN T2.pg_GroupID IS NULL THEN psn_PersonID ELSE t2.pg_GroupID END
		FROM GDB_KADE_Final.dbo.tblPerson AS T1
		LEFT JOIN GDB_Final_migration.DBO.stg_tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID  
		--79074 TC2 80653  Final 83,359

			--these are the issue record where there is an "&" or "and" on the first name AND also a groupId. 
			--for the migration we used the groupId and ignored the parsing translation rule. 
			--GDB needs to corret for the TC2. 
				UPDATE GDB_KADE_Final.dbo.tblPerson
				SET zrefFirstName = NULL,   zrefSecFirstName =NULL 
	  			FROM GDB_KADE_Final.dbo.tblPerson AS T1
				LEFT JOIN GDB_KADE_Final.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
		 			 WHERE  ( T2.pg_GroupID='5677'
						OR T2.pg_GroupID='14760'
						OR T2.pg_GroupID='15256'
						OR T2.pg_GroupID='35035'
						OR T2.pg_GroupID='67878'
						OR T2.pg_GroupID='79709') AND t1.zrefFirstName IS NOT NULL 
			--check1
				SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, 
				CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
				,T2.* 
				FROM GDB_KADE_Final.dbo.tblPerson AS T1
				LEFT JOIN GDB_KADE_Final.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
				ORDER BY T2.pg_GroupID, T1.psn_PersonID
			--check2 
				SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, 
				CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
				,T2.* 
				FROM GDB_KADE_Final.dbo.tblPerson AS T1
				LEFT JOIN GDB_KADE_Final.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
				WHERE T2.pg_GroupID='22463' OR T2.pg_GroupID='58' OR T2.pg_GroupID='62520'	  
				OR T2.pg_GroupID='14760' OR T2.pg_GroupID='15256'
	END 			

	BEGIN-- ACCOUNTS.  STG (STAGING TABLE): stg_tblPersonGroup_dnc: table that has all GroupId (PersonID) that DO NOT exist in the tblPerson. 
			SELECT  DISTINCT T1.pg_GroupID 
	 		INTO GDB_Final_migration.dbo.stg_tblPersonGroup_dnc
			FROM GDB_KADE_Final.dbo.tblPersonGroup AS T1
			LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T2 ON T1.pg_GroupID=T2.psn_PersonID
			WHERE t2.psn_PersonID IS NULL  
			ORDER BY t1.pg_GroupID 
	END  --68 GroupId TC2 68 final 67
 

	BEGIN--ACCOUNTS.  STG (STAGING TABLE): stg_tblAccounts: unique account records to create household and org accts. 
 			SELECT DISTINCT  zrefAccountId  , zrefRecordType
		 	INTO GDB_Final_migration.dbo.stg_tblAccount
			FROM GDB_KADE_Final.dbo.tblPerson T1
  			ORDER BY  zrefRecordType, zrefAccountId
	END   -- 72513 TC2 73826  final 76,215


	BEGIN--PHONES/EMAILS
			--delet stg tbls
			DROP TABLE [GDB_Final_migration].dbo.stg_Phone
			DROP TABLE [GDB_Final_migration].dbo.stg_Phone_seq2
			DROP TABLE [GDB_Final_migration].dbo.stg_Phone_acct
			DROP TABLE [GDB_Final_migration].dbo.stg_Phone_cont
 			DROP TABLE [GDB_Final_migration].dbo.stg_Phone_acct_final
			DROP TABLE [GDB_Final_migration].dbo.stg_Phone_cont_final
			DROP TABLE GDB_Final_migration.dbo.stg_phone_acct_agency
        

	 
			--create master stg tbl
			SELECT DISTINCT 
			T1.pdl_PersonID, T1.pdl_PrimaryContact, T1.pdl_PhoneID, T2.phn_PhoneID
			,T2.phn_Number, T2.phn_FormatedNum
			,CASE WHEN T3.SF_Field_API LIKE '%email%' THEN T2.phn_Number WHEN T3.SF_Field_API NOT LIKE '%email%' THEN t2.phn_FormatedNum END AS SF_Field_Value
			,T3.SF_Object, T3.SF_Field_API, T3.Custom, T3.Translation_Rules
			,T.zrefRecordType, T.zrefAccountID 
			,ROW_NUMBER() OVER(PARTITION BY t1.pdl_PersonID, t3.SF_Field_API ORDER BY t1.pdl_PersonID, t3.SF_Field_API) AS seq
	 		INTO GDB_Final_migration.dbo.stg_Phone
			FROM GDB_KADE_Final.dbo.tblPhoneDetail AS T1
			INNER JOIN GDB_KADE_Final.DBO.tblPhone AS T2 ON T2.phn_PhoneID = T1.pdl_PhoneID
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T ON T1.pdl_PersonID = T.psn_PersonID	
			INNER  JOIN GDB_Final_migration.DBO.CHART_PhoneType AS T3 ON T1.pdl_PhoneCode=T3.PhoneCode
			WHERE T3.[Convert]='Yes'  
			 --208535  TC2 212932  final 220,022

			--check nulls
				SELECT * FROM GDB_Final_migration.dbo.stg_Phone 
				WHERE SF_Field_Value IS NULL ORDER BY SF_Object, SF_Field_API

				UPDATE GDB_Final_migration.dbo.stg_Phone 
				SET SF_Field_Value=phn_Number 
				WHERE SF_Field_Value IS NULL
				--2472 TC2 2524

			--create list of record to DNC.
				SELECT * 
				INTO GDB_Final_migration.dbo.stg_Phone_seq2
				FROM GDB_Final_migration.dbo.stg_Phone
				WHERE seq>1
					--107 TC2 106
	  
			--delete record to DNC from master stg tbl. 
				DELETE GDB_Final_migration.dbo.stg_Phone WHERE seq>1
				--107 TC2 106

			--check duplicates
			SELECT * FROM GDB_Final_migration.dbo.stg_Phone
			WHERE CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API IN 
			(SELECT CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API FROM GDB_Final_migration.dbo.stg_Phone
			GROUP BY CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API HAVING count(*) >1)
			ORDER BY pdl_PersonID 
				--0 TC2 0  final 0
		
			--create stg phone acct
			SELECT * 
	 		INTO GDB_Final_migration.dbo.stg_Phone_acct
			FROM GDB_Final_migration.dbo.stg_Phone
			WHERE SF_Object='ACCOUNT' 
				AND phn_PhoneID NOT IN (SELECT phn_PhoneID FROM GDB_Final_migration.dbo.stg_Phone WHERE Translation_Rules='Migrate when Organization Account.' AND zrefRecordType='HHD')
			ORDER BY Translation_Rules, zrefRecordType
			--675 TC2 674
				--create final table
		 		EXEC HC_PivotWizard_p		'pdl_PersonID',	--fields to include as unique identifier/normally it is just a unique ID field.
											'SF_Field_API',											--column that stores all new phone types
											'SF_Field_Value',					--phone numbers
											'[GDB_Final_migration].dbo.stg_Phone_acct_final',			--INTO..     
											'[GDB_Final_migration].dbo.stg_Phone_acct',				--FROM..
											'SF_Field_Value is not null'							--WHERE..
				--672 TC2 671
				--check dupes
				SELECT * FROM [GDB_Final_migration].dbo.stg_Phone_acct_final
				WHERE pdl_PersonID IN (SELECT pdl_PersonID FROM  [GDB_Final_migration].dbo.stg_Phone_acct_final GROUP BY pdl_PersonID HAVING COUNT(*)>1)
				ORDER BY pdl_PersonId					

			--create stg phone cont
			SELECT * 
	 		INTO GDB_Final_migration.dbo.stg_Phone_cont
			FROM GDB_Final_migration.dbo.stg_Phone
			WHERE SF_Object='CONTACT'
		 		AND phn_PhoneID NOT IN (SELECT phn_PhoneID FROM GDB_Final_migration.dbo.stg_Phone WHERE Translation_Rules='Migrate when Household Account.' AND zrefRecordType='ORG')
			ORDER BY Translation_Rules, zrefRecordType
			--176369  TC2 180369 final 186978

				--create final table
		 		EXEC HC_PivotWizard_p		'pdl_PersonID',	--fields to include as unique identifier/normally it is just a unique ID field.
											'SF_Field_API',											--column that stores all new phone types
											'SF_Field_Value',										--phone numbers
											'[GDB_Final_migration].dbo.stg_Phone_cont_final',			--INTO..     
											'[GDB_Final_migration].dbo.stg_Phone_cont',				--FROM..
											'SF_Field_Value is not null'							--WHERE..
					--76871 TC2 78363  final 80982
				--check dupes
				SELECT * FROM [GDB_Final_migration].dbo.stg_Phone_cont_final
				WHERE pdl_PersonID IN (SELECT pdl_PersonID FROM  [GDB_Final_migration].dbo.stg_Phone_cont_final GROUP BY pdl_PersonID HAVING COUNT(*)>1)
				ORDER BY pdl_PersonId					

				--add Preferred Phone
				ALTER TABLE [GDB_Final_migration].dbo.stg_Phone_cont_final
				ADD npe01__PreferredPhone__c NVARCHAR(20)

				--update preferred phone
 		 		 UPDATE [GDB_Final_migration].dbo.stg_Phone_cont_final
		 		 SET npe01__PreferredPhone__c=CASE T2.sf_field_API WHEN 'MobilePhone' THEN 'Mobile' WHEN 'HomePhone' THEN 'Home' WHEN 'npe01__WorkPhone__c' THEN 'Work' WHEN 'OtherPhone' THEN 'Other' END 
				 FROM [GDB_Final_migration].dbo.stg_Phone_cont_final T1
				 LEFT JOIN (SELECT  pdl_PersonID, pdl_PrimaryContact, T2.SF_Field_Value, sf_field_API  
							FROM GDB_Final_migration.dbo.stg_Phone_cont T2
							WHERE T2.pdl_PrimaryContact=1 AND sf_field_API NOT LIKE '%email%') T2
				 ON (T1.pdl_PersonID = T2.pdl_PersonID)
				 WHERE T2.pdl_PrimaryContact=1 
				 --TC2 908  final 1068
				--check					
					SELECT t1.*, T2.*
	 				FROM [GDB_Final_migration].dbo.stg_Phone_cont_final T1
					LEFT JOIN (SELECT  pdl_PersonID, pdl_PrimaryContact, T2.SF_Field_Value, sf_field_API  
							FROM GDB_Final_migration.dbo.stg_Phone_cont T2
							WHERE T2.pdl_PrimaryContact=1 AND sf_field_API NOT LIKE '%email%') T2
					ON (T1.pdl_PersonID = T2.pdl_PersonID)
					WHERE T2.pdl_PrimaryContact=1 
		END 

		--create phones from tblagency
		SELECT DISTINCT T.oph_AgencyID, MAX(T.oph_Number) AS [Phone]
		INTO GDB_Final_migration.dbo.stg_Phone_Agency_Phone  --drop table GDB_Final_migration.dbo.stg_Phone_Agency_Phone
		FROM GDB_KADE_Final.dbo.tblOutreachPhone AS T
		WHERE T.oph_PhoneCode = 0
		GROUP BY T.oph_AgencyID


		SELECT DISTINCT T.oph_AgencyID, T.oph_Number AS [FAX]
		INTO GDB_Final_migration.dbo.stg_Phone_Agency_FAX
		FROM GDB_KADE_Final.dbo.tblOutreachPhone AS T
		WHERE T.oph_PhoneCode = 3

		BEGIN -- Account Owner tblPuppyClubDetail

		
				SELECT T1.pcd_ID, T1.pcd_ClubID, t1.pcd_PersonID, X2.ID AS OwnerID
				INTO GDB_Final_migration.dbo.stg_Account_Owner  --drop table GDB_Final_migration.dbo.stg_Account_Owner
				FROM GDB_KADE_Final.dbo.tblPuppyClubDetail AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON T1.pcd_PersonID=T2.psn_PersonID
				INNER JOIN [GDB_Final_migration].dbo.XTR_USER AS X2 ON X2.Legacy_ID__c= 'Psn-'+ CAST(T2.psn_PersonID AS NVARCHAR(30))
				WHERE T1.pcd_RelationCode='cfr' AND T2.psn_PersonID NOT IN (SELECT DISTINCT FileNum FROM GDB_KADE_Final.dbo.tblStaff)
				UNION
				SELECT T1.pcd_ID,T1.pcd_ClubID, T1.pcd_PersonID, X1.ID AS OwnerID
				FROM GDB_KADE_Final.dbo.tblPuppyClubDetail AS T1
				INNER JOIN [GDB_Final_migration].dbo.XTR_USER AS X1 ON X1.ADP__C= t1.pcd_PersonID
				WHERE T1.pcd_RelationCode='cfr'

				SELECT * FROM GDB_Final_migration.dbo.stg_Account_Owner 

--TC2: 259		final 263
		END



		BEGIN --ADDRESS 
			--delete stg tbls
			DROP TABLE [GDB_Final_migration].dbo.stg_Address
			 
			--create master stg tbl
				SELECT DISTINCT 
				 T.zrefAccountID, T1.ad_AddressDetailID, T1.ad_PersonID, T1.ad_AddressCode, T1.ad_AddressID
				,T.psn_MailingAddress
				,CASE WHEN T1.ad_AddressCode=T.psn_MailingAddress THEN 'TRUE' ELSE 'FALSE' END AS npsp__Default_Address__c
				,T3.[Description] AS npsp__Address_Type__c
				
				,T2.add_AddressID  
				,T2.add_Address  
				,T2.add_Address2  
				,T2.add_City  
 				,CASE WHEN T2.add_State IS NULL THEN T2.add_CountryRegion ELSE T2.add_State end AS add_State   --If [add_State] is null migrate [add_CountryRegion] to State/Province
				,CASE WHEN T2.add_Zip IS NULL THEN T2.add_CountryCode ELSE T2.add_Zip END AS add_ZipCode --If [add_Zip] is null migrate [add_CountryCode] to Zip/Postal Code

				,T2.add_Country  
				,T2.add_USCounty  
				,T2.add_Directions  
			
				,CASE WHEN T2.add_Address2 IS NOT NULL THEN T2.add_Address + CHAR(10) + T2.add_Address2  WHEN T2.add_Address2 IS NULL THEN T2.add_Address END AS AddressLines
		 		,T2.add_AddressID zrefAddressId ,T2.add_State zrefState, T2.add_CountryRegion zrefCountryRegion, T2.add_Zip zrefZip, T2.add_CountryCode AS zrefCountryCode
			 		
				,T1.ad_AddressCode AS zrefAddressCode1
				,T.psn_MailingAddress AS zrefAddressCode2
				INTO GDB_Final_migration.dbo.stg_Address
				FROM GDB_KADE_Final.dbo.tblAddressDetail AS T1
				INNER JOIN GDB_KADE_Final.DBO.tblAddress AS T2 ON T1.ad_AddressID=T2.add_AddressID
	 			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T ON T1.ad_PersonID = T.psn_PersonID	
				LEFT JOIN GDB_KADE_Final.DBO.trefAddressCode AS T3 ON T1.ad_AddressCode=T3.AddressCode
				ORDER BY T.zrefAccountId, T1.ad_AddressID
				--80420. TC2 82032  final 84823
		  
				SELECT * FROM GDB_Final_migration.dbo.stg_Address
				WHERE ad_AddressID IN  (SELECT ad_AddressID FROM  GDB_Final_migration.dbo.stg_Address GROUP BY ad_AddressID HAVING COUNT(*) >1)
				ORDER BY ad_AddressID


				SELECT * FROM GDB_Final_migration.dbo.stg_Address
				WHERE zrefAccountID='47155'-- OR zrefAccountID='577'
				ORDER BY zrefAccountID

				SELECT * FROM GDB_KADE_Final.dbo.tblAddress WHERE add_AddressID ='15162'
				
				SELECT * FROM GDB_KADE_Final.dbo.tblAddressDetail WHERE ad_AddressID='15162'

				-- Duplicate check

				SELECT * FROM GDB_Final_migration.dbo.stg_Address
			WHERE ad_AddressID IN (SELECT ad_AddressID FROM GDB_Final_migration.dbo.stg_Address GROUP BY ad_AddressID, zrefAccountID HAVING COUNT(*)>1)
			--ORDER BY ad_AddressID
			AND ad_addressid ='1232'

		END

		BEGIN -- Contact owner from zip code

				SELECT DISTINCT x1.id AS Onwer1, T4.ad_PersonID, T1.FieldRep, T1.ZipCode,T5.prl_ClientInstanceID
				INTO GDB_Final_migration.dbo.stg_ContactOwner -- drop table GDB_Final_migration.dbo.stg_ContactOwner
				FROM GDB_KADE_Final.dbo.trefZipCodes AS T1
				--INNER JOIN GDB_KADE_Final.dbo.tblAddress AS T2 ON T1.ZipCode = T2.add_Zip
				--INNER JOIN GDB_KADE_Final.dbo.tblAddressDetail AS T3 ON t2.add_AddressID = T3.ad_AddressID
				LEFT JOIN GDB_Final_migration.dbo.XTR_User AS X1 ON T1.FieldRep = X1.ADP__C
								INNER JOIN (SELECT * FROM [GDB_Final_migration].dbo.stg_Address WHERE npsp__Default_Address__c='TRUE')  AS T4
							ON T1.ZipCode = T4.add_ZipCode
				INNER JOIN GDB_KADE_Final.dbo.tblpersonrel AS T5 ON t5.prl_PersonID=T4.ad_PersonID
				WHERE T5.prl_ClientInstanceID<>0
				--TC2: 77341  final 24400
		--DUPLICATE check
			SELECT * FROM GDB_Final_migration.dbo.stg_ContactOwner
			WHERE ad_PersonID IN (SELECT ad_PersonID FROM GDB_Final_migration.dbo.stg_ContactOwner GROUP BY ad_PersonID HAVING COUNT(*)>1)
			ORDER BY ad_PersonID
	END

	BEGIN -- Contact Next Annual Visit  FB-01818
		SELECT  T2.prl_PersonID, MAX(T1.gfu_Date)+365 AS Next_Annual_Visit__c
		INTO GDB_Final_migration.dbo.stg_Contact_NextAnnualVisit
		FROM GDB_KADE_Final.dbo.tblGradFieldReport AS T1
		INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.gfu_ClientID
		WHERE T1.gfu_VisitType IN ('1','2')
		GROUP BY T2.prl_PersonID
		ORDER BY T2.prl_PersonID
END


		BEGIN--TBLCONTACT HH NAME

			ALTER TABLE GDB_KADE_Final.DBO.tblContacts
			ADD zrefRecordType NVARCHAR(3)

			ALTER TABLE GDB_KADE_Final.DBO.tblContacts
			ADD zrefAccountName NVARCHAR(80)
			
			
			--update record type 
				UPDATE GDB_KADE_Final.dbo.tblContacts
				SET zrefRecordType ='ORG'
				WHERE con_OrgName IS NOT NULL 

				UPDATE GDB_KADE_Final.dbo.tblContacts
				SET zrefRecordType ='HHD'
				WHERE con_OrgName IS  NULL 

			--update account name
				UPDATE GDB_KADE_Final.dbo.tblContacts
				SET zrefAccountName = con_OrgName
				WHERE con_OrgName IS NOT NULL 

				UPDATE GDB_KADE_Final.dbo.tblContacts 
				SET zrefAccountName = LTRIM(RTRIM(CONCAT(con_NamePrefix, ' ' , con_NameFirst, ' ' , con_NameLast, ' ' , con_NameSuffix)))
				WHERE con_OrgName IS null	

			--create unique account record
				DROP TABLE GDB_Final_migration.dbo.stg_tblContact_Account

				SELECT con_ContactID AS zrefAccountId, zrefAccountName, ROW_NUMBER() OVER (PARTITION BY zrefAccountName ORDER BY con_OrgName) AS zrefSeq
				INTO GDB_Final_migration.dbo.stg_tblContact_Account
				FROM GDB_KADE_Final.dbo.tblContacts
				
				DELETE GDB_Final_migration.dbo.stg_tblContact_Account 
				WHERE zrefSeq!=1

				SELECT * FROM GDB_Final_migration.dbo.stg_tblContact_Account ORDER BY zrefAccountName
		 			
					SELECT  t1.con_ContactID, t1.con_NamePrefix, t1.con_NameFirst, t1.con_NameLast, t1.con_NameSuffix, t1.con_OrgName, t1.zrefRecordType, t1.zrefAccountName
					,ROW_NUMBER() OVER (PARTITION BY t1.zrefAccountName ORDER BY t1.con_OrgName) AS zrefSeq, 
					t2.*
					FROM GDB_KADE_Final.dbo.tblContacts AS t1
					LEFT JOIN GDB_Final_migration.dbo.stg_tblContact_Account AS t2 ON t1.zrefAccountName= t2.zrefAccountName
					WHERE t1.zrefRecordType ='ORG'
					ORDER BY con_OrgName
	END
	
	BEGIN
			--create unique account record

				DROP TABLE GDB_Final_migration.dbo.stg_tblVolEquipment
			
				SELECT vps_PathScarfID, vps_OrgName, ROW_NUMBER() OVER(PARTITION BY vps_OrgName ORDER BY vps_OrgName) AS zrefSeq
				,vps_Item, vps_PersonID, vps_DogID, CAST(vps_StartDate AS DATE) vps_StartDate, CAST(vps_TestCompletion AS DATE) AS vps_TestCompletion
				,vps_OrgPhone, vps_OrgAddress, vps_OrgCity,  vps_OrgState, vps_OrgZip
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5',ISNULL(CAST(vps_OrgAddress AS NVARCHAR(20)),' ')
				+ISNULL(vps_OrgCity,' ') +ISNULL(vps_OrgState,' ') +ISNULL(CAST(vps_OrgZip AS NVARCHAR(10)),' '))), 3, 10) AS NVARCHAR(10))  AS zrefAddrId
				INTO GDB_Final_migration.dbo.stg_tblVolEquipment
				FROM GDB_KADE_Final.dbo.tblVolEquipment
				WHERE vps_OrgName IS NOT NULL 
					--USE zrefSeq=1 to create Accounts. 
				--149 TC2 154  final 159
				 
				SELECT * 
				INTO GDB_Final_migration.dbo.stg_tblVolEquipment_1
				FROM GDB_Final_migration.dbo.stg_tblVolEquipment
				WHERE zrefSeq=1
				
				SELECT *
				FROM GDB_KADE_Final.DBO.tblVolEquipment WHERE vps_OrgName IS NOT NULL 
                
				SELECT *
				FROM GDB_Final_migration.DBO.stg_tblVolEquipment    ORDER BY vps_OrgName

				SELECT T1.vps_PathScarfID, T1.vps_OrgName, T1.zrefSeq, T1.zrefAddrId, T1.vps_OrgAddress, T1.vps_OrgCity,  T1.vps_OrgState, T1.vps_OrgZip, 
				T2.vps_PathScarfID AS zrefAccountId
				FROM GDB_Final_migration.DBO.stg_tblVolEquipment  T1
				LEFT JOIN GDB_Final_migration.dbo.stg_tblVolEquipment_1 AS T2 ON T1.vps_OrgName=T2.vps_OrgName
				ORDER BY vps_OrgName, T1.zrefSeq, zrefAddrId
	END 

	
	
	 
	
			 
 
 BEGIN-- create stg tbl tblVBPayments - Vet Record

 		--tblVBPayments
				--DELETE
				DROP TABLE GDB_Final_migration.dbo.stg_tblVBPayments
				DROP TABLE GDB_Final_migration.dbo.stg_tblVBPayments_1
				
				--CREATE
				SELECT		DISTINCT
							 p.vbi_VetEntryID 
		 					,p.vbi_RefNo  AS  Reference_No__c		
							,'iv-' +CAST(v.iv_VendorID AS NVARCHAR(20))  AS  [Vendor__r:Legacy_Id__c]	-- concatenate any records that are one to many	-- Yes/Link [TblVendor]
						 	,p.vbi_BatchID  AS  Batch_ID__c		
							,p.vbi_TransactNum  AS  Transaction_Number__c		
						  
			 	INTO GDB_Final_migration.dbo.stg_tblVBPayments
				FROM GDB_KADE_Final.DBO.tblVBPayments AS p
				LEFT JOIN GDB_KADE_Final.dbo.tblGDBVendor AS v ON p.vbi_VendorID=CAST(v.iv_VendorID  AS varchar(30))
				ORDER BY vbi_VetEntryID
					--181086  TC2: 186492  final 196035
				
			 

				--CREATE
				SELECT * 
 				,ROW_NUMBER() OVER (PARTITION BY vbi_VetEntryID ORDER BY vbi_VetEntryID) AS zrefSeq
				INTO GDB_Final_migration.dbo.stg_tblVBPayments_1
				FROM GDB_Final_migration.dbo.stg_tblVBPayments
					--181086 TC2: 186492 final  196038

				--CHECK
				SELECT * FROM 	GDB_Final_migration.dbo.stg_tblVBPayments_1
				WHERE vbi_VetEntryID IN (SELECT vbi_VetEntryID FROM GDB_Final_migration.dbo.stg_tblVBPayments_1 GROUP BY vbi_VetEntryID HAVING COUNT(*)>1)
				ORDER BY vbi_VetEntryID	
				
END 	

BEGIN --create VetEntry Total Amount FB-01462 --drop table GDB_Final_migration.dbo.Vet_Proc_TotalAmount

SELECT T1.vdp_VetEntryID, SUM(T2.vpa_Amount) AS Total_Amount__c
INTO GDB_Final_migration.dbo.stg_Vet_Proc_TotalAmount
FROM GDB_KADE_Final.dbo.tblVetProc AS T1
INNER JOIN GDB_KADE_Final.dbo.tblVetProcAuth AS T2 ON T1.vdp_ProcID=T2.vpa_ProcID
WHERE T1.vdp_ProcCode<>'6000'
GROUP BY T1.vdp_VetEntryID

END



BEGIN --CREATE UNIQUE IDS to update on CONTACTS, CLIENT MEDICAL ID and MEDICAL CHARTS 
					SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblNursingMedicalNotes  --11635   TC2: 13304		final:15740
					SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblNursingDiabeticNotes -- 1670	TC2: 1915		final: 2180
					SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblNursingIntake		  -- 1097	TC2: 1277		final:1493
					
					SELECT DISTINCT  
 							T1.mn_MedicalNoteID AS MedicalChartLegacyId
							,'tblNursingMedicalNotes' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.mn_ClientID  AS ClientID 
							
							--,DENSE_RANK()  OVER( ORDER BY T3.psn_PersonID) test_dense    --create the same number of the group of id, i.e. the same Id will have the same seq num. 
					INTO GDB_Final_migration.dbo.stg_tblMedicalChartIds --drop table GDB_Final_migration.dbo.stg_tblMedicalChartIds 
					FROM GDB_KADE_Final.dbo.tblNursingMedicalNotes AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.mn_ClientID
					LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
				--	ORDER BY T3.psn_PersonID
 		 
					--11635
					UNION 
					SELECT  DISTINCT
							T1.dn_DiabeticNoteID AS MedicalChartLegacyId
							,'tblNursingDiabeticNotes' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.dn_ClientID  AS ClientID 
					FROM GDB_KADE_Final.dbo.tblNursingDiabeticNotes AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.dn_ClientID
					LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
 					  
					--1670 
					UNION 
					SELECT  DISTINCT
							T1.ni_NursingIntakeID AS MedicalChartLegacyId
							,'tblNursingIntake' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.ni_ClientID  AS ClientID 							
					FROM GDB_KADE_Final.dbo.tblNursingIntake AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.ni_ClientID
					LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
 					ORDER BY psn_PersonID, SrcTbl
					--1097
					--tc1  14402 TC2: 16496		final: 19413

					--check dupes
					SELECT * FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds
					WHERE psn_PersonID IN (SELECT psn_PersonID FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds GROUP BY psn_PersonID HAVING COUNT(*)>1)
					ORDER BY psn_PersonID
					--1005
END

 --create contact dog field --FB-01039
BEGIN
				SELECT T3.prl_PersonID AS ContactLink, T2.drl_DogID AS DogLink, T1.pd_RelCode, T1.pd_StartDate
				,ROW_NUMBER() OVER(PARTITION BY t3.prl_PersonID ORDER BY T3.prl_PersonID, T1.pd_StartDate DESC) AS seq
				FROM GDB_KADE_Final.dbo.tblPersonDog AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T2 ON T1.pd_DogRelID=T2.drl_RelationID
				INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T1.pd_PersonRelID=T3.prl_RelationID
				WHERE T1.pd_RelCode IN('BRD', 'BRDADP', 'BUDADP', 'CLI', 'CLIADP', 'FAMADP', 'PRP', 'PRPADP','RETBRD','CCH','CLS')
				AND T3.prl_PersonID='5344'
END

BEGIN	--FB-01315 REL CFR
	SELECT T1.pd_RelationID, T5.Legacy_ID__c as [CFR__r:Legacy_ID__c]
	INTO GDB_Final_migration.dbo.stg_REL_CFR --drop table GDB_Final_migration.dbo.stg_REL_CFR
	FROM GDB_KADE_Final.dbo.tblPersonDog AS T1
	INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplicationDetail AS T2 ON T1.pd_RelationID=T2.pad_PlacedPDID
	INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplication AS T3 ON T3.pra_ApplicationID=T2.pad_ApplicationID
	INNER JOIN GDB_KADE_Final.dbo.tblPuppyClubDetail AS T4 ON T4.pcd_ClubID=T3.pra_PuppyClubID
	INNER JOIN GDB_FINAL_Migration.dbo.IMP_USER AS T5 ON T5.zrefLegacyNum=T4.pcd_PersonID
	WHERE T4.pcd_RelationCode='cfr'
	ORDER BY T1.pd_RelationID

END

BEGIN	--FB-01315 REL Puppy Club
	SELECT T1.pd_RelationID, 'pcd-'+ CAST(T4.clb_ClubID AS VARCHAR(50)) AS [Puppy_Club__r:Legacy_ID__c]
	---INTO GDB_Final_migration.dbo.stg_REL_PuppyClub
	FROM GDB_KADE_Final.dbo.tblPersonDog AS T1
	INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplicationDetail AS T2 ON T1.pd_RelationID=T2.pad_PlacedPDID
	INNER JOIN GDB_KADE_Final.dbo.tblPuppyRaisingApplication AS T3 ON T3.pra_ApplicationID=T2.pad_ApplicationID
	INNER JOIN GDB_KADE_Final.dbo.trefPuppyClub AS T4 ON T4.clb_ClubID = T3.pra_PuppyClubID
	

END



--Update comma to semi-colon for multi-select picklist FIELD REPORT
		ALTER TABLE GDB_KADE_Final.dbo.tblGradFieldReport
		ADD zrefManagemetnTechUsed VARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblGradFieldReport
		ADD zrefHouseBehaviorChallenges VARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblGradFieldReport
		ADD zrefEnvironmentalSensitivites VARCHAR(255)

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefManagemetnTechUsed = gfu_IG_TechniquesUsed
	WHERE gfu_IG_TechniquesUsed IS NOT NULL

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefHouseBehaviorChallenges = gfu_IG_HomeChallenges
	WHERE gfu_IG_HomeChallenges IS NOT NULL

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefEnvironmentalSensitivites = gfu_IG_EnvironmentalIssues
	WHERE gfu_IG_EnvironmentalIssues IS NOT NULL


	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefManagemetnTechUsed = gfu_IG_TechniquesUsed
	WHERE gfu_IG_TechniquesUsed IS NOT NULL

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefHouseBehaviorChallenges = gfu_IG_HomeChallenges
	WHERE gfu_IG_HomeChallenges IS NOT NULL

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefEnvironmentalSensitivites = gfu_IG_EnvironmentalIssues
	WHERE gfu_IG_EnvironmentalIssues IS NOT NULL




	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefManagemetnTechUsed=REPLACE(zrefManagemetnTechUsed,',',';') 
	WHERE zrefManagemetnTechUsed like '%,%'

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefHouseBehaviorChallenges=REPLACE(zrefHouseBehaviorChallenges,',',';') 
	WHERE zrefHouseBehaviorChallenges like '%,%'

	UPDATE GDB_KADE_Final.dbo.tblGradFieldReport
	SET zrefEnvironmentalSensitivites=REPLACE(zrefEnvironmentalSensitivites,',',';') 
	WHERE zrefEnvironmentalSensitivites like '%,%'


	SELECT DISTINCT gfu_IG_HomeChallenges, zrefHouseBehaviorChallenges
	FROM GDB_KADE_Final.dbo.tblGradFieldReport

-- create pivot table for training

SELECT T1.ttc_TestingID, T2.tc_CategoryText, T1.ttc_CatScore
INTO GDB_FINAL_Migration.dbo.stg_Training_Test_Cat
FROM GDB_Kade_FINAL.dbo.tblTRNTestingCategories AS T1
INNER JOIN GDB_Kade_FINAL.dbo.trefTRNTestingCategories AS T2 ON T2.tc_CategoryTypeID = T1.ttc_CatIDType
ORDER BY T1.ttc_TestingID, T2.tc_CategoryText


 
		exec GDB_FINAL_Migration.dbo.HC_PivotWizard_p	'ttc_TestingID',	--fields to include as unique identifier/normally it is just a unique ID field.
			'tc_CategoryText',									--column that stores all new phone types
			'ttc_CatScore',										--phone numbers
			'[GDB_FINAL_Migration].dbo.stg_Training_Test_Cat1',			--INTO..     
			'[GDB_FINAL_Migration].dbo.stg_Training_Test_Cat',			--FROM..
			'tc_CategoryText is not null'   --WHERE..  removed nulti-select picklist values. those are handled separately below 
 
 SELECT *
 FROM GDB_FINAL_Migration.dbo.stg_Training_Test_Cat1
 ORDER BY ttc_TestingID

 SELECT T1.ttc_TestingID, T2.tc_CategoryText, T1.ttc_CatScore
FROM GDB_Kade_FINAL.dbo.tblTRNTestingCategories AS T1
INNER JOIN GDB_Kade_FINAL.dbo.trefTRNTestingCategories AS T2 ON T2.tc_CategoryTypeID = T1.ttc_CatIDType
ORDER BY T1.ttc_TestingID, T2.tc_CategoryText



--update null ADP # so duplicates aren't created

	UPDATE GDB_FINAL_Migration.dbo.Xtr_user
			SET ADP__C ='N/A'
			WHERE ADP__C IS NULL OR ADP__C =''