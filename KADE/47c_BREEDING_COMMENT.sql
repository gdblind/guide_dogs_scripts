--FB-00380
BEGIN
DROP TABLE GDB_Final_migration.dbo.IMP_BREEDING_COMMENTS
END

BEGIN

SELECT  
		GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
		,'dc-'+ CAST(T1.[dc_CommentID] AS VARCHAR(20)) AS Legacy_ID__c
		,T1.DC_DOGID AS [Dog__r:Legacy_ID__c]  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
		,Date__c = T1.dc_Date
		,T2.New_Value AS Category__c
		,Comments__c = CASE WHEN T1.dc_By IS NULL AND t1.dc_Comments   IS NOT NULL THEN 'Comments by: n/a'  +CHAR(10)+ t1.dc_Comments  
		WHEN T1.dc_By IS NOT NULL  AND t1.dc_Comments   IS NOT NULL THEN 'Comments by:'  +T1.dc_By + CHAR(10)+ t1.dc_Comments  -- concatenate [dc_By] +line break+[dc_Comments]	
				END 
		,'tblDogComments' zrefSrc
		INTO GDB_Final_migration.dbo.IMP_BREEDING_COMMENTS
		FROM GDB_KADE_Final.dbo.tblDogComments AS T1 
		LEFT JOIN GDB_Final_migration.dbo.CHART_DogComments_Category  AS T2 ON T2.dc_Category =T1.dc_Category   
		WHERE  T2.New_Value  IS NOT NULL 


END

BEGIN
		SELECT * FROM GDB_Final_migration.dbo.IMP_BREEDING_COMMENTS
END 