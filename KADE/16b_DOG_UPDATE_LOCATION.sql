
--FB-00971
SELECT DISTINCT X.DogLink, x.Legacy_Id__c AS [Dog_Location__r:Legacy_ID__c], x.ContactLink AS zrefContactLink, X.End_Date__c AS zrefEndDate
, x.Start_Date__c AS zrefStartDate
	--INTO GDB_Final_migration.dbo.stg_UPDATE_DOG_LOCATION  -- drop table GDB_Final_migration.dbo.stg_UPDATE_DOG_LOCATION
	FROM (SELECT *, max_date=MAX(start_Date__c) OVER (PARTITION BY DogLink)
		FROM GDB_Final_migration.DBO.IMP_DOG_LOCATION WHERE (GDB_Final_migration.DBO.IMP_DOG_LOCATION.END_DATE__C IS NULL 
			OR GDB_Final_migration.DBO.IMP_DOG_LOCATION.End_date__c='') 
			AND GDB_Final_migration.DBO.IMP_DOG_LOCATION.START_DATE__C <GETDATE()) AS X
	WHERE X.Start_Date__c=max_date
	ORDER BY X.DogLink

	--FB-01906
SELECT T1.DogLink, T1.[Dog_Location__r:Legacy_ID__c], T3.[name]
	,IIF(T3.[Name]='On Campus',T1.[Kennel__r:Legacy_Id__c],CAST(T1.ContactLink AS VARCHAR(50))) AS Vet_Appointment_Participant__c
	FROM GDB_Final_migration.dbo.stg_UPDATE_DOG_LOCATION AS T1
	inner JOIN GDB_Final_migration.dbo.XTR_RecordTypes AS T3 ON T1.RECORDTYPEID=T3.ID
