BEGIN --UPDATE DOG 
		
		--DISABLE TRIGGERS on tblDog. --kade --> tblDog--> Triggers --> disable all triggers (triggers prevent update on tbl)
		
		SELECT dog_DogID, dog_Status, dog_Sex FROM GDB_KADE_Final.dbo.tblDog
		WHERE dog_Status IS NULL  OR dog_Sex IS NULL  

		UPDATE GDB_KADE_Final.dbo.tblDog
		SET dog_Status='NULL' WHERE dog_Status IS NULL
        UPDATE GDB_KADE_Final.dbo.tblDog
		SET dog_Sex='U' WHERE dog_Sex IS NULL    --U in place of "NULL" 

	END 

	BEGIN --Dog Release Notes --FB-00944
		SELECT T.dds_DogID
		,CONCAT(T.dds_Location,'CHAR(10)', T.dds_RelPlacement,'CHAR(10)', T.dds_RelSummary) AS Release_Notes__c
		,T.dds_StatusChangeBy AS [Released_By__r:ADP__c]
		INTO GDB_Final_migration.dbo.stg_DogStatusChange
		FROM GDB_KADE_Final.dbo.tblDogStatusChange AS T
		WHERE T.dds_Disposition='7'
	end
/*	BEGIN --tblDogReleaseReasons --FB-00024

		SELECT ROW_NUMBER() OVER (PARTITION BY drr_DogID ORDER BY drr_DogID, drr_CreatedDate DESC, drr_ModifiedDate DESC, drr_ID DESC) AS zrefSeq
		,* 
		INTO GDB_Final_migration.dbo.stg_tblDogReleaseReasons
		FROM GDB_KADE_Final.DBO.tblDogReleaseReasons 

		SELECT * FROM GDB_Final_migration.dbo.stg_tblDogReleaseReasons
		WHERE drr_DogID IN (SELECT drr_DogID FROM GDB_Final_migration.dbo.stg_tblDogReleaseReasons GROUP BY drr_DogID HAVING COUNT(*)>1)
				
		SELECT * FROM GDB_Final_migration.dbo.stg_tblDogReleaseReasons
		WHERE zrefSeq>1

		DELETE GDB_Final_migration.dbo.stg_tblDogReleaseReasons
		WHERE zrefSeq>1
		--5216 

	END */

	BEGIN --Dog Weight --drop table GDB_Final_migration.dbo.stg_Dog_Weight

		SELECT  T.vpe_DogID, T.vdl_TreatmentDate, MAX(T.vpe_Weight1) AS Weight_Last__c
		INTO GDB_Final_migration.dbo.stg_Dog_Weight
		FROM (SELECT *, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vpe_DogID)
				FROM GDB_KADE_Final.dbo.tblVetPE_ 
				INNER JOIN GDB_KADE_Final.dbo.tblVetEntry ON vdl_VetEntryID=vpe_VetEntryID WHERE vpe_Weight1 <>'Not Checked') AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vpe_DogID, T.vdl_TreatmentDate


-- check dups

SELECT * FROM GDB_Final_migration.dbo.stg_Dog_Weight
			WHERE vpe_DogID IN (SELECT vpe_DogID FROM GDB_Final_migration.dbo.stg_Dog_Weight GROUP BY vpe_DogID 
			HAVING COUNT(*)>1)
			ORDER BY vpe_DogID	

END 

	BEGIN --Dog WeightCP --drop table GDB_Final_migration.dbo.stg_Dog_WeightCP  FB-00969

		SELECT  T.vpe_DogID, T.vdl_TreatmentDate, MAX(T.vpe_Weight1) AS WeightCP_Last__c
		INTO GDB_Final_migration.dbo.stg_Dog_WeightCP -- drop table INTO GDB_Final_migration.dbo.stg_Dog_WeightCP
		FROM (SELECT vdl_TreatmentDate, vpe_DogID,vpe_Weight1, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vpe_DogID)
				FROM GDB_KADE_Final.dbo.tblVetPE_ 
				INNER JOIN GDB_KADE_Final.dbo.tblVetEntry ON vdl_VetEntryID=vpe_VetEntryID 
				INNER JOIN GDB_KADE_Final.DBO.tblVetProc ON tblVetProc.vdp_VetEntryID = tblVetEntry.vdl_VetEntryID
				WHERE vpe_Weight1 <>'Not Checked' AND vpe_Weight1 IS NOT NULL AND vdp_ProcCode='1001') AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vpe_DogID, T.vdl_TreatmentDate



--check dup
SELECT * FROM GDB_Final_migration.dbo.stg_Dog_WeightCP
			WHERE vpe_DogID IN (SELECT vpe_DogID FROM GDB_Final_migration.dbo.stg_Dog_WeightCP GROUP BY vpe_DogID 
			HAVING COUNT(*)>1)
			ORDER BY vpe_DogID	


END 

        

	BEGIN --Dog Height --drop table GDB_Final_migration.dbo.stg_Dog_Height  FB-00969

		SELECT  T.vpe_DogID, T.vdl_TreatmentDate, MAX(T.vpe_Height) AS Height_Last__c
		INTO GDB_Final_migration.dbo.stg_Dog_SkipMating
		FROM (SELECT *, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vpe_DogID)
				FROM GDB_KADE_Final.dbo.tblVetPE_ 
				INNER JOIN GDB_KADE_Final.dbo.tblVetEntry ON vdl_VetEntryID=vpe_VetEntryID 
				WHERE vpe_Height <>'Not Checked' AND vpe_Height IS NOT NULL) AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vpe_DogID, T.vdl_TreatmentDate
END

BEGIN --Dog Weight --drop table GDB_Final_migration.dbo.stg_Dog_Height  FB-00969

		SELECT  T.vpe_DogID, T.vdl_TreatmentDate, MAX(T.vpe_Height) AS Height_Last__c
		INTO GDB_Final_migration.dbo.stg_Dog_Height
		FROM (SELECT *, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vpe_DogID)
				FROM GDB_KADE_Final.dbo.tblVetPE_ 
				INNER JOIN GDB_KADE_Final.dbo.tblVetEntry ON vdl_VetEntryID=vpe_VetEntryID 
				WHERE vpe_Height <>'Not Checked' AND vpe_Height IS NOT NULL) AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vpe_DogID, T.vdl_TreatmentDate
END

BEGIN -- create dog total workout stage tbl --FB-01714
		SELECT T.wk_DogId, SUM(T.wk_TotCnt) AS Total_Workouts__c
		INTO GDB_Final_migration.dbo.stg_Dog_TotalWorkouts
		FROM GDB_KADE_Final.dbo.tbl_WklyTrain AS T
		GROUP BY T.wk_DogId
END

BEGIN -- DOG RABIES VACINNE  --  drop table GDB_Final_migration.dbo.stg_Dog_Rabies --FB-01184
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Rabies_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Rabies
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('14','18')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
		

END


BEGIN --Rabies Last Facility
	SELECT  T.vdl_DogID, T.vdl_TreatmentDate, MAX(T.[Description]) AS Rabies_Facility
		INTO GDB_Final_migration.dbo.stg_Dog_Rabies_Facility
		FROM (SELECT vdl_VetEntryID, vdl_DogID,vdl_Facility,[Description],vdl_TreatmentDate, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vdl_DogID)
				FROM GDB_KADE_Final.dbo.tblVetEntry 
				INNER JOIN GDB_KADE_Final.dbo.trefVetFacility ON vdl_Facility=VetFacilityID
				INNER JOIN GDB_Kade_FINAL.dbo.tblVetProc ON vdp_VetEntryID=vdl_VetEntryID
				WHERE vdp_ProcCode IN ('14','18')) AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vdl_DogID, T.vdl_TreatmentDate
		ORDER BY T.vdl_DogID

		END

BEGIN --Alter Last Facility
	SELECT  T.vdl_DogID, T.vdl_TreatmentDate, MAX(T.[Description]) AS Alter_Facility
		INTO GDB_Final_migration.dbo.stg_Dog_Alter_Facility
		FROM (SELECT vdl_VetEntryID, vdl_DogID,vdl_Facility,[Description],vdl_TreatmentDate, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vdl_DogID)
				FROM GDB_KADE_Final.dbo.tblVetEntry 
				INNER JOIN GDB_KADE_Final.dbo.trefVetFacility ON vdl_Facility=VetFacilityID
				INNER JOIN GDB_Kade_FINAL.dbo.tblVetProc ON vdp_VetEntryID=vdl_VetEntryID
				WHERE vdp_ProcCode IN ('51','52','53','54','57','58')) AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vdl_DogID, T.vdl_TreatmentDate
		ORDER BY T.vdl_DogID

		END

BEGIN -- DOG DA2PP VACINNE  --  drop table GDB_Final_migration.dbo.stg_Dog_DA2PP --FB-01184
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS DA2PP_Last
		INTO GDB_Final_migration.dbo.stg_Dog_DA2PP
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('20','17','11')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
		

END

BEGIN -- DOG HW Last  --  drop table GDB_Final_migration.dbo.stg_Dog_HW --FB-01184
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS HW_Last
		INTO GDB_Final_migration.dbo.stg_Dog_HW
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('39','723')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END
BEGIN -- DOG Micro Date  --  drop table GDB_Final_migration.dbo.stg_Dog_Micro_Date --FB-01184
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Micro_Date
		INTO GDB_Final_migration.dbo.stg_Dog_Micro_Date
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('39','723')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END
/*
BEGIN -- DOG INFLUENZA LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Influenza --FB-01199
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Influenza_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Influenza
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode ='6064'
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
		
END
*/
BEGIN --DOG INFLUENZA   --  drop table GDB_Final_migration.dbo.stg_Dog_Influenza --FB-01199
		SELECT T2.vdl_DogID, T2.vdl_TreatmentDate 
		,ROW_NUMBER() OVER (PARTITION BY T2.vdl_DogID ORDER BY T2.vdl_DogID,T2.vdl_TreatmentDate DESC) AS Scq
		--		INTO GDB_Final_migration.dbo.stg_Dog_Influenza
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode ='6064'
	END
    

	BEGIN -- DOG Brucellosis LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Brucellosis --FB-00969
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Brucellosis_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Brucellosis
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('556','702')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END




	BEGIN ---- DOG Brucellosis Result  --  drop table GDB_Final_migration.dbo.stg_Dog_BrucellosisResult

		SELECT  T.vdl_DogID, T.vdl_TreatmentDate, MAX(T.vdp_Value1) AS Brucellosis_Result__c
		INTO GDB_Final_migration.dbo.stg_Dog_BrucellosisResult -- 
		FROM (SELECT vdl_TreatmentDate, vdl_DogID,vdp_Value1, max_date = MAX(vdl_TreatmentDate) OVER (PARTITION BY vdl_DogID)
				FROM  GDB_KADE_Final.dbo.tblVetEntry 
				INNER JOIN GDB_KADE_Final.DBO.tblVetProc ON tblVetProc.vdp_VetEntryID = tblVetEntry.vdl_VetEntryID
				WHERE  vdp_ProcCode IN ('556', '702')) AS T
		WHERE T.vdl_TreatmentDate=max_date 
		GROUP BY T.vdl_dogid, T.vdl_TreatmentDate



--check dup
SELECT * FROM GDB_Final_migration.dbo.stg_Dog_BrucellosisResult
			WHERE vdl_DogID IN (SELECT vdl_DogID FROM GDB_Final_migration.dbo.stg_Dog_BrucellosisResult GROUP BY vdl_DogID 
			HAVING COUNT(*)>1)
			ORDER BY vdl_DogID	

SELECT * FROM GDB_Final_migration.dbo.stg_Dog_BrucellosisResult
WHERE Brucellosis_Result__c IS NOT NULL




	BEGIN -- DOG Bordetella LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Bordetella --FB-00969
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Bordetella_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Bordetella
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('21','22','1029')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END

BEGIN -- DOG Leptospirosis LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Leptospirosis --FB-00969
	
		SELECT T2.vdl_DogID, T2.vdl_TreatmentDate 
		,ROW_NUMBER() OVER (PARTITION BY T2.vdl_DogID ORDER BY T2.vdl_DogID,T2.vdl_TreatmentDate DESC) AS Scq
				INTO GDB_Final_migration.dbo.stg_Dog_Leptospirosis
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('20','27')

END
/*
BEGIN -- DOG Lyme LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Lyme --FB-00969
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Lyme_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Lyme
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode ='29'
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END
*/

BEGIN -- DOG LyCBC/Chem T4 +UA LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_CBC_Chem --FB-00969
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS CBC_Last
		INTO GDB_Final_migration.dbo.stg_Dog_CBC
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('2077','102','103')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END
BEGIN -- DOG HipScores LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_HipScores --FB-00969
		SELECT T2.vdl_DogID,  MAX(T2.vdl_TreatmentDate) AS HipScores_Last
		INTO GDB_Final_migration.dbo.stg_Dog_HipScores
		FROM GDB_KADE_Final.DBO.tblVetDiag AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdd_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdd_DiagCode ='10005'
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END

BEGIN -- DOG HipScores Left  --  drop table GDB_Final_migration.dbo.stg_Dog_HipScore_Left --FB-00969
	
	SELECT X.vdl_DogID, X.vdd_Value1 AS Hip_Score_Left__c
	INTO GDB_Final_migration.dbo.stg_Dog_HipScores_Left
	FROM (SELECT T.vdd_Value1, T2.vdl_DogID, T2.vdl_TreatmentDate, max_date=MAX(T2.vdl_TreatmentDate) OVER (PARTITION BY T2.vdl_DogID)
		FROM GDB_KADE_Final.DBO.tblVetDiag AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdd_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdd_DiagCode ='10005') AS X
		WHERE X.vdl_TreatmentDate=X.max_date		
		ORDER BY X.vdl_DogID

END

BEGIN -- DOG HipScores Right  --  drop table GDB_Final_migration.dbo.stg_Dog_HipScore_Left --FB-00969
	
	SELECT X.vdl_DogID, X.vdd_Value2 AS Hip_Score_Right__c
	INTO GDB_Final_migration.dbo.stg_Dog_HipScores_Right
	FROM (SELECT T.vdd_Value2, T2.vdl_DogID, T2.vdl_TreatmentDate, max_date=MAX(T2.vdl_TreatmentDate) OVER (PARTITION BY T2.vdl_DogID)
		FROM GDB_KADE_Final.DBO.tblVetDiag AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdd_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdd_DiagCode ='10005') AS X
		WHERE X.vdl_TreatmentDate=X.max_date		
		ORDER BY X.vdl_DogID

END

BEGIN -- DOG Parvo LAST  --  drop table GDB_Final_migration.dbo.stg_Dog_Parvo --FB-00969
		SELECT T2.vdl_DogID, MAX(T2.vdl_TreatmentDate) AS Parvo_Last
		INTO GDB_Final_migration.dbo.stg_Dog_Parvo
		FROM GDB_KADE_Final.DBO.tblVetProc AS T
		INNER JOIN GDB_KADE_Final.DBO.tblVetEntry AS T2 ON T.vdp_VetEntryID=T2.vdl_VetEntryID
		WHERE T.vdp_ProcCode IN('17','20','22','24')
				GROUP BY T2.vdl_DogID
		ORDER BY T2.vdl_DogID
END

	BEGIN 	--tblStaffDogVacc
				DROP TABLE GDB_Final_migration.dbo.stg_tblStaffDogVacc
 				DROP TABLE GDB_Final_migration.dbo.stg_tblStaffDogVacc_final
				
				
				SELECT stv_DogID , stv_Vaccine
					  ,CASE WHEN stv_Vaccine='1' THEN  'DA2PP_Vaccine_Given__c'
							WHEN stv_Vaccine='2' THEN  'Rabies_Vaccine_Given__c'
							WHEN stv_Vaccine='3' THEN  'Leptospirosis_Vaccine_Given__c'
							WHEN stv_Vaccine='4' THEN  'Bordetella_Vaccine_Given__c'
							WHEN stv_Vaccine='5' THEN  'Influenza_Last'
							WHEN stv_Vaccine='6' THEN  'Influenza_Previous'
						END AS SF_Field
						--,CAST(CAST(stv_DateGiven AS DATE) AS NVARCHAR(10)) SF_Value
						,CONVERT(VARCHAR(10),stv_DateGiven, 101) AS SF_Value
						
			 	INTO GDB_Final_migration.dbo.stg_tblStaffDogVacc
				FROM GDB_KADE_Final.dbo.tblStaffDogVacc  
			
				UNION ALL 
			
				SELECT DISTINCT stv_DogID, NULL AS stv_Vaccine
					  ,'Vaccine_Exemption__c' AS SF_Field
					, CASE stv_Exemption WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS SF_Value
				FROM GDB_KADE_Final.dbo.tblStaffDogVacc 
				WHERE stv_DateGiven IS NOT NULL 
			 
			 --TC2 1093
				--create final table
		 		EXEC HC_PivotWizard_p	'stv_DogID',								            --fields to include as unique identifier/normally it is just a unique ID field.
					'SF_Field',												--column that stores all new column headings/fields name.
					'SF_Value',												--values
					'[GDB_Final_migration].dbo.stg_tblStaffDogVacc_final',	--INTO..     
					'[GDB_Final_migration].dbo.stg_tblStaffDogVacc',			--FROM..
					'SF_Value is not null'							--WHERE..
				--TC1: 195 TC2218


				--check
				SELECT * FROM GDB_Final_migration.dbo.stg_tblStaffDogVacc ORDER BY 	stv_DogID	
				SELECT * FROM GDB_Final_migration.dbo.stg_tblStaffDogVacc_final ORDER BY 	stv_DogID					
				--check dupes
				SELECT * FROM GDB_Final_migration.dbo.stg_tblStaffDogVacc_final 
				WHERE stv_DogID  IN (SELECT stv_DogID  FROM  GDB_Final_migration.dbo.stg_tblStaffDogVacc_final  GROUP BY stv_DogID HAVING COUNT(*)>1)
				ORDER BY 	stv_DogID					
					
	END 

	BEGIN-- DOG PLACEMENT APPLICATION fod
				--NOTE: go to kade db--> tblFosterCareQuestOtherDogs --> Triggers (disable all triggers) 

				SELECT * FROM GDB_KADE_Final.DBO.tblFosterCareQuestOtherDogs
				WHERE fod_Age IS NULL 

			--update null values/only found on age.
				UPDATE GDB_KADE_Final.DBO.tblFosterCareQuestOtherDogs
				SET fod_Age='n/a'
				WHERE fod_Age IS NULL 

 
			--create stg table for pivot
				SELECT 
 				'Other_Dog_'+CAST(ROW_NUMBER() OVER( PARTITION BY fod_QuestionnaireID ORDER BY fod_QuestionnaireID, fod_ID) AS NVARCHAR(1)) +'__c' AS SF_Field,
					'Sex: '+ fod_Sex +CHAR(10)+ 
					'Breed: '+ fod_Breed +CHAR(10)+
					'Altered: '+ fod_Altered+CHAR(10)+
					'Vaccination: '+ fod_Vacc+CHAR(10)+
					'Flea Control: '+ fod_FleaControl+CHAR(10)+
					'Age: '+ fod_Age+CHAR(10) AS SF_Value
				,*
				INTO GDB_Final_migration.dbo.stg_tblFosterCareQuestOtherDogs --drop table  GDB_Final_migration.dbo.stg_tblFosterCareQuestOtherDogs  
				FROM GDB_KADE_Final.DBO.tblFosterCareQuestOtherDogs 
				
				
			--create final table
		 		EXEC GDB_Final_migration.dbo.HC_PivotWizard_p	'fod_QuestionnaireID',								            --fields to include as unique identifier/normally it is just a unique ID field.
						'SF_Field',												--column that stores all new column headings/fields name.
						'SF_Value',												--values
						'[GDB_Final_migration].dbo.stg_tblFosterCareQuestOtherDogs_final',	--INTO..     
						'[GDB_Final_migration].dbo.stg_tblFosterCareQuestOtherDogs',			--FROM..
						'SF_Value is not null'							--WHERE..
				--TC1: 56
			--test
				SELECT * FROM [GDB_Final_migration].dbo.stg_tblFosterCareQuestOtherDogs
				SELECT * FROM [GDB_Final_migration].dbo.stg_tblFosterCareQuestOtherDogs_final

END 
			 
 	BEGIN-- DOG PLACEMENT APPLICATION
			--NOTE: go to kade db--> tblK9BuddyAppOtherDogs --> Triggers (disable all triggers) 

			  
		 
			--create stg table for pivot
				SELECT 
 				'Other_Dog_'+CAST(ROW_NUMBER() OVER( PARTITION BY kod_K9AppID ORDER BY kod_K9AppID, kod_ID) AS NVARCHAR(1)) +'__c' AS SF_Field
			 	, CASE WHEN kod_Sex IS NOT NULL THEN COALESCE('Sex: ' + LTRIM(RTRIM(kod_Sex)), '') ELSE 'Sex: N/A' END + CHAR(10) +
				  CASE WHEN kod_Breed IS NOT NULL THEN COALESCE('Breed: ' + LTRIM(RTRIM(kod_Breed)), '') ELSE 'Breed: N/A' END + CHAR(10) +
				  CASE WHEN kod_Altered IS NOT NULL THEN COALESCE('Altered: ' + LTRIM(RTRIM(kod_Altered)), '') ELSE 'Altered: N/A' END + CHAR(10) +
				  CASE WHEN kod_WhenOwned IS NOT NULL THEN COALESCE('When Owned: ' + LTRIM(RTRIM(kod_WhenOwned)), '') ELSE 'When Owned: N/A' END + CHAR(10) +
				  CASE WHEN kod_Kept IS NOT NULL THEN COALESCE('Kept: ' + LTRIM(RTRIM(kod_Kept)), '') ELSE 'Kept: N/A' END + CHAR(10) +
				  CASE WHEN kod_Age IS NOT NULL THEN COALESCE('Age: ' + LTRIM(RTRIM(kod_Age)), '') ELSE 'Age: N/A' END + CHAR(10) + 
				  CASE WHEN kod_WhatBecameDog IS NOT NULL THEN COALESCE('When Became Dog: ' + LTRIM(RTRIM(kod_WhatBecameDog)), '') ELSE 'When Became Dog: N/A' END + CHAR(10)  
				  AS SF_Value	  --RIPTION = Contactenate with NOTES 
 				,*
				INTO GDB_Final_migration.dbo.stg_tblK9BuddyAppOtherDogs
				FROM GDB_KADE_Final.DBO.tblK9BuddyAppOtherDogs 
				--tc1 314 TC2 320
				
			--create final table
		 		EXEC GDB_Final_migration.dbo.HC_PivotWizard_p	'kod_K9AppID',								            --fields to include as unique identifier/normally it is just a unique ID field.
					 'SF_Field',												--column that stores all new column headings/fields name.
					'SF_Value',												--values
					'[GDB_Final_migration].dbo.stg_tblK9BuddyAppOtherDogs_final',	--INTO..     
					'[GDB_Final_migration].dbo.stg_tblK9BuddyAppOtherDogs',			--FROM..
					'SF_Value is not null'							--WHERE..
				--TC1: 178 TC2 181
			--test
				SELECT * FROM [GDB_Final_migration].dbo.stg_tblK9BuddyAppOtherDogs
				SELECT * FROM [GDB_Final_migration].dbo.stg_tblK9BuddyAppOtherDogs_final

END 


	BEGIN -- create Primary, Secondary and Tertiary fields for Dog object FB-00024
		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Primary_Release_Code__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Secondary_Release_Code__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Tertiary_Release_Code__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Primary_Release_Location__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Secondary_Release_Location__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Tertiary_Release_Location__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Primary_Release_Reason_Text__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Secondary_Release_Reason_Text__c NVARCHAR(255)

		ALTER TABLE GDB_KADE_Final.dbo.tblDogReleaseReasons
		ADD Tertiary_Release_Reason_Text__c NVARCHAR(255)

		

		--update fields
		UPDATE GDB_KADE_Final.dbo.tblDogReleaseReasons 
			SET Primary_Release_Code__c = CASE WHEN T1.drr_Importance='Primary' THEN T1.drr_DiagnosisCode END
			,Secondary_Release_Code__c = CASE  WHEN T1.drr_Importance='Secondary' THEN T1.drr_DiagnosisCode END
			,Tertiary_Release_Code__c = CASE  WHEN T1.drr_Importance='Other' THEN T1.drr_DiagnosisCode END
			,Primary_Release_Location__c = CASE WHEN T1.drr_Importance='Primary' THEN T3.[Location] END
			,Secondary_Release_Location__c = CASE  WHEN T1.drr_Importance='Secondary' THEN T3.[Location] END
			,Tertiary_Release_Location__c = CASE  WHEN T1.drr_Importance='Other' THEN T3.[Location] END

			,Primary_Release_Reason_Text__c = CASE WHEN T1.drr_Importance='Primary' THEN t2.DiagnosisText END
			,Secondary_Release_Reason_Text__c = CASE  WHEN T1.drr_Importance='Secondary' THEN t2.DiagnosisText END
			,Tertiary_Release_Reason_Text__c = CASE  WHEN T1.drr_Importance='Other' THEN t2.DiagnosisText END
			
			FROM GDB_KADE_Final.dbo.tblDogReleaseReasons AS T1
			INNER JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T2 ON T1.drr_DiagnosisCode=T2.[DiagnosisCode]
			LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChampLoc AS T3 ON T1.drr_LocationCode = T3.LocCode 
		
		SELECT T1.drr_DogID
			,MAX(T1.Primary_Release_Code__c) AS Primary_Release_Code__c
			,MAX(T1.Primary_Release_Location__c) AS Primary_Loaction__c
			,MAX(T1.Secondary_Release_Code__c) AS Secondary_Release_Code__c
			,MAX(T1.Secondary_Release_Location__c) AS Secondary_Loaction__c
			,MAX(T1.Tertiary_Release_Code__c) AS Tertiary_Release_Code__c
			,MAX(T1.Tertiary_Release_Location__c) AS Tertiary_Loaction__c
			,MAX(T1.Primary_Release_Reason_Text__c) AS Primary_Release_Reason_Text__c
			,MAX(T1.Secondary_Release_Reason_Text__c) AS Secondary_Release_Reason_Text__c
			,MAX(T1.Tertiary_Release_Reason_Text__c) AS Tertiary_Realease_Reason_Text__c
		INTO GDB_Final_migration.dbo.stg_DogReleaseReasons  -- drop table GDB_Final_migration.dbo.stg_DogReleaseReasons
		FROM GDB_KADE_Final.dbo.tblDogReleaseReasons AS T1
		--WHERE t1.drr_id = '22'
		GROUP BY T1.drr_DogID
		
			SELECT * FROM GDB_KADE_Final.dbo.tblDogReleaseReasons AS T1
			WHERE T1.drr_DogID ='22'

			SELECT * FROM GDB_Final_migration.dbo.stg_DogReleaseReasons
			ORDER BY drr_DogID


	END 

