
SELECT X.prl_PersonID AS Legacy_ID__c, x.drl_DogID AS [Dog__r:Legacy_ID__c]
 FROM (SELECT *, max_date=MAX(T.pd_StartDate) OVER (PARTITION BY T2.prl_PersonID)
	FROM  GDB_KADE_Final.dbo.tblPersonDog AS T
	INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T.pd_PersonRelID=T2.prl_RelationID
	INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T3 ON T.pd_DogRelID=T3.drl_RelationID
	WHERE T.pd_RelCode IN ('brd', 'BRDADP', 'BUDADP', 'CLI', 'CLIADP', 'FAMADP', 'PRP', 'PRPADP','RETBRD','CCH','CLS')) AS X
WHERE X.pd_StartDate=X.max_date
 ORDER BY X.prl_PersonID
