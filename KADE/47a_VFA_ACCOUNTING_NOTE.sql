--FB-00380
BEGIN
    DROP TABLE GDB_Final_migration.dbo.IMP_VFA_ACCOUNT_NOTE

END
begin

			 SELECT  
				 GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				,Legacy_ID__c = 'an-'+ CAST(T1.an_AcctNoteID AS VARCHAR(20))
				,t1.an_DogId AS [Dog__r:Legacy_ID__c]
				,Title__c = 'Dog Acc Notes: '+  CAST(CAST(T1.an_Date AS  DATE) AS NVARCHAR(30))  + '.  Entered by: '+ T1.an_EnterBy
				,Body__c = CAST(T1.an_Note	 AS NVARCHAR(MAX))

				,T1.an_AcctNoteID AS zrefLegacyId
				,'tblDogAcctNotes' zrefSrc
			INTO GDB_Final_migration.dbo.IMP_VFA_ACCOUNT_NOTE
			FROM GDB_KADE_Final.dbo.tblDogAcctNotes AS T1
	--tc2: 3591
END		

BEGIN
	SELECT * FROM GDB_Final_migration.dbo.IMP_VFA_ACCOUNT_NOTE
	END
    