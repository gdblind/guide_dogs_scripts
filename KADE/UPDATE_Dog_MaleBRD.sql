SELECT DISTINCT [T1].[Dog__r:Legacy_Id__c], 'TRUE' AS Male_BRD__c
FROM [GDB_Final_migration].dbo.IMP_REL AS T1
INNER JOIN GDB_KADE_Final.dbo.tblDog AS t2 ON T1.[Dog__r:Legacy_Id__c]=t2.dog_DogID
WHERE T1.Rel_Code__c IN('BRD', 'BRDEVL', 'OUTBRD') AND t2.dog_Sex='M'