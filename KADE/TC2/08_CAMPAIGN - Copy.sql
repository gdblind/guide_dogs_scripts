USE GDB_TC2_migration
GO

BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC2_migration.dbo.tblAGSPrescreenDetail
			WHERE   SF_Object LIKE '%camp%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC2_migration.dbo.tblVolCalWebTourRequest 
			WHERE  SF_Object_2 LIKE '%cam%'
END 



SELECT * FROM GDB_TC2_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%campaign%'

BEGIN -- drop campaign 

	DROP TABLE GDB_TC2_migration.dbo.IMP_CAMPAIGN

END 


BEGIN --create IMP CAMPAIGN (LEVEL 1) 

		SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				,CASE WHEN MAX(T1.cls_DateEnd) > GETDATE() THEN 'In Progress' ELSE 'Completed' END AS  [Status]	-- if > then Today then 'In Progress' else 'Completed'
				,T1.cls_FirstString  +' - String' AS  [Name]	-- concatenate [cls_FirstString] +[cls_Campus] changed for UAT2
				,T1.cls_FirstString  +' '+ T1.cls_Campus AS  [Legacy_Id__c]
				,MIN(CASE WHEN T1.cls_CreatedDate IS NOT NULL THEN CAST(T1.cls_CreatedDate AS DATE) 
						  WHEN T1.cls_DateBegin IS NOT NULL THEN CAST(T1.cls_DateBegin AS DATE)  
						  WHEN T1.cls_ModifiedDate IS NOT NULL THEN CAST(T1.cls_ModifiedDate AS DATE) 
						  ELSE CAST(T1.cls_CreatedDate AS DATE) END) AS  CreatedDate	-- concatenate [cls_FirstString] +[cls_Campus]
  				,'0123D0000004awnQAA' AS RecordTypeID  ---String
				,'0123D0000004awoQAA' AS CampaignMemberRecordTypeId  --Class Participant
 
				,'tblClass' AS zrefsrc
				,T1.cls_Campus AS zrefcls_Campus
				,T1.cls_FirstString AS zrefcls_FirstString
				,T1.cls_FirstString AS String_Number__c
				,'String' AS [Type] --FB-00488
	 	INTO GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_1
		FROM GDB_TC2_kade.dbo.tblClass AS T1
	 	GROUP BY T1.cls_Campus, T1.cls_FirstString
		--TC1: 958

		UNION 
			--tblVolCalEntry  --create 1 record manually 
			SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
				,'In Progress'  [Status]	
				,'Legacy Volunteer Calendar Entry' AS  [Name]	 
				,'LVCE' [Legacy_Id__c]
				,NULL AS  CreatedDate	 
  				,'01241000000cXzZAAU' AS RecordTypeID  ---Volunteer Campaign
				,NULL AS CampaignMemberRecordTypeId
				
				,'tblVolCalEntry' AS zrefsrc
				,NULL AS zrefcls_Campus
				,NULL AS zrefcls_FirstString
				,NULL AS String_Number__c
				,'Volunteer Calendar' AS [Type] --FB-00488

		UNION 
			--tblVolCalEntry  --create 1 record manually 
			SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
				,'In Progress'  [Status]	
				,'Legacy Web Tour Request'  [Name]	 
				,'LWTR' [Legacy_Id__c]
				,NULL AS  CreatedDate	 
  				,'01241000000cXzeAAE' AS RecordTypeID  ---Standard
				,NULL AS CampaignMemberRecordTypeId
				
				,'tblVolCalWebTourRequest' AS zrefsrc
				,NULL AS zrefcls_Campus
				,NULL AS zrefcls_FirstString
				,NULL AS String_Number__c
				,'Volunteer Calendar' AS [Type] --FB-00488

		UNION
			--tblDog.brdEval
			SELECT DISTINCT 
			GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'In Progress'  [Status]	
			,'BrdEvl' + CAST(T1.dog_BrdEvlID AS VARCHAR(20))AS [Name]
			,'BrdEvl' + CAST(T1.dog_BrdEvlID AS VARCHAR(20)) AS Legacy_ID__c
			,NULL AS  CreatedDate
			,'0123D0000005PXSQA2' AS RecordTypeID
			,NULL AS CampaignMemberRecordTypeId
			,'tblDog' AS zrefsrc
			,NULL AS zrefcls_Campus
			,NULL AS zrefcls_FirstString
			,NULL AS String_Number__c
			,'Breed Eval' AS [Type]
			
			FROM GDB_TC2_kade.dbo.tblDog AS T1
			--TC2: 110

END --tc1: 960 TC2: 1070

SELECT * FROM GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_1

BEGIN -- create IMP_CAMPAING (LEVEL 2) 
			SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,T1.cls_ClassID  +' - Class ' AS  [Name]	-- concatenate [cls_ClassID] +[cls_Campus] changed for UAT2
					,'cls-'+T1.cls_ClassID  AS  [Legacy_Id__c]
					,CASE T1.cls_Retrain WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Retrain__c	
					,CAST(T1.cls_DateBegin AS DATE) AS  StartDate	
					,CAST(T1.cls_DateEnd  AS DATE) AS  EndDate	
					,CASE WHEN T1.cls_DateEnd > GETDATE() THEN 'In Progress' ELSE 'Completed' END  AS  [Status]	-- if > then Today then 'In Progress' else 'Completed'
					,CAST(T1.cls_CeremonyDate  AS DATE)  AS  Ceremony_Date__c	
					,'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(30)) AS  ClassSupervisorLink
					 ,T1.cls_Capacity  AS  Capacity__c	
					,T1.cls_Budgeted  AS  BudgetedCost 	
					,T1.cls_Notes  AS  Description	
					,CASE WHEN T1.cls_Campus='SR' THEN 'San Rafael' WHEN  T1.cls_Campus='or' THEN 'Oregon' END  AS  Campus__c	
					,T1.cls_ClassID AS Class_Number__c
					,T1.cls_Type  AS  Type__c	
					,T1.cls_FirstString  +' '+ T1.cls_Campus   AS  ParentLink	-- Parent Campaign is created from String
					,T1.cls_CeremonyNum AS Ceremony_Number__c	
					,CAST(T1.cls_CreatedDate  AS DATE) AS  CreatedDate	
					,'0123D0000004awkQAA' AS RecordTypeId --Class
					,'0123D0000004awoQAA' AS CampaignMemberRecordTypeId	-- Class Participant
					 
					,T1.cls_ClassID AS zrefClassId
					,'tblClass' AS zrefsrc
					,'Class' AS [Type] --FB-00488
		 	INTO GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_2
			FROM GDB_TC2_kade.dbo.tblClass AS T1
			LEFT JOIN GDB_TC2_kade.dbo.tblStaff AS T2 ON T1.cls_HeadInstructor=T2.FileNum

			SELECT * FROM GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_2

END --TC1: 1565  TC2: 1573

BEGIN --- CAMPAIGN LEVEL 3
 			SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'fus-'+CAST(T1.fus_FUSchID  AS NVARCHAR(30)) AS  Legacy_Id__c				-- concatenate 'fus'+[fus_FUSchId]	
			,T2.[Description]  AS  Campus__c					-- migrate value from [trefFacility].[Description]	-- Ref [trefFacility].[FacilityID]
			,'cls-'+T3.cls_ClassID    AS  ParentLink					-- Link to Campaign	-- ref [tblClass].[cls_ClassID]
			,CASE WHEN t1.fus_Destination IS NOT NULL THEN T1.fus_ClassNum  +' '+  T1.fus_Destination 
					ELSE T1.fus_ClassNum  END AS  [NAME]						-- concatenate [fus_Num] + [fus_Destination]	-- ref [tblClass].[cls_ClassID]
			,X1.ID  AS  Field_Service_Manager__c		-- Link [tblStaff].[FileNum]
			,CAST(T1.fus_FUAssignDateBegin AS DATE) AS  StartDate		
			,CAST(T1.fus_FUAssignDateEnd  AS DATE) AS  EndDate		
			,CAST(T1.fus_FUAssmDeadline  AS DATE) AS  Deadline__c		
			,CAST(T1.fus_FUListDueSC  AS DATE) AS  FU_List_Due_SC__c		
			,CAST(T1.fus_FUListDueInst  AS DATE) AS  FU_List_Due_Instructor__c		
			,CAST(T1.fus_FUAgendaDue  AS DATE) AS  FU_Agenda_Due__c		
			,X2.ID  AS  Instructor_Assigned_1__c		-- Link [tblStaff].[FileNum]
			,X3.ID  AS  Instructor_Assigned_2__c		-- Link [tblStaff].[FileNum]
			,T1.fus_Destination  AS  Destination__c		
			,T1.fus_ListStatus  AS  [Status]		
			,T1.fus_ApprtTripNum  AS  Application_Trip_Number__c		
			,X4.ID AS  [Prescreen_Staff_1__c]	-- Link [tblStaff].[FileNum]
			,X5.ID AS  [Prescreen_Staff_2__c]		-- Link [tblStaff].[FileNum]
			,X6.ID AS  [Prescreen_Staff_3__c]		-- Link [tblStaff].[FileNum]
			,CASE T1.fus_Archived WHEN 1 THEN 'FALSE' ELSE 'TRUE' END AS  IsActive	-- If TRUE (1) migrate as FALSE; if FALSE (0) migrate as TRUE	
			,'01241000000cXzeAAE' AS RecordTypeID  ---Standard
			
			,T1.fus_FUSchID AS zreffus_FUSchID
			,'tblAGSPrescreenInstrSchd' AS zrefsrc
			,'Prescreen' AS [Type] --FB-00488
			INTO GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_3    --drop table  GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_3 
			FROM GDB_TC2_kade.dbo.tblAGSPrescreenInstrSchd AS T1
			LEFT JOIN GDB_TC2_kade.dbo.tblClass AS T3 ON T3.cls_ClassID=T1.fus_ClassNum
			LEFT JOIN GDB_TC2_kade.dbo.trefFacility AS T2 ON T2.FacilityID=T1.fus_Campus
 			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T1.fus_FSM
 			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X2 ON X2.ADP__c = T1.fus_InstAssigned1
 			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X3 ON X3.ADP__c = T1.fus_InstAssigned2
			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X4 ON X4.ADP__c = T1.fus_PrescreenStaff1
			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X5 ON X5.ADP__c = T1.fus_PrescreenStaff2
			LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X6 ON X6.ADP__c = T1.fus_PrescreenStaff3

			SELECT * FROM GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_3
			WHERE Legacy_Id__c='FUS-205'
			 
END 


BEGIN-- CREATE CAMPAIGN LEVEL 4
		SELECT  GDB_TC2_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'psd-'+CAST(T1.psd_DetailID  AS NVARCHAR(30))AS  Legacy_ID__c		
			,'fus-'+CAST(T2.fus_FUSchID AS NVARCHAR(30)) AS  ParentLink		-- Link [tblAGSPrescreenInstrSchd].[fus_FUSchID]
			,T1.psd_ListName +' '+ CAST(CAST(T1.psd_TripBeginDate AS DATE) AS NVARCHAR(10)) AS  [Name]					-- concatenate [psd_ListName] + [psd_TripBeginDate]	
			,CAST(T1.psd_TripBeginDate AS DATE)  AS  Trip_Begin_Date__c		
			,CASE WHEN T1.psd_TripBeginDate > GETDATE() THEN 'In Progress' ELSE 'Closed'  END AS  [Status]	-- if > Today, 'In Progress', 'Closed'	
			,CAST(T1.psd_TripEndDate  AS DATE) AS  Trip_End_Date__c		
			,X1.ID  AS  Instructor__c		-- Yes/Link [tblStaff].[FileNum]
			,T1.psd_InstructorContact  AS  Instructor_Phone__c		
			,X2.ID  AS  Field_Service_Manager__c		-- Link [tblStaff].[FileNum]
			,CASE T1.psd_ListArchived  WHEN 1 THEN 'FALSE' ELSE 'TRUE' END AS  IsActive	-- If TRUE migrate as FALSE, if FALSE migrate as TRUE	
			,CASE T1.psd_EmailSent  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Email_Sent__c		
			,CASE T1.psd_ListComplete  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  List_Complete__c	
			,'01241000000cXzeAAE' AS RecordTypeID  ---Standard

			,t1.psd_DetailID AS zrefpsd_DetailID
			,'tblAGSPrescreenDetail' AS zrefsrc
			,'Prescreen' AS [Type] --FB-00488

			INTO GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_4
		FROM GDB_TC2_kade.dbo.tblAGSPrescreenDetail AS T1	
		LEFT JOIN GDB_TC2_kade.dbo.tblAGSPrescreenInstrSchd AS T2 ON T2.fus_FUSchID=t1.psd_FUSchID
		LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X1 ON X1.ADP__c = T1.psd_Instructor
		LEFT JOIN GDB_TC2_migration.dbo.XTR_USER AS X2 ON X2.ADP__c = T1.psd_FSM

		SELECT * FROM GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_4

END 

BEGIN --Create Campaign from tblDog.dogBrdEvlID
		SELECT DISTINCT 'BrdEvl' + CAST(T1.dog_BrdEvlID AS VARCHAR(20))AS [Name], 'BrdEvl' + CAST(T1.dog_BrdEvlID AS VARCHAR(20)) AS Legacy_ID__c
		, 'Breed Eval' AS Type
		,'' AS RecordTypeID
		--drop table GDB_TC2_migration.dbo.IMP_CampaignBrdEvl
		FROM GDB_TC2_kade.dbo.tblDog AS T1

SELECT * FROM GDB_TC2_migration.dbo.IMP_CAMPAIGN_level_3 


