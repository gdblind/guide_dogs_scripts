USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGFRQuestionDetail
			WHERE   SF_Object LIKE '%fie%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_FIELD_REPORT_DETAIL

END 

BEGIN -- CREATE IMP 

					SELECT DISTINCT
					    GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'GFRD-'+CAST(T1.gfrd_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Gfrd-'+[gfrd_ID]	
						,[Legacy_ID__c]   AS  [Field_Report__r:Legacy_ID__c]		-- Link [tblGradFieldReport].[gfu_ID]
						,T3.q_text  AS  Question__c		-- Link treGFRQuestion.q_ID
						,T4.qc_ChoiceText  AS  Answer__c		-- Link trefGFRQuestionChoice.qc_ChoiceNum
						,CAST(T1.gfrd_CreatedDate AS DATE) AS CreatedDate
						
					 
				 	INTO GDB_Final_migration.DBO.IMP_FIELD_REPORT_DETAIL
					FROM GDB_KADE_Final.dbo.tblGFRQuestionDetail AS T1
					INNER JOIN GDB_KADE_Final.dbo.tblGradFieldReport AS T2 ON T2.gfu_ID=T1.gfrd_GFR_ID
 					LEFT JOIN GDB_KADE_Final.dbo.trefGFRQuestion AS T3 ON T3.q_ID=T1.gfrd_q_ID 
					LEFT JOIN GDB_KADE_Final.dbo.trefGFRQuestionChoice AS T4 ON T4.qc_ChoiceNum=T1.gfrd_ChoiceNum AND T4.qc_QuestionID=T1.gfrd_q_ID
					INNER JOIN GDB_Final_migration.dbo.imp_field_report AS T5 ON T5.[zrefId] = T1.gfrd_GFR_ID
 
  

END --tc1: 63505  Tc2:69984  final 81073

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT_DETAIL 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT_DETAIL GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT_DETAIL 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT_DETAIL
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_FIELD_REPORT_DETAIL
	 

END 