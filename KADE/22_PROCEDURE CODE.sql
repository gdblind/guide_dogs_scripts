
SELECT T.VetProcedureCode AS Code__c, T.VetProcedureText AS [Name], T.VetProcedureStatus AS Status__c, T2.ProcedureTypeText AS Type__c
INTO GDB_Final_migration.DBO.IMP_PROCEDURE_CODE
FROM GDB_KADE_Final.dbo.trefVetProc AS T
LEFT JOIN GDB_KADE_Final.dbo.trefVetProcType AS T2 ON T.VetProcedureTypeCode=T2.ProcedureTypeCode

SELECT * FROM GDB_Final_migration.DBO.IMP_PROCEDURE_CODE

--TC2: 383