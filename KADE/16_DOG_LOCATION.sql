USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblCCHDogInfo
			WHERE   SF_Object LIKE '%loca%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


/*
SELECT * FROM GDB_Final_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%LOC%'
0121L000001UQCrQAO	Off Campus	Location__c
0121L000001UQCsQAO 	On Campus	Location__c

*/

BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_DOG_LOCATION

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DS-'+CAST(T1.ds_DogSitID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "DS-" with ds_DogSitID	
							,IIF(T3.psn_Last IS NULL,'Dog Sitting', T3.psn_Last+' Dog Sitting') AS [Name]
							,T2.dog_DogID AS  DogLink						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  ContactLink						-- Link [tblPerson].[psn_PersonID]
							,NULL AS Month__c
							,NULL AS  Expected_Start_Date__c		--FB-00971 --FB-01728
							--,NULL AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.ds_DateInToDS AS date) AS  Start_Date__c	--FB-00970	
							,CAST(T1.ds_DateOutOfDS AS DATE)  AS  End_Date__c	--FB-00970	
							--,CONVERT(TIME, T1.ds_DateInToDS, 114) AS  Start_Time__c	--FB-00970	
							--,CAST(T1.ds_DateOutOfDS AS TIME)  AS  End_Time__c	--FB-00970	
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL AS  Location__c	  --filler	
							,NULL AS  Local__c			--filler
							,NULL AS  Time__c			--filler
							,NULL AS  Department_Requesting__c --filler
							,CAST(T1.ds_Reason  AS NVARCHAR(max)) AS  Reason_Detail__c	
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							--,NULL AS  Expected_End_Time__c		--FB-00970
 							---,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.ds_CreatedDate AS DATE) AS  CreatedDate	
							,'0121L000001UQCrQAO'  AS  RecordTypeId	-- Off Campus	
							,'Dog Sitting' AS  Reason__c	-- Dog Sitting	
							,T1.ds_DogSitID AS zrefID
							,'tblDogSitting' AS zrefSrc

				  	INTO GDB_Final_migration.DBO.IMP_DOG_LOCATION
					FROM GDB_KADE_Final.dbo.tblDogSitting AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.ds_DogID
					LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.ds_PersonID  
					
			UNION 
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'REC-'+CAST(T1.rec_RecallID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "REC-" with rec_RecallID	
							,IIF(T1.rec_Location='OR', 'OR Unknown Kennel Recall','CA Unknown Kennel Recall')  AS [Name]
							,T2.dog_DogID AS  DogLink			-- Link [tblDog].[dog_DogID]
							,NULL AS  ContactLink   --filler
							,T3.[Month]  AS  Month__c		
							,CAST(T1.rec_FromDate AS DATE) AS  Expected_Start_Date__c --FB-00970	
							--,CAST(T1.rec_FromDate AS time) AS  Expected_Start_Time__c --FB-00970
							,IIF(T1.rec_FromDate < GETDATE(),CAST(T1.rec_FromDate AS DATE), NULL) AS Start_Date__c		--FB-00970	
							,IIF(T1.rec_FromDate < GETDATE(),CAST(T1.rec_FromDate AS DATE), NULL)  AS End_Date__c		--FB-00970	
							--,IIF(T1.rec_FromDate < GETDATE(),CAST(T1.rec_FromDate AS TIME), NULL) AS Start_Time__c		--FB-00970	
							--,IIF(T1.rec_FromDate < GETDATE(),CAST(T1.rec_FromDate AS TIME), NULL)  AS End_Time__c		--FB-00970
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.rec_Location  AS  Location__c		
							,T1.rec_LocalYN  AS  Local__c		
							,LEFT(CAST(CAST(T1.rec_FromDate AS DATE )AS NVARCHAR(20)) +'T'+  CAST(CAST(T1.rec_Time AS TIME) AS NVARCHAR(20)),23) +'Z' AS  Time__c
							,NULL  AS  Department_Requesting__c	 --filler
							,T1.rec_Comment  AS  Reason_Detail__c		
							,T4.TripState  AS  TripState__c
							,T4.TripCity  AS  TripCity__c
							,T4.TripRegion  AS  TripRegion__c
							,T4.TripDirection  AS  Directions__c
							,NULL AS  Reason_In_2__c					--filler
							,T1.rec_Location AS  [Organization__r:Legacy_Id__c]		--FB-01613
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c	--FB-00970
 						--	,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.rec_CreatedDate AS DATE)  AS  CreatedDate		
							,'0121L000001UQCrQAO'  AS  RecordTypeId	-- Off Campus	
							,'Recall'  AS  Reason__c	-- Hard code value = "Recall"	
 							,T1.rec_RecallID AS zrefID
							,'tblRecall' AS zrefSrc
					FROM GDB_KADE_Final.dbo.tblRecall AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.rec_DogID
					LEFT JOIN GDB_KADE_Final.dbo.trefMonth AS T3 ON T3.ID=T1.rec_MonthCode
					LEFT JOIN GDB_KADE_Final.dbo.trefRecallTrips AS T4 ON T4.TripCode=T1.rec_TripCode
					
				 
			UNION

		        SELECT  DISTINCT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'FC-'+CAST(T1.fc_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "FC-" with fc_ID	
							,IIF (T5.Legacy_ID__c IS NULL, CONCAT(T6.[name],' Foster Care'),CONCAT(T5.LastName,' Foster Care')) AS [Name] --FB-01613
							,T2.dog_DogID AS  DogLink						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  ContactLink						-- Link [tblPerson].[psn_PersonID]
							,NULL  AS  Month__c --filler
							,NULL AS  Expected_Start_Date__c		--FB-00971
						--	,NULL AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.fc_DateInToFC AS DATE) AS  Start_Date__c		--FB-00970	
							,CAST(T1.fc_DateOutOfFC AS DATE) AS  End_Date__c		--FB-00970	
							--,CAST(T1.fc_DateInToFC AS TIME) AS  Start_Time__c		--FB-00970	
							--,CAST(T1.fc_DateOutOfFC AS TIME) AS  End_Time__c		--FB-00970
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL  AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,T4.DeptLongID +' ' + T4.DeptName AS  Department_Requesting__c	-- Concatenate [DeptLongID] + ' '+ [DeptName]	-- Link [TblDepartment].[DeptNum]
							,CAST(T1.fc_Reason  AS NVARCHAR(MAX)) AS  Reason_Detail__c		
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,IIF(t5.Legacy_ID__c IS NULL,T6.Legacy_ID__c,'') AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c		--FB-00970
 						--	,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.fc_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQCrQAO'  AS  RecordTypeId	-- Off Campus	
							,'Foster Care'  AS  Reason__c	-- Foster Care	
							,T1.fc_ID AS zrefID
							,'tblFosterCare' AS zrefSrc

							FROM GDB_KADE_Final.dbo.tblFosterCare AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.fc_DogID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.fc_PersonID  
							LEFT JOIN GDB_KADE_Final.dbo.tblDepartment AS T4 ON T4.DeptNum = T1.fc_DeptRequesting
							LEFT JOIN GDB_Final_migration.dbo.IMP_CONTACT AS T5 ON CAST(T1.fc_PersonID AS VARCHAR(30))=CAST(T5.Legacy_ID__c AS VARCHAR(30))
							LEFT JOIN GDB_Final_migration.dbo.IMP_ACCOUNT AS T6 ON CAST(T1.fc_PersonID AS VARCHAR(30))=CAST(T6.Legacy_ID__c AS VARCHAR(30))
							WHERE t5.Legacy_ID__c<>'' AND T6.Legacy_ID__c<>''
						


			UNION
					 SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'KR-'+CAST(T1.kr_RegistryID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'kr-'+[kr_RegistryID]
							,CONCAT(T4.KennelName,' ', T1.kr_ReasonIn) AS [Name] --FB-01613	
							,T2.dog_DogID AS  DogLink		-- Link [tblDog].[dog_DogID]
							,NULL AS  ContactLink		--filler						 
							,NULL  AS  Month__c --filler
							,CAST(T1.kr_DateIn AS DATE) AS  Expected_Start_Date__c		--FB-00971
						--	,CAST(T1.kr_DateIn AS Time) AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.kr_DateIn AS DATE) AS  Start_Date__c		--FB-00970	
							,CAST(T1.kr_DateLeft AS DATE) AS  End_Date__c	--FB-00970
							--,CAST(T1.kr_DateIn AS TIME) AS  Start_Time__c		--FB-00970	
							--,CAST(T1.kr_DateLeft AS TIME) AS  End_Time__c	--FB-00970		
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.kr_Where  AS  Location__c		
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS Department_Requesting__c --filler
							,NULL AS Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
 							
							,T3.KennelReasonText  AS  Reason_In_2__c							-- Migrate value from [trefKennelReasons].[KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,'ken-'+CAST(T4.KennelID AS NVARCHAR(30)) AS  [Organization__r:Legacy_Id__c]		-- Ref [trefKennels].[KennelID]
							,CAST(T1.kr_DateDueOut AS DATE) AS  Expected_End_Date__c --FB-00970	
							,CAST(T1.kr_DateDueOut AS Time) AS  Expected_End_Time__c --FB-00970		
 						--	,CAST(T1.kr_WalkingNotes AS NVARCHAR(MAX)) AS  Walking_Notes__c		--FB002126
							,CAST(T1.kr_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,T1.kr_DateSeasonBegin AS  Date_Season_Begin__c		
							,T1.kr_DateSeasonEnd AS  Date_Season_End__c		
								
							,CAST(T1.kr_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQCsQAO'  AS  RecordTypeId	--ON Campus	
							,T5.KennelReasonText  AS  Reason__c	-- Migrate value from [trefKennelReasons].[KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,T1.kr_RegistryID AS zrefID
							,'tblKennelRegistry' AS zrefSrc
							
							FROM GDB_KADE_Final.dbo.tblKennelRegistry AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.kr_DogID
							LEFT JOIN GDB_KADE_Final.DBO.trefKennelReasons AS T3 ON T3.KennelReasonCode=T1.kr_ReasonIn_2
							LEFT JOIN GDB_KADE_Final.DBO.trefKennels AS T4 ON T4.KennelID=T1.kr_Kennel
							LEFT JOIN GDB_KADE_Final.DBO.trefKennelReasons AS T5 ON T5.KennelReasonCode=T1.kr_ReasonIn

			UNION
					 SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'KRS-'+CAST(T1.krs_ScheduleID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'krs-'+[krs_ScheduleID]	
							,IIF(T1.krs_Where='OR', CONCAT('OR Unknown Kennel ',T1.krs_Reason), CONCAT('CA Unknown Kennel ',T1.krs_Reason)) AS [Name] --FB-01613
							,T2.dog_DogID AS  DogLink		-- Link [tblDog].[dog_DogID]
							,NULL AS  ContactLink		--filler						 
							,NULL  AS  Month__c --filler
							,CAST(T1.krs_Date AS DATE) AS  Expected_Start_Date__c			--FB-00971
							--,CAST(T1.krs_Date AS TIME) AS  Expected_Start_Time__c			--FB-00970
							,CAST(T1.krs_Date AS date)AS  Start_Date__c		--FB-00970	
 							,CAST(T1.krs_DateOut AS DATE) AS  End_Date__c		--FB-00970	
							--,CAST(T1.krs_Date AS time)AS  Start_Time__c		--FB-00970	
 							--,CAST(T1.krs_DateOut AS TIME) AS  End_Time__c		--FB-00970
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.krs_Where  AS  Location__c		
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS Department_Requesting__c --filler
							,NULL AS Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,T1.krs_Where AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c		--FB-00970
 					--		,NULL AS  Walking_Notes__c		--filler
							,CAST(T1.krs_Notes AS NVARCHAR(MAX)) AS  Notes__c
							,NULL AS Date_Season_Begin__c
							,NULL AS Date_Season_End__c
							,CAST(T1.krs_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQCsQAO'  AS  RecordTypeID	-- On Campus
							,T3.KennelReasonText  AS  Reason__c	-- migrate value from [KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,T1.krs_ScheduleID AS zrefID
							,'tblKennelRegSchedule' AS zrefSrc	
							
							FROM GDB_KADE_Final.DBO.tblKennelRegSchedule AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.krs_DogID
							LEFT JOIN GDB_KADE_Final.DBO.trefKennelReasons AS T3 ON T3.KennelReasonCode=T1.krs_Reason

			UNION
					 SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'BS-'+CAST(T1.bs_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'bs-'+[bs_id]	
							,IIF(T3.psn_Last IS NULL,'Breeder Sitter', T3.psn_Last+' Breeder Sitter') AS [Name]
							,T2.dog_DogID AS  DogLink						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  ContactLink					-- Link [tblPerson].[psn_PersonID]
							,NULL  AS  Month__c --filler
							,NULL  AS  Expected_Start_Date__c		--FB-00971 --FB01728
							--,NULL AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.bs_DateIn AS DATE)  AS  Start_Date__c		--FB-00970	
							,CAST(T1.bs_DateOut AS DATE)   AS  End_Date__c	--FB-00970	
							--,CAST(T1.bs_DateIn AS TIME)  AS  Start_Time__c		--FB-00970	
							--,CAST(T1.bs_DateOut AS TIME)   AS  End_Time__c	--FB-00970
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS  Department_Requesting__c --filler
							,NULL AS  Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c		--FB-00970
 						--	,NULL AS  Walking_Notes__c		--filler
							,T1.bs_Notes  AS  Notes__c		
							,NULL AS Date_Season_Begin__c  --filler	
							,NULL AS Date_Season_End__c    --filler	
							,CAST(T1.bs_CreatedDate AS DATE) AS  CreatedDate
							,'0121L000001UQCrQAO'  AS  RecordTypeID	-- Off Campus	
							,'Breeder Sitter'  AS  Reason__c	-- Breeder Sitter	
							,T1.bs_ID AS zrefID
							,'tblDogBreederSitter' AS zrefSrc	
						
							FROM GDB_KADE_Final.dbo.tblDogBreederSitter AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.bs_DogID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.bs_PersonID  
								

			UNION
					 SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'CCH-'+CAST(T1.cch_DogID AS NVARCHAR(30)) +'-'+CAST(ROW_NUMBER() OVER(PARTITION BY T1.cch_DogID ORDER BY t1.cch_DogID) AS NVARCHAR(30)) AS Legacy_ID__c
							,IIF(T7.Legacy_ID__c IS NULL, CONCAT(T6.LastName,' CCH Dog'), CONCAT(T7.[name],' CCH Dog')) AS [Name] --FB-01613 
							,T2.dog_DogID AS  DogLink			-- Link [tblDog].[dog_DogID]
							,T5.prl_PersonID AS  ContactLink  --FB-01613
							,NULL  AS  Month__c --filler
							,NULL AS  Expected_Start_Date__c			--FB-00971 --FB-01728
							--,NULL AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.cch_DatePlaced  AS DATE) AS  Start_Date__c		--FB-00970	
							,CAST(T1.cch_DatePlaceEnded AS DATE) AS  End_Date__c		--FB-00970	
							--,CAST(T1.cch_DatePlaced  AS TIME) AS  Start_Time__c		--FB-00970	
							--,CAST(T1.cch_DatePlaceEnded AS TIME) AS  End_Time__c		--FB-00970	
							,T1.cch_DateFileReceived AS  Date_File_Received__c		
							,NULL AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS  Department_Requesting__c --filler
							,NULL AS  Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c		--FB-00970
 					--		,NULL AS  Walking_Notes__c		--filler
							,CAST(T1.cch_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,NULL AS Date_Season_Begin__c  --filler	
							,NULL AS Date_Season_End__c    --filler
							,CAST(T1.cch_CreatedDate AS DATE) AS  CreatedDate
							,'0121L000001UQCrQAO'  AS  RecordTypeId	-- Off Campus	
							,'CCH Dog'  AS  Reason__c	-- CCH Dog	
 							,T1.cch_DogID AS zrefID
							,'tblCCHDogInfo' AS zrefSrc

							FROM GDB_KADE_Final.dbo.tblCCHDogInfo AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.cch_DogID
							LEFT JOIN GDB_KADE_Final.dbo.tblDogRel AS T3 ON T3.drl_DogID=T1.cch_DogID
							LEFT JOIN GDB_KADE_Final.dbo.tblPersonDog AS T4 ON T4.pd_DogRelID=T3.drl_RelationID --FB-01613
							LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T5 ON T5.prl_RelationID=T4.pd_PersonRelID--FB-01613
							LEFT JOIN GDB_Final_migration.dbo.IMP_CONTACT AS T6 ON CAST(T6.Legacy_ID__c AS VARCHAR(30))=CAST(T5.prl_PersonID AS VARCHAR(30)) --FB-01613
							LEFT JOIN GDB_Final_migration.dbo.IMP_ACCOUNT AS T7 ON CAST(T7.Legacy_ID__c AS VARCHAR(30))=CAst(T5.prl_PersonID AS VARCHAR(30)) --FB-01613
							WHERE T3.drl_RelationCode='cch' AND T4.pd_CoRel='pri' --FB-01613
							


			UNION --added per FB-00927
					 SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						 	,'PD-'+CAST(T1.pd_RelationID AS NVARCHAR(30)) AS  Legacy_Id__c	
							,IIF(T4.psn_Last IS NULL,IIF(T1.pd_RelCode = 'PRP', 'Puppy Raising','Breeder Custodian'),T4.psn_Last+' '+IIF(T1.pd_RelCode = 'PRP', 'Puppy Raising','Breeder Custodian')) AS [Name] --FB-01613
							,T2.drl_DogID AS  DogLink						-- Link [tblDog].[dog_DogID]
							,T3.prl_PersonID  AS  ContactLink						-- Link [tblPerson].[psn_PersonID]
							,NULL  AS  Month__c --filler		--FB-01728
							,NULL AS  Expected_Start_Date__c			--FB-00971
							--,NULL AS  Expected_Start_Time__c		--FB-00970
							,CAST(T1.pd_StartDate AS DATE) AS  Start_Date__c		--FB-00970	
							,CAST(T1.pd_EndDate AS DATE) AS   End_Date__c		--FB-00970	
							--,CAST(T1.pd_StartDate AS TIME) AS  Start_Time__c		--FB-00970	
							--,CAST(T1.pd_EndDate AS TIME) AS   End_Time__c		--FB-00970	
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL  AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS  Department_Requesting__c	
							,NULL AS  Reason_Detail__c		
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Organization__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
							,NULL AS  Expected_End_Time__c		--FB-00970
 					--		,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.pd_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQCrQAO'  AS  RecordTypeId	-- Off Campus	
							,IIF(T1.pd_RelCode = 'PRP', 'Puppy Raising','Breeder Custodian') AS  Reason__c	-- Foster Care	
							,T1.pd_RelationID AS zrefID
							,'tblPersonDog' AS zrefSrc
							

							FROM GDB_KADE_Final.dbo.tblPersonDog AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDogRel AS T2 ON T2.drl_RelationID=T1.pd_DogRelID
							LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_RelationID=T1.pd_PersonRelID 
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON t4.psn_PersonID = T3.prl_PersonID
							WHERE T1.pd_RelCode IN ('PRP', 'BRD') AND T1.pd_CoRel='PRI'
							  ORDER BY [ContactLink]
							

END --tc1:100826   TC@: 139,063 Final 143,850


BEGIN-- AUDIT
	
			SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_LOCATION 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_DOG_LOCATION 
			GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
  		--0
			SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_LOCATION 
			GROUP BY zrefSrc
			SELECT time__c, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_LOCATION 
			GROUP BY time__c
			 
			  
END 


--salesforce datetime format '2017-07-05T16:31:00.000Z'
			--	             '2002-02-25T12:30:00.000Z'
	 SELECT DISTINCT
	 T1.rec_FromDate, t1.rec_Time,  FORMAT(T1.rec_Time,'yyyy/mm/dd hh:mm tt') datetimetest,
 	 LEFT(CAST(CAST(T1.rec_FromDate AS DATE )AS NVARCHAR(20)) +'T'+  CAST(CAST(T1.rec_Time AS TIME) AS NVARCHAR(20)),23) +'Z' AS  Time__c	
 	 FROM GDB_KADE_Final.dbo.tblRecall AS T1
	 WHERE rec_Time IS NOT NULL 


 





 