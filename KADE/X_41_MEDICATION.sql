USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVetMedication
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_MEDICATION
	SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblVetMedication  --777,923
END 

BEGIN -- CREATE IMP 
--Step 1
					SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,[Name] = T3.ii_Description
							,'VDM-'+CAST(T1.vdm_MedID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'vdm'+[vdm_MedID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,T2.vdl_DogID AS [Dog__r:Legacy_ID__c]
							--,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) AS  [Medicine__r:Legacy_Id__c]		-- trefGDBInventroyItems.ii_ItemID
							,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) + '_Iid-'+CAST(T3.ii_ItemID  AS NVARCHAR(30))+ IIF(t4.iid_Campus='0','SR', 'OR') AS  [Inventory__r:Legacy_ID__c] -- Yes/Link trefGDBInvItemsDetail.iid_ItemDetailID
							,T1.vdm_Quantity  AS  Quantity__c		
							,T1.vdm_Directions  AS  Direction__c		
							,CAST(T1.vdm_CreatedDate AS DATE) AS CreatedDate
							,T1.vdm_MedID AS zrefMedID
							,T2.vdl_Facility
						
				 	--INTO GDB_Final_migration.dbo.stg_MEDICATION  -- drop table GDB_Final_migration.dbo.stg_MEDICATION
					FROM GDB_KADE_Final.dbo.tblVetMedication AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdm_VetEntryID
					LEFT JOIN GDB_KADE_Final.dbo.trefGDBInventroyItems AS T3 ON T3.ii_ItemID=T1.vdm_MedCode
					LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvItemsDetail AS T4 ON T3.ii_ItemID=t4.iid_ItemID
					                   
					WHERE T2.vdl_Facility = t4.iid_Campus --iid_Campus

--Step 2
					SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,[Name] = T3.ii_Description
							,'VDM-'+CAST(T1.vdm_MedID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'vdm'+[vdm_MedID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,T2.vdl_DogID AS DogLink
							,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) + '_Iid-'+CAST(T3.ii_ItemID  AS NVARCHAR(30))+ 'SR' AS  [Inventory__r:Legacy_ID__c] -- Yes/Link trefGDBInvItemsDetail.iid_ItemDetailID
							,T1.vdm_Quantity  AS  Quantity__c		
							,T1.vdm_Directions  AS  Direction__c		
							,CAST(T1.vdm_CreatedDate AS DATE) AS CreatedDate
							,T1.vdm_MedID AS zrefMedID
							
				 	INTO GDB_Final_migration.dbo.IMP_MEDICATION  -- drop table GDB_Final_migration.dbo.IMP_MEDICATION
					FROM GDB_KADE_Final.dbo.tblVetMedication AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdm_VetEntryID
					LEFT JOIN GDB_KADE_Final.dbo.trefGDBInventroyItems AS T3 ON T3.ii_ItemID=T1.vdm_MedCode
					LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvItemsDetail AS T4 ON T3.ii_ItemID=t4.iid_ItemID
					LEFT JOIN GDB_Final_migration.dbo.stg_MEDICATION AS T5 ON T1.vdm_MedID = t5.zrefMedID
					WHERE T5.zrefMedID IS NULL
					

					UNION
					SELECT T1.OwnerId, T1.[Name], t1.Legacy_ID__c, T1.[Vet_Record__r:Legacy_Id__c], T1.DogLink, T1.[Inventory__r:Legacy_ID__c]
					,T1.Quantity__c , T1.Direction__c, T1.CreatedDate, T1.zrefMedID
					FROM GDB_Final_migration.dbo.IMP_MEDICATION AS T1
		 
 

END 

BEGIN-- AUDIT


	SELECT * FROM   GDB_Final_migration.dbo.IMP_MEDICATION
	SELECT COUNT(*) FROM   GDB_Final_migration.dbo.IMP_MEDICATION
	SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblVetMedication



	--duplicate check
	SELECT * 
		INTO GDB_Final_migration.dbo.stg_DUPLICATES_medication  --Drop table GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems
		FROM GDB_Final_migration.dbo.IMP_MEDICATION2
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_MEDICATION2
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 

			SELECT COUNT(*) FROM GDB_Final_migration.dbo.stg_DUPLICATES_medication
			WHERE [vdl_Facility]='3'

			SELECT * FROM GDB_Final_migration.[dbo].[stg_DUPLICATES_medication]

END 