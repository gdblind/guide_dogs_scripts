USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVetMedication
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_MEDICATION
	SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblVetMedication  --TC2: 777,923  --775865 linked to invertory tbls
END 

--Step 1
					SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'VDM-'+CAST(T1.vdm_MedID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'vdm'+[vdm_MedID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,T2.vdl_DogID AS [Dog__r:Legacy_ID__c]
							--,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) AS  [Medicine__r:Legacy_Id__c]		-- trefGDBInventroyItems.ii_ItemID
							--,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) + '_Iid-'+CAST(T3.ii_ItemID  AS NVARCHAR(30))+ IIF(t4.iid_Campus='0','SR', 'OR') AS  [Inventory__r:Legacy_ID__c] -- Yes/Link trefGDBInvItemsDetail.iid_ItemDetailID
							,T1.vdm_Quantity  AS  Quantity__c		
							,T1.vdm_Directions  AS  Direction__c		
							,CAST(T1.vdm_CreatedDate AS DATE) AS CreatedDate
							,T1.vdm_MedID AS zrefMedID
							,T2.vdl_Facility AS zrefFacility_Campus
							,T1.vdm_MedCode AS zrefGDBInvItemID
							,CONVERT(VARCHAR(10),T2.vdl_TreatmentDate,101) AS Date_Prescribed__c
						
				 	INTO GDB_Final_migration.dbo.stg_MEDICATION  -- drop table GDB_Final_migration.dbo.stg_MEDICATION
					FROM GDB_KADE_Final.dbo.tblVetMedication AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdm_VetEntryID

					--COUNT TC2: 777,923


-- step 2		
				SELECT DISTINCT T1.Legacy_ID__c, T1.OwnerId
				, T1.Quantity__c, T1.Direction__c, T1.CreatedDate
				,T1.[Vet_Record__r:Legacy_Id__c]
				,T1.[Dog__r:Legacy_ID__c]
				,t2.Legacy_ID__c AS [Inventory__r:Legacy_ID__c]
				,T2.[NAME]
				,T1.zrefFacility_Campus
				,t2.zrefCampus
				,T1.zrefMedID
				,T1.Date_Prescribed__c

				INTO GDB_Final_migration.dbo.stg_MEDICATION2 --drop table GDB_Final_migration.dbo.stg_MEDICATION2
				FROM GDB_Final_migration.dbo.stg_MEDICATION AS T1
				INNER JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS T2 ON T2.zrefItemID = T1.zrefGDBInvItemID
				WHERE T2.Source_Table__c='trefGDBInventroyItems'

             --775685  1,057387


--step 3
	SELECT * 
	INTO GDB_Final_migration.dbo.stg_MEDICATION_DUPLICATES   --drop table GDB_Final_migration.dbo.stg_MEDICATION_DUPLICATES
			FROM GDB_Final_migration.dbo.stg_MEDICATION2
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.stg_MEDICATION2
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 
			--563810

--STEP 4


			SELECT t1.*
			INTO GDB_Final_migration.dbo.stg_MEDICATION3 --drop table GDB_Final_migration.dbo.stg_MEDICATION3
			FROM GDB_Final_migration.dbo.stg_MEDICATION_DUPLICATES AS t1
			INNER JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS T2 ON t2.zrefCampus = t1.zrefFacility_Campus AND t2.Legacy_ID__c=t1.[Inventory__r:Legacy_ID__c]
			--257459


--STEP 5
			SELECT t1.*
			INTO GDB_Final_migration.dbo.stg_MEDICATION4 --drop table GDB_Final_migration.dbo.stg_MEDICATION4
			FROM GDB_Final_migration.dbo.stg_MEDICATION2 AS t1
			LEFT JOIN GDB_Final_migration.dbo.stg_MEDICATION_DUPLICATES AS T2 ON t1.Legacy_ID__c=t2.Legacy_ID__c
			WHERE T2.Legacy_ID__c IS NULL
			UNION
			SELECT *
			FROM GDB_Final_migration.dbo.stg_MEDICATION3

			--751036

	--CHECK DUPS
			SELECT * 
			FROM GDB_Final_migration.dbo.stg_MEDICATION4
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.stg_MEDICATION4
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c

--STEP 6 


			SELECT T1.* 
			INTO GDB_Final_migration.dbo.IMP_MEDICATION  --DROP TABLE GDB_Final_migration.dbo.IMP_MEDICATION 
			FROM GDB_Final_migration.dbo.stg_MEDICATION2 AS T1
			LEFT JOIN GDB_Final_migration.dbo.stg_MEDICATION4 AS t2 ON t2.Legacy_ID__c = T1.Legacy_ID__c
			WHERE t2.Legacy_ID__c IS NULL AND T1.zrefCampus='0'
			--ORDER BY T1.Legacy_ID__c
			UNION 

			SELECT *
			FROM GDB_Final_migration.dbo.stg_MEDICATION4

			--775484


--AUDIT
			SELECT T1.* 
			FROM GDB_Final_migration.dbo.stg_MEDICATION AS T1
			LEFT JOIN GDB_Final_migration.dbo.med_test4 AS t2 ON t2.Legacy_ID__c = T1.Legacy_ID__c
			LEFT JOIN GDB_Final_migration.dbo.IMP_INVENTORY AS t3 ON t3.zrefItemID=T1.zrefMedID
			WHERE t2.Legacy_ID__c IS NULL AND t3.zrefItemID IS NULL  --AND T1.zrefCampus='0'

			SELECT COUNT(*) FROM GDB_Final_migration.dbo.med_test
			SELECT COUNT(*) FROM GDB_Final_migration.dbo.med_test4

			775484
