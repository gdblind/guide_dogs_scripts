USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblNursingIntake
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%medi%'

0121L000001UQD7QAO	Nursing Intake Form
0121L000001UQD8QAO	Nursing Notes

*/
BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_NURSING_DOCUMENTATION

END 

BEGIN -- CREATE IMP 

					SELECT DISTINCT
						 CASE WHEN X2.ID IS NOT NULL THEN X2.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId   --FB-00059
 						,'MN-'+CAST(T1.mn_MedicalNoteID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'mn-'+[mnMedicalNoteID]	
						,'0121L000001UQD8QAO'  AS  RecordTypeID	-- Nursing Notes
						,X1.ID  AS  Client_Medical_Id__c		 
					 	,CAST(T1.mn_Date AS DATE) AS  Date__c		
						,CONVERT(NVARCHAR, CAST(T1.mn_Time AS TIME),108) AS  Time__c	-- migrate the 'time' hh:mm portion of the data.	
						,T1.mn_Duration  AS  Duration__c		
						--,X2.ID  AS  Signature__c		-- Link [tblStaff].[FileNum] --FB-00059
						,T1.mn_DoctorVisit  AS  Doctor_Visit__c		
						--,T1.mn_Treatment  AS  Treatment__c		
						,T1.mn_Notes  AS  Notes__c		
						,CASE T1.mn_ChartError WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS  Chart_Error__c		
						,NULL AS Blood_Sugar__c --filler
						,NULL AS Insulin__c --filler
						--,NULL AS Remarks__c --filler 

						--filler
						,NULL AS Nickname__c ,NULL AS Physica_Name__c ,NULL AS Physican_Phone__c ,NULL AS Insurance__c
						,NULL AS Emergency_Contact__c ,NULL AS Emergency_Phone__c ,NULL AS Emergency_Relationship__C ,NULL AS COB__c
						,NULL AS Height__c ,NULL AS Weight__c ,NULL AS Current_Medicine_Dosage_Frequency__c,NULL AS Allergies_Medicine__c
						,NULL AS Allergies_Food__c ,NULL AS Allergies_Other__c ,NULL AS Diet__c ,NULL AS Activity_Level__c
						,NULL AS Hearing_Impairment__c ,NULL AS Smoker__c ,NULL AS Plan_Of_Care__c ,NULL AS Primary_Diagnoses__c
						,NULL AS Health_History__c ,NULL AS HH_Neurological__c ,NULL AS HH_Seizure__c ,NULL AS HH_Headache__c
						,NULL AS HH_Endocrine__c ,NULL AS HH_Diabetes__c ,NULL AS HH_HbA1C__c ,NULL AS HH_Cardio__c
						,NULL AS HH_Pulmonary__c ,NULL AS HH_SOB__c ,NULL AS HH_Gastrointestinal__c ,NULL AS HH_Kidney__c
						,NULL AS HH_MSKL__c ,NULL AS HH_Integumentary__c ,NULL AS HH_Mental_Health__c ,NULL AS HH_Substance_Use__c
						,NULL AS HH_Immuno_Status__c ,NULL AS HH_Infectious_Disease__c ,NULL AS CD_Neurological__c ,NULL AS CD_Seizure__c
						,NULL AS CD_Headache__c ,NULL AS CD_Endocrine__c ,NULL AS CD_Diabetes__c ,NULL AS CD_HbA1C__c
						,NULL AS CD_Cardio__c ,NULL AS CD_Pulmonary__c ,NULL AS CD_SOB__c ,NULL AS CD_Gastrointestinal__c
						,NULL AS CD_Kidney__c ,NULL AS CD_MSKL__c ,NULL AS CD_Integumentary__c ,NULL AS CD_Mental_Health__c
						,NULL AS CD_Substance_Use__c ,NULL AS CD_Immuno_Status__c ,NULL AS CD_Infectious_Disease__c ,NULL AS Surgeries__c
						,NULL AS Balance__c ,NULL AS Assistance__c ,NULL AS Sensory__c ,NULL AS Last_Visit_Provider__c
						,NULL AS Hospitalization__c ,NULL AS Interview_Notes__c ,NULL AS Check_Insurance__c ,NULL AS [2_Pair_Of_Shoes__c] --FB-00663
						,NULL AS Check_Cloths_1__c ,NULL AS Weather_Appropriate_Clothing__c ,NULL AS Check_Massage__c ,NULL AS Check_Medicine__c
						,NULL AS Contact_Nursing_Updates__c ,NULL AS Check_Updated__c ,NULL AS Diabetic_Over_Ride__c
						,T1.mn_Category1 AS Category_1__c --FB-00632
						,T1.mn_Category2 AS Category_2__c
						,T1.mn_Category3 AS Category_3___c
						,T1.mn_Category4 AS Category_4__c
						,T1.mn_Treatment1 AS Treatment_1__c
						,T1.mn_Treatment2 AS Treatment_2__c
						,T1.mn_Treatment3 AS Treatment_3___c
						,T1.mn_Treatment4 AS Treatment_4__c

						--reference	
 						,T1.mn_MedicalNoteID AS zrefID
						,'tblNursingMedicalNotes' AS zrefSrc
 					INTO GDB_Final_migration.DBO.IMP_NURSING_DOCUMENTATION
					FROM GDB_KADE_Final.dbo.tblNursingMedicalNotes AS T1
					LEFT JOIN (SELECT * FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds WHERE SrcTbl='tblNursingMedicalNotes') AS T2
								ON T2.MedicalChartLegacyId = T1.mn_MedicalNoteID
					LEFT JOIN GDB_Final_migration.DBO.XTR_CLIENT_MEDICAL_ID AS X1 ON X1.[Name1]=T2.Client_Medical_ID__c
					LEFT JOIN GDB_Final_migration.DBO.XTR_USER AS X2 ON X2.ADP__C=T1.mn_Signature
				UNION 
					SELECT DISTINCT
						 CASE WHEN X2.ID IS NOT NULL THEN X2.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  --FB-00059
 						,'DN-'+CAST(T1.dn_DiabeticNoteID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'dn-'+[dn_DiabeticNoteID]	
						,'0121L000001UQD8QAO'  AS  RecordTypeID	-- Nursing Notes	
						,X1.ID  AS  Client_Medical_Id__c	
						,CAST(T1.dn_Date AS DATE) AS  Date__c	-- migrate only the Date portion of the date/time field	
						,CONVERT(NVARCHAR, CAST(T1.dn_Time AS TIME),108) AS  Time__c  -- migrate only the Time portion of the date/time field.	--FB-00596
						,NULL AS Duration__c --filler
						--,X2.ID  AS  Signature__c		-- Link [tblStaff].[FileNum]  --FB-00059
						,NULL AS Doctor_Visit__c
					--	,NULL AS Treatment__c
						,T1.dn_Remarks AS Notes__c
						,NULL AS Chart_Error__c
						,T1.dn_BloodSugar  AS  Blood_Sugar__c		
						,T1.dn_Insulin  AS  Insulin__c		
						--,T1.dn_Remarks  AS  Remarks__c		
						
						--filler
						,NULL AS Nickname__c ,NULL AS Physica_Name__c ,NULL AS Physican_Phone__c ,NULL AS Insurance__c
						,NULL AS Emergency_Contact__c ,NULL AS Emergency_Phone__c ,NULL AS Emergency_Relationship__C ,NULL AS COB__c
						,NULL AS Height__c ,NULL AS Weight__c ,NULL AS Current_Medicine_Dosage_Frequency__c,NULL AS Allergies_Medicine__c
						,NULL AS Allergies_Food__c ,NULL AS Allergies_Other__c ,NULL AS Diet__c ,NULL AS Activity_Level__c
						,NULL AS Hearing_Impairment__c ,NULL AS Smoker__c ,NULL AS Plan_Of_Care__c ,NULL AS Primary_Diagnoses__c
						,NULL AS Health_History__c ,NULL AS HH_Neurological__c ,NULL AS HH_Seizure__c ,NULL AS HH_Headache__c
						,NULL AS HH_Endocrine__c ,NULL AS HH_Diabetes__c ,NULL AS HH_HbA1C__c ,NULL AS HH_Cardio__c
						,NULL AS HH_Pulmonary__c ,NULL AS HH_SOB__c ,NULL AS HH_Gastrointestinal__c ,NULL AS HH_Kidney__c
						,NULL AS HH_MSKL__c ,NULL AS HH_Integumentary__c ,NULL AS HH_Mental_Health__c ,NULL AS HH_Substance_Use__c
						,NULL AS HH_Immuno_Status__c ,NULL AS HH_Infectious_Disease__c ,NULL AS CD_Neurological__c ,NULL AS CD_Seizure__c
						,NULL AS CD_Headache__c ,NULL AS CD_Endocrine__c ,NULL AS CD_Diabetes__c ,NULL AS CD_HbA1C__c
						,NULL AS CD_Cardio__c ,NULL AS CD_Pulmonary__c ,NULL AS CD_SOB__c ,NULL AS CD_Gastrointestinal__c
						,NULL AS CD_Kidney__c ,NULL AS CD_MSKL__c ,NULL AS CD_Integumentary__c ,NULL AS CD_Mental_Health__c
						,NULL AS CD_Substance_Use__c ,NULL AS CD_Immuno_Status__c ,NULL AS CD_Infectious_Disease__c ,NULL AS Surgeries__c
						,NULL AS Balance__c ,NULL AS Assistance__c ,NULL AS Sensory__c ,NULL AS Last_Visit_Provider__c
						,NULL AS Hospitalization__c ,NULL AS Interview_Notes__c ,NULL AS Check_Insurance__c ,NULL AS [2_Pair_Of_Shoes__c] --FB-00663
						,NULL AS Check_Cloths_1__c ,NULL AS Weather_Appropriate_Clothing__c ,NULL AS Check_Massage__c ,NULL AS Check_Medicine__c
						,NULL AS Contact_Nursing_Updates__c ,NULL AS Check_Updated__c ,NULL AS Diabetic_Over_Ride__c
						,NULL AS Category_1__c, NULL AS Category_2__c, NULL AS Category_3___c, NULL AS Category_4__c
						,NULL AS Treatment_1__c, NULL AS Treatment_2__c, NULL AS Treatment_3___c, NULL AS Treatment_4__c
						--reference
						,T1.dn_DiabeticNoteID AS zrefID
						,'tblNursingDiabeticNotes' AS zrefSrc
 					FROM GDB_KADE_Final.dbo.tblNursingDiabeticNotes AS T1
					LEFT JOIN (SELECT * FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds WHERE SrcTbl='tblNursingDiabeticNotes') AS T2
								ON T2.MedicalChartLegacyId = T1.dn_DiabeticNoteID
					LEFT JOIN GDB_Final_migration.DBO.XTR_CLIENT_MEDICAL_ID AS X1 ON X1.[Name1]=T2.Client_Medical_ID__c
					LEFT JOIN GDB_Final_migration.DBO.XTR_USER AS X2 ON X2.ADP__C=T1.dn_Signature

				UNION 
					SELECT DISTINCT
						 GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 						,'NI-'+CAST(T1.ni_NursingIntakeID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'ni-'+[ni_NursingIntakeID]	
						,'0121L000001UQD7QAO'  AS  RecordTypeId	-- Intake Form
						,X1.ID  AS  Client_Medical_Id__c	
						,NULL AS Date__c--filler
						,NULL AS Time__c		--filler
						,NULL AS Duration__c		--filler
						--,NULL AS Signature__c		--filler
						,NULL AS Doctor_Visit__c		--filler
						--,NULL AS Treatment__c		--filler
						,NULL AS Notes__c		--filler
						,NULL AS Chart_Error__c		--filler
						,NULL AS Blood_Sugar__c		--filler
						,NULL AS Insulin__c --filler
						--,NULL AS Remarks__c --filler 
					
						,T1.ni_Nickname  AS  Nickname__c		
						,T1.ni_PhysicianName  AS  Physica_Name__c		
						,T1.ni_PhysicianPhone  AS  Physican_Phone__c		
						,T1.ni_Insurance  AS  Insurance__c		
						,T1.ni_EmerContact  AS  Emergency_Contact__c 		
						,T1.ni_EmerPhone  AS  Emergency_Phone__c		
						,T1.ni_EmerRelationship  AS  Emergency_Relationship__c		
						,T1.ni_COB  AS  COB__c		
						,T1.ni_Height  AS  Height__c		
						,T1.ni_Weight  AS  Weight__c		
						,T1.ni_CurrentMeds  AS  Current_Medicine_Dosage_Frequency__c	
						,T1.ni_AllergiesMeds  AS  Allergies_Medicine__c		
						,T1.ni_AllergiesFoods  AS  Allergies_Food__c		
						,T1.ni_AllergiesOther  AS  Allergies_Other__c		
						,T1.ni_Diet  AS  Diet__c		
						,T1.ni_ActivityLevel  AS  Activity_Level__c		
						,T1.ni_HearingImprmt  AS  Hearing_Impairment__c		
						,T1.ni_Smoker  AS  Smoker__c		
						,T1.ni_PlanOfCare  AS  Plan_Of_Care__c		
						,T1.ni_PrimaryDiag  AS  Primary_Diagnoses__c		
						,T1.ni_HealthHist  AS  Health_History__c		
						,T1.ni_HH_Neurological  AS  HH_Neurological__c		
						,T1.ni_HH_Seizure  AS  HH_Seizure__c		
						,T1.ni_HH_Headache  AS  HH_Headache__c		
						,T1.ni_HH_Endocrine  AS  HH_Endocrine__c		
						,T1.ni_HH_Diabetes  AS  HH_Diabetes__c		
						,T1.ni_HH_HbAiC  AS  HH_HbA1C__c		
						,T1.ni_HH_Cardio  AS  HH_Cardio__c		
						,T1.ni_HH_Pulmonary  AS  HH_Pulmonary__c		
						,T1.ni_HH_SOB  AS  HH_SOB__c		
						,T1.ni_HH_GI  AS  HH_Gastrointestinal__c		
						,T1.ni_HH_Kidney  AS  HH_Kidney__c		
						,T1.ni_HH_MSKL  AS  HH_MSKL__c		
						,T1.ni_HH_Integumentary  AS  HH_Integumentary__c		
						,T1.ni_HH_MentalHealth  AS  HH_Mental_Health__c		
						,T1.ni_HH_SubstanceUse  AS  HH_Substance_Use__c		
						,T1.ni_HH_ImmunoStatus  AS  HH_Immuno_Status__c		
						,T1.ni_HH_InfectiousDisease  AS  HH_Infectious_Disease__c		
						,T1.ni_CD_Neurological  AS  CD_Neurological__c		
						,T1.ni_CD_Seizure  AS  CD_Seizure__c		
						,T1.ni_CD_Headache  AS  CD_Headache__c	
						,T1.ni_CD_Endocrine  AS  CD_Endocrine__c		
						,T1.ni_CD_Diabetes  AS  CD_Diabetes__c		
						,T1.ni_CD_HbAiC  AS  CD_HbA1C__c		
						,T1.ni_CD_Cardio  AS  CD_Cardio__c		
						,T1.ni_CD_Pulmonary  AS  CD_Pulmonary__c		
						,T1.ni_CD_SOB  AS  CD_SOB__c		
						,T1.ni_CD_GI  AS  CD_Gastrointestinal__c		
						,T1.ni_CD_Kidney  AS  CD_Kidney__c		
						,T1.ni_CD_MSKL  AS  CD_MSKL__c		
						,T1.ni_CD_Integumentary  AS  CD_Integumentary__c		
						,T1.ni_CD_MentalHealth  AS  CD_Mental_Health__c		
						,T1.ni_CD_SubstanceUse  AS  CD_Substance_Use__c		
						,T1.ni_CD_ImmunoStatus  AS  CD_Immuno_Status__c		
						,T1.ni_CD_InfectiousDisease  AS  CD_Infectious_Disease__c		
						,T1.ni_Surgeries  AS  Surgeries__c		
						,T1.ni_Balance  AS  Balance__c		
						,T1.ni_Assistance  AS  Assistance__c		
						,T1.ni_Sensory  AS  Sensory__c		
						,T1.ni_LastVisitProvider  AS  Last_Visit_Provider__c		
						,T1.ni_Hospitalization  AS  Hospitalization__c		
						,T1.ni_InterviewNotes  AS  Interview_Notes__c		
						,CASE T1.ni_chkInsurance WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS  Check_Insurance__c		
						,CASE T1.ni_chkCloths WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  [2_Pair_Of_Shoes__c]		--FB-00663
						,CASE T1.ni_chkCloths1 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Check_Cloths_1__c		
						,CASE T1.ni_chkCloths2 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Weather_Appropriate_Clothing__c	--FB-00663	
						,CASE T1.ni_chkMassage WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Check_Massage__c		
						,CASE T1.ni_chkMeds WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Check_Medicine__c		
						,CASE T1.ni_chkChanges WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Contact_Nursing_Updates__c		--FB-00663
						,CASE T1.ni_chkUpdates WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Check_Updated__c		
						,CASE T1.ni_DiabeticOverRide WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Diabetic_Over_Ride__c
						,NULL AS Category_1__c, NULL AS Category_2__c, NULL AS Category_3___c, NULL AS Category_4__c
						,NULL AS Treatment_1__c, NULL AS Treatment_2__c, NULL AS Treatment_3___c, NULL AS Treatment_4__c
						
						--reference
						,T1.ni_NursingIntakeID AS zrefID
						,'tblNursingIntake' AS zrefSrc
  					FROM GDB_KADE_Final.dbo.tblNursingIntake AS T1
					LEFT JOIN (SELECT * FROM GDB_Final_migration.dbo.stg_tblMedicalChartIds WHERE SrcTbl='tblNursingIntake') AS T2
								ON T2.MedicalChartLegacyId = T1.ni_NursingIntakeID
					LEFT JOIN GDB_Final_migration.DBO.XTR_CLIENT_MEDICAL_ID AS X1 ON X1.[Name1]=T2.Client_Medical_ID__c
					 
--final 19,413

END 

BEGIN-- AUDIT

 
	SELECT * FROM GDB_Final_migration.dbo.IMP_NURSING_DOCUMENTATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_NURSING_DOCUMENTATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_NURSING_DOCUMENTATION
			GROUP BY zrefSrc
		 
END 
 

 SELECT TOP 10 * FROM GDB_Final_migration.dbo.IMP_NURSING_DOCUMENTATION

 SELECT t.Legacy_ID__c,t.Treatment_4__c  FROM GDB_Final_migration.dbo.IMP_NURSING_DOCUMENTATION AS t 
 WHERE t.Treatment_4__c IS NOT NULL




























