USE GDB_Final_migration
GO


BEGIN--maps
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblTRNBRDEvl
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXXXXXXXX%'
END 

--record type SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%dog%' 

BEGIN -- drop DOG EVALUATION  
	DROP TABLE GDB_Final_migration.dbo.IMP_DOG_EVALUATION
END 

BEGIN -- drop IMP DOG EVALUATION

							SELECT   
							 'Bev-'+CAST(T1.bev_BRDEvlID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Bev-]+[bev_BRDEvalID]	
							,X1.ID  AS  OwnerId		-- Line [tblStaff].[FileNum]
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,CAST(T1.bev_EvalDate AS DATE) AS  Date__c		
							--,T1.bev_Float  AS  Float__c		
							,T1.bev_Recommend  AS  Recommend__c		
							,T1.bev_BodySense  AS  Body__c		
							,T1.bev_NeckSense  AS  Neck__c		
							,T1.bev_HarnessSense  AS  Harness__c		
							,T1.bev_FoodPeopScore  AS  Food_Peop_Score__c		
							,T1.bev_FoodDogsScore  AS  Food_Dogs_Score__c		
							,T1.bev_FoodPeopReactVocal  AS  Food_People_React_Vocal__c		
							,T1.bev_FoodPeopReactBites  AS  Food_People_React_Bites__c		
							,T1.bev_FoodDogReactVocal  AS  Food_Dog_React_Vocal__c		
							,T1.bev_FoodDogReactBites  AS  Food_Dog_React_Bites__c		
							,T1.bev_FoodDogTriestoSteal  AS  Food_Dog_Tries_to_Steal__c		
							,T1.bev_Grates  AS  Grates__c		
							,T1.bev_Stairs  AS  Stairs__c		
							,T1.bev_SlickFloors  AS  Slick_Floors__c		
							,T1.bev_Elevation  AS  Elevation__c		
							,T1.bev_Elevators  AS  Elevators__c		
							,T1.bev_SurpriseNoise  AS  Surprise_Noise__c		
							,T1.bev_Thunder  AS  Thunder__c		
							,T1.bev_Children  AS  Children__c		
							,T1.bev_Odd  AS  Odd__c		
							,T1.bev_NormalPerson  AS  Normal_Person__c		
							,T1.bev_Moving  AS  Moving__c		
							,T1.bev_Stationary  AS  Stationary__c		
							,T1.bev_Traffic  AS  Traffic__c		
							,T1.bev_Scavenging  AS  Scavenging__c		
							,T1.bev_Odors  AS  Odors__c		
							,T1.bev_CrateConfinement  AS  Crate_Confinement__c		
							,T1.bev_VehicleRiding  AS  Vehicle_Riding__c		
							,T1.bev_Dogs  AS  Dogs__c		
							,T1.bev_EatingScore  AS  Eating_Score__c		
							,T1.bev_AdjustKennel  AS  Adjust_Kennel__c		
							,T1.bev_FilthEating  AS  Filth_Eating__c		
							,T1.bev_RoomateAdjust  AS  Roommate_Adjust__c		
							,T1.bev_InitialCF  AS  CF__c		
							,T1.bev_AttitudeScore  AS  Attitude_Score__c		
							,T1.bev_Aloof  AS  Aloof__c		
							,T1.bev_Sulky  AS  Sulky__c		
							,T1.bev_OverlySolicitous  AS  Overly_Solicitous__c		
							,T1.bev_Confidence  AS  Confidence__c		
							,T1.bev_EnergyLevel  AS  Energy_Level__c		
							,T1.bev_DistractionLevel  AS  Distraction_Level__c		
							,T1.bev_Hormonal  AS  Hormonal__c		
							,T1.bev_RelievesHarness  AS  Relieves_Harness__c		
							,T1.bev_EaseHandling  AS  Ease_Handling__c		
							,T1.bev_Comments  AS  Comments__c		
						 	,X1.ADP__C
							INTO GDB_Final_migration.dbo.IMP_DOG_EVALUATION
							
							FROM GDB_KADE_Final.dbo.tblTRNBRDEvl AS T1
							LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.bev_EvaluatorID
							INNER JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.bev_DogID
							

END --tc1: 1703 TC2: 1754  Final: 1838
                                 
								 
SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_EVALUATION  
                






