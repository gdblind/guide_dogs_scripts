
			 SELECT  
				ParentId =  X1.ID  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				,Title = 'Dog Acc Notes: '+  CAST(CAST(T1.an_Date AS  DATE) AS NVARCHAR(30))  + '.  Entered by: '+ T1.an_EnterBy
				,Body = CAST(T1.an_Note	 AS NVARCHAR(MAX))

				,T1.an_AcctNoteID AS zrefLegacyId
				,'tblDogAcctNotes' zrefSrc
			INTO GDB_TC2_migration.dbo.IMP_BREEDING_COMMENT
			FROM dbo.tblDogAcctNotes AS T1
			INNER JOIN GDB_TC2_migration.dbo.XTR_DOG AS X1 ON X1.Legacy_Id__c=T1.an_DogID