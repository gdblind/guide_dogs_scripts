BEGIN
DROP TABLE GDB_Final_migration.dbo.IMP_FUND_PROCEDURE

END 

BEGIN
		SELECT  DISTINCT 
				 
				 GDB_Final_migration.[dbo].[fnc_OwnerId]()  AS OwnerId
				,'Pfps-'+ CAST(T.pfps_ProcSelID AS NVARCHAR(30)) AS Legacy_Id__c
	 			,'Pfa-'+CAST(T.pfps_AuthID AS NVARCHAR(30)) AS [Case__r:Legacy_Id__c]
				,T2.pf_MainCategory AS Main_Category__c
				,T2.pf_Category AS Category__c
				,T2.pf_SubCategory AS Sub_Category__c
			
			
			
			INTO GDB_Final_migration.dbo.IMP_FUND_PROCEDURE	
 			FROM GDB_KADE_Final.DBO.tblPrattFundProcSelect AS T 
			INNER JOIN GDB_KADE_Final.dbo.trefPrattFundAuthProc AS T2 ON T.pfps_ProcID=T2.pf_CatID
			
END 	
		

		SELECT COUNT(*) FROM GDB_KADE_Final.DBO.tblPrattFundProcSelect --tc2: 584

		SELECT * FROM GDB_Final_migration.dbo.IMP_FUND_PROCEDURE
