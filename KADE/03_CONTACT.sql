USE GDB_Final_migration
GO 

BEGIN--Map: CONTACT   
 			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblVolCalDocents 
			WHERE   SF_Object LIKE '%contac%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
			FROM GDB_Final_migration.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%contact%'
END 

--RECORDTYPE
	-- SELECT * FROM GDB_Final_migration.dbo.xtr_record_type ORDER BY sobjecttype
	--	0121L000001UQCpQAO	Household	Contact
	--	0121L000001UQCqQAO	Organization	Contact

BEGIN-- DROP IMP_CONTACT

	DROP TABLE GDB_Final_migration.dbo.IMP_CONTACT

END 


BEGIN--CREATE IMP_CONTACT
  
		--PRIMARY CONTACT FROM HOUSEHOLD 
			USE GDB_Final_migration
		 

			SELECT 
				IIF([T6].[ad_PersonID] IS NOT NULL, t6.Onwer1, GDB_Final_migration.[dbo].[fnc_OwnerId]()) AS OwnerID
				,CAST(T1.zrefAccountId AS NVARCHAR(30)) AS [Account:Legacy_ID__c]
				,CAST(T1.psn_PersonID  AS NVARCHAR(30)) AS  Legacy_ID__c	
				--dnc-- formula field. ,CASE WHEN T1.psn_PersonID=T1.zrefAccountID THEN 'TRUE' ELSE 'FALSE' END npsp__Primary_Contact__c
				,CASE T1.psn_GradEmp WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Grad_Employee__c	
				,T1.zrefSalutation  AS  Salutation				 
				,dbo.Proper(CASE WHEN T1.zrefFirstName IS NOT NULL THEN T1.zrefFirstName ELSE T1.psn_First END) AS  FirstName				-- Parse if contains ' & ' or ' and ' and Create two Contacts. 
				,dbo.Proper(T1.psn_Middle) AS  Middle_Name__c	
				,dbo.Proper(T1.psn_Last) AS  LastName	
				,CASE WHEN T1.zrefRecordType ='HHD' THEN '0121L000001UQCpQAO' WHEN T1.zrefRecordType='ORG' THEN '0121L000001UQCqQAO' END AS RecordTypeID	-- If [psn_First] is null and [psn_Last] no like '%Family%' then Organization elso HouseholD.
				--filler
				,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
				,T1.psn_Suffix  AS  Suffix__C	
				,T1.psn_NickName  AS  Nickname__c	
				,CAST(T1.psn_DOB AS DATE) AS  Birthdate	
				,CASE T1.psn_NoGDNews WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS npsp__Do_Not_Contact__c	
				,CASE T1.psn_SpanishSpeak WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Spanish_Speaker__c	-- Migrate 'Yes' and 'No' values and default everything else to 'No'
				,CASE T1.psn_Sex  WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END AS Gender__c 	
				,T1.psn_JobTitle AS Title
 				,CASE WHEN T1.psn_DateDecease  IS NOT NULL THEN 'TRUE' ELSE 'FALSE' END AS  npsp__Deceased__c 
				,CAST(T1.psn_DateDecease AS DATE) AS Deceased_Date__c	-- If [psn_DateDecease] is not null then npsp__Deceased__c=True, else False  Migrate to Deceased Date as is
				,CASE T1.psn_DontGiveDog WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS   Do_Not_Give_Dog__c	
				,CASE T1.psn_Ebark   WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Ebark__c	
				,CASE T1.psn_DirectBill    WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Direct_Bill__c	
				,CASE T1.psn_MediaPref1  WHEN 'c' THEN NULL WHEN 'Email' THEN 'E-Mail' ELSE T1.psn_MediaPref1 END AS  Media_Preference_1__c	-- DNC 'c' and migrate 'Email' as 'E-mail'
				,T1.psn_MediaPref2  AS  Media_Preference_2__c	
				,T1.psn_MediaPrefAlumni  AS  Media_Preference_3__c	
				,CASE T1.psn_ClassMaterialsPref  WHEN 'email' THEN 'E-Mail' ELSE  T1.psn_ClassMaterialsPref end AS  Class_Materials_Preference__C	
				,CASE T3.[Description] WHEN 'Conf/Conv' THEN 'Conference/Convention' WHEN 'Conf/Conv OAR' THEN 'Conference/Convention OAR' ELSE T3.[Description] END AS  Initial_Contact__c
 				,CASE T1.psn_QualifiedVeteran  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Qualified_Veteran__c	
				,CAST(T1.psn_CreatedDate  AS DATE) AS  CreatedDate	
		 
				,T2.Email	
				,T2.Email_2__c	
				,NULL AS  npe01__WorkEmail__c  --filler from other tbl.
				,NULL AS npe01__HomeEmail__c    --filler from other tbl.
				,NULL AS npe01__Preferred_Email__c --filler from other tbl.
				,T2.Emergency__c	
				,T2.HomePhone	
				,T2.MobilePhone	
				,T2.npe01__WorkPhone__c	
				,T2.OtherPhone	
				,T2.npe01__PreferredPhone__c
				,NULL AS Phone, 
				NULL AS Fax
				,T4.AddressLines AS MailingStreet
				,T4.add_City AS MailingCity
				,T4.add_State AS MailingState
				,T4.add_ZipCode AS MailingPostalCode
				,T4.add_Country AS MailingCountry
				,T40.AddressLines AS OtherStreet
				,T40.add_City AS OtherCity
				,T40.add_State AS OtherState
				,T40.add_ZipCode AS OtherPostalCode
				,T40.add_Country AS OtherCountry


				,NULL AS  [Type__c]  
				,'tblPerson'  AS  Source_Data_Table__c	
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c

				,T5.Client_Medical_ID__c
				,t7.Next_Annual_Visit__c -- FB-01818
				,T1.zrefRecordType
				,CAST(T1.psn_PersonID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.psn_First AS zrefFirstName
				,T1.psn_PersonID AS zrefPersonID
			INTO GDB_Final_migration.dbo.IMP_CONTACT --drop table GDB_Final_migration.dbo.IMP_CONTACT 
			FROM GDB_KADE_Final.DBO.tblPerson AS T1
			LEFT JOIN  [GDB_Final_migration].dbo.stg_Phone_cont_final AS T2 ON T1.psn_PersonID=T2.pdl_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.trefPersonInitialContact AS T3 ON T1.psn_InitialContact=T3.InitialContactID
			LEFT JOIN (SELECT * FROM [GDB_Final_migration].dbo.stg_Address WHERE npsp__Default_Address__c='TRUE')  AS T4
							ON T1.psn_PersonID = T4.ad_PersonID
			LEFT JOIN (SELECT * FROM [GDB_Final_migration].dbo.stg_Address WHERE [ad_AddressCode] ='0')  AS T40
							ON T1.psn_PersonID = T40.ad_PersonID
			LEFT JOIN [GDB_Final_migration].dbo.stg_tblMedicalChartIds AS T5 ON T5.psn_PersonID=T1.psn_PersonID
		
			LEFT JOIN GDB_Final_migration.dbo.stg_ContactOwner AS T6 ON T1.psn_PersonID = t6.ad_PersonID
			LEFT JOIN GDB_Final_migration.dbo.stg_Contact_NextAnnualVisit AS T7 ON T1.psn_PersonID=T7.prl_PersonID
			WHERE T1.zrefRecordType ='HHD'  
			-- test	 AND (T1.psn_Last='falla'  OR t1.zrefAccountid='58')
			-- test AND t1.psn_InitialContact IS NOT NULL 
			--	ORDER BY T1.zrefAccountId, T1.psn_Last, T1.psn_First
			--79037 (HHD)   DNC: ORG (37) 
		 UNION 
		--SECONDARY CONTACT FROM TBLPERSON WHERE FIRSTNAME LIKE '&' OR 'AND' 
 			 
			SELECT 
				IIF([T5].[ad_PersonID] IS NOT NULL, t5.Onwer1, GDB_Final_migration.[dbo].[fnc_OwnerId]()) AS OwnerID
				,CAST(T1.zrefAccountId AS NVARCHAR(30)) AS [Account:Legacy_ID__c]
				,CAST(T1.psn_PersonID AS NVARCHAR(30)) + '-2' AS  Legacy_ID__c	
				--dnc formula field ,'FALSE' AS npsp__Primary_Contact__c   
				,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation			--filler from other tbl
				,T1.zrefSecFirstName AS FirstName
				,NULL AS Middle_Name__c
				,T1.psn_Last AS LastName
				,CASE WHEN T1.zrefRecordType ='HHD' THEN '0121L000001UQCpQAO' WHEN T1.zrefRecordType='ORG' THEN '0121L000001UQCqQAO' END AS RecordTypeID	-- If [psn_First] is null and [psn_Last] no like '%Family%' then Organization elso HouseholD.
				,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
				,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
				,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
				,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
				,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	,CAST(T1.psn_CreatedDate  AS DATE) AS  CreatedDate	
 				,NULL AS Email	,NULL AS Email_2__c	, NULL AS  npe01__WorkEmail__c , NULL AS npe01__HomeEmail__c, NULL AS npe01__Preferred_Email__c, NULL AS Emergency__c	,NULL AS HomePhone	,NULL AS MobilePhone	,NULL AS npe01__WorkPhone__c	
				,NULL AS OtherPhone	,NULL AS npe01__PreferredPhone__c
				,NULL AS Phone, NULL AS Fax
				,T4.AddressLines AS MailingStreet
				,T4.add_City AS MailingCity
				,T4.add_State AS MailingState
				,T4.add_ZipCode AS MailingPostalCode
				,T4.add_Country AS MailingCountry
				,T40.AddressLines AS OtherStreet
				,T40.add_City AS OtherCity
				,T40.add_State AS OtherState
				,T40.add_ZipCode AS OtherPostalCode
				,T40.add_Country AS OtherCountry

				,NULL AS  [Type__c]  
				,'tblPerson'  AS  Source_Data_Table__c
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c

				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818, 
				,T1.zrefRecordType
				,CAST(T1.psn_PersonID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.psn_First AS zrefFirstName
				,T1.psn_PersonID AS zrefPersonID
		
			FROM GDB_KADE_Final.DBO.tblPerson AS T1
		 	LEFT JOIN (SELECT * FROM [GDB_Final_migration].dbo.stg_Address WHERE npsp__Default_Address__c='TRUE')  AS T4
		 				ON T1.psn_PersonID = T4.ad_PersonID
			LEFT JOIN (SELECT * FROM [GDB_Final_migration].dbo.stg_Address WHERE [ad_AddressCode] ='0')  AS T40
							ON T1.psn_PersonID = T40.ad_PersonID
 			LEFT JOIN GDB_Final_migration.dbo.stg_ContactOwner AS T5 ON T1.psn_PersonID = T5.ad_PersonID
			WHERE zrefSecFirstName IS NOT NULL AND  T1.zrefRecordType ='HHD' 
 	
		UNION 
		--CONTACTS FROM tblGDBVendorInfo
			SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
	 			,'iv-' +CAST(T1.ivi_VendorID AS NVARCHAR(30)) AS [Account:Legacy_ID__c]	
				,'iv-' +CAST(T1.ivi_VendorInfoID AS NVARCHAR(30)) AS  Legacy_ID__c
				--dnc formula field ,NULL AS npsp__Primary_Contact__c  --filler 
				,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation			--filler from other tbl
				,T1.ivi_ContactFName  AS  FirstName	
				,NULL AS Middle_Name__c
 				,CASE WHEN T1.ivi_ContactLName   IS NULL THEN 'N/A' ELSE T1.ivi_ContactLName END AS  LastName
				,'0121L000001UQCqQAO' AS  RecordTypeId	
				--fillers
				,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
				,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
				,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
				,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
				,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c 
				,CAST(T1.ivi_CreatedDate  AS DATE) AS  CreatedDate
				,T1.ivi_ContactEmail  AS  Email ,NULL AS Email_2__c	,  T1.ivi_ContactEmail  AS  npe01__WorkEmail__c, NULL AS npe01__HomeEmail__c
				,CASE WHEN T1.ivi_ContactEmail IS NOT NULL THEN 'Work' ELSE NULL END AS npe01__Preferred_Email__c,  NULL AS Emergency__c	,NULL AS HomePhone		
				,CASE WHEN T1.ivi_PhoneType='cell' THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN  ', '+t1.ivi_ContactExt ELSE '' END  ELSE NULL END  AS  MobilePhone	-- migrate to Mobile Phone if [ivi_PhoneType]='Cell'
				,CASE WHEN T1.ivi_PhoneType='Work' OR  T1.ivi_PhoneType='Primary' THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN  ', '+t1.ivi_ContactExt ELSE '' END ELSE NULL END  AS  npe01__WorkPhone__c	-- migrate to Work Phone if [ivi_PhoneType]='Work' or 'Primary'
				,CASE WHEN T1.ivi_PhoneType='Secondary' THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN  ', '+t1.ivi_ContactExt ELSE '' END ELSE NULL END  AS  OtherPhone	-- migrate to Other if [ivi_PhoneType]='Secondary'
				,CASE WHEN T1.ivi_PhoneType='Work' OR ivi_PhoneType='Primary' THEN 'Work' 
													WHEN ivi_PhoneType='Cell' THEN 'Mobile' 
													WHEN ivi_PhoneType='Secondary' THEN 'Other' 
													END AS  npe01__PreferredPhone__c	-- Heirarchy: Work, Mobile, Other based on data
	 			,CASE WHEN T1.ivi_PhoneType='Work' OR ivi_PhoneType='Primary'      THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN ', '+t1.ivi_ContactExt ELSE NULL END 
													WHEN ivi_PhoneType='Cell'      THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN ', '+t1.ivi_ContactExt ELSE NULL END  
													WHEN ivi_PhoneType='Secondary' THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) + CASE WHEN (T1.ivi_ContactExt<>'0' AND T1.ivi_ContactExt IS NOT NULL) THEN ', '+t1.ivi_ContactExt ELSE NULL END 
													END AS  Phone	-- migrate to phone value based on heirarchy: Work, Mobile, Other
  				,CASE WHEN T1.ivi_PhoneType='Fax' THEN [dbo].[fnc_phone_fmt](T1.ivi_ContactPhone) ELSE NULL END AS  Fax	-- migrate to Fax if [ivi_PhoneType]='Fax'
		 		,T2.iv_Address  AS  MailingStreet	
				,T2.iv_City  AS  MailingCity	
				,T2.iv_State  AS  MailingState	
				,T2.iv_ZipCode  AS  MailingPostalCode	
				,T2.iv_Country  AS  MailingCountry	
		 		,null as  OtherStreet	
				,null AS  OtherCity	
				,null AS  OtherState	
				,null AS  OtherPostalCode	
				,null AS  OtherCountry	

				,NULL AS  [Type__c]  
		 		,'tblGDBVendorInfo'  AS  Source_Data_Table__c	-- tblGDBVendorInfotion  AS  Source_Data_Table__c	-- tblGDBVendorInfo
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818,
				,'ORG' AS zrefRecordType
				,CAST(T1.ivi_VendorInfoID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.ivi_ContactFName AS zrefFirstName
				,NULL AS zrefPersonID

			FROM GDB_KADE_Final.DBO.tblGDBVendorInfo AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblGDBVendor AS T2 ON T1.ivi_VendorID=T2.iv_VendorID
		
		UNION 
		--CONTACTS FROM tblContacts
			SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
	   			,'Con-'+CAST(T2.zrefAccountID AS NVARCHAR(30)) AS [Account:Legacy_ID__c]  -- concatenate 'Con-'+[con_ContactID]
				,'Con-'+CAST(T1.con_ContactID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Con-'+[con_ContactID]
				--dnc formula field ,'FALSE' AS npsp__Primary_Contact__c   
				,NULL AS Grad_Employee__c   --filler from other tbl
				,T4.[New Value]  AS  Salutation	-- migrate [New Value] from chart
				,T1.con_NameFirst  AS  FirstName	
				,NULL AS Middle_Name__c
				,T1.con_NameLast  AS  LastName	
				,CASE WHEN T1.zrefRecordType ='HHD' THEN '0121L000001UQCpQAO' WHEN T1.zrefRecordType='ORG' THEN '0121L000001UQCqQAO' END AS RecordTypeID
				--filler
					,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
 				,T1.con_NameSuffix  AS  Suffix__c	
				--fillers
					,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
				,CAST(T1.con_CreatedDate  AS DATE) AS  CreatedDate
				,NULL AS Email	,NULL AS Email_2__c	, NULL AS  npe01__WorkEmail__c  
 	  			,CAST(T1.con_Email AS NVARCHAR(255)) AS  npe01__HomeEmail__c	
 				,CASE WHEN T1.con_Email IS NOT NULL THEN 'Personal' ELSE NULL end AS  npe01__Preferred_Email__c	-- if populated then 'Personal' else blank
				,NULL AS Emergency__c 
				,T1.con_PhoneEvening  AS  HomePhone
				,NULL AS MobilePhone
				,T1.con_PhoneDaytime   AS  npe01__WorkPhone__c	
				,T1.con_PhoneOther  AS  OtherPhone	
				,CASE WHEN T1.con_PhoneDaytime IS NOT NULL THEN 'Work' 
						WHEN T1.con_PhoneEvening IS NOT NULL THEN 'Home' 
						WHEN T1.con_PhoneOther   IS NOT NULL THEN 'Other' ELSE NULL END AS npe01__PreferredPhone__c
				,CASE WHEN T1.con_PhoneDaytime IS NOT NULL THEN   T1.con_PhoneDaytime 
						WHEN T1.con_PhoneEvening IS NOT NULL THEN   T1.con_PhoneEvening 
						WHEN T1.con_PhoneOther   IS NOT NULL THEN   T1.con_PhoneOther   ELSE NULL END AS Phone
				, T1.con_PhoneFax   AS  Fax	
	  			,CASE WHEN T1.con_Address2 IS NOT NULL THEN T1.con_Address1+CHAR(10)+T1.con_Address2 ELSE T1.con_Address1 END AS MailingStreet 	-- Concatenate [con_Address1] and [con_Address2] into Account.BillingStreet with a line break in between
				,T1.con_City  AS  MailingCity	
				,CASE WHEN T1.con_State IS NULL THEN T1.con_CountryRegion ELSE T1.con_State END  AS  MailingState	-- If [con_State] is null migrate [con_CountryRegion] to State/Province
				,CASE WHEN T1.con_ZipCode IS NULL THEN T1.con_CountryCode ELSE T1.con_ZipCode END AS  MailingPostalCode	-- If [con_Zipcode] is null migrate [con_CountryCode] to Zip/Postal Code
				,T1.con_Country  AS  MailingCountry	
		 		,null as  OtherStreet	
				,null AS  OtherCity	
				,null AS  OtherState	
				,null AS  OtherPostalCode	
				,null AS  OtherCountry	

		 	 	,CASE WHEN T1.zrefRecordType='HHD' THEN T3.[Description] ELSE NULL END  AS  [Type__c]  -- Migrate values from [Description] if [con_OrgName] is blank migrate to Contact
				,'tblContacts' AS  Source_Data_Table__c	-- tblContacts
 				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c

				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818,
 				,T1.zrefRecordType
				,CAST(T1.con_ContactID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.con_NameFirst AS zrefFirstName
				,NULL AS zrefPersonID

 				FROM GDB_KADE_Final.dbo.tblContacts AS T1
				LEFT JOIN GDB_Final_migration.dbo.stg_tblContact_Account AS T2 ON T1.zrefAccountName= T2.zrefAccountName
				LEFT JOIN GDB_KADE_Final.dbo.trefContactTypes AS T3 ON T1.con_ContactType=T3.ContactType
				LEFT JOIN GDB_Final_migration.dbo.CHART_ContactPrefix AS T4 ON T1.con_NamePrefix=T4.con_NamePrefix
				WHERE T1.con_NameLast IS NOT NULL 
		UNION
        --CONTACTS from tblVolCalOrgContact
			
			SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
			    ,'Org-'+CAST(T1.vc_OrganizationID AS NVARCHAR(30))	AS [Account:Legacy_ID__c]
			 	,'Vc-'+CAST(T1.vc_ContactID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Vc-'+[vc_ConcatID]
				--dnc formula field ,'FALSE' AS npsp__Primary_Contact__c   
				,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
			 	,T1.vc_FirstName  AS  FirstName	
				,NULL AS Middle_Name__c
				,T1.vc_LastName  AS  LastName	
				,'0121L000001UQCpQAO' AS RecordTypeID --Organization
			 	--fillers
					,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
				,CAST(T1.vc_CreatedDate AS DATE) AS  CreatedDate
				,T1.vc_Email1  AS  Email
				,NULL AS Email_2__c		
				,T1.vc_Email1  AS  npe01__WorkEmail__c	
				,NULL AS npe01__HomeEmail__c
				,CASE WHEN T1.vc_Email1  IS NOT NULL THEN 'Work' ELSE NULL end AS npe01__Preferred_Email__c
				,NULL AS Emergency__c
				,NULL AS HomePhone 
				,NULL AS MobilePhone
				,T1.vc_Phone1  AS  npe01__WorkPhone__c	
				,T1.vc_Phone2  AS  OtherPhone	
				,CASE WHEN T1.vc_Phone1 IS NOT NULL THEN 'Work' ELSE NULL END AS npe01__PreferredPhone__c
				,T1.vc_Phone1  AS  Phone	
				,NULL AS Fax
				,T2.org_Address  AS  MailingStreet	
				,T2.org_City  AS  MailingCity	
				,T2.org_State  AS  MailingState	
				,T2.org_Zip  AS  MailingPostalCode
 				,NULL AS MailingCountry
				,T2.org_Address  AS  OtherStreet	
				,T2.org_City  AS  OtherCity	
				,T2.org_State  AS  OtherState	
				,T2.org_Zip  AS  OtherPostalCode
 				,NULL AS OtherCountry


				,NULL AS Type__c

		  		,'tblVolCalOrgContact'  AS  Source_Data_Table__c	-- tblVolCalOrgContact
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818,
				,'ORG' AS zrefRecordType
				,CAST(T1.vc_ContactID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.vc_FirstName AS zrefFirstName
				,NULL AS zrefPersonID

			 	FROM GDB_KADE_Final.dbo.tblVolCalOrgContact AS T1
				INNER JOIN GDB_KADE_Final.dbo.tblVolCalOrganization AS T2 ON T1.vc_OrganizationID=T2.org_OrganizationID
			UNION 
		--CONTACTS from tblStaff
				
			SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'GDB' AS [Account:Legacy_ID__c]   
				,'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30)) AS  Legacy_ID__c
				--dnc formula field ,'FALSE' AS npsp__Primary_Contact__c 
				,NULL AS Grad_Employee__c   ,NULL AS Salutation
				,T1.FirstName  AS  FirstName
				, NULL AS Middle_Name__c
				,T1.LastName  AS  LastName
			 	,'0121L000001UQCqQAO' AS RecordTypeID --Organization
				
				--fillers
					,NULL AS GW_Volunteers__Volunteer_Skills__c, NULL AS GW_Volunteers__Volunteer_Status__c, NULL AS GW_Volunteers__Last_Volunteer_Date__c
					,NULL AS Suffix__c, NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c
				,CASE T1.Sex WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE NULL END AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
				,CAST(T1.HireDate AS DATE) AS  CreatedDate
				,NULL AS  Email ,NULL AS Email_2__c, NULL  AS  npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c, NULL  AS npe01__Preferred_Email__c
				,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone, NULL AS npe01__WorkPhone__c,NULL  AS  OtherPhone,NULL  AS npe01__PreferredPhone__c
				,NULL AS   Phone ,NULL AS Fax 
				,null  AS  MailingStreet, NULL AS   MailingCity	, NULL  AS  MailingState, NULL  AS  MailingPostalCode ,NULL AS MailingCountry
				,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

				,NULL AS Type__c
		  		,'tblStaff'  AS  Source_Data_Table__c	-- tblStaff
			
		 		,T2.db_CurFloat  AS  Current_Float__c
				,CAST(T2.db_ApprDate AS DATE) AS  Approval_Date__c
				,CAST(T2.db_ApprDateLevII AS DATE)  AS  Approval_Date_Level_1__c
				,CAST(T2.db_ApprDateLevIII AS DATE)  AS  Approval_Date_Level_2__c
				,CAST(T2.db_LicDate AS DATE)  AS  License_Date__c
				,CAST(T2.db_StateLicDate AS DATE)  AS  State_License_Date__c
				,CAST(T2.db_SeniorLicDate AS DATE)  AS  Senior_License_Date__c
				,CAST(T2.db_MasterLicDate AS DATE)  AS  Master_License_Date__c
				,CAST(T2.db_IADOTOMtgAfterHire AS DATE)  AS  IADOTO_Meeting_After_Hire__c
				,CAST(T2.db_AppDOTOMtgAfterBldFold AS DATE)  AS  Applic_DOTO_Meeting_After_Blind_Fold__c
				,T2.db_InHouseWritten  AS  In_House_Written__c
				,T2.db_InHouseOral  AS  In_House_Oral__c
				,T2.db_InHousePractical  AS  In_House_Practical__c
				,T2.db_Presentation  AS  Presentation__c
				,T2.db_FieldTrip1  AS  Field_Trip_1__c
				,T2.db_FieldTrip2  AS  Field_Trip_2__c
				,T2.db_FieldTrip3  AS  Field_Trip_3__c
				,T2.db_HomeInterview  AS  Home_Interview__c
				,T2.db_ProgressSatisfactory  AS  Progress_Satisfactory__c
				,T2.db_1Quiz  AS  [X1_Quiz__c]
				,T2.db_2Quiz  AS  [X2_Quiz__c]
				,T2.db_3Quiz  AS  [X3_Quiz__c]
				,CAST(T2.db_Comments  AS NVARCHAR(MAX)) AS  Comments__c
				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818,
				,'ORG' AS zrefRecordType
				,CAST(T1.FileNum AS NVARCHAR(30)) AS zrefLegacyId
				,T1.FirstName AS zrefFirstName
				,NULL AS zrefPersonID

				FROM GDB_KADE_Final.dbo.tblStaff AS T1
				LEFT JOIN GDB_KADE_Final.dbo.tblInstr AS T2 ON T1.FileNum=T2.db_InstrID
		 UNION
		--tblVolCalDocents
			SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'RE-'+CAST(T1.ConID  AS NVARCHAR(30)) AS [Account:Legacy_ID__c]
				,'RE-'+CAST(T1.ConID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'RE-' [conid]	
				--dnc formula field ,'TRUE' AS npsp__Primary_Contact__c
				,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
				,SUBSTRING(T1.FullName,1,CHARINDEX(' ',T1.FullName,1)-1)  AS  FirstName	-- Parse [FullName]
				,NULL AS Middle_Name__c
				,SUBSTRING(T1.FullName, CHARINDEX(' ',T1.FullName,1)+1, LEN(T1.FullName)-CHARINDEX(' ',T1.FullName,1)) AS LastName -- Parse [FullName]
		 		,'0121L000001UQCpQAO' AS RecordTypeId  --household
		
				,T1.Job  AS  GW_Volunteers__Volunteer_Skills__c	
				,T1.VolType  AS  GW_Volunteers__Volunteer_Status__c	
				,CAST(T1.MaxDate AS DATE)  AS  GW_Volunteers__Last_Volunteer_Date__c
				--fillers
					 
					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
					,NULL AS  CreatedDate
					,NULL AS  Email ,NULL AS Email_2__c, NULL  AS  npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c, NULL  AS npe01__Preferred_Email__c
					,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone, NULL AS npe01__WorkPhone__c,NULL  AS  OtherPhone,NULL  AS npe01__PreferredPhone__c
					,NULL AS   Phone ,NULL AS Fax , NULL  AS  MailingStreet, NULL AS   MailingCity	, NULL  AS  MailingState, NULL  AS  MailingPostalCode ,NULL AS MailingCountry
					,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

					,NULL AS Type__c
		  		,'tblVolCalDocents'  AS  Source_Data_Table__c	-- tblVolCalDocents
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
  				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818
				,'HHD' AS zrefRecordType
				,CAST(T1.ConID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.FullName AS zrefFirstName
				,NULL AS zrefPersonID
	 			FROM GDB_KADE_Final.dbo.tblVolCalDocents AS T1
				
			UNION 
			--trefDogSourceNew
				SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'BRS-'+CAST(T1.brs_SourceID  AS NVARCHAR(30))  AS [Account:Legacy_ID__c]
				,'BRS-'+CAST(T1.brs_SourceID  AS NVARCHAR(30))  Legacy_ID__c	 	
			 	,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
				,T1.brs_FirstName  AS  FirstName	 
				,NULL AS Middle_Name__c
				,T1.brs_LastName AS  LastName
		 		,'0121L000001UQCqQAO' AS RecordTypeID --Organization
		
				--fillers
					,NULL AS  GW_Volunteers__Volunteer_Skills__c , NULL AS  GW_Volunteers__Volunteer_Status__c	,NULL AS  GW_Volunteers__Last_Volunteer_Date__c
 					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
					,NULL AS  CreatedDate
					,NULL AS  Email ,NULL AS Email_2__c 
				,T1.brs_Email AS npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c
				,CASE WHEN T1.brs_Email  IS NOT NULL THEN 'Work' ELSE NULL END  AS npe01__Preferred_Email__c
					,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone
				,T1.brs_Phone  AS npe01__WorkPhone__c
				,T1.brs_Phone2  AS  OtherPhone
				,CASE WHEN T1.brs_Phone IS NOT NULL THEN 'Work' WHEN T1.brs_Phone2 IS NOT NULL THEN 'Other' ELSE NULL END  AS npe01__PreferredPhone__c
					,NULL AS   Phone
				,T1.brs_Fax  AS Fax 
				,T1.brs_Address AS  MailingStreet
				,T1.brs_City AS   MailingCity	
				,T1.brs_State AS  MailingState
				,T1.brs_Zip AS  MailingPostalCode 
				,T1.brs_Country AS MailingCountry
				,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

				,NULL AS Type__c
		  		,'trefDogSourceNew'  AS  Source_Data_Table__c	-- trefDogSourceNew
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
  				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818
				,'HHD' AS zrefRecordType
				,CAST(T1.brs_SourceID AS NVARCHAR(30)) AS zrefLegacyId
				,T1.brs_FirstName AS zrefFirstName
				,NULL AS zrefPersonID

				FROM GDB_KADE_Final.DBO.trefDogSourceNew AS T1
				WHERE T1.brs_LastName IS NOT NULL 
				 
UNION  --FB-01922
	SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'ken-'+CAST(T1.KennelID AS NVARCHAR(30)) AS  [Account:Legacy_ID__c]
				,'ken-'+CAST(T1.KennelID AS NVARCHAR(30)) AS  Legacy_ID__c	 	
			 	,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
				,'Kennel Contact'  AS  FirstName	 
				,NULL AS Middle_Name__c
				,T1.KennelName AS  LastName
		 		,'0121L000001UQCqQAO' AS RecordTypeID --Organization
		
				--fillers
					,NULL AS  GW_Volunteers__Volunteer_Skills__c , NULL AS  GW_Volunteers__Volunteer_Status__c	,NULL AS  GW_Volunteers__Last_Volunteer_Date__c
 					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
					,NULL AS  CreatedDate ,NULL AS  Email ,NULL AS Email_2__c ,NULL AS npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c
				,NULL AS npe01__Preferred_Email__c ,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone
				,NULL  AS npe01__WorkPhone__c ,NULL  AS  OtherPhone ,NULL  AS npe01__PreferredPhone__c ,NULL AS   Phone
				,NULL  AS Fax 
				,NULL AS  MailingStreet
				,NULL AS   MailingCity	
				,NULL AS  MailingState
				,NULL AS  MailingPostalCode 
				,NULL AS MailingCountry
				,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

				,NULL AS Type__c
		  		,'trefKennel'  AS  Source_Data_Table__c	
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
  				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818
				,'ORG' AS zrefRecordType
				,CAST(T1.KennelID AS NVARCHAR(30)) AS zrefLegacyId
				,NULL AS zrefFirstName
				,NULL AS zrefPersonID

				FROM GDB_KADE_Final.DBO.trefKennels AS T1
				
UNION  --FB-01922
	SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'CA' AS  [Account:Legacy_ID__c]
				,'CA' AS  Legacy_ID__c	 	
			 	,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
				,'Kennel Contact'  AS  FirstName	 
				,NULL AS Middle_Name__c
				,'CA Unknown Kennel' AS  LastName
		 		,'0121L000001UQCqQAO' AS RecordTypeID --Organization
		
				--fillers
					,NULL AS  GW_Volunteers__Volunteer_Skills__c , NULL AS  GW_Volunteers__Volunteer_Status__c	,NULL AS  GW_Volunteers__Last_Volunteer_Date__c
 					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
					,NULL AS  CreatedDate ,NULL AS  Email ,NULL AS Email_2__c ,NULL AS npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c
				,NULL AS npe01__Preferred_Email__c ,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone
				,NULL  AS npe01__WorkPhone__c ,NULL  AS  OtherPhone ,NULL  AS npe01__PreferredPhone__c ,NULL AS   Phone
				,NULL  AS Fax 
				,NULL AS  MailingStreet
				,NULL AS   MailingCity	
				,NULL AS  MailingState
				,NULL AS  MailingPostalCode 
				,NULL AS MailingCountry
				,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

				,NULL AS Type__c
		  		,'Generic'  AS  Source_Data_Table__c	
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
  				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818
				,'ORG' AS zrefRecordType
				,'CA' AS zrefLegacyId
				,NULL AS zrefFirstName
				,NULL AS zrefPersonID

UNION  --FB-01922
	SELECT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				,'OR' AS  [Account:Legacy_ID__c]
				,'OR' AS  Legacy_ID__c	 	
			 	,NULL AS Grad_Employee__c   --filler from other tbl
				,NULL AS Salutation
				,'Kennel Contact'  AS  FirstName	 
				,NULL AS Middle_Name__c
				,'OR Unknown Kennel' AS  LastName
		 		,'0121L000001UQCqQAO' AS RecordTypeID --Organization
		
				--fillers
					,NULL AS  GW_Volunteers__Volunteer_Skills__c , NULL AS  GW_Volunteers__Volunteer_Status__c	,NULL AS  GW_Volunteers__Last_Volunteer_Date__c
 					,NULL AS Suffix__c ,NULL AS Nickname__c	,NULL AS Birthdate	,NULL AS npsp__Do_Not_Contact__c, NULL AS Spanish_Speaker__c, NULL AS Gender__c 	
					,NULL AS Title ,NULL AS npsp__Deceased__c ,NULL AS Deceased_Date__c	 ,NULL AS Do_Not_Give_Dog__c ,NULL AS Ebark__c	,NULL AS Direct_Bill__c	
					,NULL AS Media_Preference_1__c ,NULL AS Media_Preference_2__c	,NULL AS Media_Preference_3__c	,NULL AS Class_Materials_Preference__C	
					,NULL AS Initial_Contact__c ,NULL AS Qualified_Veteran__c	
					,NULL AS  CreatedDate ,NULL AS  Email ,NULL AS Email_2__c ,NULL AS npe01__WorkEmail__c ,NULL AS npe01__HomeEmail__c
				,NULL AS npe01__Preferred_Email__c ,NULL AS Emergency__c ,NULL AS HomePhone  ,NULL AS MobilePhone
				,NULL  AS npe01__WorkPhone__c ,NULL  AS  OtherPhone ,NULL  AS npe01__PreferredPhone__c ,NULL AS   Phone
				,NULL  AS Fax 
				,NULL AS  MailingStreet
				,NULL AS   MailingCity	
				,NULL AS  MailingState
				,NULL AS  MailingPostalCode 
				,NULL AS MailingCountry
				,null  AS  OtherStreet, NULL AS   OtherCity	, NULL  AS  OtherState, NULL  AS  OtherPostalCode ,NULL AS OtherCountry

				,NULL AS Type__c
		  		,'Generic'  AS  Source_Data_Table__c	
				--fillers
					,NULL AS Current_Float__c ,NULL AS Approval_Date__c ,NULL AS Approval_Date_Level_1__c ,NULL AS Approval_Date_Level_2__c ,NULL AS License_Date__c ,NULL AS State_License_Date__c
					,NULL AS Senior_License_Date__c ,NULL AS Master_License_Date__c ,NULL AS IADOTO_Meeting_After_Hire__c ,NULL AS Applic_DOTO_Meeting_After_Blind_Fold__c
					,NULL AS In_House_Written__c ,NULL AS In_House_Oral__c ,NULL AS In_House_Practical__c ,NULL AS Presentation__c
					,NULL AS Field_Trip_1__c ,NULL AS Field_Trip_2__c ,NULL AS Field_Trip_3__c ,NULL AS Home_Interview__c ,NULL AS Progress_Satisfactory__c
					,NULL AS [X1_Quiz__c] ,NULL AS [X2_Quiz__c] ,NULL AS [X3_Quiz__c] ,NULL AS Comments__c
  				,NULL AS Client_Medical_ID__c
				,NULL AS Next_Annual_Visit__c -- FB-01818
				,'ORG' AS zrefRecordType
				,'OR' AS zrefLegacyId
				,NULL AS zrefFirstName
				,NULL AS zrefPersonID

					 



END --eof 124,116			TC2: 126,065  final: 129,275



BEGIN--AUDITS
		  
	   	 	
		--PHONE CLEAN UP
			--HomePhone
				SELECT DISTINCT HomePhone, LEN(HomePhone) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  

				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET HomePhone=GDB_Final_migration.dbo.fnc_phone_fmt(HomePhone)
				WHERE HomePhone LIKE '%[0-9]%' AND LEN(HomePhone) =10

			--Phone
				SELECT DISTINCT Phone, LEN(Phone) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  
				 
				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET Phone=GDB_Final_migration.dbo.fnc_phone_fmt(Phone)
				WHERE Phone LIKE '%[0-9]%' AND LEN(Phone) =10

			--WorkPhone
				SELECT DISTINCT npe01__WorkPhone__c, LEN(npe01__WorkPhone__c) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  
			
				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET npe01__WorkPhone__c=GDB_Final_migration.dbo.fnc_phone_fmt(npe01__WorkPhone__c)
				WHERE npe01__WorkPhone__c LIKE '%[0-9]%' AND LEN(npe01__WorkPhone__c) =10

			--MobilePhone
				SELECT DISTINCT MobilePhone, LEN(MobilePhone) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  
			
				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET MobilePhone=GDB_Final_migration.dbo.fnc_phone_fmt(MobilePhone)
				WHERE MobilePhone LIKE '%[0-9]%' AND LEN(MobilePhone) =10

			--OtherPhone
				SELECT DISTINCT OtherPhone, LEN(OtherPhone) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  
			
				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET OtherPhone=GDB_Final_migration.dbo.fnc_phone_fmt(OtherPhone)
				WHERE OtherPhone LIKE '%[0-9]%' AND LEN(OtherPhone) =10

 	 		 
			--Fax
				SELECT DISTINCT Fax, LEN(Fax) AS l 
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				ORDER BY l desc  
			
				UPDATE GDB_Final_migration.dbo.IMP_CONTACT SET Fax=GDB_Final_migration.dbo.fnc_phone_fmt(Fax)
				WHERE Fax LIKE '%[0-9]%' AND LEN(Fax) =10
  
		
		--COUNTRY CLEANUP
				SELECT MailingCountry, COUNT(*) C
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				GROUP BY MailingCountry
				ORDER BY c DESC

				UPDATE GDB_Final_migration.dbo.IMP_CONTACT
				SET MailingCountry='United States' 
				WHERE MailingCountry='USA'

				UPDATE GDB_Final_migration.dbo.IMP_CONTACT
				SET OtherCountry='United States' 
				WHERE OtherCountry='USA'

				SELECT MailingState, COUNT(*) C
				FROM GDB_Final_migration.dbo.IMP_CONTACT
				WHERE MailingCountry IS null
				GROUP BY MailingState
				ORDER BY c DESC
                
				 UPDATE  GDB_Final_migration.dbo.IMP_CONTACT 
				 SET MailingCountry = 'United States'  
				 WHERE MailingCountry IS NULL 
				 AND ([MailingState]='AL' OR [MailingState]='AK' OR [MailingState]='AZ' OR [MailingState]='AR' OR [MailingState]='CA' OR [MailingState]='CO' OR [MailingState]='CT' OR [MailingState]='DE' OR [MailingState]='DC' OR [MailingState]='FL'
				   OR [MailingState]='GA' OR [MailingState]='HI' OR [MailingState]='ID' OR [MailingState]='IL' OR [MailingState]='IN' OR [MailingState]='IA' OR [MailingState]='KS' OR [MailingState]='KY' OR [MailingState]='LA' OR [MailingState]='ME' 
				   OR [MailingState]='MD' OR [MailingState]='MA' OR [MailingState]='MI' OR [MailingState]='MN' OR [MailingState]='MS' OR [MailingState]='MO' OR [MailingState]='MT' OR [MailingState]='NE' OR [MailingState]='NV' OR [MailingState]='NH' 
				   OR [MailingState]='NJ' OR [MailingState]='NM' OR [MailingState]='NY' OR [MailingState]='NC' OR [MailingState]='ND' OR [MailingState]='OH' OR [MailingState]='OK' OR [MailingState]='OR' OR [MailingState]='PA' OR [MailingState]='RI' 
				   OR [MailingState]='SC' OR [MailingState]='SD' OR [MailingState]='TN' OR [MailingState]='TX' OR [MailingState]='UT' OR [MailingState]='VT' OR [MailingState]='VA' OR [MailingState]='WA' OR [MailingState]='WV' OR [MailingState]='WI' OR [MailingState]='WY')
 
 		 UPDATE  GDB_Final_migration.dbo.IMP_CONTACT 
				 SET MailingCountry = 'United States'  
				 WHERE MailingCountry IS NULL 
				 AND ([OtherState]='AL' OR [OtherState]='AK' OR [OtherState]='AZ' OR [OtherState]='AR' OR [OtherState]='CA' OR [OtherState]='CO' OR [OtherState]='CT' OR [OtherState]='DE' OR [OtherState]='DC' OR [OtherState]='FL'
				   OR [OtherState]='GA' OR [OtherState]='HI' OR [OtherState]='ID' OR [OtherState]='IL' OR [OtherState]='IN' OR [OtherState]='IA' OR [OtherState]='KS' OR [OtherState]='KY' OR [OtherState]='LA' OR [OtherState]='ME' 
				   OR [OtherState]='MD' OR [OtherState]='MA' OR [OtherState]='MI' OR [OtherState]='MN' OR [OtherState]='MS' OR [OtherState]='MO' OR [OtherState]='MT' OR [OtherState]='NE' OR [OtherState]='NV' OR [OtherState]='NH' 
				   OR [OtherState]='NJ' OR [OtherState]='NM' OR [OtherState]='NY' OR [OtherState]='NC' OR [OtherState]='ND' OR [OtherState]='OH' OR [OtherState]='OK' OR [OtherState]='OR' OR [OtherState]='PA' OR [OtherState]='RI' 
				   OR [OtherState]='SC' OR [OtherState]='SD' OR [OtherState]='TN' OR [OtherState]='TX' OR [OtherState]='UT' OR [OtherState]='VT' OR [OtherState]='VA' OR [OtherState]='WA' OR [OtherState]='WV' OR [OtherState]='WI' OR [OtherState]='WY')
 


		--DUPLICATE
			SELECT * FROM GDB_Final_migration.dbo.IMP_CONTACT 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CONTACT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c
 
		--CHECK
			SELECT * FROM GDB_Final_migration.dbo.IMP_CONTACT ORDER BY [Account:Legacy_ID__c]

			SELECT Source_Data_Table__c, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_CONTACT 
			GROUP BY Source_Data_Table__c
			ORDER BY Source_Data_Table__c

			SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CONTACT  --TC1 124116

END-- eof audits
 


 SELECT DISTINCT GW_Volunteers__Last_Volunteer_Date__c  FROM dbo.IMP_CONTACT

 SELECT Legacy_ID__c, Comments__c  FROM GDB_Final_migration.DBO.IMP_CONTACT
 WHERE Comments__c IS NOT NULL