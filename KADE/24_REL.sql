USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblPersonRel
			WHERE   SF_Object LIKE '%gdb%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*

SELECT * FROM GDB_Final_migration.DBO.XTR_RECORDTYPEs WHERE SOBJECTTYPE LIKE '%rel%'
			
			0121L000001UQDTQA4	Contact
			0121L000001UQDUQA4	Contact-Dog
			0121L000001UQDVQA4	Dog


*/

BEGIN

--Step 1 Co-Raiser
	SELECT DISTINCT T1.pd_RelationID, T1.pd_RelCode, T1.pd_CoRel, T3.drl_RelationCode, T3.drl_DogID, T2.prl_PersonID
			INTO GDB_Final_migration.dbo.stg_REL_CoRaiser1 --drop table GDB_Final_migration.dbo.stg_REL_CoRaiser1
			FROM GDB_KADE_Final.dbo.tblPersonDog AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_RelationID = T1.pd_PersonRelID
			INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T3 ON T1.pd_DogRelID = T3.drl_RelationID
			WHERE T1.pd_RelCode = 'prp' AND T1.pd_EndDate Is Null

	ALTER TABLE GDB_Final_migration.dbo.stg_REL_CoRaiser1
		ADD zrefCoRaiser NVARCHAR(50)


--test script
	SELECT T1.*, T2.drl_DogID
	,CASE WHEN T1.drl_DogID=T2.drl_DogID THEN 'TRUE' END AS Co_Raiser
	FROM GDB_Final_migration.dbo.stg_REL_CoRaiser1 AS T1
	INNER  JOIN (SELECT drl_DogID FROM GDB_Final_migration.dbo.stg_REL_CoRaiser1
			GROUP BY drl_DogID HAVING count(*) >1) AS T2 ON T2.drl_DogID = T1.drl_DogID
	ORDER BY T1.drl_DogID


--update zrefCoRaiser
	UPDATE GDB_Final_migration.dbo.stg_REL_CoRaiser1
		SET zrefCoRaiser =  CASE WHEN T1.drl_DogID=T2.drl_DogID THEN 'TRUE' END 
	FROM GDB_Final_migration.dbo.stg_REL_CoRaiser1 AS T1
	INNER JOIN (SELECT drl_DogID FROM GDB_Final_migration.dbo.stg_REL_CoRaiser1
	GROUP BY drl_DogID HAVING count(*) >1) AS T2 ON T2.drl_DogID = T1.drl_DogID

	SELECT * FROM GDB_Final_migration.dbo.stg_REL_CoRaiser1
	ORDER BY drl_DogID

--Step 3 create REL import

SELECT

		GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
		,'PD-'+ CAST(T1.pd_RelationID AS VARCHAR(50)) AS  Legacy_ID__c	
		,T5.[Puppy_Club__r:Legacy_ID__c]	--FB-01315
		,T6.[CFR__r:Legacy_ID__c]	--FB-01315
		,IIF(X1.id IS NULL, NULL,T3.prl_PersonID)  AS  [Contact:Legacy_ID__c]
		,IIF(x1.id IS NULL,x2.id,'') AS Organization__c --FB-00934
		,T2.drl_DogID AS [Dog__r:Legacy_Id__c]
		,CASE T1.pd_CoRel 
			WHEN 'SEC' THEN 'Secondary'
			WHEN 'PRI' THEN 'Primary'
			WHEN 'RRI' THEN 'Primary' END AS Co_Person_Relationship__c
		,T1.pd_RelCode AS Rel_Code__c
		,NULL AS Rel_Description__c
		,CAST(T1.pd_StartDate AS DATE) AS Start_Date__c
		,CAST(T1.pd_EndDate AS DATE) AS End_Date__c
		,T1.pd_EndReason AS Notes__c
		,IIF(t4.zrefCoRaiser='true', 'TRUE','FALSE') AS Co_Raiser__c
		,CAST(T1.pd_CreatedDate AS DATE) AS CreatedDate
	--	,NULL AS Campus__c
		,IIF(T3.prl_ClientInstanceID=0,'','cli-' + CAST(prl_ClientInstanceID AS VARCHAR(50)) AS [Application__r:Legacy_ID__c]
		,NULL AS End_Reason__c
		,NULL AS End_Reason_Category__c
		,'0121L000001UQDUQA4' AS RecordTypeID
		,'tblPesonDog' AS zrefSource
		,CAST(T1.pd_RelationID AS VARCHAR(20)) AS zrefID

	--INTO [GDB_Final_migration].dbo.IMP_REL --drop table [GDB_Final_migration].dbo.IMP_REL
	FROM [GDB_KADE_Final].dbo.tblPersonDog AS T1
 					LEFT JOIN GDB_KADE_Final.dbo.tblDogRel AS T2 ON T2.drl_RelationID=T1.pd_DogRelID
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_RelationID=T1.pd_PersonRelID
					LEFT JOIN GDB_Final_migration.dbo.stg_REL_CoRaiser1 AS T4 ON T4.pd_RelationID = T1.pd_RelationID
					LEFT JOIN GDB_Final_migration.dbo.XTR_Contact AS X1 ON x1.LEGACY_ID__C=CAST(T3.prl_PersonID AS VARCHAR(50))		--FB-00934
					LEFT JOIN GDB_Final_migration.dbo.XTR_Account AS X2 ON X2.LEGACY_ID__C=CAST(T3.prl_PersonID AS VARCHAR(50))		--FB-00934
					LEFT JOIN GDB_Final_migration.dbo.stg_REL_PuppyClub AS T5 ON T1.pd_RelationID=t5.pd_RelationID	--FB-01315
					LEFT JOIN GDB_Final_migration.dbo.stg_REL_CFR AS T6 ON T1.pd_RelationID=t6.pd_RelationID	--FB-01315

/* UNION --DNC PER FB-00944
	SELECT 
		X1.id AS OwnerID
		,'DDS-'+ CAST(T1.dds_DogStatusID AS VARCHAR(50)) AS Legacy_ID__c
		,NULL AS [Contact:Legacy_ID__c]
		,T1.dds_DogID AS [Dog__r:Legacy_Id__c]
		,NULL AS Co_Person_Relationship__c
		,t2.dsc_NewCS AS Rel_Code__c			--FB-00933
		,T2.dsc_text AS Rel_Description__c		--Fb-00933
		,CAST(T1.dds_Date AS DATE) AS Start_Date__c
		,NULL AS End_Date__c
		,T1.dds_RelPlacement +'Chr(10)' + T1.dds_IntoString +'Chr(10)' + T1.dds_StatusChangeNotes +'Chr(10)' + T1.dds_PriorityPlacement 
		+'Chr(10)' + CAST(T1.dds_RelSummary AS VARCHAR(MAX)) AS Notes__c
		,'FALSE' AS Co_Raiser__c
		,CAST(T1.dds_CreatedDate AS DATE) AS CreatedDate
		,T1.dds_Location AS Campus__c
		,NULL AS [Application__r:Legacy_ID__c]
		,NULL AS End_Reason__c
		,NULL AS End_Reason_Category__c
		,'0121L000001UQDVQA4' AS RecordTypeID
		,'tblDogStatusChange' AS zrefSource
		,CAST(T1.dds_DogStatusID AS VARCHAR(20)) AS zrefId

	FROM 	[GDB_KADE_Final].dbo.tblDogStatusChange AS T1		
			LEFT JOIN 	[GDB_Final_migration].dbo.XTR_USER AS X1 ON T1.dds_StatusChangeBy = X1.ADP__C
			INNER JOIN 	[GDB_KADE_Final].dbo.trefDogStatusChange AS T2 ON T2.dsc_ID = T1.dds_Disposition

			*/
UNION 
	SELECT DISTINCT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
		,'CS-' + CAST(T1.cs_StatusChgID AS VARCHAR(50)) AS Legacy_ID__c
		,NULL AS [Puppy_Club__r:Legacy_ID__c]	--FB-01315
		,NULL AS [CFR__r:Legacy_ID__c]	--FB-01315
		,T2.prl_PersonID AS [Contact:Legacy_ID__c]
		,NULL AS Organization__c --FB-00934
		,NULL AS [Dog__r:Legacy_Id__c]
		,NULL AS Co_Person_Relationship__c
		,t1.cs_StatusCode AS Rel_Code__c
		,T3.ClientStatus AS Rel_Description__c		--Fb-00933
		,CAST(T1.cs_DateChanged AS DATE) AS Start_Date__c
		,CAST(T1.cs_DateEnd AS DATE) AS End_Date__c
		,CAST(T1.cs_Notes AS VARCHAR(MAX)) AS Notes__c
		,'FALSE' AS Co_Raisers__c
		,CAST(T1.cs_CreatedDate AS DATE) AS CreatedDate
	--	,NULL AS Campus__c
		,'cli-' + CAST(T1.cs_ClientID AS VARCHAR(50)) AS [Application__r:Legacy_ID__c]
		,NULL AS End_Reason__c
		,NULL AS End_Reason_Category__c
		,'0121L000001UQDTQA4' AS RecordTypeID
		,'tblClientStatus' AS zrefSource
		,CAST(T1.cs_StatusChgID AS VARCHAR (20)) AS zrefId
		
 	FROM [GDB_KADE_Final].dbo.tblClientStatus AS T1	
		INNER JOIN [GDB_KADE_Final].dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID = T1.cs_ClientID
		LEFT JOIN GDB_KADE_Final.dbo.trefClientStatus AS T3 ON t3.StatusCode=t1.cs_StatusCode
		LEFT JOIN (SELECT T.cs_StatusChgID
					FROM GDB_KADE_Final.dbo.tblClientStatus AS T
					INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID = T.cs_ClientID  
							AND T.cs_DateChanged = T2.prl_StartDate AND T.cs_StatusCode = T2.prl_RelationCode
							AND T.cs_DateEnd = T2.prl_EndDate
					) AS T4 ON T4.cs_StatusChgID = T1.cs_StatusChgID --FB-01131
	
		WHERE T4.cs_StatusChgID IS NULL AND T1.cs_DateChanged IS NOT NULL
UNION
	SELECT DISTINCT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
	,'DRL-' + CAST(t1.DRL_RELATIONID AS VARCHAR(50)) AS Legacy_ID__c
	,NULL AS [Puppy_Club__r:Legacy_ID__c]	--FB-01315
	,NULL AS [CFR__r:Legacy_ID__c]	--FB-01315
	,NULL AS [Contact:Legacy_ID__c]
	,NULL AS Organization__c --FB-00934
	,T1.drl_dogID AS [Dog__r:Legacy_Id__c]
	,NULL  AS Co_Person_Relationship__c
	,T1.drl_RelationCode AS Rel_Code__c
	,NULL AS Rel_Description__c
	,CAST(T1.drl_StartDate AS DATE) AS Start_Date__c
	,CAST(T1.drl_EndDate AS DATE) AS End_Date__c
	,NULL AS Notes__C
	,'FALSE' AS Co_raiser__c
	,CAST(T1.drl_CreatedDate AS DATE) AS CreatedDate
	--,NULL AS Campus__c
	,NULL AS [Application__r:Legacy_ID__c]
	,NULL AS End_Reason__c
	,NULL AS End_Reason_Category__c
	,'0121L000001UQDVQA4' AS RecordTypeID
	,'tblDogRel' AS zrefSource
	,CAST(T1.drl_RelationID AS VARCHAR(20)) AS zrefID

	--INTO GDB_Final_migration.dbo.stg_REL_tblDog --drop table GDB_Final_migration.dbo.stg_REL_tblDog
	FROM  [GDB_KADE_Final].dbo.tblDogRel AS T1	
	WHERE  T1.drl_RelationID not IN  (SELECT T2.drl_RelationID
					FROM [GDB_KADE_Final].dbo.tblDogRel AS T2	
					INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T3 ON t2.drl_RelationID = t3.pd_DogRelID
					WHERE T2.drl_StartDate = t3.pd_StartDate AND T2.drl_EndDate=t3.pd_EndDate
					UNION
					SELECT DISTINCT T4.drl_RelationID 
							FROM [GDB_KADE_Final].dbo.tblDogRel AS T4
							INNER JOIN GDB_KADE_Final.DBO.tblPersonDog AS T5 ON T5.pd_DogRelID = T4.drl_RelationID
							INNER JOIN GDB_KADE_Final.DBO.tblPersonRel AS T6 ON T6.prl_RelationID = T5.pd_PersonRelID
							WHERE T1.drl_RelationCode IN ('AMB', 'BRDADP', 'BUD', 'BUDADP', 'CCH', 'CK9', 'CLI', 'CLS', 'COBRD', 'EMP', 'FAMADP', 'FOS', 'NPCLI', 'NPK9', 
									'OUTBRD', 'PAS', 'PFC', 'PLADP', 'PLBRD', 'PLBUD', 'PLCLI', 'PLPRP', 'PRP', 'PRPADP', 'RETBRD', 'RTNDNR') )	
	
    

UNION

	SELECT DISTINCT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerID
	,'PRL-' + CAST(t1.pRL_RELATIONID AS VARCHAR(50)) AS Legacy_ID__c
	,NULL AS [Puppy_Club__r:Legacy_ID__c]	--FB-01315
	,NULL AS [CFR__r:Legacy_ID__c]	--FB-01315
	,T1.prl_PersonID AS [Contact:Legacy_ID__c]
	,NULL AS Organization__c --FB-00934
	,NULL AS [Dog__r:Legacy_Id__c]
	,NULL  AS Co_Person_Relationship__c
	,T1.prl_RelationCode AS Rel_Code__c
	,NULL AS Rel_Description__c
	,CAST(T1.prl_StartDate AS DATE) AS Start_Date__c
	,CAST(T1.prl_EndDate AS DATE) AS End_Date__c
	,NULL AS Notes__C
	,'FALSE' AS Co_raiser__c
	,CAST(T1.prl_CreatedDate AS DATE) AS CreatedDate
	--,NULL AS Campus__c
	,'cli-' + CAST(T1.prl_ClientInstanceID AS VARCHAR(50)) AS [Application__r:Legacy_ID__c]
	,T4.erc_ERCodeText AS End_Reason__c
	,IIF(T5.rct_ERCodeCatText='NA', 'N/A',T5.rct_ERCodeCatText) AS End_Reason_Category__c
	,'0121L000001UQDTQA4' AS RecordTypeID
	,'tblPersonRel' AS zrefSource
	,CAST(T1.prl_RelationID AS VARCHAR(20)) AS zrefID

	FROM  [GDB_KADE_Final].dbo.tblPersonRel AS T1	
	LEFT JOIN GDB_KADE_Final.dbo.trefEndReasonCodes AS T4 ON T1.prl_EndReason = T4.erc_ID
	LEFT JOIN GDB_KADE_Final.dbo.trefEndReasonCodesCat AS T5 ON t4.erc_RECodeCat = T5.rct_ID
	WHERE T1.prl_RelationID not IN  (SELECT T2.prl_RelationID									-- removed T1.prl_ClientInstanceID <>'0' AND per FB-01134
					FROM [GDB_KADE_Final].dbo.tblPersonRel AS T2	
					INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T3 ON t2.prl_RelationID = t3.pd_PersonRelID --FB-01133
					WHERE T2.prl_StartDate = t3.pd_StartDate AND T2.prl_EndDate = t3.pd_EndDate AND T2.prl_RelationCode=T3.pd_RelCode)

					-- tc2: 440,354
	
	

	

----- AUDITING

		SELECT DISTINCT T1.drl_RelationID, t1.drl_RelationCode
FROM [GDB_KADE_Final].dbo.tblDogRel AS T1	
	INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T2 ON t1.drl_RelationID = t2.pd_DogRelID
	WHERE T1.drl_StartDate = t2.pd_StartDate AND T1.drl_EndDate=t2.pd_EndDate

SELECT DISTINCT t3.drl_RelationCode
	FROM  [GDB_KADE_Final].dbo.tblDogRel AS T3	
	WHERE  T3.drl_RelationID not IN  (SELECT T1.drl_RelationID
								FROM [GDB_KADE_Final].dbo.tblDogRel AS T1	
								INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T2 ON t1.drl_RelationID = t2.pd_DogRelID
								WHERE T1.drl_StartDate = t2.pd_StartDate AND T1.drl_EndDate=t2.pd_EndDate)
    


SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblDogRel


SELECT COUNT(T3.drl_RelationID), t3.drl_RelationCode
	FROM  [GDB_KADE_Final].dbo.tblDogRel AS T3	
		INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T4 ON T4.pd_DogRelID=t3.drl_RelationID
		INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T5 ON t4.pd_PersonRelID = T5.prl_RelationID
	WHERE  T3.drl_RelationID NOT IN  (SELECT T1.drl_RelationID
								FROM [GDB_KADE_Final].dbo.tblDogRel AS T1	
								INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T2 ON t1.drl_RelationID = t2.pd_DogRelID
								WHERE T1.drl_StartDate = t2.pd_StartDate AND T1.drl_EndDate=t2.pd_EndDate)
	GROUP BY T3.drl_RelationCode
	ORDER BY T3.drl_RelationCode



	SELECT COUNT(T3.drl_RelationID), t3.drl_RelationCode
	FROM  [GDB_KADE_Final].dbo.tblDogRel AS T3	

	WHERE  T3.drl_RelationID IN  (SELECT T1.drl_RelationID
								FROM [GDB_KADE_Final].dbo.tblDogRel AS T1	
								INNER JOIN [GDB_KADE_Final].dbo.tblPersonDog AS T2 ON t1.drl_RelationID = t2.pd_DogRelID
								WHERE T1.drl_StartDate = t2.pd_StartDate AND T1.drl_EndDate=t2.pd_EndDate)
	GROUP BY T3.drl_RelationCode
	ORDER BY T3.drl_RelationCode

	--DUP CHECK
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_REL
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_REL GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	

	SELECT zrefSource, COUNT(*) c
	FROM GDB_Final_migration.dbo.IMP_REL
	GROUP BY zrefSource