USE GDB_FINAL_Migration
GO
 
-- FUNCTION: Set OwnerID
 		SET QUOTED_IDENTIFIER ON
		GO
		SET ANSI_NULLS ON
		GO

		
		CREATE  FUNCTION [dbo].[fnc_OwnerId]()      
 		  RETURNS varchar(18)  
		  BEGIN 
			declare @Owner as varchar(18)     
			set @Owner = '0051L0000090o16QAA'   -- UserName: SystemAdmin
			return @Owner
		  END
 		GO

--PHONE NUMBER FORMATING FROM 9999999999 TO (999) 999-9999.
   		SET QUOTED_IDENTIFIER ON
		GO
		SET ANSI_NULLS ON
		go
		CREATE  FUNCTION [dbo].[fnc_phone_fmt](@Phone AS NVARCHAR(14))
			RETURNS NVARCHAR(14)
			BEGIN
				DECLARE @Phone_fmt AS NVARCHAR(14)
				SET @Phone_fmt = '(' + Stuff(Stuff(@Phone,7,0,'-'),4,0,') ')  
           		RETURN @Phone_fmt
			END 
		GO
 
 	 
 
	CREATE FUNCTION Proper(@Text AS NVARCHAR(max) )
		RETURNS NVARCHAR(max)
		AS
			BEGIN
				DECLARE @Reset BIT;
				DECLARE @Ret NVARCHAR(max);
				DECLARE @i INT;
				DECLARE @c CHAR(1);

				SELECT  @Reset = 1 ,
						@i = 1 ,
						@Ret = '';

				WHILE ( @i <= LEN(@Text) )
					SELECT  @c = SUBSTRING(@Text, @i, 1) ,
							@Ret = @Ret + CASE WHEN @Reset = 1 THEN UPPER(@c)
											   ELSE LOWER(@c)
										  END ,
							@Reset = CASE WHEN @c LIKE '[a-zA-Z]' THEN 0
										  ELSE 1
									 END ,
							@i = @i + 1
				RETURN @Ret
			END


 
		CREATE FUNCTION dbo.LTrimX(@str NVARCHAR(MAX) )
		RETURNS NVARCHAR(MAX)
		AS
			BEGIN
				DECLARE @trimchars NVARCHAR(10)
				SET @trimchars = CHAR(9) + CHAR(10) + CHAR(13) + CHAR(32)
				IF @str LIKE '[' + @trimchars + ']%'
					SET @str = SUBSTRING(@str,
										 PATINDEX('%[^' + @trimchars + ']%', @str),
										 8000)
				RETURN @str
			END
			GO
		
		
		CREATE FUNCTION dbo.RTrimX( @str NVARCHAR(MAX) )
		RETURNS NVARCHAR(MAX)
		AS
			BEGIN
				DECLARE @trimchars NVARCHAR(10)
				SET @trimchars = CHAR(9) + CHAR(10) + CHAR(13) + CHAR(32)
				IF @str LIKE '%[' + @trimchars + ']'
					SET @str = REVERSE(dbo.LTrimX(REVERSE(@str)))
				RETURN @str
			END
			GO
		
		
		CREATE FUNCTION dbo.TrimX( @str NVARCHAR(MAX) )
		RETURNS NVARCHAR(MAX)
		AS
			BEGIN
				RETURN dbo.LTrimX(dbo.RTrimX(@str))
			END
  		GO
 
		/* Run the created function */  SELECT dbo.Proper(dbo.TrimX('      word leading trailing spaces                      ')) AS 'TrimmedWord'
	  
 
 
	
--find string on a table. e,g, EXEC sp_FindStringInTable '%"%', 'IMP', 'ACCOUNT'  (Strg, schema name, table name)
--https://www.mssqltips.com/sqlservertip/1522/searching-and-finding-a-string-value-in-all-columns-in-a-sql-server-table/ 
 

  EXEC sp_FindStringInTable '%"%', 'DBO', 'IMP_FIELD_REPORT' 


		create PROCEDURE dbo.sp_FindStringInTable @stringToFind NVARCHAR(100), @schema sysname, @table sysname 
		AS 

		DECLARE @sqlCommand NVARCHAR(max) 
		DECLARE @where NVARCHAR(max) 
		DECLARE @columnName sysname 
		DECLARE @cursor NVARCHAR(max) 

		BEGIN TRY 
		   SET @sqlCommand = 'SELECT * FROM [' + @schema + '].[' + @table + '] WHERE' 
		   SET @where = '' 

		   SET @cursor = 'DECLARE col_cursor CURSOR FOR SELECT COLUMN_NAME 
		   FROM ' + DB_NAME() + '.INFORMATION_SCHEMA.COLUMNS 
		   WHERE TABLE_SCHEMA = ''' + @schema + ''' 
		   AND TABLE_NAME = ''' + @table + ''' 
		   AND DATA_TYPE IN (''char'',''nchar'',''ntext'',''nvarchar'',''text'',''varchar'')' 

		   EXEC (@cursor) 

		   OPEN col_cursor    
		   FETCH NEXT FROM col_cursor INTO @columnName    

		   WHILE @@FETCH_STATUS = 0    
		   BEGIN    
			   IF @where <> '' 
				   SET @where = @where + ' OR' 

			   SET @where = @where + ' [' + @columnName + '] LIKE ''' + @stringToFind + '''' 
			   FETCH NEXT FROM col_cursor INTO @columnName    
		   END    

		   CLOSE col_cursor    
		   DEALLOCATE col_cursor  

		   SET @sqlCommand = @sqlCommand + @where 
		   PRINT @sqlCommand 
		   EXEC (@sqlCommand)  
		END TRY 
		BEGIN CATCH 
		   PRINT 'There was an error. Check to make sure object exists.'
		   PRINT error_message()
    
		   IF CURSOR_STATUS('variable', 'col_cursor') <> -3 
		   BEGIN 
			   CLOSE col_cursor    
			   DEALLOCATE col_cursor  
		   END 
		END CATCH 

  
-- STORED PROCEDURE FOR PHONE NUMBERS STORED IN CONSTITUENT ADDRESSES.
		USE GDB_Final_migration
		GO
        
		ALTER PROCEDURE HC_PivotWizard_p
		   @P_Row_Field    NVARCHAR(255),
		   @P_Column_Field NVARCHAR(255),
		   @P_Value        NVARCHAR(4000),
		   @P_Into         NVARCHAR(4000),
		   @P_From         NVARCHAR(4000),
		   @P_Where        NVARCHAR(4000) = '1=1'

		AS

		  DECLARE @SQL NVARCHAR(4000)

		  -- Build SQL statment that upload @Columns string 
		  -- with @P_Column_Filed values
		  CREATE TABLE #TEMP  (ColumnField varchar(255))
		  SET @sql ='SELECT DISTINCT '+@P_Column_Field+' AS ColumnField'+
					  ' FROM '+@P_From+
					  ' WHERE '+@P_Where+
					  ' ORDER BY '+@P_Column_Field
		  INSERT INTO #TEMP
		  EXEC(@sql)
		  --PRINT @sql

		  -- Check count of columns
		  DECLARE @Count_Columns int
		  SELECT @Count_Columns = COUNT(*) FROM #Temp
		  IF (@Count_Columns<1) OR (@Count_Columns>255)  BEGIN
			  DROP TABLE #Temp
			  RAISERROR('%d is invalid columns amount. Valid is 1-255',
						16,1,@Count_columns)
			  RETURN
		  END
		  -- Upload @Columns from #Temp
		  DECLARE @Columns NVARCHAR(MAX),
				  @Column_Field NVARCHAR(MAX)

		  SET @Columns = ''
		  DECLARE Column_cursor CURSOR LOCAL FOR
		  SELECT CAST(ColumnField AS NVARCHAR(60))
		  FROM #Temp
		  OPEN Column_cursor
		  FETCH NEXT FROM Column_cursor
		  INTO @Column_Field 
		  WHILE @@FETCH_STATUS = 0 BEGIN
			  SET @Columns = @Columns +
				' MAX('+
					 ' CASE WHEN '+@P_Column_Field+'='''+ @Column_Field+''''+
					 ' THEN '+@P_Value+
					 ' ELSE null END'+
					 ') AS ['+ @Column_Field +'], '
			  FETCH NEXT FROM Column_cursor
			  INTO @Column_Field
		  END
		  CLOSE Column_cursor
		  DEALLOCATE Column_cursor
		  DROP TABLE #Temp

		  IF @Columns='' RETURN 1
		  SET @Columns = Left(@Columns,Len(@Columns)-1)

		  -- Build Pivot SQL statment
		  DECLARE @Pivot_SQL NVARCHAR(MAX)
		  SET @Pivot_SQL =              'SELECT '  +@P_Row_Field+', '+@Columns
		  SET @Pivot_SQL = @Pivot_SQL +' INTO '    +@P_Into  
		  SET @Pivot_SQL = @Pivot_SQL +' FROM '    +@P_From
		  SET @Pivot_SQL = @Pivot_SQL +' WHERE '   +@P_Where
		  SET @Pivot_SQL = @Pivot_SQL +' GROUP BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL +' ORDER BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL + '#'

		  IF Right(@Pivot_SQL,1)<>'#'
		  BEGIN
			 RAISERROR('SQL statement is too long. It must be less
						than 8000 charachter!',16,1)
			 RETURN 1
		  END
		  SET @Pivot_SQL = Left(@Pivot_SQL,Len(@Pivot_SQL)-1)

		  -- PRINT @Pivot_SQL
		  EXEC(@Pivot_SQL)

		  RETURN 0

 
