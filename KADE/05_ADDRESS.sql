USE GDB_Final_migration
GO
 

 
BEGIN--Map: address   
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblOutsideVet 
			WHERE   SF_Object LIKE '%addr%'

			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVolCalOrganization 
			WHERE  SF_Object_2 LIKE '%addr%'

END 

BEGIN--DROP TABLE IMP ADDRESS
	
	DROP TABLE GDB_Final_migration.DBO.IMP_ADDRESS

END 

BEGIN --CREATE IMP ADDRESS  --address detail. 
	--tblAddress (stg_address)
			SELECT DISTINCT
					CAST(zrefAccountId AS NVARCHAR(30)) AS [npsp__Household_Account__r:Legacy_Id__c] 
					,'add-' + CAST(ad_AddressID AS NVARCHAR(20)) AS Legacy_ID__c --+ '_' + CAST(ad_AddressDetailID AS NVARCHAR(20)) AS Legacy_Id__c
					,npsp__Default_Address__c
					,npsp__Address_Type__c	
					,add_Address AS npsp__MailingStreet__c	
					,add_Address2 AS npsp__MailingStreet2__c
					,add_City AS npsp__MailingCity__c	
					,add_State AS npsp__MailingState__c	
					,CAST(add_ZipCode AS NVARCHAR(30)) AS npsp__MailingPostalCode__c	
					,add_Country AS npsp__MailingCountry__c	
					,add_USCounty AS npsp__County_Name__c	
					,add_Directions AS Directions__c 
					,'TRUE' AS Active__c --FB-02030
					,ad_AddressID AS zrefID
					,'tblAddress' zrefSource
				--	INTO GDB_Final_migration.dbo.stg_Address_test -- drop table GDB_Final_migration.dbo.stg_Address_test
				 INTO GDB_Final_migration.dbo.IMP_ADDRESS
				FROM  GDB_Final_migration.dbo.stg_Address

					UNION 
			SELECT  DISTINCT	
					CAST(T2.zrefAccountId AS NVARCHAR(30)) AS [npsp__Household_Account__r:Legacy_Id__c] 
					,MAX('addH-'+CAST(T1.add_AddressHistID  AS NVARCHAR(20)))  AS  Legacy_ID__c		-- Concatenate with "addH-"&[add_AddressID]
  					,'FALSE' AS  npsp__Default_Address__c	-- FALSE
					,T3.[Description]  AS  npsp__Address_Type__c	
					,T1.add_Address  AS  npsp__MailingStreet__c	
					,T1.add_Address2  AS  npsp__MailingStreet2__c	
					,T1.add_City  AS  npsp__MailingCity__c	
					,CASE WHEN T1.add_State IS NULL THEN T1.add_CountryRegion ELSE T1.add_State END AS npsp__MailingState__c	-- If [add_State] is null migrate [add_CountryRegion] to State/Province
					,CAST(CASE WHEN T1.add_Zip IS NULL THEN T1.add_CountryCode ELSE T1.add_Zip END  AS NVARCHAR(30))  AS   npsp__MailingPostalCode__c	-- If [add_Zip] is null migrate [add_CountryCode] to Zip/Postal Code
				 	,T1.add_Country  AS  npsp__MailingCountry__c	
			 		,MAX(T1.add_USCounty)  AS  npsp__County_Name__c	
					,NULL AS Directions__c			--filler for other tbl.
					,'FALSE' AS Active__c --FB-02030
					,MAX(T1.add_AddressHistID) AS zrefId
					,'tblAddressHistory' zrefSource 
			FROM GDB_KADE_Final.dbo.tblAddressHistory AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON T1.add_PersonID=T2.psn_PersonID
			LEFT JOIN GDB_KADE_Final.DBO.trefAddressCode AS T3 ON T1.add_AddressCode=T3.AddressCode
			WHERE T1.add_AddressHistID NOT IN (SELECT T11.add_AddressHistID FROM GDB_Final_migration.dbo.stg_Address AS T10 
												INNER JOIN GDB_KADE_Final.dbo.tblAddressHistory AS T11 ON T11.add_Address = T10.add_Address 
												AND T11.add_City = T10.add_City AND T10.ad_PersonID = T11.add_PersonID 
												AND T10.zrefAddressCode1=T11.add_AddressCode)
			GROUP BY CAST(T2.zrefAccountId AS NVARCHAR(30)),T3.Description,T1.add_Address, T1.add_Address2, T1.add_City,
						CASE WHEN T1.add_State IS NULL THEN T1.add_CountryRegion ELSE T1.add_State END, 
						CAST(CASE WHEN T1.add_Zip IS NULL THEN T1.add_CountryCode ELSE T1.add_Zip END  AS NVARCHAR(30)),
						T1.add_Country
			UNION 
	--tblAgency
			SELECT 
			 	 'acy-' +CAST(T1.acy_AgencyID AS NVARCHAR(20)) AS [npsp__Household_Account__r:Legacy_Id__c] 
				,'acy-' +CAST(T1.acy_AgencyID AS NVARCHAR(20)) AS Legacy_Id__c
		 		,'TRUE' AS  npsp__Default_Address__c	-- TRUE
				,NULL AS npsp__Address_Type__c
				,T1.acy_Address1  AS  npsp__MailingStreet__c	
				,T1.acy_Address2  AS  npsp__MailingStreet2__c	
				,T1.acy_City  AS  npsp__MailingCity__c	
				,CASE WHEN T1.acy_State IS NULL THEN T1.acy_CountryRegion ELSE T1.acy_State END AS  npsp__MailingState__c	-- If [acy_State] is null migrate [acy_CountryRegion] to State/Province
				,CAST(CASE WHEN T1.acy_ZipCode IS NULL THEN T1.acy_CountryCode ELSE T1.acy_ZipCode END  AS NVARCHAR(30))  AS  npsp__MailingPostalCode__c	-- If [acy_ZipCode] is null migrate [acy_CountryCode] to Zip/Postal Code
				,T1.acy_Country  AS  npsp__MailingCountry__c	
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				 ,T1.acy_AgencyID AS zrefId
				 ,'tblAgency' zrefSource
			FROM GDB_KADE_Final.dbo.tblAgency AS T1
			
			UNION 
	--tblOutsideVet
			SELECT
				 'ov-' +CAST(T1.OutsideVetCode AS NVARCHAR(20)) AS  [npsp__Household_Account__r:Legacy_Id__c] 
				,'ov-' +CAST(T1.OutsideVetCode AS NVARCHAR(20)) AS  Legacy_ID__c
				,'TRUE' AS  npsp__Default_Address__c	-- TRUE
				,NULL AS npsp__Address_Type__c
				,T1.OutsideVetStreet  AS npsp__MailingStreet__c
				,NULL AS npsp__MailingStreet2__c
				,T1.OutsideVetCity  AS npsp__MailingCity__c		
 				,T1.OutsideVetState  AS  npsp__MailingState__c
 				,CAST(T1.OutsideVetZipCode  AS NVARCHAR(30))  AS npsp__MailingPostalCode__c
				,T1.OutsideVetCountry  AS  npsp__MailingCountry__c
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,T1.OutsideVetCode AS zrefId
				,'trefOutsideVet' zrefSource
			FROM GDB_KADE_Final.dbo.trefOutsideVet AS T1
		
			UNION
	--tblGDBVendor
			SELECT 
				'iv-' +CAST(T1.iv_VendorID AS NVARCHAR(20))  AS  [npsp__Household_Account__r:Legacy_Id__c] 	-- concatenate 'Iv-'+[iv_VendorID]
				,'iv-' +CAST(T1.iv_VendorID AS NVARCHAR(20))  AS  Legacy_ID__c	-- concatenate 'Iv-'+[iv_VendorID]
				,'TRUE' AS  npsp__Default_Address__c	-- TRUE
				,NULL AS npsp__Address_Type__c
				,T1.iv_Address  AS  npsp__MailingStreet__c	
				,NULL AS npsp__MailingStreet2__c
				,T1.iv_City  AS  npsp__MailingCity__c	
				,T1.iv_State  AS  npsp__MailingState__c	
				,CAST(T1.iv_ZipCode  AS NVARCHAR(30))  AS  npsp__MailingPostalCode__c	
				,T1.iv_Country  AS  npsp__MailingCountry__c	
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,t1.iv_VendorID AS zrefId
				,'tblGDBVendor'AS zrefSource
			FROM GDB_KADE_Final.DBO.tblGDBVendor AS T1

 			UNION
	--tblContacts
			SELECT
				 'Con-'+CAST(T1.con_ContactID  AS NVARCHAR(30)) AS  [npsp__Household_Account__r:Legacy_Id__c] 
				,'Con-'+CAST(T1.con_ContactID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Con-'+[con_ContactID]
				,'TRUE' AS  npsp__Default_Address__c
				,NULL AS npsp__Address_Type__c
				,T1.con_Address1 AS npsp__MailingStreet__c
				,T1.con_Address2 AS npsp__MailingStreet2__c
				,T1.con_City  AS  npsp__MailingCity__c	
				,CASE WHEN T1.con_State IS NULL THEN T1.con_CountryRegion ELSE T1.con_State END  AS  npsp__MailingState__c	-- If [con_State] is null migrate [con_CountryRegion] to State/Province
				,CAST( CASE WHEN T1.con_ZipCode IS NULL THEN T1.con_CountryCode ELSE T1.con_ZipCode END  AS NVARCHAR(30)) AS  npsp__MailingPostalCode__c	-- If [con_Zipcode] is null migrate [con_CountryCode] to Zip/Postal Code
				,T1.con_Country  AS  npsp__MailingCountry__c	
		 		,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				, T1.con_ContactID  AS zrefId
			 	,'tblContacts'  AS  zrefSource	-- tblContacts	
			
		 		FROM GDB_KADE_Final.dbo.tblContacts AS T1
				INNER JOIN GDB_Final_migration.dbo.stg_tblContact_Account  AS T2 ON T1.con_ContactID=T2.zrefAccountId
				WHERE   T1.con_ContactID NOT IN (SELECT T1.con_ContactID FROM GDB_KADE_Final.DBO.tblContacts 
												  WHERE (T1.con_Address1 IS NULL AND T1.con_Address2 IS NULL AND T1.con_City IS NULL AND T1.con_State IS NULL 
														AND T1.con_Country IS NULL AND T1.con_CountryRegion IS NULL AND T1.con_CountryCode IS NULL))

			UNION 					 
	--tblVolCalOrganization
			SELECT
				 'Org-'+CAST(T1.org_OrganizationID AS NVARCHAR(30))   AS  [npsp__Household_Account__r:Legacy_Id__c]  -- concatenate 'Org-'[Org_OrganizationID]
				,'Org-'+CAST(T1.org_OrganizationID AS NVARCHAR(30))   AS  Legacy_ID__c	-- concatenate 'Org-'[Org_OrganizationID]
				,'TRUE' AS  npsp__Default_Address__c -- TRUE
				,NULL AS npsp__Address_Type__c
				,T1.org_Address  AS  npsp__MailingStreet__c	
				,NULL AS npsp__MailingStreet2__c
				,T1.org_City  AS  npsp__MailingCity__c	
				,T1.org_State  AS  npsp__MailingState__c	
				,CAST(T1.org_Zip  AS NVARCHAR(30))  AS  npsp__MailingPostalCode__c	
				,NULL AS npsp__MailingCountry__c
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,T1.org_OrganizationID  AS zrefId
			 	,'tblVolCalOrganization'  AS  zrefSource	-- tblContacts	
				FROM GDB_KADE_Final.DBO.tblVolCalOrganization AS T1	 
				WHERE T1.org_OrganizationID NOT IN (SELECT org_OrganizationID FROM GDB_KADE_Final.DBO.tblVolCalOrganization 
													WHERE org_Address IS NULL AND org_City IS NULL AND org_State IS NULL AND org_Zip IS NULL)
			UNION
    --tblStaff (1 record only for GDB)
			SELECT 
				 'GDB'  AS  [npsp__Household_Account__r:Legacy_Id__c] 
				,'GDB'  AS  Legacy_ID__c	 
				,'TRUE' AS  npsp__Default_Address__c -- TRUE
				,NULL AS npsp__Address_Type__c
				,'350 Los Ranchitos Road' AS  npsp__MailingStreet__c
				,NULL AS npsp__MailingStreet2__c
				,'San Rafael'  AS  npsp__MailingCity__c	
				,'CA'  AS  npsp__MailingState__c	
				,CAST('94903'  AS NVARCHAR(30))  AS  npsp__MailingPostalCode__c
				,'USA' AS npsp__MailingCountry__c
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,null  AS zrefId
			 	,'tblStaff'  AS  zrefSource	 
					
			UNION 
	--tblVolEquipment

			SELECT  DISTINCT
		 
				 'vps-'+CAST(T2.vps_PathScarfID AS NVARCHAR(30)) AS  [npsp__Household_Account__r:Legacy_Id__c] 
				,'vps-'+CAST(T2.vps_PathScarfID AS NVARCHAR(30)) AS Legacy_ID__c
				,'FALSE' AS  npsp__Default_Address__c 
				,NULL AS npsp__Address_Type__c
				,MAX(T1.vps_OrgAddress)  AS npsp__MailingStreet__c 
				,NULL AS npsp__MailingStreet2__c
				,T1.vps_OrgCity AS npsp__MailingCity__c
				,T1.vps_OrgState AS npsp__MailingState__c
				,CAST(T1.vps_OrgZip  AS NVARCHAR(30)) AS npsp__MailingPostalCode__c
				,NULL AS npsp__MailingCountry__c
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,NULL  AS zrefId
				,'stg_tblVolEquipment' AS zrefSource

				FROM GDB_Final_migration.DBO.stg_tblVolEquipment  T1
				LEFT JOIN GDB_Final_migration.dbo.stg_tblVolEquipment_1 AS T2 ON T1.vps_OrgName=T2.vps_OrgName
				 
				GROUP BY T2.vps_PathScarfID   
				,T1.vps_OrgName, T1.zrefAddrId, T1.vps_OrgCity,  T1.vps_OrgState, T1.vps_OrgZip 
		
			UNION 
	--trefDogSourceNew
			SELECT
				 'BRS-'+CAST(T1.brs_SourceID  AS NVARCHAR(30)) AS  [npsp__Household_Account__r:Legacy_Id__c]   
				,'BRS-'+CAST(T1.brs_SourceID  AS NVARCHAR(30))   AS  Legacy_ID__c	 
				,'TRUE' AS  npsp__Default_Address__c -- TRUE
				,NULL AS npsp__Address_Type__c
				,T1.brs_Address  AS  npsp__MailingStreet__c	
				,NULL AS npsp__MailingStreet2__c
				,T1.brs_City  AS  npsp__MailingCity__c	
				,T1.brs_State  AS  npsp__MailingState__c	
				,T1.brs_Zip  AS  npsp__MailingPostalCode__c	
				,T1.brs_Country AS npsp__MailingCountry__c
				,NULL AS npsp__County_Name__c  --filler for other tbl.
				,NULL AS Directions__c			--filler for other tbl.
				,'TRUE' AS Active__c --FB-02030
				,T1.brs_SourceID  AS zrefId
			 	,'trefDogSourceNew'  AS  zrefSource	-- trefDogSourceNew	
				FROM GDB_KADE_Final.DBO.trefDogSourceNew AS T1	 
			 
				 
			 			 
END--EOF IMP ADDRESSS  TC1 132988		Final 117195


BEGIN--AUDIT IMP ADDRESS

 	
		--COUNTRY CLEANUP
				SELECT npsp__MailingCountry__c, COUNT(*) C
				FROM GDB_Final_migration.dbo.IMP_ADDRESS
				GROUP BY npsp__MailingCountry__c
				ORDER BY c DESC

				UPDATE GDB_Final_migration.dbo.IMP_ADDRESS
				SET npsp__MailingCountry__c='United States' 
				WHERE npsp__MailingCountry__c='USA'

				SELECT npsp__MailingState__c, COUNT(*) C
				FROM GDB_Final_migration.dbo.IMP_ADDRESS
				WHERE npsp__MailingCountry__c IS null
				GROUP BY npsp__MailingState__c
				ORDER BY c DESC
                
				 UPDATE  GDB_Final_migration.dbo.IMP_ADDRESS 
				 SET npsp__MailingCountry__c = 'United States'  
				 WHERE npsp__MailingCountry__c IS NULL 
				 AND ([npsp__MailingState__c]='AL' OR [npsp__MailingState__c]='AK' OR [npsp__MailingState__c]='AZ' OR [npsp__MailingState__c]='AR' OR [npsp__MailingState__c]='CA' OR [npsp__MailingState__c]='CO' OR [npsp__MailingState__c]='CT' OR [npsp__MailingState__c]='DE' OR [npsp__MailingState__c]='DC' OR [npsp__MailingState__c]='FL'
				   OR [npsp__MailingState__c]='GA' OR [npsp__MailingState__c]='HI' OR [npsp__MailingState__c]='ID' OR [npsp__MailingState__c]='IL' OR [npsp__MailingState__c]='IN' OR [npsp__MailingState__c]='IA' OR [npsp__MailingState__c]='KS' OR [npsp__MailingState__c]='KY' OR [npsp__MailingState__c]='LA' OR [npsp__MailingState__c]='ME' 
				   OR [npsp__MailingState__c]='MD' OR [npsp__MailingState__c]='MA' OR [npsp__MailingState__c]='MI' OR [npsp__MailingState__c]='MN' OR [npsp__MailingState__c]='MS' OR [npsp__MailingState__c]='MO' OR [npsp__MailingState__c]='MT' OR [npsp__MailingState__c]='NE' OR [npsp__MailingState__c]='NV' OR [npsp__MailingState__c]='NH' 
				   OR [npsp__MailingState__c]='NJ' OR [npsp__MailingState__c]='NM' OR [npsp__MailingState__c]='NY' OR [npsp__MailingState__c]='NC' OR [npsp__MailingState__c]='ND' OR [npsp__MailingState__c]='OH' OR [npsp__MailingState__c]='OK' OR [npsp__MailingState__c]='OR' OR [npsp__MailingState__c]='PA' OR [npsp__MailingState__c]='RI' 
				   OR [npsp__MailingState__c]='SC' OR [npsp__MailingState__c]='SD' OR [npsp__MailingState__c]='TN' OR [npsp__MailingState__c]='TX' OR [npsp__MailingState__c]='UT' OR [npsp__MailingState__c]='VT' OR [npsp__MailingState__c]='VA' OR [npsp__MailingState__c]='WA' OR [npsp__MailingState__c]='WV' OR [npsp__MailingState__c]='WI' OR [npsp__MailingState__c]='WY')
 
		--DUPLICATE
			SELECT * FROM GDB_Final_migration.dbo.IMP_ADDRESS 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_ADDRESS GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c
 
		--CHECK
		 
			SELECT zrefSource, COUNT(*) C 
			FROM GDB_Final_migration.dbo.IMP_ADDRESS 
			GROUP BY zrefSource
			ORDER BY zrefSource

			SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_ADDRESS  --TC1 132988  TC2  136426


			 SELECT * FROM GDB_Final_migration.dbo.IMP_CONTACT
			 WHERE [Account:Legacy_ID__c]='22463' -- Robert's

END 

SELECT * FROM GDB_Final_migration.dbo.IMP_ADDRESS
	where [npsp__Household_Account__r:Legacy_Id__c]='22463'


	SELECT * FROM GDB_TC2_migration.dbo.IMP_ADDRESS_orgin
	where [npsp__Household_Account__r:Legacy_Id__c]='22463'