USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDogBreedingCheckList
			WHERE   SF_Object LIKE '%bree%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDogBreeding 
			WHERE  SF_Object_2 LIKE '%bre%'


END 

/*
SELECT * FROM GDB_Final_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%BREE%'


0121L000001UQDWQA4	Checklist
0121L000001UQDXQA4	Mating


*/

BEGIN -- DROP IMP

--	DROP TABLE GDB_Final_migration.DBO.IMP_Season_date
--	DROP TABLE GDB_Final_migration.DBO.IMP_BREEDING_ACTION_dmd
--	DROP TABLE GDB_Final_migration.DBO.IMP_BREEDING_ACTION_dcl
END 

BEGIN -- CREATE IMP 

		/* move to SEASON	 (new BREEDING object) per FB-00006
		
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DBG-'+CAST(T1.dbg_BreedingID AS NVARCHAR(30)) AS  Legacy_Id__c	
							,'DBG-'+CAST(T1.dbg_BreedingID AS NVARCHAR(30)) AS  [Breeding__r:Legacy_Id__c]
							,T2.dog_DogID AS  [Bitch__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,T3.dog_DogID AS  [Stud__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,'0123D0000004awiQAA'  AS  RecordTypeID	-- Litter
							,CASE T1.dbg_LitterConceived WHEN 1 THEN 'TRUE'  ELSE 'FALSE' END   AS  Litter_Conceived__c		
							,CASE T1.dbg_DonatedLitterUtero WHEN 1 THEN 'TRUE'  ELSE 'FALSE' END  AS  Donated_Litter_Utero__c		
							,CAST(T1.dbg_DateDue AS DATE)  AS  Date_Due__c		
							,CAST(T1.dbg_DateWhelp   AS DATE) AS  Date_Whelp__c		
							,CAST(T1.dbg_FstPupWhelp  AS DATE) AS  First_Pup_Whelp__c		
							,CAST(T1.dbg_LstPupWhelp   AS DATE)AS  Last_Pup_Whelp__c		
							,T1.dbg_WhelpingComments  AS  Whelping_Comments__c		
							,CASE WHEN T1.dbg_PullEarly IS NULL THEN 'No' ELSE T1.dbg_PullEarly  END  AS  Pull_Early__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_RetainedPlacentas IS NULL THEN 'No' ELSE T1.dbg_RetainedPlacentas  END  AS  Retained_Placentas__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ExcessiveBleeding IS NULL THEN 'No' ELSE T1.dbg_ExcessiveBleeding  END  AS  Excessive_Bleeding__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_NonSequentialP4 IS NULL THEN 'No' ELSE T1.dbg_NonSequentialP4  END  AS  Non_Sequential_P4__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_MultipleLH IS NULL THEN 'No' ELSE T1.dbg_MultipleLH  END  AS  Multiple_LH__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_QuickRise IS NULL THEN 'No' ELSE T1.dbg_QuickRise  END  AS  Quick_Rise__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Dystocia IS NULL THEN 'No' ELSE T1.dbg_Dystocia  END  AS  Dystocia__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_UterineTorsion IS NULL THEN 'No' ELSE T1.dbg_UterineTorsion  END  AS  Uterine_Torsion__c	-- if blank migrate as "No"	
							,T1.dbg_UltraSound  AS  Ultra_Sound__c		
							,CASE WHEN T1.dbg_ResorptionSite IS NULL THEN 'No' ELSE T1.dbg_ResorptionSite  END  AS  Resorption_Site__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ResorptionNumber IS NULL THEN 'No' ELSE T1.dbg_ResorptionNumber  END  AS  Resorption_Number__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ResorptionLocation IS NULL THEN 'No' ELSE T1.dbg_ResorptionLocation  END  AS  Resorption_Location__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_LitterSize IS NULL THEN 'No' ELSE T1.dbg_LitterSize  END  AS  Litter_Size__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Natural IS NULL THEN 'No' ELSE T1.dbg_Natural  END  AS  Natural__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Invasive IS NULL THEN 'No' ELSE T1.dbg_Invasive  END  AS  Invasive__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Malposition IS NULL THEN 'No' ELSE T1.dbg_Malposition  END  AS  Malposition__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Medical IS NULL THEN 'No' ELSE T1.dbg_Medical  END  AS  Medical__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_CSection IS NULL THEN 'No' ELSE T1.dbg_CSection  END  AS  CSection__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_PUI IS NULL THEN 'No' ELSE T1.dbg_PUI  END  AS  PUI__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_SUI IS NULL THEN 'No' ELSE T1.dbg_SUI  END  AS  SUI__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_UterineMonitoring IS NULL THEN 'No' ELSE T1.dbg_UterineMonitoring  END  AS  Uterine_Monitoring__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_FetalMonitoring IS NULL THEN 'No' ELSE T1.dbg_FetalMonitoring  END  AS  Fetal_Monitoring__c	-- if blank migrate as "No"	
							,T1.dbg_WhelpLenHours  AS  Whelp_Len_Hours__c		
							,T1.dbg_Stage2LaborLen  AS  Stage_to_Labor_Len__c		
							,T1.dbg_FTBT  AS  FTBT__c		
							,CAST(T1.dbg_FTBTStart  AS DATE) AS  FTBT_Start__c		
							,CAST(T1.dbg_HC AS DATE) AS  HC__c		
							,CAST(T1.dbg_LP AS DATE) AS  LP__c		
							,T1.dbg_Comp  AS  Comp__c		
							
							--reference
							,T1.dbg_BreedingID AS zrefID
							,'tblDogBreeding'	 AS zrefSrc
					-- INTO GDB_Final_migration.DBO.IMP_BREEDING_ACTION_dbg
					FROM GDB_KADE_Final.dbo.tblDogBreeding AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.dbg_BitchID
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T3 ON T3.dog_DogID=T1.dbg_StudID
				 	WHERE T1.dbg_LitterConceived=1
					*/

 
					SELECT  DISTINCT
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DMD-'+CAST(T1.dmd_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'dmd-'+[dmd_id]	
							,CAST(T1.dmd_Date AS DATE) AS  Date__c 		
							,'DBG-'+CAST(T2.dbg_BreedingID AS NVARCHAR(30))  AS  [Season__r:Legacy_ID__c]		-- Link [tblDogBreeding].[dbg_BreedingID]
							,T1.dmd_TypeMating  AS  Type_Mating__c		
							,T1.dmd_BitchBehavior  AS  Bitch_Behavior__c	-- Good, Poor, Fair	
							,T1.dmd_StudBehavior  AS  Stud_Behavior__c		-- Good, Poor, Fair, MB/VE	
							,CASE T1.dmd_MateAgain WHEN 1 THEN 'TRUE' ELSE 'FALSE' end AS  Mate_Again__c		
							,T1.dmd_Initials  AS  Initials__c		
							,'DSB-'+CAST(T3.dsb_CollectionID AS NVARCHAR(30)) AS  CollectionLink		-- Yes/Link [tblsemeninventory].[collectionid]
							,T1.dmd_StrawsUsed  AS  Straws_Used__c		
							,T1.dmd_Notes  AS  Mating_Comments__c	
							,NULL AS Check_List_Comments__c
							,NULL  AS  Color__c								
							,NULL  AS  Color_Comments__c		
							,NULL AS  Vaginal_Smear__c				
							,NULL  AS  Vaginal_Smear_Comments__c		
							,NULL  AS  Vaginoscopy__c					
							,NULL  AS  Vaginoscopy_Comments__c		
							,NULL AS  Progesterone_Assay__c	
							,NULL  AS  Progesterone_Assay_Comments__c		
							,NULL  AS  Swelling__c						
							,NULL  AS  Swelling_Comments__c		
							,NULL  AS  LHAssay__c							
							,NULL  AS  LHAssay_Comments__c		
									
														
							,'0121L000001UQDXQA4'  AS  RecordTypeID	-- Mating	

							--reference
							,T1.dmd_ID AS zrefID
							,'tblDogMatingDates'	 AS zrefSrc
					INTO GDB_Final_migration.DBO.IMP_SEASON_DATE
					FROM GDB_KADE_Final.dbo.tblDogMatingDates AS T1 
					LEFT JOIN GDB_KADE_Final.dbo.tblDogBreeding AS T2 ON T2.dbg_BreedingID=T1.dmd_BreedingID
					LEFT JOIN GDB_KADE_Final.dbo.tblDogSemenInventory AS T3 ON T3.dsb_CollectionID=T1.dmd_CollectionID

		UNION
			 
					SELECT  DISTINCT
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DCL-'+CAST(T1.dcl_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- dcl-'+[dcl_Id]	
							,CAST(T1.dcl_Date AS DATE) AS  Date__c
							,'DBG-'+CAST(T2.dbg_BreedingID AS NVARCHAR(30))  AS  [Season__r:Legacy_ID__c]		-- Link [tblDogBreeding].[dbg_BreedingID]
							,NULL  AS  Type_Mating__c
							,NULL  AS  Bitch_Behavior__c		
							,Null AS  Stud_Behavior__c
							,NULL AS Mate_Again__c
							,NULL AS Initials__c	
							,NULL AS CollectionLink
							,NULL AS Straws_Used__c	
							,NULL AS Mating_Comments__c
							,T1.dcl_Comments  AS  Check_List_Comments__c
							,T3.ResultText  AS  Color__c								-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_ColorComments  AS  Color_Comments__c		
							,T4.ResultText AS  Vaginal_Smear__c				-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_VaginalSmearComments  AS  Vaginal_Smear_Comments__c		
							,T5.ResultText  AS  Vaginoscopy__c					-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_VaginoscopyComments  AS  Vaginoscopy_Comments__c		
							,T6.ResultText  AS  Progesterone_Assay__c	-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_ProgesteroneAssayComments  AS  Progesterone_Assay_Comments__c		
							,T7.ResultText  AS  Swelling__c						-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_SwellingComments  AS  Swelling_Comments__c		
							,T8.ResultText  AS  LHAssay__c							-- migrate value from [ResultText]	-- Yes/Link trefDogBreedingResultCode.ResultCode
							,T1.dcl_LHAssayComments  AS  LHAssay_Comments__c		
							
									
							,'0121L000001UQDWQA4'  AS  RecordTypeID	-- Checklist	

							--reference
							,T1.dcl_ID AS zrefID
							,'tblDogBreedingCheckList'	 AS zrefSrc
						--	INTO GDB_Final_migration.DBO.IMP_SEASON_DATE_dcl
							FROM GDB_KADE_Final.dbo.tblDogBreedingCheckList AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblDogBreeding AS T2 ON T2.dbg_BreedingID=T1.dcl_BreedingID
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T3 ON T3.ResultCode = T1.dcl_Color
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T4 ON T4.ResultCode = T1.dcl_VaginalSmear
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T5 ON T5.ResultCode = T1.dcl_Vaginoscopy
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T6 ON T6.ResultCode = T1.dcl_ProgesteroneAssay
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T7 ON T7.ResultCode = T1.dcl_Swelling
							LEFT JOIN GDB_KADE_Final.dbo.trefDogBreedingResultCode AS T8 ON T8.ResultCode = T1.dcl_LHAssay

--TC2: 43,384 final 45002
END 

BEGIN-- AUDIT

 
	--SELECT * FROM GDB_Final_migration.dbo.IMP_BREEDING_ACTION_dbg
	--WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_BREEDING_ACTION_dbg GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_SEASON_DATE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_SEASON_DATE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	
	
			 
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_SEASON_DATE_dcl
	
END 
 
