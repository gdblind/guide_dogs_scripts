USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGDBInvDispenseTrans
			WHERE   SF_Object LIKE '%entr%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_RESOURCE

END 

BEGIN -- CREATE IMP ENTRY
  			SELECT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'vps-'+ CAST(T1.vps_PathScarfID  AS NVARCHAR(30)) AS [Legacy_Id__c]
					,'vps-'+ CAST(T2.vps_PathScarfID  AS NVARCHAR(30)) AS [Account__r:Legacy_Id__c]
	 				,'vps-'+ T1.vps_Item    AS [Inventory__r:Legacy_Id__c] 
					,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]
					,T4.dog_DogID  AS [Dog__r:Legacy_Id__c]
				
				 	,NULL AS Instructions__c
					,NULL AS Serial_Number__c
					,NULL AS Issued_Where__c
					,NULL AS Issued_By__c
					,NULL AS Purchase_Date__c
		 			,'tblVolEquipment' AS zrefSrc
					 
	
			INTO GDB_Final_migration.dbo.IMP_RESOURCE
			FROM GDB_KADE_Final.dbo.tblVolEquipment AS T1
			LEFT JOIN GDB_Final_migration.dbo.stg_tblVolEquipment_1 AS T2 ON T1.vps_OrgName=T2.vps_OrgName
			LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.vps_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T4 ON T4.dog_DogID=T1.vps_DogID
	
	 	UNION 

			SELECT  DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'DEQ-'+CAST(T1.deq_EquipID AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'deq - '+[deq_EquipID]
					,NULL AS [Account__r:Legacy_Id__c]
					,'deq-'+T4.Item_Name  AS  [Inventory__r:Legacy_ID__c]
					,T3.psn_PersonID AS  [Contact__r:Legacy_ID__c]	-- link to [tblPerson] through [tblPersonRel]
					,T5.dog_DogID AS  [Dog__r:Legacy_Id__c]	
				 	
					,CASE WHEN T4.[Classifaction] ='Client Issued Food' THEN T1.deq_Size ELSE NULL end  AS  Instructions__c	-- migarte when [CHART_EquipFood].[Type]=Food
					,T1.deq_SerialNum  AS  Serial_Number__c	
					,T1.deq_IssuedWhere  AS  Issued_Where__c	
					,X.ID  AS  Issued_By__c	
					,CAST(T1.deq_IssuedDate  AS DATE) AS  Purchase_Date__c
					 
					,'stg_tblEquipFood' AS zrefSrc
					 
			FROM GDB_Final_migration.dbo.stg_tblEquipFood AS T1
			INNER JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T1.deq_ClientID=T2.prl_ClientInstanceID
			INNER JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T1.deq_DogID
			LEFT JOIN GDB_Final_migration.dbo.CHART_EquipFood  AS T4 ON T1.deq_EquipFood =T4.deq_EquipFood 
																			 AND T1.deq_Type=T4.deq_Type 
																			 AND T1.deq_Size=T4.deq_Size
			LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X ON T1.deq_Issuedby=X.ADP__C
			 --test WHERE t1.deq_EquipID='19628'
	 
		  	 --TC2: 53558 final 56082

END 


BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_RESOURCE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_RESOURCE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_RESOURCE 
			GROUP BY zrefSrc
		 
END 




























