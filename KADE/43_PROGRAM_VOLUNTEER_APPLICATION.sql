USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblPuppyRaisingApplication
			WHERE   SF_Object LIKE '%VOL%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

SELECT * FROM GDB_Final_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%VOL%'
/*
0121L000001UQDGQA4	Breeder Custodian Application
0121L000001UQDHQA4	Dog Handling Assessment
0121L000001UQDIQA4	Puppy Club Application
0121L000001UQDJQA4	Puppy Placement Questionnaire
0121L000001UQDKQA4	Puppy Raising Application
0121L000001UQDLQA4	Puppy Raising Interest Request
*/

BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_PROGRAM_VOLUNTEER_APPLICATION

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'BKA-'+CAST(T1.bka_bkaID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "BKA-" with bka_bkaID	
							,'0121L000001UQDGQA4'  AS  RecordTypeID	--Breeder Custodian Application
							,T2.psn_PersonID AS [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
							,T1.bka_Breed  AS  Breed__c		
							,T1.bka_Sex  AS  Sex__c		
							,T1.bka_Source  AS  Source__c		
							,CASE T1.bka_Status WHEN 'F' THEN 'Failed' 
												WHEN 'H' THEN 'Hold' 
												WHEN 'K' THEN 'Keeper' 
												WHEN 'N' THEN 'Not Interested' 
												WHEN 'P' THEN 'Priority' 
												WHEN 'R' THEN 'Regular' 
												WHEN 'W' THEN 'Waiting' END  AS  Status__c	-- F=Failed H=Hold K=Keeper N=Not Interested P=Priority R=Regular W=Waiting	
							,CAST(T1.bka_AppSent AS DATE) AS  App_Sent__c		
							,CAST(T1.bka_AppDate AS DATE) AS  Application_Date__c		
							,T1.bka_AppOK  AS  App_OK__c		
							,T1.bka_IntOK  AS  Int_OK__c		
							,T1.bka_DogEval  AS  Dog_Eval__c		
							,T1.bka_HomeEval  AS  Home_Eval__c		
							,CAST(T1.bka_DateHold AS DATE) AS  Date_Hold__c		
							,CAST(T1.bka_DatePlaced AS DATE) AS  Date_Placed__c	
							,NULL AS Home_Season_Pup__c
							,NULL AS [Puppy_Club__r:Legacy_Id__c]
							,NULL AS [Leader__r:Legacy_Id__c]
							,NULL AS [CFR__r:Legacy_Id__c]
							,NULL AS Learn_About_GDB__c

							,CAST(T1.bka_CreatedDate AS DATE) AS  CreatedDate		

						--reference
							,T1.bka_bkaID AS zrefID
							,'tblBreederKeeperApplicants' AS zrefSrc
		 			INTO GDB_Final_migration.DBO.IMP_PROGRAM_VOLUNTEER_APPLICATION
					FROM GDB_KADE_Final.dbo.tblBreederKeeperApplicants AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON T2.psn_PersonID=T1.bka_PersonID
				UNION 
					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 							,'PRA-'+CAST(T1.pra_ApplicationID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "PRA-" with pra_ApplicationID	
							,'0121L000001UQDKQA4'  AS  RecordTypeID	-- Puppy Raising Application
							,NULL AS [Contact__r:Legacy_Id__c]
							,NULL AS Breed__c
							,NULL AS Sex__c
							,NULL AS Source__c
							,NULL AS Status__c
							,NULL AS App_Sent__c
							,CAST(T1.pra_ApplicationDate AS DATE) AS  Application_Date__c
							,NULL AS App_OK__c
							,NULL AS Int_OK__c
							,NULL AS Dog_Eval__c		
							,NULL AS Home_Eval__c
							,NULL AS Date_Hold__c
							,NULL AS Date_Placed__c
							,T1.pra_HomeSeasonPup  AS  Home_Season_Pup__c		
							,'pcd-'+CAST(T2.pcd_ClubID  AS NVARCHAR(30))  AS  [Puppy_Club__r:Legacy_Id__c]	-- Link to Account created from [tblPuppyClubDetail]	-- Link [trefPuppyClub].[clb_ClubID]
							,T3.psn_PersonID  AS  [Leader__r:Legacy_Id__c]		-- Link [tblPerson].[psn_PersonID]
							,T4.psn_PersonID  AS  [CFR__r:Legacy_Id__c]		-- Link [tblPerson].[psn_PersonID]
							,T1.pra_LearnAboutGDB  AS  Learn_About_GDB__c	
						 			
							,CAST(T1.pra_CreatedDate AS DATE) AS  CreatedDate		
							--reference
							,T1.pra_ApplicationID AS zrefID
							,'tblPuppyRaisingApplication' AS zrefSrc
					 	
					FROM GDB_KADE_Final.DBO.tblPuppyRaisingApplication AS T1
					LEFT JOIN (SELECT T.pcd_ClubID
						FROM GDB_KADE_Final.dbo.trefPuppyClub AS TT
						INNER JOIN GDB_KADE_Final.dbo.tblPuppyClubDetail AS T ON T.pcd_ClubID=TT.clb_ClubID
						GROUP BY T.pcd_ClubID ) AS T2 ON T2.pcd_ClubID=T1.pra_PuppyClubID
					LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pra_LeaderID
					LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T1.pra_AdvisorID

					--TC2: 37874  FINAL 38,98

END 

BEGIN-- AUDIT

	SELECT * FROM GDB_Final_migration.dbo.IMP_PROGRAM_VOLUNTEER_APPLICATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_PROGRAM_VOLUNTEER_APPLICATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	
	SELECT * FROM GDB_Final_migration.DBO.IMP_PROGRAM_VOLUNTEER_APPLICATION
 
	SELECT COUNT(*) FROM  GDB_Final_migration.DBO.IMP_PROGRAM_VOLUNTEER_APPLICATION
 
 END 