BEGIN
DROP TABLE  GDB_Final_migration.DBO.IMP_PROCEDURE_AUTHORIZATION
END 


SELECT DISTINCT --GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				'Vpa-'+ CAST(T1.vpa_ID  AS NVARCHAR(30)) AS  Legacy_ID__c							-- concatenate 'Vpa-'+[vpa_ID]
				,T2.aut_Type  AS  Type__c															-- migrate value from [aut_Type]  
				,T1.vpa_Amount AS Amount__c   --FB-001462 Amount_Paid__c	
				,'Vdp-'+ CAST(T4.vdp_ProcID AS varchar(30)) AS  [Vet_Procedure__r:Legacy_Id__c]		--Link tblVetProc.vdp_ProcID
				,T1.vpa_ID AS zrefID
				,'tblVetProcAuth' AS zrefSrc
				,'Vta-' + CAST(T1.vpa_VetAuthID AS NVARCHAR(20)) AS [Authorization__r:Legacy_ID__c]
				--,T4.vdp_ProcCode AS [Procedure__r:Legacy_ID__c]
			
			INTO GDB_Final_migration.dbo.IMP_PROCEDURE_AUTHORIZATION
			FROM GDB_KADE_Final.DBO.tblVetProcAuth AS T1
			LEFT JOIN GDB_KADE_Final.dbo.trefVetAuthorizationType AS T2 ON T2.aut_ID=T1.vpa_AuthType
			LEFT JOIN GDB_KADE_Final.dbo.tblVetAuthorization AS T3 ON T3.vta_ID=T1.vpa_VetAuthID
			LEFT JOIN GDB_KADE_Final.dbo.tblVetProc AS T4 ON T4.vdp_ProcID = T1.vpa_ProcID
			LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T5 ON T5.vdl_VetEntryID=T4.vdp_VetEntryID
			LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T6 ON T6.dog_DogID=T5.vdl_DogID
			

			SELECT * FROM GDB_Final_migration.DBO.IMP_PROCEDURE_AUTHORIZATION
			WHERE [Vet_Procedure__r:Legacy_Id__c] IS NOT NULL
			ORDER BY [Vet_Procedure__r:Legacy_Id__c]
			
