USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGDBInvDispese
			WHERE   SF_Object LIKE '%order%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.dbo.IMP_GDB_ORDER
	       
END 

BEGIN -- CREATE IMP 
		UPDATE GDB_KADE_Final.dbo.tblGDBInvDispese
			SET id_EmployeeID = NULL
			WHERE id_EmployeeID= ''

			


					SELECT  DISTINCT 
						CASE WHEN X1.ID IS NOT NULL THEN X1.id ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]()  END  AS  OwnerID		-- Link [tblStaff].[FileNum]
						,'id-'+CAST(T1.id_DispenseID AS NVARCHAR(30)) AS  Legacy_ID__c	-- Concatenate 'id-'+[id_DispenseID]	
						,T2.psn_PersonID AS  [Contact__r:Legacy_Id__c]	-- Link [tblPerson].[PersonID]
						,T3.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
 						,CAST(T1.id_Date AS DATE) AS  Date__c		
						,T4.[Description]  AS  Campus__c				-- Migrate values from [Description]	-- Ref [trefFacility].[FacilityID]
						,IIF(T5.iidc_Department='Support Center', 'SC', T5.iidc_Department)  AS  Department__c	-- Migrate values from [trefGDBInvDepartments].[iidc_Department]	-- Ref [trefGDBInvDepartments].[iidc_DeptCode]
						,T1.id_OrderPrep  AS  Order_Prep__c		
						,T1.id_TrackingNum  AS  Tracking_Number__c		
						,CAST(T1.id_ShipDate AS DATE)  AS  Ship_Date__c		
						,T1.id_OrderClosed  AS  Closed__c		
						,T1.id_Notes  AS  Notes__c		

					 INTO GDB_Final_migration.dbo.IMP_GDB_ORDER
					FROM GDB_KADE_Final.dbo.tblGDBInvDispese AS T1
					LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T2 ON T2.psn_PersonID=T1.id_PersonID
					LEFT JOIN GDB_KADE_Final.DBO.tblDog AS T3 ON T3.dog_DogID=T1.id_DogID
					LEFT JOIN GDB_Final_migration.DBO.XTR_USER AS X1 ON X1.ADP__C=T1.id_EmployeeID
					LEFT JOIN GDB_KADE_Final.DBO.trefFacility AS T4 ON T4.FacilityID=T1.id_Campus
					LEFT JOIN GDB_KADE_Final.DBO.trefGDBInvDepartments AS T5 ON T5.iidc_DeptCode=T1.id_Department




END  -- TC1 : 2692  TC2: 3010

BEGIN-- AUDIT

			SELECT * FROM GDB_Final_migration.dbo.IMP_GDB_ORDER 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_GDB_ORDER GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			


			SELECT COUNT(*) FROM GDB_KADE_Final.DBO.tblGDBInvDispese

			SELECT * FROM GDB_KADE_Final.dbo.tblGDBInvDispese
			WHERE id_DispenseID='4285'
			 

END 






























