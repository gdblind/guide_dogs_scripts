USE GDB_Final_migration
GO


BEGIN--maps 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblFinalStudentDogSummary
			WHERE   SF_Object LIKE '%class%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%class%'

0121L000001UQClQAO	Continued Assessment
0121L000001UQCmQAO	Dog Final
0121L000001UQCnQAO	Student Final
0121L000001UQCoQAO	Student and Dog Final
0121L000001UQEAQA4	Student Concerns
0121L000001UQEFQA4	Class Meeting

*/

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_cfd
	DROP TABLE GDB_Final_migration.DBO.IMP_CLASS_REPORT_cfd
END 

BEGIN -- CREATE IMP_CLASS_REPORT_cfd 

					SELECT	DISTINCT
									GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0121L000001UQCmQAO' AS RecordTypeId   --Dog Final
									,'CFD-'+CAST(T1.cfd_ID  AS NVARCHAR(30))+'_'+ CAST(t6.drl_DogID AS VARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CFD-" with cfd_ID	
									,T4.psn_PersonID AS [Client__r:Legacy_ID__c]-- Link [tblPersonRel].[prl_ClientInstanceID] FB-01095
 								 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_ID__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
									,'Staff-'+T2.cli_Instructor AS [Instructor__r:Legacy_ID__c]--FB-01156
									,T2.cli_Instructor AS [Instructor__r:ADP__c]
									,T2.cli_Supervisor AS [Supervisor_r:Adp__c]
									,T6.drl_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
								 	,CAST(T1.cfd_Health  AS NVARCHAR(MAX)) AS  Health__c		
									,T1.cfd_FinalClassPace  AS  Final_Class_Pace__c		
									,T1.cfd_PaceDescription  AS  Pace_Description__c		
									,T1.cfd_Sensitivity  AS  Sensitivity__c		
									,T1.cfd_Energy  AS  Energy__c		
									,T1.cfd_Confidence  AS  Confidence__c		
									,T1.cfd_Willingness  AS  Willingness__c		
									,T1.cfd_Focus  AS  Focus__c		
									,T1.cfd_Dogs  AS  Dogs__c		
									,T1.cfd_Cats  AS  Cats__c		
									,T1.cfd_Birds  AS  Birds__c		
									,T1.cfd_Food  AS  Food__c		
									,T1.cfd_People  AS  People__c		
									,T1.cfd_Scents  AS  Scents__c		
									,CAST(T1.cfd_OtherDistractions AS NVARCHAR(MAX)) AS  Brief_Summary_on_Dog__c	--FB_00329	
									,CAST(T1.cfd_TOH_HandlingTechnique  AS NVARCHAR(MAX)) AS  Most_Effective_Handling_Technique__c	  --FB-00528
									,CAST(T1.cfd_TOH_RewardStrategy  AS NVARCHAR(MAX)) AS  Reward_Strategy__c		--FB-00528
									--,CAST(T1.cfd_TOH_Pace AS NVARCHAR(MAX))  AS  Pace__c		--FB-00528 and FB-00329
									--,CAST(T1.cfd_TOH_OtherDistractions  AS NVARCHAR(MAX)) AS  Other_Distractions__c		--FB-00528 and FB-00329
									,T1.cfd_RelievingNormalLimits  AS  Relieving_Normal_Limits__c		
									,T1.cfd_Inconsistent  AS  Inconsistent__c		
									,T1.cfd_Distracted  AS  Distracted__c		
									,T1.cfd_ReluctanttoMove  AS  Reluctant_to_Move__c		
									,T1.cfd_#Urinations  AS  Number_of_Urinations__c
									,T1.cfd_SlowCementRelieve AS Slow_Cement_Relieve__c  --updated per FB-00529
									,T1.cfd_WeakHandlerTech AS Weak_Handler_Tech__c		--updated per FB-00529
									,T1.cfd_InsuffienctTime AS Insufficient_Time__c		--updated per FB-00529
									,T1.cfd_Reissue AS Reissue__c		--updated per FB-00529
									,T1.cfd_#Defecations AS Number_of_Defecations__c	--updated per FB-00529	
									,CAST(T1.cfd_Challenges  AS NVARCHAR(MAX)) AS  Challenges_Resolutions_Areas_for_Develop__c		--updated per FB-00329
									,T1.cfd_CreatedBy  AS  KD_Created_By__c		
									,CAST(T1.cfd_CreatedDate AS DATE) AS  CreatedDate		
									,T1.cfd_ModifiedBy  AS  KD_Modified_By__c		
									,CAST(T1.cfd_ModifiedDate AS DATE) AS  KD_Modified_Date__c		
									 	
					 INTO GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd
					FROM GDB_KADE_Final.dbo.tblClassFinalDog AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.cfd_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.cfd_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 	LEFT JOIN GDB_KADE_Final.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
							INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
							INNER JOIN GDB_KADE_Final.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID

 
END-- end IMP_CLASS_REPORT_cfd: tc1:  3667 tC2: 3820  final 4039

BEGIN -- AUDIT IMP_CLASS_REPORT_cfd
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
		--update legacy id to make unique. 
	UPDATE GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy_Id__c] AS NVARCHAR(30))
		WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)


	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfd
	 
END 

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_cfs
	DROP TABLE GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs 
END 

BEGIN -- CREATE IMP_CLASS_REPORT_cfs
 
		 SELECT DISTINCT
					GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'0121L000001UQCnQAO' AS RecordTypeId   -- Student Final
					,'CFS-'+CAST(T1.cfs_ID AS NVARCHAR(30)) +'_'+ CAST(T6.drl_DogID AS VARCHAR(30))  AS  Legacy_Id__c	-- Concatenate "CFS-" with cfs_ID	
					,T4.psn_PersonID AS [Client__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID] --FB-01095
 				 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_Id__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
					,T6.drl_DogID AS [Dog__r:Legacy_ID__c]
					,T2.cli_Supervisor AS [Supervisor_r:Adp__c]
					,'Staff-'+T2.cli_Instructor AS [Instructor__r:Legacy_ID__c]
					,T2.cli_Instructor AS [Instructor__r:ADP__c]
					,T1.cfs_VisionLoss  AS  Vision_Loss__c		
					,T1.cfs_Retrain  AS  Retrain__c		
					,T1.cfs_DegreeVisionLoss  AS  Degree_Vision_Loss__c		
					,T1.cfs_ClassType  AS  Class_Type__c		
					,T1.cfs_LearningStyle  AS  Learning_Style__c		
					,CAST(T1.cfs_Health  AS NVARCHAR(MAX)) AS  Health__c		
					,CAST(T1.cfs_ClassExperience  AS NVARCHAR(MAX)) AS  Class_Experience__c		
					,CAST(T1.cfs_OM   AS NVARCHAR(MAX))AS  OM__c		
					,T1.cfs_DogChange  AS  Dog_Change__c		
					,CAST(T1.cfs_Challenges  AS NVARCHAR(MAX)) AS  Challenges_Resolutions_Areas_for_Develop__c		
					,CAST(T1.cfs_DHS_FoodReward  AS NVARCHAR(MAX)) AS  Food_Reward__c		
					,CAST(T1.cfs_DHS_PassDirections  AS NVARCHAR(MAX)) AS  Pass_Directions__c		
					,CAST(T1.cfs_DHS_SecReinforcement AS NVARCHAR(MAX))  AS  Sec_Reinforcement__c		
					,CAST(T1.cfs_DHS_HandlingStyle  AS NVARCHAR(MAX)) AS  Handling_Style__c		
					,CAST(T1.cfs_DHS_HandlingTech  AS NVARCHAR(MAX)) AS Most_Effective_Handling_Technique__c		
					,CAST(T1.cfs_DHS_DogAsGuide  AS NVARCHAR(MAX)) AS  Dog_As_Guide__c		
					,CAST(T1.cfs_DHS_PacePull  AS NVARCHAR(MAX)) AS  Pace_Pull__c		
					,CAST(T1.cfs_DHS_Escalator  AS NVARCHAR(MAX)) AS  Escalator__c		
					,CAST(T1.cfs_Goals  AS NVARCHAR(MAX)) AS  Goals__c		
					,T1.cfs_Graduated  AS  Graduated__c		
					,CAST(T1.cfs_CreatedDate AS DATE)  AS  CreatedDate	
					,T1.cfs_ID AS zrefSourceID	

						 		INTO GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs 
								FROM GDB_KADE_Final.dbo.tblClassFinalStudent AS T1
								LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.cfs_ClientID
									LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
									LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
								LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.cfs_ClientID
									LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 	            INNER JOIN GDB_KADE_Final.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
									INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
									INNER JOIN GDB_KADE_Final.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID
						

END -- end IMP_CLASS_REPORT_cfs: tc1:  3507  tc2: 3878 final 4100


BEGIN -- AUDIT IMP_CLASS_REPORT_cfs
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cfs
	 
END 

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_ca
	DROP TABLE GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca
END 

BEGIN -- CREATE IMP_CLASS_REPORT_ca --field labels updated per FB-00395

							SELECT  DISTINCT
									GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0121L000001UQClQAO' AS RecordTypeId   -- Continued Assessment
 									,'CA-'+CAST(T1.ca_ContAssessID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CA-" with ca_ConstAssessID	
									,T4.psn_PersonID AS [Client__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
									,'Staff-'+T2.cli_Instructor AS [Instructor__r:Legacy_ID__c]--FB-01156
									,t2.cli_Instructor AS [Instructor__r:ADP__c]
									,T2.cli_Supervisor AS [Supervisor_r:Adp__c] --FB-01156
									,'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))  AS  [Application__r:Legacy_Id__c]		 
 									,T1.ca_AGSNumber  AS  CA_Number__c		
									,T1.ca_Campus  AS  Campus__c		
									,CAST(T1.ca_StartDate  AS DATE) AS  Start_Date__c		
									,CAST(T1.ca_EndDate  AS DATE)  AS   End_Date__c				
									,T1.ca_OMTravelInd  AS  Travel_indep_in_dorm_after_orientation__c	
									,T1.ca_OMStraightLine  AS  Maintain_straight_line__c		
									,T1.ca_OMManeuverOb  AS  maneuver_around_obstacles__c	
									,T1.ca_OMCues  AS  Use_environment_cues__c		
									,T1.ca_OMSolveProb  AS  solve_problems_systematic_manner__c		
									,T1.ca_OMTrafficControl  AS  Identify_traffic_control_at_intersec__c		
									,T1.ca_OMTrafficFlow  AS  Determine_direction_of_traffic_flow__c		
									,T1.ca_OMStreetCross  AS  Initiate_street_crossings___c		
									,T1.ca_OMAlignCurb  AS  Align_at_a_curb_for_crossing__c		
									,T1.ca_OMCorrectVeer  AS  Correct_for_veer_street_crossing__c		
									,CAST(T1.ca_OMComments AS NVARCHAR(MAX))  AS  Correct_for_veer_comments__c		
									,T1.ca_JunoUseHarness  AS  Effectively_use_harness_and_leash__c	
									,T1.ca_JunoFootwork  AS  Execute_footwork_in_correct_manner__c		
									,T1.ca_JunoSequence  AS  Sequence_steps_and_commands__c		
									,T1.ca_JunoControl  AS  Convey_assertiveness_and_control__c		
									,T1.ca_JunoPraise  AS  Relate_pleasure_praise_and_affection__c		
									,T1.ca_JunoReactions  AS  React_quickly_and_effectively__c		
									,T1.ca_JunoRelate  AS  Relate_to_the_dog__c		
									,T1.ca_JunoIntegrate  AS  Integrate_new_practices__c		
									,CAST(T1.ca_JunoComments  AS NVARCHAR(MAX)) AS  New_practices_comments__c		
									,CASE T1.ca_PMSMedStable WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Was_applicant_medically_stable__c	
									,CAST(T1.ca_PMSMedStableComments AS NVARCHAR(MAX))  AS  Medically_stable_comments__c		
									,CASE T1.ca_PMSPhysStable  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Was_applicant_physically_stable__c		
									,CAST(T1.ca_PMSPhysStableComments AS NVARCHAR(MAX)) AS  Physically_stable_comments__c		
									,CASE T1.ca_STNeuro  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Does_applicant_have_neuropathy__c		
									,CAST(T1.ca_STNeuroComments AS NVARCHAR(MAX))  AS  Neuropathy_comments__c		
									,CASE T1.ca_STProp  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Proprioception_problems__c		
									,CAST(T1.ca_STPropComments AS NVARCHAR(MAX))  AS  Proprioception_comments__c		
									,CASE T1.ca_STBalance  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Balance_problems__c		
									,CAST(T1.ca_STBalanceComments AS NVARCHAR(MAX))  AS  Balance_problem_Comments__c		
									,CASE T1.ca_StSupport  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Applicant_require_a_support_cane__c		
									,CAST(T1.ca_STSupportComments AS NVARCHAR(MAX))  AS  Support_cane_comments__c		
									,CASE T1.ca_SHNormLimits  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  	Ability_to_hear_within_normal_limits__c	
									,CAST(T1.ca_SHNormLimitsComments AS NVARCHAR(MAX))  AS  Normal_limits_comments__c	
									,CASE T1.ca_SHLossRightEar WHEN '1' THEN 'TRUE' ELSE 'FALSE' END   AS  Hearing_Loss_Right_Ear__c		
									,CASE T1.ca_SHLossLeftEar  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Hearing_Loss_Left_Ear__c		
									,CASE T1.ca_SHAidRightEar  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Hearing_aids_Right_Ear__c		
									,CASE T1.ca_SHAidLeftEar   WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Hearing_aids_Left_Ear__c		
									,CASE T1.ca_SHLocal  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  H_A__effective_for_sound_localization__c		
									,CASE T1.ca_SHSpeech WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END   AS  Normal_speech_apprehension__c	
									,CASE T1.ca_SHFM  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Does_applicant_use_FM_system__c	
									,T1.ca_CSApplyInstr  AS  Ability_to_apply_instruction__c	
									,T1.ca_CSRememberInstr  AS  Ability_to_remember_instruction__c		
									,T1.ca_CSSpatial  AS  Demonstrate_spatial_awareness__c		
									,T1.ca_CSExtInt  AS  Demonstrate__ext_int_awareness__c		
									,T1.ca_CSConfidence  AS  Demonstrate_sufficient_confidence__c		
									,T1.ca_CSMotivation  AS  Describe_applicant_s__motivation__c		
									,CASE T1.ca_CSConsistent  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Did_abilities_remain_consistent__c		
									,CAST(T1.ca_CSConsistentComments AS NVARCHAR(MAX)) AS  Consistent_comments__c		
									,CAST(T1.ca_ESInstructionComments AS NVARCHAR(MAX))  AS  Attitude_during_instruction__c		
									,CASE T1.ca_ESConsistent  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Consistent_demeanor__c		
									,CAST(T1.ca_ESConsistentComments AS NVARCHAR(MAX)) AS  Consistent_demeanor_comments__c		
									,CAST(T1.ca_ContAssessSummary AS NVARCHAR(MAX))  AS  Continued_assessment_summary__c		
									,CASE T1.ca_DeterAccept  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Recommended_for_Training__c		
									,T1.ca_DeterAcceptClass  AS  If_yes_Regular_Custom_Class__c		
								 	,X1.ID AS Continued_Assessment_Team_1__c				-- Link [tblStaff].[FileNum]
							 		,X2.ID AS  Continued_Assessment_Team_2__c				-- Link [tblStaff].[FileNum]
							 		,X3.ID AS  Continued_Assessment_Team_3__c				-- Link [tblStaff].[FileNum]
							 		--,X4.ID AS  Con_Assess_Reporter_Emp__c		-- Link [tblStaff].[FileNum]
									,CAST(T1.ca_CreatedDate  AS DATE) AS  CreatedDate		

								INTO GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca 
								FROM GDB_KADE_Final.dbo.tblClientContAssess AS T1
							 	LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.ca_ClientID
								LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T2.cli_ClientID
									LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID				 			
								LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.ca_ContAssessEmp1
								LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=T1.ca_ContAssessEmp2
								LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X3 ON X3.ADP__C=T1.ca_ContAssessEmp3
								LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X4 ON X4.ADP__C=T1.ca_ContAssessReporterEmp 


END -- end IMP_CLASS_REPORT_ca: tc1: 138   TC2: 140  final 143

BEGIN -- AUDIT IMP_CLASS_REPORT_ca
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_ca
	 
END 

/*****************************************************************************************************************************/


BEGIN -- DROP IMP_CLASS_REPORT_fsd
	DROP TABLE GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd
END 

BEGIN -- CREATE IMP_CLASS_REPORT_fsd

							SELECT  DISTINCT
									GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0121L000001UQCoQAO' AS RecordTypeId   -- Student and Dog Final
 									,'FSD-'+CAST(T1.fsd_ClientID AS NVARCHAR(30))+'_'+CAST(T6.drl_DogID AS VARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "FSD-" with fsd_ClientID	-- Link [tblPersonRel].[prl_ClientInstanceID]
									,T4.psn_PersonID AS [Client__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID] FB-01095
 								 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_Id__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
									,T6.drl_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
									,'Staff-'+T2.cli_Instructor AS [Instructor__r:Legacy_ID__c]--FB-01156
									,T2.cli_Instructor AS [Instructor_User__r:ADP__c]
									,T2.cli_Supervisor AS [Supervisor_r:Adp__c] --Fb-01156
									,T1.fsd_AltEquip  AS  Alt_Equip__c		
									,T1.fsd_ApplicationGuideWork  AS  Application_Guide_Work__c		
									,T1.fsd_ApplicationObedience  AS  Application_Obedience__c		
									,T1.fsd_AttitudeClassmates  AS  Attitude_Classmates__c		
									,T1.fsd_AttitudeCriticism  AS  Attitude_Criticism__c		
									,T1.fsd_AttitudeDog  AS  Attitude_Dog__c		
									,T1.fsd_AttitudeInstruction  AS  Attitude_Instruction__c		
									,T1.fsd_AttitudeSchool  AS  Attitude_School__c		
									,T1.fsd_Balance  AS  Balance__c		
									,T1.fsd_CareOfDog  AS  Care_Of_Dog__c		
									,T1.fsd_Coordination  AS  Coordination__c		
									,T1.fsd_Distractions  AS  Distractions__c		
									,T1.fsd_DogCF  AS  Dog_CF__c		
									,T1.fsd_DogChange  AS  Dog_Change__c		
									,T1.fsd_DogDormBehavior  AS  Dog_Dorm_Behavior__c		
									,T1.fsd_DogGuideWork  AS  Dog_Guide_Work__c		
									,T1.fsd_DogLeashRelieving  AS  Dog_Leash_Relieving__c		
									,T1.fsd_DogObedience  AS  Dog_Obedience__c		
									,T1.fsd_DogWorkCF  AS  Dog_Work_CF__c		
									,T1.fsd_Grooming  AS  Grooming__c		
									,T1.fsd_HealthBegin  AS  Health_Begin__c		
									,T1.fsd_HealthEnd  AS  Health_End__c		
									,T1.fsd_Hearing  AS  Hearing__c		
									,CAST(T1.fsd_Narrative AS NVARCHAR(30)) AS  Narrative__c		
									,T1.fsd_OMDorm  AS  OM_Dorm__c		
									,T1.fsd_OMGuideWork  AS  OM_Guide_Work__c		
									,T1.fsd_OMRouteLearning  AS  OM_Route_Learning__c		
									,T1.fsd_OMTrafficReading  AS  OM_Traffic_Reading__c		
									,T1.fsd_PreferredPace  AS  Preferred_Pace__c		
									,T1.fsd_Retrain  AS  Retrain__c		
									,CAST(T1.fsd_StudentInfoComments AS NVARCHAR(30)) AS  Student_Info_Comments__c		
									,CASE T1.fsd_TotalVisionLoss WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Total_Vision_Loss__c		
									,T1.fsd_Understanding  AS  Understanding__c		
									,T1.fsd_Willingness  AS  Willingness__c		
									,CASE T1.fsd_WorkCityBus  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_City_Bus__c		
									,CASE T1.fsd_WorkCountryRoad WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Work_Country_Road__c		
									,T1.fsd_WorkCustomRoutes  AS  Work_Custom_Routes__c		
									,CASE T1.fsd_WorkEscalators  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Escalators__c		
									,CASE T1.fsd_WorkPlatforms  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Platforms__c		
									,CASE T1.fsd_WorkRevolvingDoor  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Revolving_Door__c		
									,CASE T1.fsd_WorkRoundCorners  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Round_Corners__c		
									,CASE T1.fsd_WorkSidewalkless  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Sidewalkless__c		
									,CASE T1.fsd_WorkTraffic  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Traffic__c		
									,CAST(T1.fsd_CreatedDate  AS DATE) AS  CreatedDate		
 	
							 	INTO GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd 
								FROM GDB_KADE_Final.dbo.tblFinalStudentDogSummary AS T1
							 	LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.fsd_ClientID
										LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
										LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
								LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.fsd_ClientID
										LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 				LEFT JOIN GDB_KADE_Final.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
										INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
										INNER JOIN GDB_KADE_Final.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID
								ORDER BY Legacy_Id__c


END -- end IMP_CLASS_REPORT_ca: tc1:   tc2: 1777  final 1777
  

BEGIN -- AUDIT IMP_CLASS_REPORT_fsd
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 --update legacy id to make unique. 
	UPDATE GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy_Id__c] AS NVARCHAR(30))
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_fsd
	 
END 

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_mee
	DROP TABLE GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm
END

	SELECT  DISTINCT
						 OwnerId= GDB_Final_migration.[dbo].[fnc_OwnerId]() 
						,RecordTypeID = '0121L000001UQEFQA4'     	-- Class Meeting	
						,Legacy_Id__c = 'MEE-'+CAST(T1.mee_ID  AS NVARCHAR(30))    	-- concatenate 'Mee-'+[mee_ID]	
						,[Class__r:Legacy_ID__c] = T2I.Legacy_Id__c 				-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
 						,[Client__r:Legacy_ID__c] = T4.psn_PersonID   				-- Link [tblPersonRel].[prl_ClientInstanceID]
						,[Dog__r:Legacy_ID__c]=NULL
						,T1.mee_DayHeld AS Day_Began__c
						, NULL AS Day_Resolved__c
						,Type__c = T5.cme_Type   								-- migrate value form [cme_type]	-- Yes/Link [trefClassMeetings].[cle_TypeID]
 						,Meeting_Comments__c = CAST(T1.mee_Summary AS NVARCHAR(MAX))  	--FB-00002	
			 			,Supervisor_Review__c  = NULL
						,Supervisor__c = NULL												
						,Tell_Student__c=NULL
						,Status__c=NULL
						,Issue__c = NULL
                        ,Class_Report_Plan__c = Null
						,Outcome__c = NULL
                        
						,Kade_Created_By__c = NULL
						,CreatedDate = CAST(T1.mee_CreatedDate AS DATE) 	
						,zrefID = T1.mee_ID
						,zrefSrc = 'tblClassMeetings'
					INTO GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm
					FROM GDB_KADE_Final.dbo.tblClassMeetings AS T1
 					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.mee_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.mee_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID --fixed FB-01123
					LEFT JOIN GDB_KADE_Final.dbo.trefClassMeetings AS T5 ON T5.cme_TypeID=T1.mee_Type


					UNION

					SELECT  DISTINCT
						 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END   -- Link [tblStaff].[Filenum] 
						,RecordTypeID = '0121L000001UQEAQA4'	-- Record type name ="Student Concerns"							
						,Legacy_Id__c = 'SCO-'+CAST(T1.sco_ID	AS NVARCHAR(30)) -- Concatenate "SCO-" with sco_ID	
						,[Class__r:Legacy_ID__c] = T2I.Legacy_Id__c 				-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
 						,[Client__r:Legacy_ID__c] = T3.prl_PersonID  				-- Link [tblPersonRel].[prl_ClientInstanceID] --FB-01435
 						,[Dog__r:Legacy_ID__c] = T.dog_DogID  	-- Link [tblDog].[dog_DogID]
						,T1.sco_DayBegan AS Day_Began__c
						,T1.sco_DayResolved AS Day_Resolved__c
						,Type__c = T1.sco_Type		
 						, NULL AS Meeting_Comments__c
			 			,Supervisor_Review__c  = CASE T1.sco_SupReview	WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE'	END 
						,Supervisor__c = X2.ID		-- Link [tblStaff].[FileNum]
						,Tell_Student__c = CASE T1.sco_TellStdnt 	WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE'	END 
						,Status__c = T1.sco_Status 
						,Issues__c = CAST(T1.sco_Issues	AS NVARCHAR(MAX))	
						,Class_Report_Plan__c = CAST(T1.sco_Plan	AS NVARCHAR(MAX))	
						,Outcome__c = CAST(T1.sco_Outcome AS NVARCHAR(MAX))
						
						,Kade_Created_By__c = T1.sco_CreatedBy		
						,CreatedDate = CAST(T1.sco_CreatedDate	AS DATE)	
						,zrefID = T1.sco_ID   
						,zrefSrc = 'tblClassStdntConcerns'   

					FROM GDB_KADE_Final.dbo.tblClassStdntConcerns AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T ON T.dog_DogID=T1.sco_DogID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=t1.sco_WhoBy
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=t1.sco_Supervisor
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.sco_ClientID
							LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.sco_ClientID  --fixed FB-01123
						--	LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID




BEGIN -- AUDIT IMP_CLASS_REPORT_cm
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	
	SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_CLASS_REPORT_cm
	 
END 



























