
--Drop table  GDB_Final_migration.DBO.IMP_CLASS_INSTRUCTION

BEGIN
		SELECT  DISTINCT   --FB-00074
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'CAW-'+CAST(T1.caw_ID AS NVARCHAR(30)) +'_'+ CAST(T4.drl_DogID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CAW-" with caw_ID	
							,T2.prl_PersonID AS [Contact__r:Legacy_ID__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
						 	,T4.drl_DogID  AS  [Dog__r:Legacy_ID__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
							

							,T1.caw_Traffic  AS  Traffic__c		
							,T1.caw_City  AS  City__c		
							,T1.caw_RevolvingDoor  AS  Revolving_Door__c		
							,T1.caw_Escalators  AS  Escalators__c		
							,T1.caw_PublicTransit  AS  Public_Transit__c		
							,T1.caw_NightRoute  AS  Night_Route__c		
							,T1.caw_Sidewalkless  AS  Sidewalkless__c		
							,T1.caw_CountryRoad  AS  Country_Road__c		
							,T1.caw_RoundedCorners  AS  Rounded_Corners__c		
							,T1.caw_Freelance  AS  Freelance__c		
							,T1.caw_BusLounge  AS  Bus_Lounge__c		
							,T1.caw_CollegeCampus  AS  College_Campus__c		
							,T1.caw_UndergroundTran  AS  Underground_Tran__c		
							,T1.caw_NursingHome  AS  Nursing_Home__c		
							,T1.caw_SelfOrientation  AS  Self_Orientation__c		
							,T1.caw_WheelchairExposure  AS  Wheelchair_Exposure__c		
							,T1.caw_KidExposure  AS  Kid_Exposure__c		
							,T1.caw_AdditionalSWL  AS  Additional_SWL__c		
							,T1.caw_OffsetCrossings  AS  Offset_Crossings__c		
							,T1.caw_WideCrossing  AS  Wide_Crossing__c		
							,T1.caw_IslandCrossing  AS  Island_Crossing__c		
 							,CAST(T1.caw_CustomRoute AS NVARCHAR(MAX)) AS  Custom_Route__c		
				 			,CAST(T1.caw_CreatedDate AS DATE) AS  CreatedDate		
							,T1.caw_ID AS zrefId
							,'tblClassAreasWorked' AS zrefSrc	
						INTO GDB_Final_migration.DBO.IMP_CLASS_INSTRUCTION
						FROM GDB_KADE_Final.dbo.tblClassAreasWorked AS T1
					 	LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.caw_ClientID
						LEFT JOIN GDB_KADE_Final.dbo.tblPersonDog AS T3 ON T3.pd_PersonRelID=T2.prl_RelationID
						INNER JOIN GDB_KADE_Final.dbo.tblDogRel AS T4 ON T4.drl_RelationID=T3.pd_DogRelID
						



							--TC2: 3418  final 3611
	END
	--AUDIT
	BEGIN 

		SELECT * FROM GDB_Final_migration.dbo.IMP_CLASS_INSTRUCTION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_CLASS_INSTRUCTION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c


	SELECT * FROM 		GDB_Final_migration.DBO.IMP_CLASS_INSTRUCTION	
	END	 