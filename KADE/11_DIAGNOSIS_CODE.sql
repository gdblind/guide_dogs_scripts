SELECT COUNT(*) FROM GDB_KADE_Final.dbo.trefVetPetChamp

USE GDB_Final_migration
GO
BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tbldog 
			WHERE   SF_Object LIKE '%dog%' OR Translation_Rules IS NOT null
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDog 
			WHERE  SF_Object_2 LIKE '%dog%' OR Translation_Rules_2 IS NOT NULL
END 


BEGIN -- drop IMP_DIAGNOSIS_CODE

	--DROP TABLE GDB_Final_migration.dbo.IMP_DIAGNOSIS_CODE

END 

BEGIN--- CREATE IMP_DIAGNOSIS_CODE
					 SELECT	  
  					 GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 					,T1.DiagnosisCode      AS  Diagnosis_Code__c		-- Ref trefVetPetChamp --FB-01472
					,T1.DiagnosisCode      AS  Legacy_Id__c
					,T1.DiagnosisTextCombined      AS  [Name]
 					--,T1.DiagnosisText      AS  Diagnosis__c	-- migrate [DiagnosisText]	-- Ref trefVetPetChamp
					,T3.[location] AS Location__c
					,T1.Value1Text AS Value_1_Text__c
					,T1.Value2Text	AS Value_2_Text__c
					,T2.CategoryDesc	   AS  Diagnosis_Category__c	-- link through [trefVetPetChamp].[DiagnosisCategory]=[trefVetDiagCategory], migrate value from [CategoryDesc]	-- Ref trefVetPetChamp
 					,T1.DogReleaseCategory AS  Diagnosis_Release_Category__c	-- migrate [DogReleaseCategory]	-- Ref trefVetPetChamp
					,T1.RelCodeStatus      AS  Release_Code_Status__c	-- migrate [RelCodeStatus]	-- Ref trefVetPetChamp
					--,IIF(T1.vdd_Severity='0','True','False') AS Severity_Deceased__c
					,T1.[Description] AS Description__c
	 	 			INTO GDB_Final_migration.dbo.IMP_DIAGNOSIS_CODE	
					FROM GDB_KADE_Final.dbo.trefVetPetChamp AS T1 
					LEFT JOIN GDB_KADE_Final.DBO.trefVetDiagCategory AS T2 ON T2.CategoryCode=T1.DiagnosisCategory
					LEFT JOIN GDB_KADE_Final.[dbo].[trefVetPetChampLoc] AS T3 ON T3.[LocCode]=T1.[Location]


END 	 --tc1: 1,428 TC2: 1,436 final: 1,442


SELECT * FROM  GDB_Final_migration.dbo.IMP_DIAGNOSIS_CODE