USE GDB_Final_migration
GO


BEGIN--MAPS 
   			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API
					,','+SF_Field_API+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblAGSPhoneConsultation
			WHERE   SF_Object LIKE '%app%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2
					,','+SF_Field_API_2+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%app%'

0121L000001UQCCQA4	Home Visit
0121L000001UQCDQA4	Nursing Phone Consultation
0121L000001UQCEQA4	O&M Phone Consultation
0121L000001UQCFQA4	Phone Consultation
0121L000001UQCGQA4	Second Phone Consultation

*/



BEGIN -- DROP   IMP_APP_ADMIN_apq

	DROP TABLE GDB_Final_migration.DBO.IMP_APP_ADMIN_apq

END 

/*  --DNC per FB-00366
BEGIN -- CREATE IMP_APP_ADMIN_apq 

					SELECT DISTINCT   
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 							,[Name1] = 'Phone Interview - ' + CAST(CAST( T1.apq_Date AS DATE) AS VARCHAR(50))
							,[Application__r:Legacy_Id__c] = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30)) 		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,Legacy_ID__c ='APQ-'+CAST( T1.apq_AppInstanceID	AS NVARCHAR(30)) -- concatenate 'apq'+[apq_AppInstanceID]	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,Date__c =CAST( T1.apq_Date	AS DATE)
							,Interviewer__c = X1.ID		-- Link [tblStaff].[FIleNum]
							,Major_Move__c = CASE T1.apq_MajorMove	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Wheelchair__c = CASE T1.apq_Wheelchair		WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Veteran__c = CASE T1.apq_Veteran 	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 		
							,Withdrawn_No_German_Shepards__c = CASE T1.apq_WithdrawnNoGrmnShpds	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Lifestyle_Change__c = CASE T1.apq_LifeStyleChange WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Minor__c = CASE T1.apq_Minor WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Felony__c = CASE T1.apq_Felony WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Medications__c = CASE T1.apq_Medication	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Height__c = CASE T1.apq_Height  WHEN 'Yes' THEN 'Yes' ELSE 'No' END 		
							,Weight__c = T1.apq_Weight		
							,Gender__c =CASE  T1.apq_Gender WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END 	-- migrate M as Male and F as Female	
							,Hospitalized__c = CASE T1.apq_Hospitalized WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Seizures__c = CASE T1.apq_Seizures WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Last_Seizure_Date__c = CAST(T1.apq_LastSeizureDate	AS DATE) 
							,Diabetes__c = CASE T1.apq_Diabetic	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Heart__c = CASE T1.apq_Heart	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Mental_Health__c = CASE T1.apq_Mental	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Pending_Medication__c = CASE T1.apq_PendingMed	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Other_Phys_Disability__c = CASE T1.apq_OtherPhysDisab WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Other_Phys_Disability_Detail__c = CAST( T1.apq_OtherPhysDisabYes AS NVARCHAR(MAX)) 		
							,Drug_Alcohol_Issue__c = CASE T1.apq_DrugAlcoholIssue WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Drug_Alcohol_Issue_Detail__c = CAST(T1.apq_DrugAlcoholIssueYes	AS NVARCHAR(MAX)) 	
							,Difficulty_Hearing__c = CASE T1.apq_Hearing WHEN 'No' THEN 'Yes' ELSE 'No' END-- Flipped the field to be "Difficulty Hearing" thus requires translation rule.  If Yes, then No.  If No, then Yes. Migrate blank values as 'No'	
							,Cause_of_Blindness_2__c = T1.apq_CauseofBlindness2		
							,Cause_of_Blindness__c = T4.cob_Description	-- migrate value from [cob_Description]	-- Yes/Link [tref_CauseOfBlindness91].[cob_ID]
							,Visual_Acuity_Right__c = T1.apq_VisualAcuityRight		
							,Visual_Acuity_Left__c = CAST( T1.apq_VisualAcuityLeft AS NVARCHAR(MAX)) 		
							,Visual_Field_Right__c = T1.apq_VisualFieldRight 		
							,Visual_Field_Left__c = CAST(T1.apq_VisualFieldLeft	AS NVARCHAR(MAX)) 	
							,Inc_independence__c = CAST(T1.apq_IncIndependence AS  NVARCHAR(MAX)) 	-- Migrate blank values as 'No'	
							,Outdoors_Alone__c = T1.apq_OutdoorsAlone		
							,Regular_Routes__c = CASE T1.apq_ReqularRoutes WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,How_Many_Blocks__c = T1.apq_HowmanyBlocks		
							,Mile__c = CASE T1.apq_Mile	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Street_Crossing__c = CASE T1.apq_StreetXing	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Safe_for_Dog__c = CASE T1.apq_SafeforDog	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Mobility_Aid__c = T1.apq_MobilityAid		
							,OM_Training__c = CASE  T1.apq_OMTraining	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Emergency_Contact__c = T1.apq_EmergencyContact		
							,Emergency_Contact_Phone__c = CAST(T1.apq_EmergencyPhone AS NVARCHAR(MAX))
							,Emergency_Contact_Relation__c = T1.apq_EmergencyContactRel		
							,Prompt_App__c = CASE  T1.apq_PromptApp  WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Family_Support__c = CASE T1.apq_FamilySupport	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Dog_Impact__c = CASE T1.apq_DogImpact WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
							,Work_Dog__c = CASE T1.apq_WorkDog	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Home_Interview__c = CASE T1.apq_HomeInt  WHEN 'Yes' THEN 'Yes' ELSE 'No' END  -- Migrate blank values as 'No'	
							,Employed_Yes__c = T1.apq_EmployedYes	-- Yes, No, Unknown	
							,Family_House_Available__c = CASE T1.apq_FamilyHouseAvail	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
							,Recommend__c = CAST(T1.apq_RecommendHI	AS NVARCHAR(MAX))		
							,Months_Till_Home_Interview__c = CAST( T1.apq_MonthsTillHomeInt	AS NVARCHAR(MAX))			
							,Referral__c = T1.apq_Referral		
							,Name_Address__c = T1.apq_NameAddress		
							,Major_Move_Detail__c =CAST(T1.apq_MajorMoveYes AS NVARCHAR(MAX))	
							,Lifestyle_Change_Detail__c = CAST(T1.apq_LifeStyleChangeYes	 AS NVARCHAR(MAX))	
					
					 		,Minor_Yes__c = CAST(T1.apq_MinorYes	 AS NVARCHAR(MAX))	
							,Felony_Detail__c = CAST(T1.apq_FelonyYes AS NVARCHAR(MAX))		
							,Medications_Detail__c = CAST(T1.apq_MedicationYes AS NVARCHAR(MAX))		
							,Hospitalized_Detail__c = CAST(T1.apq_HospitalizedYes  AS NVARCHAR(MAX))		
							,Pending_Med_Detail__c = CAST(T1.apq_PendingMedYes	  AS NVARCHAR(MAX))
							,Vision_Loss_Knowledge__c = CAST(T1.apq_VisionLossKnowledge AS NVARCHAR(MAX))		
							,Outdoors_Alone_No__c = CAST(T1.apq_OutdoorsAloneNo AS NVARCHAR(MAX))		
							,Example_Destinations__c = CAST(T1.apq_ExampleDestinations	AS NVARCHAR(MAX))			
							,Street_Crossing_Yes__c = CAST(T1.apq_StreetXingYes	AS NVARCHAR(MAX))	
							,Mobility_Aid_No__c = CAST(T1.apq_MobilityAidNo	AS NVARCHAR(MAX))	
							,OM_Training_Yes__c = CAST(T1.apq_OMTrainingYes	AS NVARCHAR(MAX))
				 
							,Prompt_App_Yes__c = CAST( T1.apq_PromptAppYes 	AS NVARCHAR(MAX))		
							,Family_Support_No__c = CAST(T1.apq_FamilySupportNo	AS NVARCHAR(MAX))	
							,Notes__c = CAST(T1.apq_Notes 	AS NVARCHAR(MAX))
							,RecordTypeId = '0123D0000004awdQAA'	-- Phone Interview	
					
				 	INTO GDB_Final_migration.DBO.IMP_APP_ADMIN_apq
					FROM GDB_KADE_Final.dbo.tblAGSApplicantPhoneInt AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.apq_AppInstanceID
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.apq_Interviewer
					LEFT JOIN GDB_KADE_Final.dbo.trefCauseOfBlindness_91 AS T4 ON T4.cob_Id=T1.apq_CauseofBlindness
					ORDER BY Legacy_ID__c
					--4088
					
		 

END 


BEGIN -- AUDIT  IMP_APP_ADMIN_apq
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apq 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apq GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apq 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apq
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apq
	 

END 
*/
/*********************************************************************************************************************/

BEGIN -- DROP IMP_APP_ADMIN_hv

	DROP TABLE GDB_Final_migration.DBO.IMP_APP_ADMIN_hv

END 

BEGIN -- CREATE IMP_APP_ADMIN_hv 

					SELECT DISTINCT   
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,[Name1] = 'Home Visit - ' + CAST(CAST( T1.hv_Date AS DATE) AS VARCHAR(50))
							,Legacy_Id__c = 'HV-'+CAST(T1.hv_HomeVisitID AS NVARCHAR(30))	-- concatenate 'hv-'+[hv_HomeVisitID]	
							,[Application__r:Legacy_Id__c] = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30)) 		-- Link [tbPersonRel].[prl_ClientInstanceID]
							,Interviewer__c = X1.ID -- Link [tblStaff].[FIleNum]
							,Date__c = CAST(T1.hv_Date	AS DATE)
							,Preferred_Airport__c = T1.hv_PreferredAirport		
							,Support_Concerns__c = CASE T1.hv_SupportConcerns WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
							,Support_Concerns_Comments__c = T1.hv_SupportConcernsDetail		
							,[Object_to_a_guide_dog_treat_inappropriately__c] = CASE T1.hv_InappropriateTreatment	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
							,[Objections_and_treatment comments__c] = T1.hv_InappropriateTreatmentDetail		
							,Safety_Concerns__c = CASE T1.hv_SafetyConcerns  WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
							,Safety_Concerns_Comments__C = T1.hv_SafetyConcernsDetail		
							,Pets_in_home__c = T1.hv_PetsInHome		
							,Leash_Relieving_Areas__c = T1.hv_LeashRelievingAreas		
							,Medications_Detail__c = T1.hv_Medications		
							,Safe_Competent_Travel__c =CASE  WHEN T1.hv_SafeCompetentTravel  IS NULL THEN 'No' ELSE T1.hv_SafeCompetentTravel END 	-- migrate blanks as 'No' Picklist values: Yes, No, Questionable	
							,Pace__c = T1.hv_Pace		
							,Juno_safe_competent__c = CASE  WHEN  T1.hv_Juno IS NULL THEN 'No' ELSE T1.hv_SafeCompetentTravel END	-- migrate blanks as 'No' Picklist values: Yes, No, Questionable	
							,CF__c = T1.hv_CF		
							,Able_to_Apply_Instructions__c = CASE WHEN T1.hv_ApplyInstructions IS NULL THEN 'No' ELSE T1.hv_ApplyInstructions END	-- migrate blanks as 'No' Picklist values: Yes, No, Questionable	
							,Functional_Vision__c = T1.hv_FunctionalVision		
							,Visual_Occlusion_Used__c = CASE WHEN  T1.hv_VisualOcclusionUsed	 IS NULL THEN 'No' ELSE T1.hv_VisualOcclusionUsed END-- migrate blanks as 'No' Picklist values: Yes, No, N/A	
							,Occlusion_Used_In_Class__c = CASE WHEN  T1.hv_OcclusionUsedInClass	 IS NULL THEN 'No' ELSE T1.hv_OcclusionUsedInClass END-- migrate blanks as 'No' Picklist values: Yes, No, N/A	
							,Continue_application__c = CASE WHEN T1.hv_Continue	 IS NULL THEN 'No' ELSE T1.hv_Continue END-- migrate blanks as 'No' Picklist values: Yes, No, N/A	
							,[Open_to_experienced_guide__c] =CASE when T1.hv_ExperiencedGuide IS NULL THEN 'No' ELSE T1.hv_ExperiencedGuide END	-- migrate blanks as 'No'	
							,[Open_to_experienced_guide_comments__c] = T1.hv_ExperiencedGuideDetail		
							,Dog_Preferences__c = T1.hv_DogPreferences		
							,Recommendation_of_Interviewer__c = T1.hv_Recommendation		
							,Requested_Class__c = T1.hv_RequestedClass		
							,Recommended_Class__c = T1.hv_RecommendedClass		
							,Preferred_Campus__c = T1.hv_PreferredCampus		
							,Preferred_Campus_Detail__c = T1.hv_PreferredCampusDetail		
							,]Willing_to_take_any_class_available__c] = CASE WHEN T1.hv_AvailableClass	 IS NULL THEN 'No' ELSE T1.hv_AvailableClass END-- migrate blanks as 'No' Picklist values: Yes, No, N/A	
							,Timeframe_Requested__c = T1.hv_TimeframeRequested		
							,Notes__c = CAST( T1.hv_Narrative	AS NVARCHAR(MAX))	
							,RecordTypeID = '0121L000001UQCCQA4'	-- Home Visit	  		
				 	INTO GDB_Final_migration.DBO.IMP_APP_ADMIN_hv
					FROM GDB_KADE_Final.dbo.tblAGSHomeVisit AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.hv_AppInstanceID
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C = T1.hv_Interviewer
					 ORDER BY Legacy_ID__c
					  
	  

END 

BEGIN-- AUDIT  IMP_APP_ADMIN_hv
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_hv 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_hv GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_hv 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_hv
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_hv
	 

END 

/*********************************************************************************************************************/

BEGIN -- DROP IMP_APP_ADMIN_apc

	DROP TABLE GDB_Final_migration.DBO.IMP_APP_ADMIN_apc

END 

BEGIN -- CREATE IMP_APP_ADMIN_apc 

					SELECT DISTINCT   
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
								,[Name1] = 'Phone Consultation - ' + CAST(CAST( T1.apc_Date AS DATE) AS VARCHAR(50))
								,RecordTypeId = '0121L000001UQCFQA4' -- Phone Consultation						
								,Legacy_ID__c = 'APC-'+CAST(T1.apc_AppInstanceID AS NVARCHAR(30))	-- concatenate 'apc-'+[apc_AppinstanceID]	-- Link [tblPersonRel].[prl_ClientInstanceID]
								,[Application__r:Legacy_Id__c] = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30)) 			-- Link [tblPersonRel].[prl_ClientInstanceID]
								,Date__c = CAST (T1.apc_Date AS DATE)		
								,Interviewer__c =IIF(t1.apc_Interviewer='', NULL, X1.ID)		-- Link [TblStaff].[FIleNum]
								,Placement_Current_Guide__c = T1.apc_PlacementCurrentGuide		
								,Drug_Alcohol_Issue_Detail__c = T1.apc_DrugAbuseDetail		
								,Significant_Changes__c = T1.apc_SignificantChanges		
								,Greatest_Influence__c = T1.apc_GreatestInfluence		
								,Greatest_Influence_Contact_Info__c = T1.apc_GreatestInfluenceContactInfo		
								,GDB_Lifestyle__c = CASE WHEN T1.apc_GDBLifestyle IS NULL THEN 'No' ELSE T1.apc_GDBLifestyle END 	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,GDB_Presentation__c = CASE T1.apc_GDBPresentation WHEN 'Yes' THEN 'Yes' ELSE 'No' END	-- migrate blanks as 'No' Picklist values Yes, No	
								,GDB_Staff_Contact__c = CASE T1.apc_GDBStaffContact	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No' Picklist values Yes, No	
								,GDB_Website__c = CASE T1.apc_GDBWebsite	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No' Picklist values Yes, No	
								,Read_GD_User_Contract__c = CASE T1.apc_ReadGDUserContract	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No' Picklist values Yes, No	
								,Signing_Contract__c = CASE T1.apc_SigningContract	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No'	
								,Signing_Contract_Comments__c = T1.apc_SigningContractComments		
								,Read_Guide_Dog_Right_You__c = CASE WHEN T1.apc_ReadGuideDogRightYou IS NULL THEN 'No' ELSE T1.apc_ReadGuideDogRightYou END 	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Benefits__c = T1.apc_Benefits		
								,Considered_Responsibility__c = CASE T1.apc_ConsideredResponsibility WHEN 'Yes' THEN 'Yes' ELSE 'No' END	-- Migrate blank values as 'No'	
								,Considered_Responsibility_Comments__c = T1.apc_ConsideredResponsibilityComments		
								,Financial_Impact__c =CASE  T1.apc_FinancialImpact	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- Migrate blank values as 'No'	
								,Giving_Commands__c = CASE T1.apc_GivingCommands WHEN 'Yes' THEN 'Yes' ELSE 'No' END	-- Migrate blank values as 'No'	
								,What_Convicted_Of__c = CAST(T1.apc_WhatConvictedOf	AS NVARCHAR(MAX))
								,When_Convicted__c = T1.apc_WhenConvicted		
								,Completed_All_Requirements__c = T1.apc_CompletedAllRequirements		
								,How_Long_Sober__c = T1.apc_HowLongSober		
								,Sobriety_References__c = T1.apc_SobrietyReferences		
								,Attend_Support_Classes__c = CASE T1.apc_AttendSupportClasses  WHEN 'Yes' THEN 'Yes' ELSE 'No' END	-- Migrate blank values as 'No'	
								,Seizures__c = CASE T1.apc_Seizures	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No' Picklist values Yes, No	
								,Seizures_Detail__c = T1.apc_SeizuresDetail		
								,Lifestyle_Change__c = CASE T1.apc_LifestyleChange	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No' Picklist values Yes, No	
								,Lifestyle_Change_Detail__c = T1.apc_LifestyleChangeDetail		
								,Housing_Type__c = T1.apc_HousingType		
								,Length_Residence__c = T1.apc_LengthResidence		
								,Whom_Reside__c = T1.apc_WhomReside		
								,Pets__c = CASE T1.apc_Pets	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END-- migrate blanks as 'No'	
								,Pets_in_home__c = T1.apc_PetsDetail		
								,Home_Relief_Area__c = T1.apc_HomeReliefArea		
								,Experience_With_Dogs__c = T1.apc_ExperienceWithDogs		
								,Home_Leash_Relieve_Dog__c = T1.apc_HomeLeashRelieveDog		
								,Employed_Student__c = T1.apc_EmployedStudent		
								,Work_School_Environment__c = T1.apc_WorkSchoolEnvironment		
								,Work_Relief_Area__c = T1.apc_WorkReliefArea		
								,Work_School_Leash_Relieve_Dog__c = T1.apc_WorkSchoolLeashRelieveDog		
								,Agency__c = T1.apc_Agency		
								,Mobility_Aid_Changed__c = T1.apc_MobilityAidChanged		
								,Travel_Routes_Alone__c = T1.apc_TravelRoutesAlone		
								,Functional_Vision__c = T1.apc_FunctionalVision		
								,Vision_Helpful__c = T1.apc_VisionHelpful		
								,Lighting_Conditions__c = T1.apc_LightingConditions		
								,Typical_Week__c = T1.apc_TypicalWeek		
								,Residential__c = CASE T1.apc_Residential WHEN '0' THEN NULL ELSE T1.apc_Residential END 
								,Busy_Urban__c = CASE T1.apc_BusyUrban WHEN '0' THEN NULL ELSE T1.apc_BusyUrban END  	
								,Rural_Country__c = CASE T1.apc_RuralCountry	WHEN '0' THEN NULL ELSE T1.apc_RuralCountry END 	
								,Sidewalk_Less__c = CASE T1.apc_SidewalkLess	WHEN '0' THEN NULL ELSE T1.apc_SidewalkLess END 	
								,Shopping_Malls__c = CASE T1.apc_ShoppingMalls	WHEN '0' THEN NULL ELSE T1.apc_ShoppingMalls END 	
								,Escalators__c =CASE  T1.apc_Escalators		WHEN '0' THEN NULL ELSE T1.apc_Escalators END 
								,Night_Travel__c = CASE T1.apc_NightTravel WHEN '0' THEN NULL ELSE T1.apc_NightTravel END 		
								,Autos__c = CASE T1.apc_Autos	WHEN '0' THEN NULL ELSE T1.apc_Autos END 	
								,Buses__c = CASE T1.apc_Buses	WHEN '0' THEN NULL ELSE T1.apc_Buses END 	
								,Light_Rail__c = CASE T1.apc_LightRail	WHEN '0' THEN NULL ELSE T1.apc_LightRail END 	
								,Trains__c =CASE  T1.apc_Trains		WHEN '0' THEN NULL ELSE T1.apc_Trains END 
								,Ferry_Ship__c =CASE  T1.apc_FerryShip	WHEN '0' THEN NULL ELSE T1.apc_FerryShip END 	
								,Taxi__c = CASE T1.apc_Taxi		WHEN '0' THEN NULL ELSE T1.apc_Taxi END 
								,Para_Transit__c = CASE T1.apc_ParaTransit	WHEN '0' THEN NULL ELSE T1.apc_ParaTransit END 	
								,Subway__c = CASE T1.apc_Subway		WHEN '0' THEN NULL ELSE T1.apc_Subway END 
								,Plane__c =CASE  T1.apc_Plane	WHEN '0' THEN NULL ELSE T1.apc_Plane END 	
								,Unfamiliar__c = T1.apc_Unfamiliar		
								,Approx_Length__c = T1.apc_ApproxLength		
								,Time_To_Travel__c = T1.apc_TimeToTravel		
								,Traffic_Control__c = T1.apc_TrafficControl		
								,How_Often__c = T1.apc_HowOften		
								,Traffic__c = T1.apc_Traffic		
								,How_Decision_Cross_Street__c = T1.apc_HowDecisionCrossStreet		
								,Remaining_Vision__c = T1.apc_RemainingVision		
								,Most_Challenging_Route__c = T1.apc_MostChallengingRoute		
								,Dest_Notes__c = T1.apc_DestNotes		
								,Approx_Length_2__c = T1.apc_ApproxLength2		
								,Time_To_Travel_2__c = T1.apc_TimeToTravel2		
								,Traffic_Control_2__c = T1.apc_TrafficControl2		
								,How_Often_2__c = T1.apc_HowOften2		
								,Traffic_2__c = T1.apc_Traffic2		
								,How_Decision_Cross_Street_2__c = T1.apc_HowDecisionCrossStreet2		
								,Remaining_Vision_2__c = T1.apc_RemainingVision2		
								,Most_Challenging_Route_2__c = T1.apc_MostChallengingRoute2		
								,Dest_Notes_2__c = T1.apc_DestNotes2		
								,Approx_Length_3__c = T1.apc_ApproxLength3		
								,Time_To_Travel_3__c = T1.apc_TimeToTravel3		
								,Traffic_Control_3__c = T1.apc_TrafficControl3		
								,How_Often_3__c = T1.apc_HowOften3		
								,Traffic_3__c = T1.apc_Traffic3		
								,How_Decision_Cross_Street_3__c = T1.apc_HowDecisionCrossStreet3		
								,Remaining_Vision_3__c = T1.apc_RemainingVision3		
								,Most_Challenging_Route_3__c = T1.apc_MostChallengingRoute3		
								,Dest_Notes_3__c = T1.apc_DestNotes3		
								,Height__c = T1.apc_Height		
								,Weight__c = T1.apc_Weight		
								,Hospitalized__c = CASE T1.apc_Hospitalized	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Hospitalized_Detail__c = T1.apc_HospitalizedDetail		
								,Pending_Medical_Procedures__c = CASE T1.apc_PendingMedicalProcedures	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Pending_Medical_Procedures_Detail__c = CAST(T1.apc_PendingMedicalProceduresDetail	AS NVARCHAR(MAX))
								,Smoke__c = CASE T1.apc_Smoke 	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- Migrate blank values as 'No'	
								,Prescribed_Diet__c =CASE  T1.apc_PrescribedDiet		WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Prescribed_Diet_Detail__c = T1.apc_PrescribedDietDetail		
								,Diabetes__c = CASE T1.apc_Diabetes		WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- Migrate blank values as 'No'	
								,Stable_Blood_Sugar__c = CASE T1.apc_StableBloodSugar	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Intervention__c = CASE T1.apc_Intervention	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Monitor_Administer_Independently__c = CASE T1.apc_MonitorAdministerIndependently	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Secondary_Conditions_Diabetes__c = T1.apc_SecondaryConditionsDiabetes		
								,Physical_Stamina__c = CASE T1.apc_PhysicalStamina	WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Physical_Stamina_Detail__c = T1.apc_PhysicalStaminaDetail		
								,Stroke__c = CASE T1.apc_Stroke		WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No' Picklist values Yes, No	
								,Stroke_Date__c = CAST(T1.apc_StrokeDate AS DATE)		
								,Stroke_Detail__c = T1.apc_StrokeDetail		
								,Brain_Injury__c = CASE WHEN T1.apc_BrainInjury	IS NULL THEN 'No' ELSE T1.apc_BrainInjury END -- migrate blanks as 'No' Picklist values Yes, No	
								,Brain_Injury_Date__c =CAST( T1.apc_BrainInjuryDate	 AS DATE)	
								,Brain_Injury_Detail__c = T1.apc_BrainInjuryDetail		
								,Other_Head_Injury__c = CASE WHEN T1.apc_OtherHeadInjury	IS NULL THEN 'No' ELSE T1.apc_OtherHeadInjury END 	-- migrate blanks as 'No' Picklist values Yes, No	
								,Other_Head_Injury_Date__c = CAST(T1.apc_OtherHeadInjuryDate	 AS DATE)	
								,Other_Head_Injury_Detail__c = T1.apc_OtherHeadInjuryDetail		
								,Alzheimer__c =CASE WHEN  T1.apc_Alzheimer 	IS NULL THEN 'No' ELSE T1.apc_Alzheimer END -- migrate blanks as 'No' Picklist values Yes, No	
								,Alzheimer_Date__c = CAST(T1.apc_AlzheimerDate AS DATE)		
								,Alzheimer_Detail__c = T1.apc_AlzheimerDetail		
								,Parkinson__c =CASE WHEN  T1.apc_Parkinson	IS NULL THEN 'No' ELSE T1.apc_Parkinson END 	-- migrate blanks as 'No' Picklist values Yes, No	
								,Parkinson_Date__c = CAST(T1.apc_ParkinsonDate AS DATE)		
								,Parkinson_Detail__c = T1.apc_ParkinsonDetail		
								,Dementia__c = CASE WHEN T1.apc_Dementia 	IS NULL THEN 'No' ELSE T1.apc_Dementia END -- migrate blanks as 'No' Picklist values Yes, No	
								,Dementia_Date__c = CAST(T1.apc_DementiaDate AS DATE)		
								,Dementia_Detail__c = T1.apc_DementiaDetail		
								,Multiple_Sclerosis__c = CASE WHEN T1.apc_MultipleSclerosis	 IS NULL THEN 'No' ELSE T1.apc_MultipleSclerosis END -- migrate blanks as 'No' Picklist values Yes, No	
								,Multiple_Sclerosis_Date__c = CAST(T1.apc_MultipleSclerosisDate	 AS DATE) 
								,Multiple_Sclerosis_Detail__c = T1.apc_MultipleSclerosisDetail		
								,Learning_Difficulties__c =CASE WHEN  T1.apc_LearningDifficulties 	IS NULL THEN 'No' ELSE T1.apc_LearningDifficulties END -- migrate blanks as 'No' Picklist values Yes, No	
								,Learning_Difficulties_Date__c = CAST(T1.apc_LearningDifficultiesDate  AS DATE)		
								,Learning_Difficulties_Detail__c = T1.apc_LearningDifficultiesDetail		
								,Mental_Health__c = CASE T1.apc_DiagMentalHealth	WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No' Picklist values Yes, No	
								,Prefers_Licensed_Counselor__c = T1.apc_PrefersLicensedCounselor	-- migrate blanks as 'No'	
								,Diagnosis__c = CAST(T1.apc_Diagnosis	AS NVARCHAR(MAX))
								,Dealing_Condition__c = T1.apc_DealingCondition		
								,Received_Counseling__c = CASE T1.apc_ReceivedCounseling  WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Medications__c = CASE T1.apc_AnyMedications WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No' Picklist values Yes, No	
								,Medications_Detail__c = CAST(T1.apc_AnyMedicationsDetails	AS NVARCHAR(MAX))
								,How_Condition_Affect__c = T1.apc_HowConditionAffect		
								,How_Manage_Symptoms__c = T1.apc_HowManageSymptoms		
								,Professional_Care__c = T1.apc_ProfessionalCare		
								,Concerns__c = T1.apc_Concerns		
								,Concerns_Comments__c = T1.apc_ConcernsComments		
								,How_Long_Medications__c = T1.apc_HowLongMedications		
								,Medications_Work_You__c = CASE WHEN T1.apc_MedicationsWorkYou IS NULL THEN 'No' ELSE T1.apc_MedicationsWorkYou END 	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Side_Effects__c = CASE WHEN T1.apc_SideEffects IS NULL THEN 'No' ELSE T1.apc_SideEffects END 	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Side_Effects_Details__c = T1.apc_SideEffectsDetails		
								,Difficulty_Hearing__c = CASE T1.apc_DifficultyHearing	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Difficulty_Hearing_Left__c =  CASE T1.apc_DifficultyHearingLeft	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Difficulty_Hearing_Right__c = CASE T1.apc_DifficultyHearingRight WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Hearing_Aids__c = CASE T1.apc_HearingAids	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No' Picklist values Yes, No	
								,Hearing_Aids_Left__c = CASE T1.apc_HearingAidsLeft	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Hearing_Aids_Right__c = CASE T1.apc_HearingAidsRight WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Cochlear_Implant__c =CASE  T1.apc_CochlearImplant WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No' Picklist values Yes, No	
								,FM_Loop_System__c = CASE T1.apc_FMLoopSystem	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Communication__c =CASE  T1.apc_Communication WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Communication_Detail__c = T1.apc_CommunicationDetail		
								,Traffic_Orientation__c = CASE  T1.apc_TrafficOrientation	 WHEN 'Yes' THEN 'Yes' ELSE 'No' END -- migrate blanks as 'No'	
								,Traffic_Orientation_Detail__c = T1.apc_TrafficOrientationDetail		
								,Concerns_Physical_Condition__c = T1.apc_ConcernsPhysicalCondition		
								,Concerns_Physical_Condition_Detail__c = T1.apc_ConcernsPhysicalConditionDetail		
								,Family_Support_Application__c = CASE WHEN T1.apc_FamilySupportApplication IS NULL THEN 'No' ELSE T1.apc_FamilySupportApplication END	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Living_Arrangement_Changed__c = CASE WHEN T1.apc_LivingArrangementChanged IS NULL THEN 'No' ELSE T1.apc_LivingArrangementChanged END	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Object__c = CASE T1.apc_Object WHEN 'Yes' THEN 'Yes' ELSE 'No' END 	-- migrate blanks as 'No'	
								,Object_Detail__c = T1.apc_ObjectDetail		
								,Coworkers_Supportive__c = CASE WHEN T1.apc_CoworkersSupportive IS NULL THEN 'No' ELSE T1.apc_CoworkersSupportive END	-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Coworkers_Supportive_Detail__c = T1.apc_CoworkersSupportiveDetail		
								,Object_Work_Place__c = CASE WHEN T1.apc_ObjectWorkPlace	 IS NULL THEN 'No' ELSE T1.apc_ObjectWorkPlace END-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Object_Work_Place_Detail__c = T1.apc_ObjectWorkPlaceDetail		
								,Landlord__c = CASE WHEN T1.apc_Landlord	 IS NULL THEN 'No' ELSE T1.apc_Landlord END-- migrate blanks as 'No' Picklist values Yes, No, N/A	
								,Landlord_Detail__c = T1.apc_LandlordDetail		
								,Emergency_Contact__c = T1.apc_EmergencyContact		
								,Emergency_Contact_Phone__c = T1.apc_EmergencyContactPhone		
								,Emergency_Contact_Relationship__c = T1.apc_EmergencyContactRelationship		
								,Recommendation_of_Interviewer__c = T1.apc_Recommendation		
								,Set_Aside_Hours__c = T1.apc_SetAsideHours		
								,Notes__c = CAST(T1.apc_Narrative AS NVARCHAR(MAX))		--FB-02429
 						 	INTO GDB_Final_migration.DBO.IMP_APP_ADMIN_apc
							FROM GDB_KADE_Final.dbo.tblAGSPhoneConsultation AS T1
							LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.apc_AppInstanceID
							LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
							LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C = T1.apc_Interviewer
							-- WHERE T1.apc_AppInstanceID='142426'
							 ORDER BY Legacy_ID__c

		 
END 

BEGIN-- AUDIT  IMP_APP_ADMIN_apc
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apc 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apc GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apc 
	GROUP BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apc
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_apc
	 

END 

/*********************************************************************************************************************/
BEGIN -- DROP IMP_APP_ADMIN_spc

	DROP TABLE GDB_Final_migration.DBO.IMP_APP_ADMIN_spc

END 

BEGIN -- CREATE IMP_APP_ADMIN_spc
			SELECT  DISTINCT
							 OwnerId = IIF(T1.spc_Interviewer='',GDB_Final_migration.[dbo].[fnc_OwnerId](), X1.ID)
							,Legacy_ID__c = 'SPC-'+CAST( T1.spc_AppInstanceID  AS NVARCHAR(30)) 
							--,T2.prl_PersonID AS [Contact__r:Legacy_ID__c]
							--,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30)) AS [Application__r:Legacy_ID__c]		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
						 	,Interviewer__c = IIF(T1.spc_Interviewer='',NULL, X1.ID)
							,[Date__c] = CAST(T1.spc_Date AS DATE)
							,[Notes__c] = CAST(T1.spc_narrative	AS NVARCHAR(MAX))
							,Recommendation_of_Interviewer__c = T1.spc_recommendation		
							,CreatedDate =CAST( T1.spc_CreadtedDate	AS DATE)			
							,RecordTypeId = '0121L000001UQCGQA4'	-- Second Phone Consultation
							,zrefSrc = 'tblAGSSecondPhoneConsultation'
					INTO GDB_Final_migration.dbo.IMP_APP_ADMIN_spc
					FROM GDB_KADE_Final.dbo.tblAGSSecondPhoneConsultation AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.spc_AppInstanceID
					LEFT JOIN GDB_KADE_Final.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C = T1.spc_Interviewer
					
END 
--Audit

BEGIN

	SELECT * FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_spc 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_spc GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

END 


SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_APP_ADMIN_spc 
