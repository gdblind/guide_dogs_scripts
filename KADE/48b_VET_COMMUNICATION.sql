		BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_VET_MEMO


END 
		
BEGIN
			SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'VDL-'+CAST(T1.vdl_VetEntryMemoID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'vdl'+[vdl_VetEntryMemoID]	
						,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Entry__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
						,T2.vdl_DogID AS [Dog__r:Legacy_ID__c]
						,CASE WHEN T2.vdl_Complaint IS NULL THEN T3.VetSourceDesc ELSE T2.vdl_Complaint END  AS  [Name]	--FB-00933
 						,'VTA-'+CAST(T4.vta_ID AS VARCHAR(30)) AS [AUTHORIZATION__R:LEGACY_ID__C]
						,T1.vdl_MemoRecip  AS  Recipient__c		
						,CAST(T1.vdl_DateMemoSent AS DATE) AS Date_Sent__c		
						,t1.vdl_CreatedBy  + ' ' +CAST(T1.vdl_VetMemo AS NVARCHAR(MAX)) AS  Memo__c		
 						,CAST(T1.vdl_CreatedDate AS DATE) AS CreatedDate
						,t1.vdl_VetEntryMemoID AS zrefId
						,'tblVetEntryMemo' AS zrefSrc
					INTO GDB_Final_migration.DBO.IMP_VET_COMMUNICATION
					FROM GDB_KADE_Final.dbo.tblVetEntryMemo AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdl_VetEntryID 
					LEFT JOIN GDB_KADE_Final.dbo.trefVetSource AS T3 ON T3.VetSourceID=T2.vdl_Source
					LEFT JOIN GDB_KADE_Final.dbo.tblVetAuthorization AS T4 ON T1.vdl_VetEntryID=T4.vta_VetEntryID  --FB-02029
					

END 

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_VET_COMMUNICATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_VET_COMMUNICATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	

	SELECT * FROM GDB_Final_migration.dbo.IMP_VET_COMMUNICATION ORDER BY [Vet_Entry__r:Legacy_Id__c]
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_VET_COMMUNICATION
	 

END 