USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVetProc
			WHERE   SF_Object LIKE '%vet%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 



BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_VET_PROCEDURE

END 

BEGIN -- CREATE IMP 

					SELECT   'VDP-'+ CAST(T1.vdp_ProcID AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'Vdp-'+[vdp_ProcID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,T2.vdl_DogID AS [Dog__r:Legacy_ID__c]
							,X1.ID AS [Procedure_Code__c]
							--,T4.ProcedureTypeText  AS  Type__c	-- link [trefVetProc].[VetProcedureTypeCode]=[trefVetProcType].[ProcedureTypeCode] and migrate value from [trefVetProcType].[ProcedureTypeText]	-- link [trefVetProc].[VetProcedureCode]
							--,CAST(T3.VetProcedureText AS NVARCHAR(255)) Procedure__c	-- migrate value from [trefVetProc].[VetProcedureText]	-- link [trefVetProc].[VetProcedureCode]
							,CAST(T1.vdp_Value1  AS NVARCHAR(255)) AS  Value_1__c		
							,CASE T1.vdp_Recheck  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END   AS  Recheck__c		
							,CASE T1.vdp_PostOpInstr WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Post_Op_Instruction__c		
							,CASE T1.vdp_ProcCode WHEN '6000' THEN 'TRUE' ELSE 'FALSE' END AS Donation__c  --FB-00933
					INTO GDB_Final_migration.DBO.IMP_VET_PROCEDURE
					FROM GDB_KADE_Final.dbo.tblVetProc AS T1
					INNER JOIN GDB_Final_migration.dbo.xtr_ProcedureCode AS X1 ON T1.vdp_ProcCode=X1.Code__c
					LEFT JOIN GDB_KADE_Final.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdp_VetEntryID
				--	LEFT JOIN GDB_KADE_Final.dbo.trefVetProc AS T3 ON T3.VetProcedureCode=T1.vdp_ProcCode
				--	LEFT JOIN GDB_KADE_Final.dbo.trefVetProcType AS T4 ON T4.ProcedureTypeCode = T3.VetProcedureTypeCode

  
END   ---TC1: 1767379 TC2: 1,806,693 FINAL 1874797

BEGIN-- AUDIT
 
			USE GDB_Final_migration
 			EXEC sp_FindStringInTable '%"%', 'DBO', 'IMP_VET_PROCEDURE'
			
			UPDATE GDB_Final_migration.dbo.IMP_VET_PROCEDURE SET Value_1__c=REPLACE(Value_1__c,'"','''') where Value_1__c like '%"%'
			--UPDATE GDB_Final_migration.dbo.IMP_VET_PROCEDURE SET Procedure__c=REPLACE(Procedure__c,'"','''') where Procedure__c like '%"%'
			
			SELECT Value_1__c, LEN(Value_1__c) l 
			FROM GDB_Final_migration.DBO.IMP_VET_PROCEDURE
			ORDER BY l desc

			SELECT * FROM GDB_Final_migration.DBO.IMP_VET_PROCEDURE
			WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_Final_migration.DBO.IMP_VET_PROCEDURE GROUP BY Legacy_Id__c HAVING COUNT(*)>1)
				--0		
END 





























