USE GDB_Final_migration
GO
BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblStaffDogVacc
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN -- drop IMP DOG

	DROP TABLE GDB_Final_migration.dbo.IMP_DOG

END 

BEGIN--- CREATE DOG
			SELECT	 DISTINCT
				--tblDog	
					 IIF(X4.ADP__C IS NOT NULL,x4.id, GDB_Final_migration.[dbo].[fnc_OwnerId]()) AS OwnerId
					,T1.dog_DogID  AS  Legacy_ID__c		
					,T1.dog_DogID  AS  Dog_ID__c
					,T1.dog_GeneticDBNum  AS  Genetic_DB_Num__c		
					,T1.dog_GDBTattoo  AS  GDB_ID__c		
					,T1.dog_Name AS [NAME]
					--,X1.[NAME]  per FB-00943
					,IIF(T1.dog_Status='ped','', X1.ID) AS  Call_Name__c
					,T1.dog_Status		
					,X2.ID  AS RecordTypeId
					,T1.dog_FullAKCName  AS  Full_AKC_Name__c	
					,T1.dog_Name AS zrefName	
					,T1.dog_AKCNumber  AS  AKC_Number__c		
					,T1.dog_MicrochipID  AS  Microchip_ID__c		
					,T1.dog_MicrochipID2  AS  Microchip_ID_2__c		
					,T1.dog_Breed AS Breed__c-- per FB=00267 migrating abbr. ,T3.[Description]  AS  Breed__c		-- Yes/Link[trefDogBreed].[DogBreedCode]
					,T1.dog_LGXCode  AS  LGX__c		
					,T1.dog_PercentLAB  AS  Percent_LAB__c		
					,T1.dog_PercentGLD  AS  Percent_GLD__c		
					,T1.dog_FilialCode  AS  Filial__c		
					,T1.dog_Color  AS  Breed_Color__c		
					,T1.dog_Sex AS Sex__c-- updated per FB-00269,CASE T1.dog_Sex  WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END AS  Sex__c		
					,CAST(T1.dog_DateWhelp  AS DATE) AS  Date_Whelp__c		
					,CAST(T1.dog_DateInProgram   AS DATE) AS  Date_In_Program__c		
					,CAST(T1.dog_DateRelease   AS DATE) AS  Date_Release__c		
					,CAST(T1.dog_GiftofDogSent AS DATE)  AS  Gift_of_Dog_Sent__c		
					,T4.psn_PersonID  AS  [Gift_of_Dog_Person__r:Legacy_Id__c]			-- Link [tblPerson].[PersonID]
					,CAST(T1.dog_GiftofDogSecReq AS DATE)   AS  Gift_of_Dog_Sec_Req__c		
					,CAST(T1.dog_GiftofDogRec AS DATE)   AS  Gift_of_Dog_Rec__c		
					,CAST(T1.dog_DateNeuter AS DATE)   AS  Alter_Date__c		--FB-00574
					,CAST(T1.dog_NeuterLetterSent AS DATE)   AS  Alter_Letter_Sent__c		
					,CASE T1.dog_EarlyNeuterActive WHEN 1 THEN 'TRUE' ELSE 'FALSE' end AS  Early_Alter_Active__c		
					,CAST(T1.dog_DateObit  AS DATE)   AS Date_Obit__c		
 					,T1.dog_Status  AS  Dog_Level__c		
					,CASE WHEN T5.zrefRecordType='HHD' THEN T5.psn_PersonID end  AS  [Person__r:Legacy_Id__c] 		-- Yes/Link [tblPerson].[psn_PersonID]
					,CASE WHEN T5.zrefRecordType='org' THEN T5.psn_PersonID END AS  [Organization__r:Legacy_Id__c] 		-- Yes/Link [tblPerson].[psn_PersonID]
					,T1.dog_Notes  AS  Notes__c		
					,CASE T1.dog_Ponly WHEN 1 THEN 'TRUE' ELSE 'FALSE' end   AS  Ponly__c		
					,CAST(T1.dog_AlterPerBrd  AS DATE)  AS  Alter_Per_Brd__c		
					,T1.dog_CutNotification   AS  Cut_Notification__c		
					,CAST(T1.dog_DatePlaced  AS DATE)  AS  Date_Placed__c		
					,T1.dog_TRNCampus  AS  Training_Campus__c		
					--,CAST(T1.dog_DateRecall  AS DATE)  AS  Date_Recall__c		--FB-00184
					,T1.dog_FYRecall  AS  FY_Recall__c		
					,CAST(T1.dog_DateTrain  AS DATE)  AS  Date_Train__c		
					,CAST(T1.dog_DateInClass  AS DATE)  AS  Date_In_Class__c		
					,T1.dog_FY1stGrad  AS  FY_1st_Grad__c		
					,T1.dog_PedigreeText  AS  Pedigree_Text__c		
					,CAST(T1.dog_DateBreedWatchEnd  AS DATE)  AS  Date_Breed_Watch_End__c		
					,T1.tmpPedigreeID  AS  Pedigree_ID__c		
					,CASE WHEN T6.[Description] IS NULL THEN T1.dog_Source     ELSE T6.[Description] END  AS  Historical_Source__c	-- migreate [description]	-- Yes/Link [tref_DogSource].[SourceCode] --FB-00385
					,CASE WHEN T7.[Description] IS NULL THEN T1.dog_Source2    ELSE T7.[Description] END AS  Source__c	-- migreate [description]	-- Yes/Link [tref_DogSource].[SourceCode] --FB00385
					,T1.dog_CurrStatus AS  Dog_Status__c		
					,CAST(T1.dog_Date1stClassReady  AS DATE)  AS  Date_1st_Class_Ready__c		
					,T9.StingLink  AS  [First_String__r:Legacy_Id__c]		-- Yes/Link [tblClass].[cls_FirstString]  --campaign level 1 
					,CASE T1.dog_GrandfatheredVPC  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS     Grandfathered_VPC__c		
					--,T1.dog_RelPlacement  AS  Rel_Placement__c		--FB-00944
					--,CAST(T1.dog_RelSummary AS NVARCHAR(MAX)) AS  Rel_Summary__c		--FB-00944
					--,T1.dog_Location  AS  Location__c	  -- migrate value from [Location]		--FB-00944
					--,T1.dog_Disposition  AS  Disposition__c		--FB-00944
					--,T1.dog_StatusChangeBy  AS  Status_Change_By__c		--FB-00944
					--,T10. StingLink  AS  [Into_String__r:Legacy_Id__c]	--FB-00944	-- Yes/ Link [tblClass] --mk added: .[cls_FirstString]  --campaign level 1 
					--,T1.dog_StatusChangeNotes  AS  Status_Change_Notes__c		--FB-00944
					,T25. [Released_By__r:ADP__c] --FB-00944
					,T25.Release_Notes__c --FB-00944
					,CASE T1.dog_TRNEVL WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  TRNEVL__c	-- Migrate null values as 'No'	
					,T1.dog_CreatedBy  AS  Kade_Created_By__c	
					,'BrdEvl'+CAST(T1.dog_BrdEvlID AS VARCHAR(20)) AS Breed_Evaluation_CampaignLink --FB-00272	trumped by FB-00780
	  			--tblBreedingConformation
					,CAST(T13.bc_EvalDate AS DATE)  AS  Evaluation_Date__c		
					,X3.ID  AS  Evaluator__c		-- Link [tblStaff].[FileNum]
					,T13.bc_Gait  AS  Gait__c		
					,T13.bc_GaitDesc  AS  Gait_Description__c		
					,T13.bc_Profile  AS  Profile__c		
					,T13.bc_ProfileDesc  AS  Profile_Description__c		
					,T13.bc_Head  AS  Head__c		
					,T13.bc_HeadDesc  AS  Head_Description__c		
					,T13.bc_EyeEyelid  AS  Eyelid__c		
					,T13.bc_EyeEyelidDesc  AS  Eyelid_Description__c		
					,T13.bc_Bite  AS  Bite__c		
					,T13.bc_BiteDesc  AS  Bite_Description__c		
					,T13.bc_FrontAssb  AS  Front_Assembly__c		
					,T13.bc_FrontAssbDesc  AS  Front_Assembly_Description__c		
					,T13.bc_RearAssb  AS  Rear_Assembly__c		
					,T13.bc_RearAssbDesc  AS  Rear_Assembly_Description__c		
					,T13.bc_Feet  AS  Feet__c		
					,T13.bc_FeetDesc  AS  Feet_Description__c		
					,T13.bc_ColorPigment  AS  Color_Pigment__c		
					,T13.bc_ColorPigmentDesc  AS  Color_Pigment_Description__c		
					,T13.bc_Coat  AS  Coat__c		
					,T13.bc_CoatDesc  AS  Coat_Description__c		
					,T13.bc_Overall  AS  Overall__c		
					,T13.bc_TblBehavior  AS  Behavior__c		
					,T13.bc_TblBehaviorDesc  AS  Behavior_Description__c		
				--tblDogWhelping
					,T14.whp_WhelpType  AS  Whelp_Type__c
					,T14.whp_FirstWeight  AS  First_Weight__c
					,T14.whp_Clip  AS  Clip__c
					,T14.whp_Status  AS  Whelp_Status__c
				--tblControlFactor --fb-01761
					,T15.cf_Prelim  AS  Control_Factor_Preliminary__c
					,T15.cf_PrelimTrend  AS  Control_Factor_Preliminary_Trend__c
					,T15.cf_Final  AS  Control_Factor_Final__c
					,T15.cf_Class  AS  Control_Factor_Class__c
					,T15.cf_Student  AS  Control_Factor_student__c
					,T15.cf_InitialComments  AS  Control_Factor_Initial_Comments__c
					,T15.cf_OtherComments  AS  Control_Factor_Other_Comments__c
		/*		--tblStaffDogVacc
					,T16.DA2PP_Vaccine_Given__c
					,T16.Rabies_Vaccine_Given__c
					,T16.Leptospirosis_Vaccine_Given__c
					,T16.Bordetella_Vaccine_Given__c
					,T16.Vaccine_Exemption__c  
					*/
				--tblDogReleaseReasons  --updated FB-00024
					--,T17.drr_Importance     AS  Importance__c		
					--,T17.drr_DiagnosisCode  AS  [Diagnosis_Code__r:Legacy_Id__c]		-- Ref trefVetPetChamp: Mapped to new custom object
 					--,T17.drr_Notes          AS  Release_Notes__c	
					,T17.[Primary_Release_Code__c]
					,T17.[Secondary_Release_Code__c]
					,T17.[Tertiary_Release_Code__c]
					,T17.[Primary_Loaction__c]
					,T17.[Secondary_Loaction__c]
					,T17.[Tertiary_Loaction__c]
					,T17.[Primary_Release_Reason_Text__c]
					,T17.[Secondary_Release_Reason_Text__c]
					,T17.[Tertiary_Realease_Reason_Text__c]
				--tblStaffDog  --FB-00024
					,T21.stf_Breed AS Staff_Dog_Breed__c 
					,IIF(T21.stf_DogID IS NULL, 'FALSE', IIF(T21.stf_BoardingOnly='No', 'FALSE', 'TRUE')) AS Boarding_Only__c
					,T22.Weight_Last__c
					,CONVERT(VARCHAR(20),T23.Rabies_Last, 101) AS Rabies_Last__c
					,CONVERT(VARCHAR(20), T24.DA2PP_Last, 101) AS DA2PP_Last__c
					,T26.Total_Workouts__c --FB-01714
					,CONVERT(VARCHAR(20), T27.vdl_TreatmentDate, 101) AS Influenza_Last__c
					,CONVERT(VARCHAR(20), T27b.vdl_TreatmentDate, 101) AS Influenza_Prevous__c
					,CONVERT(VARCHAR(20), T28.Brucellosis_Last, 101) AS Brucellosis_Last__c	--FB-00969
					,CONVERT(VARCHAR(20), T29.Bordetella_Last, 101) AS Bordetella_Last__c	--FB-00969
					--,CONVERT(VARCHAR(20), T31.Lyme_Last, 101) AS Lyme_Last__c	--FB-00969
					,CONVERT(VARCHAR(20), T32.HipScores_Last, 101) AS HipScores_Last__c	--FB-00969
					,CONVERT(VARCHAR(20), T33.Parvo_Last, 101) AS Parvo_Last__c	--FB-00969
					,CONVERT(VARCHAR(20), T30.vdl_TreatmentDate, 101) AS Leptospirosis_Last__c	--FB-00969
					,CONVERT(VARCHAR(20), T30b.vdl_TreatmentDate, 101) AS Leptospirosis_Previous__c	--FB-00969
					,CONVERT(VARCHAR(20),T39.HW_Last, 101) AS HW_LAST__c
					,CONVERT(VARCHAR(20),T38.[CBC_Last], 101) AS CBC_LAST__c
					,CONVERT(VARCHAR(20),T40.Micro_Date, 101) AS Micro_Date__c
					,T41.Rabies_Facility AS Rabies__Facility__c
					,T42.Alter_Facility AS Alter__Facility__c
					,T34.WeightCP_Last__c  --FB-00969
					,T35.Height_Last__c   --FB-00969
					,T36.Hip_Score_Left__c	--FB-00969
					,T37.Hip_Score_Right__c		--FB-00969
					,T43.Brucellosis_Result__c

	 	 	INTO GDB_Final_migration.dbo.IMP_DOG -- DROP TABLE GDB_Final_migration.dbo.IMP_DOG_AUDIT
			FROM GDB_KADE_Final.dbo.tblDog AS T1
			LEFT JOIN GDB_Final_migration.dbo.XTR_DOG_NAME AS X1 ON T1.dog_Name=X1.[Name1]
			LEFT JOIN GDB_Final_migration.dbo.CHART_DOG AS T2 ON T2.dog_Sex = T1.dog_Sex AND T2.dog_Status = T1.dog_Status
			LEFT JOIN (SELECT * FROM GDB_Final_migration.dbo.XTR_RecordTypes WHERE SOBJECTTYPE='DOG__c') AS X2 ON T2.RecordTypeName=X2.[NAME1]
			LEFT JOIN GDB_KADE_Final.dbo.trefDogBreed AS T3 ON T3.DogBreedCode=T1.dog_Breed
			LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T4 ON T4.psn_PersonID=T1.dog_GiftofDogPersonID
			LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T5 ON T5.psn_PersonID=T1.dog_PersonID
			LEFT JOIN GDB_KADE_Final.dbo.trefDogSource AS T6 ON T6.SourceCode=T1.dog_Source
			LEFT JOIN GDB_KADE_Final.dbo.trefDogSource AS T7 ON T7.SourceCode=T1.dog_Source2
			LEFT JOIN GDB_KADE_Final.dbo.trefRelCodeNew  AS T8 ON T8.RelationCode = T1.dog_CurrStatus

			LEFT JOIN GDB_Final_migration.dbo.stg_CampaignString_transtbl AS T9 ON T1.dog_FirstString = T9.zrefClassId
			LEFT JOIN GDB_Final_migration.dbo.stg_CampaignString_transtbl AS T10 ON T1.dog_IntoString = T10.zrefClassId
			/* FB-00965
			--camp first strig
			LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T9 ON T9.cls_Campus= T1.dog_TRNCampus AND T9.cls_FirstString =T1.dog_FirstString
			LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_1 T10 ON T10.Legacy_Id__c=(T9.cls_FirstString  +' '+ T9.cls_Campus)
			--camp into  strig
			LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T11 ON T11.cls_Campus= T1.dog_TRNCampus AND T11.cls_FirstString =T1.dog_IntoString
			LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_1 T12 ON T12.Legacy_Id__c=(T11.cls_FirstString  +' '+ T11.cls_Campus)
			*/
			--
			LEFT JOIN GDB_KADE_Final.dbo.tblBreedingConformation AS T13 ON T13.bc_DogID = T1.dog_DogID
			LEFT JOIN GDB_Final_migration.DBO.XTR_USER AS X3 ON T13.bc_Evaluator=X3.ADP__C
			--
			LEFT JOIN GDB_KADE_Final.DBO.tblDogWhelping AS T14 ON T14.whp_DogID=T1.dog_DogID
			--
			LEFT JOIN GDB_KADE_Final.DBO.tblControlFactor AS T15 ON T15.cf_DogID = T1.dog_DogID
			--
		 	LEFT JOIN GDB_Final_migration.dbo.stg_tblStaffDogVacc_final  T16 ON T16.stv_DogID = T1.dog_DogID
			--
			LEFT JOIN GDB_Final_migration.dbo.stg_DogReleaseReasons AS T17 ON T17.drr_DogID=T1.dog_DogID
			--LEFT JOIN GDB_KADE_Final.dbo.trefVetPetChamp AS T18 ON T18.DiagnosisCode=T17.drr_DiagnosisCode
			--LEFT JOIN GDB_KADE_Final.DBO.trefVetDiagCategory T19 ON T19.CategoryCode=T18.DiagnosisCategory
			LEFT JOIN GDB_KADE_Final.DBO.tblStaffDog AS T21 ON T1.dog_DogID=T21.stf_DogID
			LEFT JOIN GDB_Final_migration.DBO.XTR_USER AS X4 ON T21.stf_StaffID = X4.ADP__C
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Weight AS T22 ON T1.dog_DogID=T22.vpe_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Rabies AS T23 ON T1.dog_DogID=T23.vdl_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_DA2PP AS T24 ON T1.dog_DogID=T24.vdl_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_DogStatusChange AS T25 ON T25.dds_DogID=T1.dog_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_TotalWorkouts AS T26 ON T26.wk_DogId=T1.dog_DogID --FB-01714
			
			LEFT JOIN (SELECT * from GDB_Final_migration.dbo.stg_Dog_Influenza WHERE scq='1') AS T27 ON T1.dog_DogID=T27.vdl_DogID 
			LEFT JOIN (SELECT * from GDB_Final_migration.dbo.stg_Dog_Influenza WHERE scq='2') AS T27b ON T1.dog_DogID=T27b.vdl_DogID 
			
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Brucellosis AS T28 ON T1.dog_DogID=t28.[vdl_DogID]	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Bordetella AS T29 ON T1.dog_DogID=t29.[vdl_DogID]	--FB-00969
			LEFT JOIN (SELECT * from GDB_Final_migration.dbo.stg_Dog_Leptospirosis WHERE scq='1') AS T30 ON T1.dog_DogID=T30.vdl_DogID  --FB-00969
			LEFT JOIN (SELECT * from GDB_Final_migration.dbo.stg_Dog_Leptospirosis WHERE scq='2') AS T30b ON T1.dog_DogID=T30b.vdl_DogID	--FB-00969		

			--LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Lyme AS T31 ON T1.dog_DogID=T31.[vdl_DogID]	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_HipScores AS T32 ON T1.dog_DogID=T32.[vdl_DogID]	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Parvo AS T33 ON T1.dog_DogID=T33.[vdl_DogID]	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_WeightCP AS T34 ON T1.dog_DogID=T34.[vpe_DogID]	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_Height AS T35 ON T1.dog_DogID=T35.vpe_DogID	--FB-00969
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_HipScores_Left AS T36 ON T1.dog_DogID=T36.vdl_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_HipScores_Right AS T37 ON T1.dog_DogID=T37.vdl_DogID
			LEFT JOIN GDB_FINAL_Migration.dbo.stg_DOG_CBC AS T38 ON T1.dog_DogID=T38.vdl_DogID
			LEFT JOIN GDB_FINAL_Migration.dbo.stg_DOG_HW AS T39 ON T1.dog_DogID=T39.vdl_DogID
			LEFT JOIN GDB_FINAL_Migration.dbo.stg_Dog_Micro_Date AS T40 ON T1.dog_DogID=T40.vdl_DogID
			LEFT JOIN GDB_FINAL_Migration.dbo.stg_Dog_Rabies_Facility AS T41 ON T1.dog_DogID=T41.vdl_DogID
			LEFT JOIN GDB_FINAL_Migration.dbo.stg_Dog_Alter_Facility AS T42 ON T1.dog_DogID=T42.vdl_DogID
			LEFT JOIN GDB_Final_migration.dbo.stg_Dog_BrucellosisResult AS T43 ON T1.dog_DogID=T42.vdl_DogID


BEGIN --updates last dates from tblStaffVac

		UPDATE GDB_FINAL_Migration.dbo.IMP_DOG
		SET Bordetella_Last__c = T2.Bordetella_Vaccine_Given__c
		,DA2PP_Last__c = T2.DA2PP_Vaccine_Given__c
		,Rabies_Last__c = T2.Rabies_Vaccine_Given__c
		,Leptospirosis_Last__c = T2.Leptospirosis_Vaccine_Given__c
		,Influenza_Last__c = T2.Influenza_Last
		,Influenza_Prevous__c = T2.Influenza_Previous
		FROM GDB_FINAL_Migration.dbo.IMP_DOG AS T
		INNER JOIN GDB_FINAL_Migration.dbo.stg_tblStaffDogVacc_final AS T2 ON T.Dog_ID__c=T2.stv_DogID

END
		 
   
BEGIN--AUDITS
		  
		--REMOVE QUOTES -- - CSV read/write UTF-8 enconding allows double quotes.
		 SELECT Legacy_ID__c, Status__c FROM GDB_Final_migration.dbo.IMP_DOG
 	
		--check duple

			SELECT COUNT(*) FROM GDB_KADE_Final.dbo.tblDog --45128		tC2: 45528  final 46160
			SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_DOG --45128  TC2: 45530  final 46165

			SELECT * FROM GDB_Final_migration.dbo.IMP_DOG --45128
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_DOG GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
END 


--check campaings.
		SELECT dog_TRNCampus, dog_IntoString  
		FROM GDB_KADE_Final.dbo.tblDog  
		GROUP BY dog_TRNCampus, dog_IntoString 
		ORDER BY dog_TRNCampus, dog_IntoString 


		SELECT dog_TRNCampus, dog_IntoString  
		FROM GDB_KADE_Final.dbo.tblDog  
		GROUP BY dog_TRNCampus, dog_IntoString 
		ORDER BY dog_TRNCampus, dog_IntoString 


		SELECT T1.cls_Campus, T1.cls_FirstString  
		,T1.cls_FirstString  +' '+ T1.cls_Campus AS  [Legacy_Id__c]
		FROM GDB_KADE_Final.dbo.tblClass AS T1
		GROUP BY T1.cls_Campus, t1.cls_FirstString
		ORDER BY T1.cls_Campus, t1.cls_FirstString

--check person

	SELECT * FROM GDB_KADE_Final.dbo.tblPerson
	WHERE psn_PersonID='25504' OR psn_PersonId='69034'


	SELECT DISTINCT Legacy_ID__c, [NAME] FROM GDB_Final_migration.dbo.IMP_DOG


	SELECT * FROM GDB_Final_migration.dbo.IMP_DOG


	
		
        