USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblK9BuddyHomeInterview
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

SELECT * FROM GDB_Final_migration.dbo.XTR_RecordTypes WHERE SOBJECTTYPE LIKE '%place%' ORDER BY SOBJECTTYPE, NAME

BEGIN -- drop
	DROP TABLE GDB_Final_migration.dbo.IMP_DOG_PLACEMENT_ADMIN
END

BEGIN --create			
							SELECT    
							 		 'k9h-'+CAST(T1.k9h_k9HomeIntID AS NVARCHAR(30)) AS  Legacy_ID__c		
									,T2.psn_PersonID  AS  [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
									,X1.id AS  OwnerID		-- Link [tblStaff].[FileNum]
									,CAST(T1.k9h_Date  AS DATE) AS  Date__c		
									,T1.k9h_PeopleatHouse  AS  People_at_House__c		
									,T1.k9h_School  AS  School__c		
									,T1.k9h_Grade  AS  Grade__c		
									,CASE T1.k9h_InresidenceSchool  WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  In_Residence_School__c	-- when value blank migrate as 'No'	
									,T1.k9h_DaysAwayatSchool  AS  Days_Away_at_School__c		
									,T1.k9h_SpecialInterest  AS  Special_Interest__c		
									,T1.k9h_ExtraActivities  AS  Extra_Activities__c		
									,T1.k9h_DescripListofPets  AS  Descript_List_of_Pets__c		
									,CASE T1.k9h_HousingOK  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Housing_OK__c	-- when value blank migrate as 'No'	
									,T1.k9h_HousingComments  AS  Housing_Comments__c		
									,T1.k9h_Whyk9Buddy  AS  Why_K9_Buddy__c		
									,CASE T1.k9h_ChildWantDog  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Child_Want_Dog__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_FamilySupport  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Family_Support__c	-- when value blank migrate as 'No'	
									,T1.k9h_PastDogPetExper  AS  Past_Dog_Pet_Experience__c		
									,T1.k9h_MedicalIssues  AS  Medical_Issues__c		
									,T1.k9h_ConceptsParent  AS  Concepts_Parent__c		
									,T1.k9h_ConfidenceParent  AS  Confidence_Parent__c		
									,T1.k9h_DexterityParent  AS  Dexterity_Parent__c		
									,T1.k9h_CoordinationParent  AS  Coordination_Parent__c		
									,T1.k9h_QtyLeashCorParent  AS  Quantity_Leash_Cor_Parent__c		
									,T1.k9h_VerbalAbilityParent  AS  Verbal_Ability_Parent__c		
									,T1.k9h_AbilityReactParent  AS  Ability_React_Parent__c		
									,T1.k9h_AbilityMonitorParen  AS  Ability_Monitor_Parent__c		
									,T1.k9h_AbilityImpInstrParent  AS  Ability_Imp_Instructor_Parent__c		
									,T1.k9h_EngagementParent  AS  Engagement_Parent__c		
									,T1.k9h_ConceptsYouth  AS  Concepts_Youth__c		
									,T1.k9h_ConfidenceYouth  AS  Confidence_Youth__c		
									,T1.k9h_DexterityYouth  AS  Dexterity_Youth__c		
									,T1.k9h_CoordinationYouth  AS  Coordination_Youth__c		
									,T1.k9h_QtyLeashCorYouth  AS  Quantity_Leash_Cor_Youth__c		
									,T1.k9h_VerbalAbilityYouth  AS  Verbal_Ability_Youth__c		
									,T1.k9h_AbilityReactYouth  AS  Ability_React_Youth__c		
									,T1.k9h_AbilityMonitorYouth  AS  Ability_Monitor_Youth__c		
									,T1.k9h_AbilityImpInstrYouth  AS  Ability_Imp_Instructor_Youth__c		
									,T1.k9h_EngagementYouth  AS  Engagement_Youth__c		
									,CASE T1.k9h_CD  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  CD__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_Braille  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Braille__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_LargePrint  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Large_Print__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_RegularPrint  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Regular_Print__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_ComfortDogYouth  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Comfort_Dog_Youth__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_ComfortDogFamily  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Comfort_Dog_Family__c	-- when value blank migrate as 'No'	
									,CASE T1.k9h_DoginHouseOK  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Dog_in_House_OK__c	-- when value blank migrate as 'No'	
									,T1.k9h_DesiredDogPersonality  AS  Desired_Dog_Personality__c		
									,T1.k9h_Size  AS  Size__c		
									,T1.k9h_EnergyLevel  AS  Energy_Level__c		
									,T1.k9h_Gender  AS  Gender__c		
									,CASE T1.k9h_OlderDog  WHEN 'Yes' THEN 'Yes' ELSE 'No' END AS  Older_Dog__c	-- when value blank migrate as 'No'	
									,T1.k9h_Narrative  AS  Narrative__c		
									,T1.k9h_Recommodation  AS  Recommendation__c		
									,T1.k9h_CommentsifNotRecom  AS  Comments_if_Not_Recommended__c		
								 
									INTO GDB_Final_migration.dbo.IMP_DOG_PLACEMENT_ADMIN
									FROM GDB_KADE_Final.dbo.tblK9BuddyHomeInterview AS T1
									LEFT JOIN GDB_KADE_Final.dbo.tblPerson AS T2 ON T2.psn_PersonID = T1.k9h_PersonID
									LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.k9h_Interviewer

END --tc1 :104 TC2: 107  Final 111

BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_DOG_PLACEMENT_ADMIN
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_DOG_PLACEMENT_ADMIN GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
END 

