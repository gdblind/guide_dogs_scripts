SELECT COUNT(*) FROM GDB_KADE_Final.dbo.trefZipCodes

SELECT 
	[GDB_Final_migration].[dbo].[fnc_OwnerId]() AS OwnerId
	,T1.Country AS Country__c
	,T1.ZipCode AS Zip_Code__c
	,T1.ZipCode AS [Name]
	,T1.County AS County__c
	,T1.StateFull AS State_Full__c
	,T1.[STATE] AS State__c
	,T1.City AS City__c
	,X1.id AS Field_Service_Manager__c

INTO [GDB_Final_migration].dbo.IMP_ZIPCODE -- DROP TABLE [GDB_Final_migration].dbo.IMP_ZIPCODE
FROM [GDB_KADE_Final].dbo.trefZipCodes AS T1
LEFT JOIN [GDB_Final_migration].dbo.XTR_USER AS X1 ON X1.ADP__C=T1.FieldRep

-- COUNT TC2: 885,485  final 885,486

SELECT * FROM [GDB_Final_migration].dbo.IMP_ZIPCODE
--picklist values
SELECT DISTINCT T.Country__c
FROM [dbo].[IMP_ZIPCODE] AS T