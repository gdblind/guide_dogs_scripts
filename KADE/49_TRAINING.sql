USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblTRNBackUpTest
			WHERE   SF_Object LIKE '%trai%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_Final_migration.dbo.XTR_RECORD_TYPE  WHERE SOBJECTTYPE LIKE '%train%'

0121L000001UQDYQA4	Final BLD
0121L000001UQDZQA4	Final GW
0121L000001UQDaQAO	Final OB
0121L000001UQDbQAO	Guide Evaluation
0121L000001UQDcQAO	Passback
0121L000001UQDdQAO	Prelim GW
0121L000001UQDeQAO	Prelim OB
0121L000001UQDfQAO	Sidewalkless
0121L000001UQDgQAO	Testing
0121L000001UQDhQAO	Traffic
0121L000001UQDiQAO	Traffic Backup
0121L000001UQDjQAO	Weekly

*/

BEGIN -- DROP IMP
	DROP TABLE GDB_Final_migration.DBO.xtr_training
	DROP TABLE GDB_Final_migration.DBO.IMP_TRAINING
	DROP TABLE GDB_Final_migration.DBO.IMP_TRAINING_trntesting

END 

BEGIN -- CREATE IMP 

					SELECT DISTINCT  
						CASE WHEN X2.ID IS NOT NULL THEN X2.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
						,'TRT-'+CAST(T1.trt_TrafficID AS NVARCHAR(30)) AS  Legacy_ID__c	-- migrate 'Trt-'+[trt_TrafficID]	
						--,NULL AS ContactLink --filler
						,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
						,IIF(t1.trt_Trainer='' OR T1.trt_trainer IS NULL, NULL, X2.LEGACY_ID__C) AS [Instructor_Trainer__r:Legacy_ID__c]		-- Link [tblStaff].[FileNum]
						,x2.ADP__C AS [Instructor_Trainer_User__r:ADP__c]
						,NULL AS  [String__r:Legacy_ID__c]
							,NULL AS  ParentTrainingLink
							,NULL AS  Backup_Test__c
						--	,NULL AS Pass_Back_Date__c		FB-00933
						--	,NULL AS New_Instructor_Trainer_Link
							,NULL AS Reason_for_Pass_Back__c	 
					 	--	,NULL AS NewStringLink		-FB-00337
						--	,NULL AS  Test_Date__c		--filler FB-00933
							,NULL AS  Tester__c		 --filler
							--,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler

						,T1.trt_TrafficTest  AS  Traffic_Type__c		--FB-01712
						,IIF(T1.trt_Date IS NULL, CAST(T1.trt_CreatedDate AS DATE), CAST(T1.trt_Date  AS DATE)) AS  Date__c		
						,T1.trt_WorkoutNumber  AS  Workout_Number__c		
						,IIF(T1.trt_Driver ='' OR T1.trt_Driver IS NULL, NULL,X1.id)  AS  Driver__c				-- Link [tblStaff].[FileNum]
						,T1.trt_AcceptableResponse  AS  Acceptable_Response__c		
						,T1.trt_BackupTRNRequired  AS  Backup_Training_Required__c		
							--filler
							,NULL AS Train_Week__c		
							,NULL AS Float__c		
 							,NULL AS PB__c		
							,NULL AS Current_Instructor__c		
							,NULL AS Current_Weekly_Report__c		
							,NULL AS Hold__c		
							,NULL AS Phase__c		
							,NULL AS Total_Workouts__c		
							,NULL AS This_Weeks_Workouts__c
							,NULL AS First_Date__c		
							,NULL AS Counter_Con__c		
							,NULL AS Previous_Experience__c		
							,NULL AS CLR__c		
							,NULL AS RG__c		
							,NULL AS Relieving__c		
							,NULL AS Health__c		
							,NULL AS Breeding__c		
							,NULL AS Control__c		
							,NULL AS Temperament__c		
							,'FALSE' AS Body_Sensitivity__c		
							,NULL AS Rough_Food__c		
							,NULL AS [2ndry_R_concerns__c]		
							,NULL AS High_Value__c		
							,NULL AS Train_Notes__c		
							,'FALSE' AS Archive__c		
							,NULL AS Non_Escalator__c		
							,NULL AS Normal_Progress__c	
							--,NULL  AS  Traffic__c		
							/*,NULL  AS  City__c		
							,NULL  AS  Revolving_Door__c		
							,NULL  AS  Escalators__c		
							,NULL  AS  Public_Transit__c		
							,NULL  AS  Night_Route__c		
							,NULL  AS  Sidewalkless__c		
							,NULL  AS  Country_Road__c		
							,NULL  AS  Rounded_Corners__c		
							,NULL  AS  Freelance__c		
							,NULL  AS  Bus_Lounge__c		
							,NULL  AS  College_Campus__c		
							,NULL  AS  Underground_Tran__c		
							,NULL  AS  Nursing_Home__c		
							,NULL  AS  Self_Orientation__c		
							,NULL  AS  Wheelchair_Exposure__c		
							,NULL   AS  Kid_Exposure__c		
							,NULL  AS  Additional_SWL__c		
							,NULL  AS  Offset_Crossings__c		
							,NULL  AS  Wide_Crossing__c		
							,NULL  AS  Island_Crossing__c		
							,NULL  AS  Custom_Route__c	
							*/
							,NULL AS Issue_Date__c
							,NULL AS Reason__c
							,NULL AS Graduation_Notes__c
							,NULL AS PRP_Name__c
							,NULL AS Name__c
							,NULL AS PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  Summary_Recommendation__C	
							,NULL AS Disposition__c
							--,NULL AS  Director_of_training__c		FB-01686
							,NULL AS Type_of_Evaluation --FB-01251
							,NULL AS Visit_Category__c
										
						,CAST(T1.trt_CreatedDate AS DATE) AS CreatedDate
						,'0121L000001UQDhQAO'  AS  RecordTypeId	-- Traffic	
 						,T1.trt_TrafficID AS zrefId
						,'1-tblTRNTrafficTest' AS zrefSrc
						

					INTO GDB_Final_migration.DBO.IMP_TRAINING
					FROM GDB_KADE_Final.dbo.tblTRNTrafficTest AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.trt_DogID
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.trt_Driver
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=T1.trt_Trainer
					--WHERE T1.trt_TrafficID='456' to audit
					

				UNION 

 					SELECT   DISTINCT 		
							CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
							,'WK-'+CAST(T1.wk_WklyTrainID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "WK-" with wk_WklyTrainID	
						--	,NULL AS ContactLink  --filler
							,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
							,X1.LEGACY_ID__C AS  [Instructor_Trainer__r:Legacy_ID__c]		-- Link [tblStaff].[FileNum]
							,x1.adp__c AS [Instructor_Trainer_User__r:ADP__c]
							,T4.cls_FirstString  +' '+ T4.cls_Campus   AS  [String__r:Legacy_ID__c]  	-- Link [tblClass].[cls_FirstString] 
							,NULL AS  ParentTrainingLink
							,NULL AS  Backup_Test__c

 						--	,NULL AS Pass_Back_Date__c		FB-00933
								--,NULL AS New_Instructor_Trainer_Link	 
								,NULL AS Reason_for_Pass_Back__c	
						--		,NULL AS NewStringLink
						--	,NULL AS  Test_Date__c		--filler FB-00933
							,NULL AS  Tester__c		 --filler
							--,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler
							,NULL AS Traffic_Type__c --filler FB-01712
							,CAST(T1.wk_Date AS DATE)  AS  Date__c
							,NULL AS Workout_Number__c   --filler
							,NULL AS Driver__c	 --filler
							,NULL AS Acceptable_Response__c --filler
							,NULL AS Backup_Training_Required__c --filler
							,T1.wk_TrainWeek  AS  Train_Week__c		
							,CASE T1.wk_Float WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Float__c		
 							,CASE T1.wk_PB WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  PB__c		
							,CASE T1.wk_CurrInstr WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Current_Instructor__c		
							,CASE T1.wk_CurWklyRpt WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Current_Weekly_Report__c		
							,CASE T1.wk_Hold WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Hold__c		
							,T1.wk_Phase  AS  Phase__c		
							,T1.wk_TotWkouts  AS  Total_Workouts__c		
							,T1.wk_TotCnt  AS  This_Weeks_Workouts__c  --FB-00229
							,CAST(T1.wk_FirstDate  AS DATE) AS  First_Date__c		
							,CASE T1.wk_CounterCon  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Counter_Con__c		
							,CASE T1.wk_PreExper  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Previous_Experience__c		
							,CASE T1.wk_CLR  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  CLR__c		
							,CASE T1.wk_RG   WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS  RG__c		
							,CASE T1.wk_RelievingProgram  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Relieving__c		
							,CASE T1.wk_Health  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Health__c		
							,CASE T1.wk_Breeding  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Breeding__c		
							,CASE T1.wk_Control  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Control__c		
							,CASE T1.wk_Temperament  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Temperament__c		
							,CASE T1.wk_BodySensitivity  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Body_Sensitivity__c		
							,CASE T1.wk_RoughFood  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Rough_Food__c		
							,CASE T1.wk_SecRConcern  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  [2ndry_R_concerns__c]		
							,CASE T1.wk_HighValue  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  High_Value__c		
							,CAST(T1.wk_TrainNotes AS NVARCHAR(MAX)) AS  Train_Notes__c		
							,CASE T1.wk_Archive  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Archive__c		
							,CASE T1.wk_NonEscalator   WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Non_Escalator__c		
							,CASE T1.wk_NormalProgress   WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Normal_Progress__c		
						--	,NULL  AS  Traffic__c		
						/*	,NULL  AS  City__c		
							,NULL  AS  Revolving_Door__c		
							,NULL  AS  Escalators__c		
							,NULL  AS  Public_Transit__c		
							,NULL  AS  Night_Route__c		
							,NULL  AS  Sidewalkless__c		
							,NULL  AS  Country_Road__c		
							,NULL  AS  Rounded_Corners__c		
							,NULL  AS  Freelance__c		
							,NULL  AS  Bus_Lounge__c		
							,NULL  AS  College_Campus__c		
							,NULL  AS  Underground_Tran__c		
							,NULL  AS  Nursing_Home__c		
							,NULL  AS  Self_Orientation__c		
							,NULL  AS  Wheelchair_Exposure__c		
							,NULL  AS  Kid_Exposure__c		
							,NULL  AS  Additional_SWL__c		
							,NULL  AS  Offset_Crossings__c		
							,NULL  AS  Wide_Crossing__c		
							,NULL  AS  Island_Crossing__c	
							,NULL  AS  Custom_Route__c	
							*/
							,NULL AS Issue_Date__c
							,NULL AS Reason__c
							,NULL AS Graduation_Notes__c
							,NULL AS PRP_Name__c
							,NULL AS Name__c
							,NULL AS PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  Summary_Recommendation__C	
							,NULL AS Disposition__c
							--,NULL AS  Director_of_training__c		 FB-01686
							,NULL AS Type_of_Evaluation --FB-01251
							,NULL AS Visit_Category__c
							
 							,CAST(T1.wk_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQDjQAO'  AS  RecordTypeID	-- Weekly
 							,T1.wk_WklyTrainID AS zrefId
							,'2-tbl_WklyTrain' AS zrefSrc	

						FROM GDB_KADE_Final.dbo.tbl_WklyTrain AS T1
						LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.wk_DogId
						LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.wk_InstrId
						--camp  
						LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.wk_StringId
						--LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.st=('cls-'+T4.cls_FirstString )
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.wk_InstrId
	
						
					UNION
					
						SELECT  DISTINCT   
							GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
 							,'PBI-'+CAST(T1.pbi_PassBackID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Pbi-'+[pbi_PassBackID]	
						--	,NULL AS ContactLink
							,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
							--,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS  [Instructor_Trainer__r:Legacy_ID__c]		-- Link [tblStaff].[FileNum]
							,NULL AS [Instructor_Trainer__r:Legacy_ID__c]
							,NULL AS [Instructor_Trainer_User__r:ADP__c]
							,T5.Legacy_Id__c   AS [String__r:Legacy_ID__c]
							,NULL AS  ParentTrainingLink
							,NULL AS  Backup_Test__c

							--,CAST(T1.pbi_PBDate  AS DATE) AS  Pass_Back_Date__c		fB-00933
							--,'Staff-'+ CAST(T3N.FileNum  AS NVARCHAR(30))   AS  New_Instructor_Trainer_Link		-- Yes/Link tblStaff.FileNum
							,T1.pbi_ReasonforPassBack  AS  Reason_for_Pass_Back__c		
 					 	  --  ,T7.Legacy_Id__c   AS NewStringLink-- Yes/ Link tblClass.cls_FirstString
						--		,NULL AS  Test_Date__c		--filler  FB-00933
								,NULL AS  Tester__c		 --filler
						--		,NULL AS  Test_Type__c		--filler 
								,NULL AS  Grade__c		 --filler
								,NULL AS  Comments__c	--filler	
								,NULL AS  Preliminary_GW_Pace__c	--filler	
								,NULL AS  Final_GW_Pace__c	--filler	
								,NULL AS  Route__c	--filler
 								,NULL AS  Traffic_Type__c --FB-01712		
								,CAST(T1.pbi_PBDate  AS DATE) AS  Date__c		
								,NULL AS  Workout_Number__c		
								,NULL AS  Driver__c					 
								,NULL AS  Acceptable_Response__c		
								,NULL AS  Backup_Training_Required__c		
								--filler
								,NULL AS Train_Week__c		
								,NULL AS Float__c		
 								,NULL AS PB__c		
								,NULL AS Current_Instructor__c		
								,NULL AS Current_Weekly_Report__c		
								,NULL AS Hold__c		
								,NULL AS Phase__c		
								,NULL AS Total_Workouts__c		
								,NULL AS This_Weeks_Workouts__c		
								,NULL AS First_Date__c		
								,NULL AS Counter_Con__c		
								,NULL AS Previous_Experience__c		
								,NULL AS CLR__c		
								,NULL AS RG__c		
								,NULL AS Relieving__c		
								,NULL AS Health__c		
								,NULL AS Breeding__c		
								,NULL AS Control__c		
								,NULL AS Temperament__c		
								,'FALSE' AS Body_Sensitivity__c		
								,NULL AS Rough_Food__c		
								,NULL AS [2ndry_R_concerns__c]		
								,NULL AS High_Value__c		
								,NULL AS Train_Notes__c		
								,'FALSE' AS Archive__c		
								,NULL AS Non_Escalator__c		
								,NULL AS Normal_Progress__c	
							--,T1.pbi_Traffic  AS  Traffic__c			
							/*	,NULL  AS  City__c		
								,NULL  AS  Revolving_Door__c		
								,NULL  AS  Escalators__c		
								,NULL  AS  Public_Transit__c		
								,NULL  AS  Night_Route__c		
								,NULL  AS  Sidewalkless__c		
								,NULL  AS  Country_Road__c		
								,NULL  AS  Rounded_Corners__c		
								,NULL  AS  Freelance__c		
								,NULL  AS  Bus_Lounge__c		
								,NULL  AS  College_Campus__c		
								,NULL  AS  Underground_Tran__c		
								,NULL  AS  Nursing_Home__c		
								,NULL  AS  Self_Orientation__c		
								,NULL  AS  Wheelchair_Exposure__c		
								,NULL   AS  Kid_Exposure__c		
								,NULL  AS  Additional_SWL__c		
								,NULL  AS  Offset_Crossings__c		
								,NULL  AS  Wide_Crossing__c		
								,NULL  AS  Island_Crossing__c		
								,NULL  AS  Custom_Route__c	
									*/
							,NULL AS Issue_Date__c
							,NULL AS Reason__c
							,NULL AS Graduation_Notes__c
							,NULL AS PRP_Name__c
							,NULL AS Name__c
							,NULL AS PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  Summary_Recommendation__C		
							,NULL AS Disposition__c
							--,NULL AS  Director_of_training__c	FB-01686	
							,NULL AS Type_of_Evaluation --FB-01251
							,NULL AS Visit_Category__c
							
						 	,CAST(T1.pbi_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQDcQAO'  AS  RecordTypeID	--- Passback	
 							,T1.pbi_PassBackID AS zrefId
							,'5-tblTRNPassbackInfo' AS zrefSrc	
						
						FROM GDB_KADE_Final.dbo.tblTRNPassbackInfo AS T1 
						INNER JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pbi_DogID
						LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.pbi_PassedFrom
						LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3N ON T3N.FileNum=T1.pbi_PassedTo
						--string
						LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.pbi_CurrentString
						LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.Legacy_Id__c=('cls-'+T4.cls_FirstString)
						--new string
						--LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T6 ON  T6.cls_FirstString =T1.pbi_NewString
						--LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T7 ON T7.Legacy_Id__c=('cls-'+T6.cls_FirstString )
				UNION
                

					SELECT  DISTINCT 
						
							CASE WHEN X2.ID IS NOT NULL THEN X2.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
  							,'BUT-'+CAST(T1.but_BackupID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'But-'+[but_BackupId]	
						--	,NULL AS ContactLink
							,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
							,X2.LEGACY_ID__C   AS [Instructor_Trainer__r:Legacy_ID__c]		-- Link [tblStaff].[FileNum]
							,x2.ADP__C AS [Instructor_Trainer_User__r:ADP__c]
							,NULL AS [String__r:Legacy_ID__c] 
							
							,'TRT-'+CAST(T4.trt_TrafficID AS NVARCHAR(30)) AS  ParentTrainingLink
							,T1.but_BackupTest  AS  Backup_Test__c
 						--		,NULL AS Pass_Back_Date__c		FB-00933
 						--		,NULL AS New_Instructor_Trainer_Link
								,NULL AS Reason_for_Pass_Back__c		 
							--	,NULL AS NewStringLink 
 							--	,NULL AS  Test_Date__c		--filler  FB-00933
								,NULL AS  Tester__c		 --filler
							--	,NULL AS  Test_Type__c		--filler 
								,NULL AS  Grade__c		 --filler
								,NULL AS  Comments__c	--filler	
								,NULL AS  Preliminary_GW_Pace__c	--filler	
								,NULL AS  Final_GW_Pace__c	--filler	
								,NULL AS  Route__c	--filler
 								,NULL AS  Traffic_Type__c		--fB-01712
								,IIf (T1.but_Date IS NULL, CAST(T1.but_CreatedDate AS DATE),CAST(T1.but_Date AS DATE)) AS  Date__c		
								,T1.but_WorkoutNumber AS  Workout_Number__c		
								,X1.ID   AS  Driver__c					 
								,T1.but_AcceptableResponse AS  Acceptable_Response__c		
								,NULL AS  Backup_Training_Required__c		
								--filler
								,NULL AS Train_Week__c		
								,NULL AS Float__c		
 								,NULL AS PB__c		
								,NULL AS Current_Instructor__c		
								,NULL AS Current_Weekly_Report__c		
								,NULL AS Hold__c		
								,NULL AS Phase__c		
								,NULL AS Total_Workouts__c		
								,NULL AS This_Weeks_Workouts__c		
								,NULL AS First_Date__c		
								,NULL AS Counter_Con__c		
								,NULL AS Previous_Experience__c		
								,NULL AS CLR__c		
								,NULL AS RG__c		
								,NULL AS Relieving__c		
								,NULL AS Health__c		
								,NULL AS Breeding__c		
								,NULL AS Control__c		
								,NULL AS Temperament__c		
								,'FALSE' AS Body_Sensitivity__c		
								,NULL AS Rough_Food__c		
								,NULL AS [2ndry_R_concerns__c]		
								,NULL AS High_Value__c		
								,NULL AS Train_Notes__c		
								,'FALSE' AS Archive__c		
								,NULL AS Non_Escalator__c		
								,NULL AS Normal_Progress__c	
								--,NULL AS Traffic__c			
								/*,NULL  AS  City__c		
								,NULL  AS  Revolving_Door__c		
								,NULL  AS  Escalators__c		
								,NULL  AS  Public_Transit__c		
								,NULL  AS  Night_Route__c		
								,NULL  AS  Sidewalkless__c		
								,NULL  AS  Country_Road__c		
								,NULL  AS  Rounded_Corners__c		
								,NULL  AS  Freelance__c		
								,NULL  AS  Bus_Lounge__c		
								,NULL  AS  College_Campus__c		
								,NULL  AS  Underground_Tran__c		
								,NULL  AS  Nursing_Home__c		
								,NULL  AS  Self_Orientation__c		
								,NULL  AS  Wheelchair_Exposure__c		
								,NULL  AS  Kid_Exposure__c		
								,NULL  AS  Additional_SWL__c		
								,NULL  AS  Offset_Crossings__c		
								,NULL  AS  Wide_Crossing__c		
								,NULL  AS  Island_Crossing__c		
								,NULL  AS  Custom_Route__c	
						*/
							,NULL AS Issue_Date__c
							,NULL AS Reason__c
							,NULL AS Graduation_Notes__c
							,NULL AS PRP_Name__c
							,NULL AS Name__c
							,NULL AS PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  Summary_Recommendation__C		
							,NULL AS Disposition__c
							--,NULL AS  Director_of_training__c		  FB-01686
							,NULL AS Type_of_Evaluation --FB-01251
							,NULL AS Visit_Category__c
							
							,CAST(T1.but_CreatedDate AS DATE) AS  CreatedDate		
							,'0121L000001UQDhQAO'  AS  RecordTypeID	--- Traffic	
 							,T1.but_BackupID AS zrefId
							,'6-tblTRNBackUpTest' AS zrefSrc	
						
						FROM GDB_KADE_Final.dbo.tblTRNBackUpTest AS T1 
						LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.but_DogID
						LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.but_Trainer
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.but_Driver
						LEFT JOIN GDB_KADE_Final.dbo.tblTRNTrafficTest AS T4 ON T4.trt_TrafficID=T1.but_TrafficID
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=T1.but_Trainer
 

 UNION --fb-00046

					SELECT  
							CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
							--,[Name] = CASE WHEN T1.ev_EvalType IS NOT NULL THEN 'Evaluation ' + T1.ev_EvalType WHEN t1.ev_EvalType IS NULL THEN 'Evaluation' END 
							,'EV-'+CAST(T1.ev_Count AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "EV-" with ev_Count	
							--,NULL AS ContactLink
							,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
							,X2.LEGACY_ID__C AS [Instructor_Trainer__r:Legacy_ID__c]
							,X2.ADP__C AS [Instructor_Trainer_User__r:ADP__c]
							,NULL AS [String__r:Legacy_ID__c]
							,NULL AS ParentTrainingLink
							,NULL AS Backup_Test__c
							--,NULL AS Pass_Back_Date__c  FB-00933
						--	,NULL AS New_Instructor_Trainer_Link
							,NULL AS Reason_for_Pass_Back__c	
							--,NULL AS NewStringLink 
 					--		,NULL AS  Test_Date__c		--filler FB-00933
							,NULL AS  Tester__c		 --filler
						--	,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler
 							,NULL AS  Traffic_Type__c	 --FB-01712	
							,IIF(T1.ev_Date IS NOT NULL, CAST(T1.ev_Date  AS DATE), CAST(T1.ev_CreatedDate AS DATE)) AS  Date__c
							,NULL AS  Workout_Number__c		
							,NULL   AS  Driver__c					 
							,NULL AS  Acceptable_Response__c		
							,NULL AS  Backup_Training_Required__c									--filler
							,NULL AS Train_Week__c		
								,T1.ev_Float  AS  Float__c			
 								,NULL AS PB__c		
								,NULL AS Current_Instructor__c		
								,NULL AS Current_Weekly_Report__c		
								,NULL AS Hold__c		
								,NULL AS Phase__c		
								,NULL AS Total_Workouts__c		
								,NULL AS This_Weeks_Workouts__c		
								,NULL AS First_Date__c		
								,NULL AS Counter_Con__c		
								,NULL AS Previous_Experience__c		
								,NULL AS CLR__c		
								,NULL AS RG__c		
								,NULL AS Relieving__c		
								,NULL AS Health__c		
								,NULL AS Breeding__c		
								,NULL AS Control__c		
								,NULL AS Temperament__c		
								,'FALSE' AS Body_Sensitivity__c		
								,NULL AS Rough_Food__c		
								,NULL AS [2ndry_R_concerns__c]		
								,NULL AS High_Value__c		
								,NULL AS Train_Notes__c		
								,'FALSE' AS Archive__c		
								,NULL AS Non_Escalator__c		
								,NULL AS Normal_Progress__c	
								--,NULL AS Traffic__c			
							/*	,NULL  AS  City__c		
								,NULL  AS  Revolving_Door__c		
								,NULL  AS  Escalators__c		
								,NULL  AS  Public_Transit__c		
								,NULL  AS  Night_Route__c		
								,NULL  AS  Sidewalkless__c		
								,NULL  AS  Country_Road__c		
								,NULL  AS  Rounded_Corners__c		
								,NULL  AS  Freelance__c		
								,NULL  AS  Bus_Lounge__c		
								,NULL  AS  College_Campus__c		
								,NULL  AS  Underground_Tran__c		
								,NULL  AS  Nursing_Home__c		
								,NULL  AS  Self_Orientation__c		
								,NULL  AS  Wheelchair_Exposure__c		
								,NULL  AS  Kid_Exposure__c		
								,NULL  AS  Additional_SWL__c		
								,NULL  AS  Offset_Crossings__c		
								,NULL  AS  Wide_Crossing__c		
								,NULL  AS  Island_Crossing__c		
								,NULL  AS  Custom_Route__c*/
 							,CAST(T1.ev_IssueDate AS DATE) AS  Issue_Date__c		
							,CAST(T1.ev_Reason   AS NVARCHAR(MAX))   AS  Reason__c		
							,CAST(T1.ev_Gradnotes  AS NVARCHAR(MAX)) AS  Graduation_Notes__c		
							,T1.ev_PRPName  AS  PRP_Name__c		
							,T1.ev_Name  AS  Name__c		
							,CAST(T1.ev_PRPDate AS DATE)  AS  PRP_Date__c		
							,CAST(T1.ev_PRPnotes  AS NVARCHAR(MAX))   AS  PRP_Notes__c		
							,CAST(T1.ev_WorkOut  AS NVARCHAR(MAX))  AS  Workout__c		
							,CAST(T1.ev_SupervisorSummary AS NVARCHAR(MAX)) AS  Summary_Recommendation__C -- FB-01259		
							,CASE  T1.ev_Disp
							    WHEN '1' THEN 'Return to Grad'  
								WHEN '2' THEN 'Reissue'  
								WHEN '3' THEN 'Retire'  
								END AS  Disposition__c		
							--,CAST(T1.ev_DOT AS DATE) AS  Director_of_training__c		FB-01686
							,T1.ev_EvalType   AS Type_of_Evaluation --FB-01251
							,'Evaluation' AS Visit_Category__c

							,CAST(T1.ev_CreatedDate AS DATE) AS CreatedDate
							,'0121L000001UQDbQAO' AS   RecordTypeID	-- Guide Evaluation	
				      	    ,T1.ev_Count AS zrefId
							,'tbl_GuideEval' AS zrefSrc

					FROM GDB_KADE_Final.dbo.tbl_GuideEval AS T1
					LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.ev_FileNum
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.ev_DogId
					LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.ev_Instructor
					LEFT JOIN GDB_FINAL_Migration.dbo.Xtr_user AS X2 ON X2.ADP__C=T1.ev_Instructor




END --tc1: 278344  tC2:283,344  final 256591

BEGIN-- AUDIT
	
	SELECT * FROM GDB_Final_migration.dbo.IMP_TRAINING 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_TRAINING GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	--update legacy id to make unique. 
	UPDATE GDB_Final_migration.dbo.IMP_TRAINING 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy__ID_c] AS NVARCHAR(30))
		WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_TRAINING GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	--dog is required. check for nulls. 

	SELECT * FROM GDB_Final_migration.dbo.IMP_TRAINING 
	WHERE [Dog__r:Legacy__ID_c] IS null


	SELECT zrefSrc, COUNT(*) C
	FROM GDB_Final_migration.dbo.IMP_TRAINING 
	GROUP BY zrefSrc
	ORDER BY zrefSrc

	SELECT * FROM GDB_Final_migration.dbo.IMP_TRAINING ORDER BY  [Dog__r:Legacy__ID_c]
	
	SELECT COUNT(*) FROM GDB_Final_migration.dbo.IMP_TRAINING
	 

END


---exceptions
SELECT t.*
	FROM GDB_FINAL_Migration.dbo.IMP_TRAINING AS T
	LEFT JOIN GDB_FINAL_Migration.dbo.xtr_training AS T2 ON T.Legacy_ID__c=T2.legacy_ID__c
	WHERE t2.id IS null

		 
--create import from [tblTRNTesting]
 		
						
		SELECT  DISTINCT   
				IIF(X2.ID IS NULL, GDB_Final_migration.[dbo].[fnc_OwnerId](),X2.ID) AS OwnerID
		--		CASE WHEN X2.ID IS NOT NULL THEN X2.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
 				,'TDT-'+CAST(T1.tdt_TestingID AS NVARCHAR(30)) AS  Legacy_ID	-- concatenate 'tdt-'+[tdt_TestingId]	
		--		,NULL AS ContactLink
				,T2.dog_DogID  AS  [Dog__r:Legacy__ID_c]		-- Link [tblDog].[dog_DogID]
				,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS [Instructor_Trainer__r:Legacy_ID__c]		-- Link [tblStaff].[FileNum]
				,X2.id AS [Instructor_Trainer_User__C]
				,T5.Legacy_Id__c   AS  [String__r:Legacy_ID__c]   	-- Link [tblClass].[cls_FirstString] 
 				,NULL AS  ParentTrainingLink
				,NULL AS  Backup_Test__c

 		--		,NULL AS Pass_Back_Date__c		FB-00933
		--		,NULL AS New_Instructor_Trainer_Link	
				,NULL AS Reason_for_Pass_Back__c	 
			--	,NULL AS NewStringLink 
 		--	,CAST(T1.tdt_TestDate AS DATE) AS  Test_Date__c		 FB-00933
			,IIF(T1.tdt_Tester ='' OR T1.tdt_Tester IS NULL, NULL, X1.ID)  AS  Tester__c		-- Link [tblStaff].[FileNum]
			--	,T6.tc_CategoryText  AS  Test_Type__c		-- Ref [trefTRNDogTestType].[dt_DogTestID]
				,T1.tdt_Grade  AS  Grade__c		-- Yes/Link?
				,CAST(T1.tdt_Comments  AS NVARCHAR(MAX)) AS  Comments__c		
				,T1.tdt_PrelimGWPace  AS  Preliminary_GW_Pace__c		
				,T1.tdt_FinalGWPace  AS  Final_GW_Pace__c		
				,CAST(T1.tdt_Route AS NVARCHAR(MAX))   AS  Route__c							
				,NULL AS Traffic_Type__c --filler  FB-01712
				,CAST(T1.tdt_TestDate AS DATE) AS Date__c --filler  -FB-00933
				,T1.tdt_WorkoutNumber  AS  Workout_Number__c
				--TRNTestingCategories
				,T7.[2ndry Reinforcement] AS x2ndry_Reinforcement__c
			  ,T7.[Attitude/Rapport] AS Attitude_Rapport__c
				  ,T7.[Body Handling(Head/Mouth/Layover)] AS Body_Handling_Head_Mouth_Layover__c
				  ,T7.[Body Handling(Head/Mouth/Stand/Rollover)] AS Body_Handling_Head_Mouth_Stand_Rollover__c
				  ,T7.[Body Position] AS Body_Position__c
				  ,IIF(T7.[Casual Greeting-Release] IS NOT NULL, T7.[Casual Greeting-Release], T7.[Casual Greeting - Release]) AS Casual_Greeting_Release__c
				  ,T7.[Casual Greeting-Release]
				  ,T7.[Clearances/Drop Offs] AS Clearances_Drop_Offs__c
				  ,T7.[Clearances/Moving Obstacles] AS Clearances_Moving_Obstacles__c
				  ,T7.[Clearances/Obstacles] AS Clearances_Obstacles__c
				  ,T7.[Clearances/Stationary Obstacles] Clearances_Stationary_Obstacles__c
				  ,T7.[Collar Response] AS Collar_Response__c
				  ,T7.[Come Recall] AS Come_Recall__cc
				  ,T7.[Command Response] AS Command_Response__c
				  ,T7.[Crossings] AS Crossings__c
				  ,T7.[Down Curbs] AS Down_Curbs__c
				  ,T7.[Down Stay] AS Down_Stay__c
				  ,T7.[Downs] AS Downs__c
				  ,T7.[Elevator] AS Elevator__c
				  ,T7.[Escalator] AS Escalator__c
				  ,T7.[Focus] AS Focus__c
				  ,T7.[Food Refusal] AS Food_Refusal__c
				  ,T7.[Food Refusal-Food on Ground] AS Food_Refusal_Food_on_Ground__c
				  ,T7.[Food Refusal-Offered by tester] AS Food_Refusal_Offered_by_Tester__c
				  ,T7.[Greeting-Tester and dog] AS Greeting_tester_and_Dog__c
				  ,T7.[Greeting-Tester only (no dog)] AS Greeting_Tester_only_no_dog__c
				  ,T7.[Head Collar] AS Head_Collar__c
				  ,T7.[Heel] AS Heel__c
				  ,T7.[Heel Recall] AS Heel_Recall__c
				  ,T7.[In Harness � Down and Sit] AS In_Harness_Down_Sit__c
				  ,T7.[Informal Come] AS Informal_Come__c
				  ,T7.[Initiative] AS Initiative__c
				  ,T7.[Lead] AS Lead__c
				  ,T7.[Lead in Block] AS Lead_in_Block__c
				  ,T7.[Line] AS Line__c
				  ,T7.[Line in Block] AS Line_in_Block__c
				  ,T7.[Obedience on Route] AS Obedience_on_Route__c
				  ,T7.[Obstacles] AS Obstacles__c
				  ,T7.[Off Leash Down] AS Off_Leash_Down__c
				  ,T7.[Off Leash Recall] AS Off_Leash_recall__c
				  ,T7.[Off Leash Sit] AS Off_Leash_Sit__c
				  ,T7.[Release] AS Release__c
				  ,T7.[Reposition to Heel] AS Reposition_to_Heel__c
				  ,T7.[Roll Over] AS Roll_Over__c
				  ,T7.[Seating Positioning / Collar Response] AS Seating_Positioning_Collar_Response__c
				  ,T7.[Sidewalkless] AS Sidewalkless__c
				  ,T7.[Sit Stay] AS Sit_Stay__c
				  ,T7.[Sits] AS Sits__c
				  ,T7.[Stairs - Down] AS Stairs_Down__c
				  ,T7.[Stairs - Up] AS Stairs_Up__c
				  ,T7.[Stand] AS Stand__c
				  ,T7.[Street Edge] AS Street_edge__c
				  ,T7.[Surfaces] AS Surfaces__c
				  ,T7.[Turns] AS Turns__c
				  ,T7.[Turns - Moving] AS Turns_Moving__c
				  ,T7.[Turns -Stationary] AS Turns_Stationary__c
				  ,T7.[Up Curbs] AS Up_Curbs__c
						
						 	,CAST(T1.tdt_CreatedDate AS DATE) AS  CreatedDate		
						--	,CASE''  AS  RecordTypeID	-- Testing --FB-01318
									/*Test Type = Record Type Name
									Prelim Obedience = Prelim OB
									Prelim Guidework = Prelim GW
									Final obedience = Final OB
									Final Guidework = Final GW
									Final Building = Final BLD
									Sidewalkless training = Sidewalkless*/
						,CASE  T1.tdt_TestType
							    WHEN '1' THEN '0121L000001UQDeQAO'  --	Prelim OB
 								WHEN '2' THEN '0121L000001UQDdQAO'	--	Prelim GW
  								WHEN '3' THEN '0121L000001UQDaQAO' --	Final OB
								WHEN '4' THEN '0121L000001UQDZQA4'	--	Final GW
								WHEN '5' THEN '0121L000001UQDYQA4'	--	Final BLD
								WHEN '6' THEN '0121L000001UQDhQAO'	--	Traffic
								WHEN '7' THEN '0121L000001UQDfQAO'	--	Sidewalkless
								END AS RecordTypeID
 							,T1.tdt_TestingID AS zrefId
							,'4-tblTRNTesting' AS zrefSrc	
						INTO GDB_FINAL_Migration.dbo.IMP_TRAINING_trntesting
						FROM GDB_KADE_Final.dbo.tblTRNTesting AS T1 
						LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.tdt_DogID
						LEFT JOIN GDB_KADE_Final.dbo.tblStaff AS T3 ON T3.FileNum=T1.tdt_Instr
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.tdt_Tester
						--camp  
						LEFT JOIN GDB_KADE_Final.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.tdt_TestingString
						LEFT JOIN GDB_Final_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.Legacy_Id__c=('cls-'+T4.cls_FirstString )
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X2 ON X2.ADP__C=T1.tdt_Instr
						LEFT JOIN GDB_KADE_Final.dbo.[trefTRNDogTestType] AS T6 ON T1.tdt_TestType=T6.dt_dogtestid
						left join GDB_FINAL_Migration.dbo.stg_Training_Test_Cat1 AS T7 ON T1.tdt_TestingID = T7.ttc_TestingID
					--	WHERE T1.tdt_TestingID='11378' --27420 to audit
					-- final 35554


					--dup checking

						
	SELECT * FROM GDB_Final_migration.dbo.IMP_TRAINING_trntesting
	WHERE Legacy_ID IN (SELECT Legacy_ID FROM GDB_Final_migration.dbo.IMP_TRAINING_trntesting GROUP BY Legacy_ID HAVING COUNT(*)>1)
	ORDER BY Legacy_ID


		SELECT * FROM GDB_FINAL_Migration.dbo.IMP_TRAINING_trntesting
		WHERE Legacy_ID='TDT-27420'

		SELECT T.Legacy_ID, T.[Instructor_Trainer__r:Legacy_ID__c]
		, T.[Instructor_Trainer_User__C], T.OwnerID
		--,t.[String__r:Legacy_ID__c]
				 FROM GDB_FINAL_Migration.dbo.IMP_TRAINING_trntesting AS T
				-- WHERE T.Instructor_Trainer_User__C='0051L0000090o16QAA'
				
	SELECT T.Legacy_ID__c, T.[Instructor_Trainer__r:Legacy_ID__c]
		, T.[Instructor_Trainer_User__r:ADP__c], T.OwnerID
		
				 FROM GDB_FINAL_Migration.dbo.IMP_TRAINING AS T
				
				

		--update ParentTraining
BEGIN

SELECT CAST(T.Legacy_ID__c AS VARCHAR(30)) AS Legacy_ID__c, CAST(t.ParentTrainingLink AS VARCHAR(30)) AS [Parent__r:Legacy_ID__c]
FROM GDB_Final_migration.dbo.IMP_TRAINING AS T
WHERE t.ParentTrainingLink IS NOT NULL
UNION
SELECT CAST(T.Legacy_ID AS VARCHAR(30)) AS Legacy_ID__c, CAST(t.ParentTrainingLink AS VARCHAR(30)) AS [Parent__r:Legacy_ID__c]
FROM GDB_Final_migration.dbo.IMP_TRAINING_trntesting AS T
WHERE t.ParentTrainingLink IS NOT NULL

END 