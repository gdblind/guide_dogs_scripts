USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblVBPayments
			WHERE   SF_Object LIKE '%rec%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
	SELECT * FROM GDB_Final_migration.dbo.XTR_RECORDTYPEs WHERE SOBJECTTYPE LIKE '%vet%'
	
	0121L000001UQDtQAO	GDB
	0121L000001UQDuQAO	GDB - Locked
	0121L000001UQDvQAO	GDB Quick Vet Note
	0121L000001UQDwQAO	Outside Vet

*/

BEGIN -- DROP IMP
	DROP TABLE GDB_Final_migration.DBO.IMP_VET_RECORD
	DROP TABLE GDB_Final_migration.DBO.IMP_VET_RECORD_srt
END 

BEGIN -- CREATE IMP 
					SELECT   DISTINCT 
							IIF(X1.id IS NOT NULL, X1.id,GDB_Final_migration.[dbo].[fnc_OwnerId]()) AS OwnerId
							--IIF(T1.vdl_VetID IS NULL, GDB_Final_migration.[dbo].[fnc_OwnerId](), X1.ID ) AS OwnerId --FB-00933
						--	,X1.ID AS Doctor__c --FB-01015
							--tblVetEntry
							,X1.ADP__C AS zrefADP
							,T13.FileNum AS zrefFilNum
							,T1.vdl_VetID AS zrefVetID
							,CASE WHEN T10.zrefSeq>1 THEN ('VDL-'+CAST(T1.vdl_VetEntryID AS NVARCHAR(30)) +'-'+CAST(T10.zrefSeq AS NVARCHAR(1)))
								ELSE 'VDL-'+CAST(T1.vdl_VetEntryID AS NVARCHAR(30)) END AS  Legacy_Id__c	-- Concatenate "VDL-" with vdl_VetEntryID	
							,T4.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tbDog].[dog_DogID]
							,CAST(T1.vdl_Date  AS DATE) AS  Date__c		
							,CAST(T1.vdl_TreatmentDate AS DATE) AS  Treatment_Date__c		
							,LEFT(CASE WHEN T1.vdl_Complaint IS NULL THEN T5.VetSourceDesc ELSE T1.vdl_Complaint END, 80)  AS  [Name]	
							,T5.VetSourceDesc AS  Source__c	-- migrate value from [VetSourceDesc]	-- Link/trefVetSource.VetSourceID
							,T6.[Description]  AS  Facility__c	-- migrate value from [Description]	-- Yes/Link [trefVetFacility].[VetFacilityID]
							--,X1.ID AS Provider__c
							,T13.[Vet Type] AS Vet_Type__c
							,IIF(T13.[Vet Type]='GDB Veterinarian',X1.ID,'') AS DVM__C
 							,LTRIM((CASE WHEN T7.VetFirstName IS NOT NULL THEN COALESCE(T7.VetFirstName, '') ELSE '' END + ' ' +
							+ CASE WHEN T7.VetLastName IS NOT NULL THEN COALESCE(T7.VetLastName, '') ELSE '' END)) AS Vet_Type_Detail_c	  	-- migrate concatenation of [VetFirstName] +" "+[VetLastName]	-- Yes/Link trefVet.VetID
 							,CAST(T1.vdl_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,T1.vdl_DonatedBy  AS  Donated_By__c		
							,T1.vdl_PaidTo  AS  Paid_To__c		
							,CAST(T1.vdl_CreatedDate  AS DATE) AS  CreatedDate		
							,CAST(T1.vdl_DatePaid  AS DATE) AS  Date_Paid__c		
							--,CASE WHEN T3.vpe_VetEntryID IS NOT NULL THEN '0123D0000004dsPQAQ'  ELSE '0123D0000004dsKQAQ' END  AS  RecordTypeId	-- If linked to [tblVetPE] then 'Physical Exam' else 'Dog Concern'	
							,CASE WHEN T1.vdl_VetID = '9' OR T1.vdl_Source = '5' THEN '0121L000001UQDwQAO' ELSE '0121L000001UQDuQAO' END AS RecordTypeID
							,CASE WHEN T1.vdl_VetID = '9' OR T1.vdl_Source = '5' THEN 'FALSE' ELSE 'TRUE' END AS Locked__C		--meeting with Val
							--tblVetPE		--update labels per FB-00631
							--,T3.vpe_TC  AS  TC__c			--FB-00631
							--,T3.vpe_TCR  AS  TCR__c		--FB-00631
							,T3.vpe_GA  AS  GA__c
							,T3.vpe_GAAbFind  AS  GA_Findings__c
							,T3.vpe_Eyes  AS  Eyes__c
							,T3.vpe_EyesAbFind  AS  Eyes_Findings__c
							,T3.vpe_Oral  AS  Oral__c
							,T3.vpe_OralAbFind  AS  Oral_Findings__c
							,T3.vpe_MucousMemb  AS  Mucous_Membrane__c
							,T3.vpe_MucousMembAbFind  AS  Mucous_Membrane_Findings__c
							,T3.vpe_Ears  AS  Ears__c
							,T3.vpe_EarsAbFind  AS  Ears_Findings__c
							,T3.vpe_Integ  AS  Integ__c
							,T3.vpe_IntegAbFind  AS  Integl_Findings__c
							,T3.vpe_Abdomen  AS  Abdomen__c
							,T3.vpe_AbdomenAbFind  AS  Abdomen_Findings__c
							,T3.vpe_UrogenitAL  AS  Urogenital__c
							,T3.vpe_UrogenitALAbFind  AS  Urogenital_Findings__c
							,T3.vpe_HeartLungs  AS  Heart_and_Lungs__c
							,T3.vpe_HeartLungsAbFind  AS  Heart_and_Lungs_Findings__c
							,T3.vpe_LymphNodes  AS  Lymph_Nodes__c
							,T3.vpe_LymphNodesAbFind  AS  Lymph_Nodes_Findings__c
							,T3.vpe_MusSkel  AS  Musculo_Skeletal__c
							,T3.vpe_MusSkelAbFind  AS  Musculo_Skeletal_Findings__c
							,T3.vpe_NeuSys  AS  Neural_Systems__c
							,T3.vpe_NeuSysAbFind  AS  Neural_Systems_Findings__c
							,T3.vpe_Pulse  AS  Pulse__c
							,T3.vpe_Respiration  AS  Respiration__c
							,T3.vpe_Temp  AS  Temp__c
							,IIF(T3.vpe_Weight1='Not Checked',NULL,T3.vpe_Weight1) AS  Weight__c
							,IIF (T3.vpe_Height='Not Checked',NULL, T3.vpe_Height) AS Height_inches__c
							,T3.vpe_BodyConditionScore  AS  Body_Condition_Score__c
							,IIF(T3.vpe_Weight1='Not Checked','Not Checked',T3.vpe_WeightUnit) AS Weight_Unit__c --FB-00974
							--tblVetLabs
							,CAST(T8.vlb_Labs AS NVARCHAR(MAX)) AS Labs__c
							,T14.Total_Amount__c

							--tblVetSOAP
							,CAST(T9.vsp_Subject  AS NVARCHAR(MAX))  AS  Subjective__c
							,CAST(T9.vsp_Objective  AS NVARCHAR(MAX))  AS  Objective__c
							,CAST(T9.vsp_Assessment  AS NVARCHAR(MAX))  AS  Assessment__c
							,CAST(T9.vsp_Plan  AS NVARCHAR(MAX))  AS  Plan__c

							--tblVBPayments
		 					,T10.Reference_No__c		
							,T10.[Vendor__r:Legacy_Id__c]	-- concatenate any records that are one to many	-- Yes/Link [TblVendor]
						 	,T10.Batch_ID__c		
							,T10.Transaction_Number__c		
							
							--tblOutsideVet
							,'ov-' +CAST(T12.OutsideVetCode AS NVARCHAR(20)) AS [Outside_Veterinary__r:Legacy_Id__c]   --link [trefOutsidevet].[OutsideVetcode]

 							--tblSupportCtrSurvey
							,CAST(CAST(T2.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T2.scs_SurveyOffered AS Survey_Summary__c
										
							
							--ZREF
							,T1.vdl_VetEntryID AS zrefVetEntryId
							,T10.zrefSeq
							--INTO GDB_Final_migration.DBO.IMP_VET_RECORD
							FROM GDB_KADE_Final.dbo.tblVetEntry AS T1
							LEFT JOIN (SELECT * FROM GDB_KADE_Final.DBO.tblSupportCtrSurvey WHERE scs_FormID=3) AS T2 ON T2.scs_FormRecID=T1.vdl_VetEntryID
							LEFT JOIN GDB_KADE_Final.dbo.tblVetPE_ AS T3 ON T3.vpe_VetEntryID=T1.vdl_VetEntryID AND T3.vpe_DogID=T1.vdl_DogID
							LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T4 ON T4.dog_DogID=T1.vdl_DogID
							LEFT JOIN GDB_KADE_Final.dbo.trefVetSource AS T5 ON T5.VetSourceID=T1.vdl_Source
							LEFT JOIN GDB_KADE_Final.dbo.trefVetFacility AS T6 ON T6.VetFacilityID=T1.vdl_Facility
							LEFT JOIN GDB_KADE_Final.dbo.trefVet AS T7 ON T7.VetID = T1.vdl_VetID
							LEFT JOIN GDB_KADE_Final.dbo.tblVetLabs AS T8 ON T8.vlb_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_KADE_Final.dbo.tblVetSOAP AS T9 ON T9.vsp_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_Final_migration.dbo.stg_tblVBPayments_1 AS T10 ON T10.vbi_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_KADE_Final.dbo.tblOutsideVet AS T11 ON T11.ov_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_KADE_Final.dbo.trefOutsidevet AS T12 ON T12.OutsideVetCode=T11.ov_OutsideVetCode
							LEFT JOIN GDB_Final_migration.dbo.Chart_trefVet AS T13 ON T1.vdl_VetID=t13.VetID
							LEFT JOIN GDB_Final_migration.dbo.XTR_User AS X1 ON X1.ADP__C=T13.FileNum
							LEFT JOIN GDB_Final_migration.dbo.stg_Vet_Proc_TotalAmount AS T14 ON T1.vdl_VetEntryID=T14.vdp_VetEntryID
							 
							--TEST WHERE T1.vdl_VetEntryID='934141' OR T1.vdl_VetEntryID='729587'

							 
END --tc1: 1200136 TC2: 1,229,059 final 1,278,128
 
  
BEGIN-- AUDIT

			SELECT ROW_NUMBER() OVER(ORDER BY [Dog__r:Legacy_Id__c], Legacy_Id__c) AS zrefSrtId
				   ,*
			INTO GDB_Final_migration.dbo.IMP_VET_RECORD_srt    
 			FROM GDB_Final_migration.dbo.IMP_VET_RECORD
	
			SELECT * FROM GDB_Final_migration.dbo.IMP_VET_RECORD 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_VET_RECORD GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
  		    --0
		 
		 --create imps.--this is to be able to export direclty from SQL results to preserve double quotes. 
		 SELECT * FROM GDB_Final_migration.dbo.IMP_VET_RECORD_srt 
		 WHERE zrefSrtId <600001
		 ORDER BY zrefSrtId

		 SELECT * FROM GDB_Final_migration.dbo.IMP_VET_RECORD_srt 
		 WHERE zrefSrtId >600000
		 ORDER BY zrefSrtId

END 


SELECT COUNT(*) FROM GDB_Final_migration.DBO.IMP_VET_RECORD
SELECT COUNT(*) FROM GDB_Final_migration.DBO.IMP_VET_RECORD_srt

--exceptions

SELECT t.*
FROM GDB_Final_migration.DBO.IMP_VET_RECORD_srt AS T
 WHERE T.Legacy_Id__c NOT IN (SELECT t2.legacy_ID__c FROM  GDB_FINAL_Migration.dbo.Xtr_VetRecord AS T2)-- ON T2.LEGACY_ID__C = T.Legacy_Id__c
ORDER BY T.zrefSrtId





























