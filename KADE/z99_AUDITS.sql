-- secondary contacts with 'and' OR '&'

	SELECT CASE WHEN psn_First LIKE  '% & %'  THEN SUBSTRING(psn_First, 1, CHARINDEX('&', psn_First)-2 ) 
				WHEN AS firstname
		,SUBSTRING(psn_First, CHARINDEX('&', psn_First)+2, 255)  AS secpndarycontactname
	,* FROM GDB_KADE_Final.dbo.tblPerson
	WHERE psn_First LIKE  '% & %' 
	 
	SELECT   SUBSTRING(psn_First, 1, CHARINDEX(' and ', psn_First)-1) [prifirstname]
 			,SUBSTRING(psn_First, CHARINDEX(' and ', psn_First)+5, 255)  AS secpndarycontactname
	,* FROM GDB_KADE_Final.dbo.tblPerson
	WHERE psn_First LIKE  '% and %' 
	

--audit household accounts and organization accounts. 
		SELECT COUNT(*) FROM GDB_KADE_Final.DBO.tblPerson  --79074
		
		SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, T1.zrefFirstName, t1.zrefSecFirstName
		,CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
		,T2.* 
		FROM GDB_KADE_Final.dbo.tblPerson AS T1
		LEFT JOIN GDB_KADE_Final.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
		--WHERE t1.psn_PersonID='22463'
		--WHERE T2.pg_PersonID IS   NULL
		--WHERE T2.pg_GroupID IS NULL
		--WHERE T1.psn_PersonID='58' OR T1.psn_PersonID='12209' OR t1.psn_PersonID='22463' OR T2.pg_GroupID='22463' OR T1.psn_PersonID='10' 
			--OR T1.psn_PersonID='63822' OR T1.psn_PersonID='63821' OR t1.psn_PersonID='2610' OR T1.psn_PersonID='5729'
			--OR t2.pg_GroupID='52475' OR t2.pg_GroupID='5729'
		 --WHERE psn_First LIKE  '% & %' OR  psn_First LIKE '% and %'
		 WHERE  ( T2.pg_GroupID='5677'
				OR T2.pg_GroupID='14760'
				OR T2.pg_GroupID='15256'
				OR T2.pg_GroupID='35035'
				OR T2.pg_GroupID='67878'
				OR T2.pg_GroupID='79709')  
		ORDER BY T2.pg_GroupID, T1.psn_PersonID


--
		SELECT  T1.* 
		 FROM GDB_KADE_Final.dbo.tblPersonGroup AS T1
		LEFT JOIN GDB_KADE_Final.DBO.tblPerson AS T2 ON T1.pg_GroupID=T2.psn_PersonID
		WHERE t2.psn_PersonID IS NOT NULL  
		ORDER BY t1.pg_GroupID, t1.pg_PersonID
		
		 
	SELECT * FROM GDB_KADE_Final.dbo.tblPerson WHERE psn_PersonID='82540' OR psn_PersonID='82540' OR psn_PersonId='64382'

	SELECT * FROM GDB_KADE_Final.dbo.tblPersonGroup WHERE pg_GroupID='64380'

				zrefAccoundId
				45657
				45841
				46939
				47580
				47980
				53153
				53538
				56682
				57596
				59614
				60651
				62254
				64578
				64725
				64757
				65082

--preferred phones
	SELECT  FROM GDB_Final_migration.dbo.stg_Phone 
	WHERE pdl_PrimaryContact =1


--tblTRNEvalApprInstr
			SELECT 
			t1.aie_Class
				 
				,t7.ecc_Class
				,CASE WHEN t1.aie_Class = t7.ecc_Class THEN 'true' ELSE 'FALSE' END AS test
			FROM GDB_KADE_Final.dbo.tblTRNEvalApprInstr AS T1
			LEFT JOIN GDB_KADE_Final.dbo.tblTRNEvalClassClientSkills AS T7 ON T7.ecc_AIEval=T1.aie_AIEval
			WHERE t7.ecc_Class IS NOT NULL 
			ORDER BY test 

--test source control w/git