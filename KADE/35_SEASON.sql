USE GDB_Final_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblCWTPostWhelp
			WHERE   SF_Object LIKE '%bree%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblDogBreeding 
			WHERE  SF_Object_2 LIKE '%bre%'
END 
 
 
/*

SELECT * FROM GDB_Final_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%BREE%'
 

*/
BEGIN -- DROP IMP

	DROP TABLE GDB_Final_migration.DBO.IMP_SEASON

END 

BEGIN

UPDATE GDB_KADE_Final.dbo.tblCWTPostWhelp
			SET pw_ReportedBy ='Null'
			WHERE pw_ReportedBy IS NULL OR pw_ReportedBy=''

END


BEGIN -- CREATE IMP 

					SELECT   GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 							,'DBG-'+CAST(T1.dbg_BreedingID AS NVARCHAR(30)) AS  Legacy_Id__c		
							,T2.dog_DogID AS  [Bitch__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,T3.dog_DogID AS  [Stud__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,T5.dog_DogID AS [Stud_2__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,CAST(T1.dbg_DateSeason AS DATE) AS  Date_Season__c		
							,CAST(T1.dbg_DateSeasonEnd AS DATE) AS  Date_Season_End__c		
							,CASE T1.dbg_WNL WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  WNL__c		
							--,CASE T1.dbg_SkipMating WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Skip_Mating__c	--FB_0976
							,CASE T1.dbg_SkipMating WHEN '1' THEN 'Skip' ELSE '' END  AS  Skip_Breed__c	--FB-00976
							,CAST(T1.dbg_LastUltraDate  AS DATE) AS  Last_Ultra_Date__c		
							,CASE T1.dbg_LitterConceived WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Litter_Conceived__c		
							,CAST(T1.dbg_SeasonComments AS NVARCHAR(MAX)) AS  Season_Comments__c		
							,CAST(T1.dbg_KennelComments AS NVARCHAR(MAX)) AS  Kennel_Comments__c		
							,CASE WHEN T1.dbg_IR IS NULL THEN 'No' ELSE T1.dbg_IR END  AS  IR__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_SLSDiet  IS NULL THEN 'No' ELSE  T1.dbg_SLSDiet END  AS  SLS_Diet__c	-- if blank migrate as "No"	
							,T1.dbg_VetNotification  AS  Vet_Notification__c		
							,CASE WHEN T1.dbg_SeasonType IS NULL THEN 'No' ELSE  T1.dbg_SeasonType END  AS  Season_Type__c	-- if blank migrate as "No"	
							,CAST(T1.dbg_CreatedDate AS DATE) AS  CreatedDate	
 							,NULL AS  Whelp_Date__c		
							,NULL AS  Litter_Info__c		
							,NULL AS  Weight__c		
							,NULL AS  Weight_Date__c		
							,NULL AS  Body_Condition__c		
							,NULL AS  Heartguard_Given__c		
							,NULL AS  BSC_Date__c		
							,NULL AS  Food_Type__c		
							,NULL AS  Food_Amount__c		
							,NULL AS  Vacs__c		
							--,NULL AS  Vaccine_Date__c		
							--,NULL AS  Vaccine_Due__c		
							,NULL AS  Current_Treatment_Diagnosis__c	

							-- added per FB-00006
							,CASE T1.dbg_DonatedLitterUtero WHEN 1 THEN 'TRUE'  ELSE 'FALSE' END  AS  Donated_Litter_Utero__c		
							,CAST(T1.dbg_DateDue AS DATE)  AS  Date_Due__c		
							,CAST(T1.dbg_DateWhelp   AS DATE) AS  Date_Whelp__c		
							,CAST(T1.dbg_FstPupWhelp  AS DATE) AS  First_Pup_Whelp__c		
							,CAST(T1.dbg_LstPupWhelp   AS DATE)AS  Last_Pup_Whelp__c		
							,CAST(T1.dbg_WhelpingComments AS NVARCHAR(MAX))  AS  Whelping_Comments__c		
							,CASE WHEN T1.dbg_PullEarly IS NULL THEN 'No' ELSE T1.dbg_PullEarly  END  AS  Pull_Early__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_RetainedPlacentas IS NULL THEN 'No' ELSE T1.dbg_RetainedPlacentas  END  AS  Retained_Placentas__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ExcessiveBleeding IS NULL THEN 'No' ELSE T1.dbg_ExcessiveBleeding  END  AS  Excessive_Bleeding__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_NonSequentialP4 IS NULL THEN 'No' ELSE T1.dbg_NonSequentialP4  END  AS  Non_Sequential_P4__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_MultipleLH IS NULL THEN 'No' ELSE T1.dbg_MultipleLH  END  AS  Multiple_LH__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_QuickRise IS NULL THEN 'No' ELSE T1.dbg_QuickRise  END  AS  Quick_Rise__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Dystocia IS NULL THEN 'No' ELSE T1.dbg_Dystocia  END  AS  Dystocia__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_UterineTorsion IS NULL THEN 'No' ELSE T1.dbg_UterineTorsion  END  AS  Uterine_Torsion__c	-- if blank migrate as "No"	
							,T1.dbg_UltraSound  AS  Ultra_Sound__c		
							,CASE WHEN T1.dbg_ResorptionSite IS NULL THEN 'No' ELSE T1.dbg_ResorptionSite  END  AS  Resorption_Site__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ResorptionNumber IS NULL THEN 'No' ELSE T1.dbg_ResorptionNumber  END  AS  Resorption_Number__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_ResorptionLocation IS NULL THEN 'No' ELSE T1.dbg_ResorptionLocation  END  AS  Resorption_Location__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_LitterSize IS NULL THEN 'No' ELSE T1.dbg_LitterSize  END  AS  Litter_Size__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Natural IS NULL THEN 'No' ELSE T1.dbg_Natural  END  AS  Natural__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Invasive IS NULL THEN 'No' ELSE T1.dbg_Invasive  END  AS  Invasive__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Malposition IS NULL THEN 'No' ELSE T1.dbg_Malposition  END  AS  Malposition__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_Medical IS NULL THEN 'No' ELSE T1.dbg_Medical  END  AS  Medical__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_CSection IS NULL THEN 'No' ELSE T1.dbg_CSection  END  AS  CSection__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_PUI IS NULL THEN 'No' ELSE T1.dbg_PUI  END  AS  PUI__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_SUI IS NULL THEN 'No' ELSE T1.dbg_SUI  END  AS  SUI__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_UterineMonitoring IS NULL THEN 'No' ELSE T1.dbg_UterineMonitoring  END  AS  Uterine_Monitoring__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_FetalMonitoring IS NULL THEN 'No' ELSE T1.dbg_FetalMonitoring  END  AS  Fetal_Monitoring__c	-- if blank migrate as "No"	
						--	,T1.dbg_WhelpLenHours  AS  Whelp_Len_Hours__c		--fb-02296
							,T1.dbg_Stage2LaborLen  AS  Stage_to_Labor_Len__c		
						--	,T1.dbg_FTBT  AS  FTBT__c		--DNC per FB-00006 and FB-00558
						--	,CAST(T1.dbg_FTBTStart  AS DATE) AS  FTBT_Start__c		--DNC per FB-00006 and FB-00558
						--	,CAST(T1.dbg_HC AS DATE) AS  HC__c		--DNC per FB-00006 and FB-00558
						--	,CAST(T1.dbg_LP AS DATE) AS  LP__c		--DNC per FB-00006 and FB-00558
							,T1.dbg_Comp  AS  Comp__c								
							--reference
							,T1.dbg_BreedingID AS zrefID
							,'tblDogBreeding' AS zrefSrc
					
					INTO GDB_Final_migration.DBO.IMP_SEASON
					FROM GDB_KADE_Final.dbo.tblDogBreeding AS T1
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.dbg_BitchID
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T3 ON T3.dog_DogID=T1.dbg_StudID
					LEFT JOIN GDB_KADE_Final.dbo.tblMultiSireLitter AS T4 ON T4.msl_LitterID=T1.dbg_BreedingID
					LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T5 ON T5.dog_DogID=T4.msl_StudID

				
				UNION
					SELECT   DISTINCT CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_Final_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblStaff].[FileNum]
					 		,'PW-'+CAST(T1.pw_RecID AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Pw-'+[pw_RecID]	
 							,T2.dog_DogID AS  [Bitch__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
 							--filler
								,NULL AS [Stud__r:Legacy_Id__c]
								,NULL AS [Stud_2__r:Legacy_Id__c]
 								,NULL  AS  Date_Season__c		
								,NULL  AS  Date_Season_End__c		
								,NULL  AS  WNL__c		
							--	,NULL  Skip_Mating__c
								,'Null' AS  Skip_Breed__c		
								,NULL  Last_Ultra_Date__c		
								,NULL  Litter_Conceived__c		
								,NULL  Season_Comments__c		
								,NULL  Kennel_Comments__c		
								,NULL  IR__c	 
								,NULL  SLS_Diet__c	 
								,NULL  Vet_Notification__c		
								,NULL  Season_Type__c	
							,CAST(T1.pw_CreatedDate AS DATE) AS CreatedDate 
	 						,CAST(T1.pw_WhelpDate AS DATE) AS  Whelp_Date__c		
							,T1.pw_LitterInfo  AS  Litter_Info__c		
							,T1.pw_Weight  AS  Weight__c		
							,CAST(T1.pw_WeightDate AS DATE) AS  Weight_Date__c		
							,T1.pw_BodyCondition  AS  Body_Condition__c		
							,CAST(T1.pw_HeartguardGiven  AS DATE) AS  Heartguard_Given__c		
							,CAST(T1.pw_BCSDate AS DATE)  AS  BSC_Date__c		
							,X2.[ID]  AS  Food_Type__c		
							,T1.pw_FoodAmount  AS  Food_Amount__c		
							,T1.pw_Vacs  AS  Vacs__c		
							--,T1.pw_VaccDate  AS  Vaccine_Date__c		
							--,T1.pw_VaccDue  AS  Vaccine_Due__c		
							,T1.pw_CurrTreatDiag  AS  Current_Treatment_Diagnosis__c	
							
							,NULL AS  Donated_Litter_Utero__c		
							,NULL AS  Date_Due__c		
							,NULL AS  Date_Whelp__c		
							,NULL AS  First_Pup_Whelp__c		
							,NULL AS  Last_Pup_Whelp__c		
							,NULL AS  Whelping_Comments__c		
							,NULL AS  Pull_Early__c	
							,NULL AS  Retained_Placentas__c	
							,NULL AS  Excessive_Bleeding__c		
							,NULL AS  Non_Sequential_P4__c	
							,NULL AS  Multiple_LH__c	
							,NULL AS  Quick_Rise__c	
							,NULL AS  Dystocia__c	
							,NULL AS  Uterine_Torsion__c	
							,NULL AS  Ultra_Sound__c		
							,NULL AS  Resorption_Site__c	
							,NULL AS  Resorption_Number__c		
							,NULL AS  Resorption_Location__c	
							,NULL AS  Litter_Size__c		
							,NULL AS  Natural__c	
							,NULL AS  Invasive__c		
							,NULL AS  Malposition__c		
							,NULL AS  Medical__c	
							,NULL AS  CSection__c	
							,NULL AS  PUI__c	
							,NULL AS  SUI__c	
							,NULL AS  Uterine_Monitoring__c		
							,NULL AS  Fetal_Monitoring__c		
						--	,NULL AS  Whelp_Len_Hours__c		
							,NULL AS  Stage_to_Labor_Len__c		
							,NULL AS  Comp__c		
 							--reference
							,T1.pw_RecID AS zrefID
							,'tblCWTPostWhelp' AS zrefSrc
						FROM GDB_KADE_Final.dbo.tblCWTPostWhelp AS T1
						LEFT JOIN GDB_KADE_Final.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pw_DogID
						LEFT JOIN GDB_Final_migration.dbo.XTR_USER AS X1 ON X1.ADP__C=T1.pw_ReportedBy
						LEFT JOIN (SELECT * FROM GDB_Final_migration.dbo.XTR_Inventory WHERE SOURCE_TABLE__C='tblCWTPostWhelp') AS X2 ON x2.NAME1 = T1.pw_foodType

					
							
					

					--final 284	 
					--final 10878

END 

BEGIN-- AUDIT

 
	SELECT * FROM GDB_Final_migration.dbo.IMP_SEASON
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_SEASON GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_Id__c

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_SEASON 
			GROUP BY zrefSrc
		 
END 
 

























