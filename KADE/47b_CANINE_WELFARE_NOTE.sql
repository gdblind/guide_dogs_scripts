BEGIN
    DROP TABLE GDB_Final_migration.dbo.IMP_CANINE_WELFARE

END

BEGIN

		   SELECT  
				GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				,t1.dkn_DogKennelNotesID AS Legacy_id__c
				,T1.dkn_DogID AS [Dog__r:Legacy_id__c]  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				--,Title = 'Canine Welfare Notes: '+  CAST(CAST(T1.dkn_CreatedDate AS  DATE) AS NVARCHAR(30))   
				,RecordTypeID='0123D0000005PZlQAM'
				,Notes__c = CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))
				,Start_Date__c = T1.dkn_createdDate
				,CreatedDate = T1.dkn_CreatedDate
				,T1.dkn_DogKennelNotesID AS zrefLegacyId
				,'tblDogKennelNotes' zrefSrc
			INTO GDB_Final_migration.dbo.IMP_CANINE_WELFARE
			FROM GDB_KADE_Final.dbo.tblDogKennelNotes AS T1
			WHERE LEN(CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))) >0

END

BEGIN
SELECT * FROM GDB_Final_migration.dbo.IMP_CANINE_WELFARE
END