USE GDB_Final_migration
GO
 
BEGIN--Maps   
  			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblGDBInvDispenseTrans
			WHERE   SF_Object LIKE '%ent%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_Final_migration.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN --drop IMP INVENTORY
	DROP TABLE  GDB_Final_migration.dbo.IMP_INVENTORY
END 

BEGIN--INVENTORY

		--tblVolEquipment	
			SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			 ,'vps-'+ vps_Item AS Legacy_ID__c 
			,vps_Item AS [Name] 
			,'Equipment'  AS  Classification__c	 
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,'Yes'  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,'Both'  AS  Campus__c	 
		 	,'TRN'  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,'Unit'  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,'Unit'  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,'No'  AS  Maintain_QOH__c
 			,NULL  AS  Initial_Inventory_Count__c
			  ,null as Legacy_QOH__c
			,'tblVolEquipment' AS Source_Table__c
			,NULL AS zrefItemDetailID
			,NULL AS zrefItemID
			,NULL AS zrefCampus

		 	INTO GDB_Final_migration.dbo.IMP_INVENTORY  --drop table GDB_Final_migration.dbo.IMP_INVENTORY
			FROM GDB_KADE_Final.dbo.tblVolEquipment
			--TC2: 3
		UNION 
		--stg_tblEquipFood
			SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'deq-'+T2.Item_Name AS Legacy_ID__c
			,T2.Item_Name AS [Name] 
			,T2.[Classifaction]  AS  Classification__c	 
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,'Yes'  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,'Both'  AS  Campus__c	 
		 	,'TRN'  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,'Unit'  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,'Unit'  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,'No'  AS  Maintain_QOH__c
 			,NULL  AS  Initial_Inventory_Count__c	
			,null as Legacy_QOH__c		
			,'stg_tblEquipFood' AS Source_Table__c
			,NULL AS zrefItemDetailID
			,NULL AS zrefItemID
			,NULL AS zrefCampus
			
			FROM GDB_Final_migration.dbo.stg_tblEquipFood AS T1
			LEFT JOIN GDB_Final_migration.dbo.CHART_EquipFood  AS T2 ON T1.deq_EquipFood =T2.deq_EquipFood 
																AND T1.deq_Type=T2.deq_Type 
																AND T1.deq_Size=T2.deq_Size
			WHERE  T2.Item_Name IS NOT NULL 
	 	UNION
			-- trefGDBInventroyItems +trefGDBInvItemsDetail


		SELECT DISTINCT  --NON-DUPLICATE RECORDS ON [tblGDBInventroyItems].[ii_ItemID]
			T1.[OwnerId]
		   ,T1.[Legacy_ID__c]
		   ,T1.[Name__c] AS [Name]
		   ,T1.[Classification__c]
		   ,T1.[Category__c]
		   ,T1.[Controlled_Item__c]
		   ,'No' as [Active__c]
		   ,T1.[OrderFromAcctLink]
		  ,T1.[Campus__c]
		  ,T1.[Department_Code__c]
		  ,T1.[Status__c]
		  ,T1.[Buy_Ratio__c]
		  ,T1.[Buy_UOM__c]
		  ,T1.[Sell_Ratio__c]
		  ,T1.[Sell_UOM__c]
		  ,T1.[ReOrder_Point__c]
		  ,T1.[ReOrder_Qty__c]
		  ,T1.[Overstock_Point__c]
		  ,T1.[Required_Qty__c]
		  ,T1.[Maintain_QOH__c]
		  ,null as [Initial_Inventory_Count__c]	--FB-02472
		  ,T1.[Initial_Inventory_Count__c] as Legacy_QOH__c
		  ,t1.over_the_counter__c
		--  ,[zrefActive]
		  ,'Legacy_'+T1.[zrefSrc] AS Source_Table__c --FB-01757
		  ,T1.zrefItemDetailID
		  ,T1.zrefItemID
		  ,t1.zrefCampus
		  
		FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1   

	 

			/*	SELECT -- not Vet dups
					T1.[OwnerId]
				   ,T1.[Legacy_ID__c] +'_'+LEFT([T1].Department_Code__c,1) AS Legacy_ID__c
				   ,T1.[Name__c] AS [Name]
				   ,T1.[Classification__c]
				   ,T1.[Category__c]
				   ,T1.[Controlled_Item__c]
				   ,T1.[Active__c]
				   ,T1.[OrderFromAcctLink]
				  ,T1.[Campus__c]
				  ,T1.[Department_Code__c]
				  ,T1.[Status__c]
				  ,T1.[Buy_Ratio__c]
				  ,T1.[Buy_UOM__c]
				  ,T1.[Sell_Ratio__c]
				  ,T1.[Sell_UOM__c]
				  ,T1.[ReOrder_Point__c]
				  ,T1.[ReOrder_Qty__c]
				  ,T1.[Overstock_Point__c]
				  ,T1.[Required_Qty__c]
				  ,T1.[Maintain_QOH__c]
				  ,T1.[Initial_Inventory_Count__c]
				  ,null as Legacy_QOH__c
				--  ,[zrefActive]
				  ,'Legacy_'+T1.[zrefSrc] AS Source_Table__c  --FB-01757
				  ,T1.zrefItemDetailID
				  ,T1.zrefItemID
				  ,t1.zrefCampus
		  		 
				FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1 
				LEFT JOIN GDB_Final_migration.dbo.stg_trefInvItems2 AS T2 ON T1.Legacy_ID__c = T2.Legacy_ID__c 
				LEFT JOIN GDB_Final_migration.dbo.stg_trefInvItems3 AS T3 ON T1.zrefItemDetailID=T3.zrefItemDetailID
				WHERE T2.Legacy_ID__c IS NULL AND T1.Department_Code__c IS NOT NULL AND T3.zrefItemDetailID IS null
				*/

	UNION

		SELECT DISTINCT --NEW GOING FORWARD
			T1.[OwnerId]
		   ,T1.[Legacy_ID__c]+'_A' AS Legacy_ID__c
		   ,T1.[Name__c]
		   ,T1.[Classification__c]
		   ,T1.[Category__c]
		   ,T1.[Controlled_Item__c]
		   ,'Yes' AS [Active__c]
		   ,T1.[OrderFromAcctLink]
		  ,T1.[Campus__c]
		  ,T1.[Department_Code__c]
		  ,T1.[Status__c]
		  ,T1.[Buy_Ratio__c]
		  ,T1.[Buy_UOM__c]
		  ,T1.[Sell_Ratio__c]
		  ,T1.[Sell_UOM__c]
		  ,T1.[ReOrder_Point__c]
		  ,T1.[ReOrder_Qty__c]
		  ,T1.[Overstock_Point__c]
		  ,T1.[Required_Qty__c]
		  ,T1.[Maintain_QOH__c]
		  ,T1.[Initial_Inventory_Count__c]
		  ,null as Legacy_QOH__c
		--  ,[zrefActive]
		  ,T1.[zrefSrc] AS Source_Table__c
		  ,NULL AS zrefItemDetailID
		  ,NULL AS zrefItemID
		  , NULL AS zrefCampus

  
		FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1
		WHERE T1.zrefActive='yes' AND T1.Status__c='Active'  --Fb-01757

	UNION	-- needed legacy records from duplicate file to match itemDetail ID to [tblgDBInvOrderTrans] and [tblGDBInvDispenseTrans]
		/* 	SELECT 
				T1.[OwnerId]
			   ,T1.[Legacy_ID__c] AS Legacy_ID__c
			   ,T1.[Name__c] AS [Name]
			   ,T1.[Classification__c]
			   ,T1.[Category__c]
			   ,T1.[Controlled_Item__c]
			   ,T1.[Active__c]
			   ,T1.[OrderFromAcctLink]
			  ,T1.[Campus__c]
			  ,T1.[Department_Code__c]
			  ,T1.[Status__c]
			  ,T1.[Buy_Ratio__c]
			  ,T1.[Buy_UOM__c]
			  ,T1.[Sell_Ratio__c]
			  ,T1.[Sell_UOM__c]
			  ,T1.[ReOrder_Point__c]
			  ,T1.[ReOrder_Qty__c]
			  ,T1.[Overstock_Point__c]
			  ,T1.[Required_Qty__c]
			  ,T1.[Maintain_QOH__c]
			  ,T1.[Initial_Inventory_Count__c]
			  ,null as Legacy_QOH__c
			--  ,[zrefActive]
			  ,'Legacy_'+T1.[zrefSrc] +'VE_E' AS Source_Table__c  --Fb-01757
			  ,T1.zrefItemDetailID
			  ,T1.zrefItemID
			  ,t1.zrefCampus

			FROM  GDB_Final_migration.dbo.stg_trefInvItems3 AS T1
		

			 UNION */

		--tblDogPuppyMonthlyProgressRpt
			SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'mpr-'+T1.mpr_DogFood AS Legacy_ID__c
			,T1.mpr_DogFood AS [Name] 
			,'Cainine Welfare Food' AS Classification__c
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,'Yes'  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,'Both'  AS  Campus__c	 
		 	,'PR'  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,'Unit'  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,'Unit'  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,'No'  AS  Maintain_QOH__c
 			,NULL  AS  Initial_Inventory_Count__c
			,null as Legacy_QOH__c			
			,'tblDogPuppyMonthlyProgressRpt' AS Source_Table__c
			,NULL AS zrefItemDetailID
			,NULL AS zrefItemID
			,NULL AS zrefCampus
			
			FROM GDB_KADE_Final.dbo.tblDogPuppyMonthlyProgressRpt AS T1

	union --FB00941
		--tblDogPuppyMonthlyFinalReport
			SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'pfr-'+T1.pfr_FoodBrand AS Legacy_ID__c
			,T1.pfr_FoodBrand AS [Name] 
			,'Cainine Welfare Food' AS Classification__c
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,'Yes'  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,'Both'  AS  Campus__c	 
		 	,'PR'  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,'Unit'  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,'Unit'  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,'No'  AS  Maintain_QOH__c
 			,NULL  AS  Initial_Inventory_Count__c	
			,null as Legacy_QOH__c		
			,'tblDogPuppyFinalReport' AS Source_Table__c
			,NULL AS zrefItemDetailID
			,NULL AS zrefItemID
			,NULL AS zrefCampus
			
			FROM GDB_KADE_Final.dbo.tblDogPuppyFinalReport AS T1
			
	UNION 
		--tblCWPPostWhelp --FB-00941
		SELECT DISTINCT  GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,NULL AS Legacy_ID__c
			,T1.pw_FoodType AS [Name] 
			,'Food/Additives' AS Classification__c --FB-00941/FB-01755
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,'Yes'  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,'Both'  AS  Campus__c	 
		 	,'CWND'  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,'Unit'  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,'Unit'  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,'No'  AS  Maintain_QOH__c
 			,NULL  AS  Initial_Inventory_Count__c
			,null as Legacy_QOH__c			
			,'tblCWTPostWhelp' AS Source_Table__c
			,NULL AS zrefItemDetailID
			,NULL AS zrefItemID
			,NULL AS zrefCampus
			FROM GDB_KADE_Final.dbo.tblCWTPostWhelp AS T1
	 
END --TC1 2438   4158  FINAL: 4284


BEGIN --audit

	SELECT * FROM GDB_Final_migration.dbo.IMP_INVENTORY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.IMP_INVENTORY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT * FROM GDB_Final_migration.dbo.IMP_INVENTORY
	WHERE zrefItemDetailID IN (SELECT zrefItemDetailID FROM GDB_Final_migration.dbo.IMP_INVENTORY GROUP BY zrefItemDetailID HAVING COUNT(*)>1)
	AND zrefItemDetailID IS NOT NULL
	ORDER BY zrefItemDetailID


	
	SELECT * FROM GDB_Final_migration.dbo.IMP_INVENTORY
	WHERE [NAME] IN (SELECT [NAME] FROM GDB_Final_migration.dbo.IMP_INVENTORY GROUP BY [NAME] HAVING COUNT(*)>1)
	AND Active__c='YES'

	 		sELECT Source_Table__c, COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_INVENTORY 
			GROUP BY Source_Table__c


			SELECT COUNT(*) C
			FROM GDB_Final_migration.dbo.IMP_INVENTORY 

				SELECT COUNT(*) C
			FROM GDB_Final_migration.dbo.Xtr_INVENTORY 

			
	SELECT * FROM GDB_Final_migration.dbo.IMP_INVENTORY
	WHERE [name] IN (SELECT [Name] FROM GDB_Final_migration.dbo.IMP_INVENTORY GROUP BY [Name] HAVING COUNT(*)>1) AND source_table__c <> 'trefGDBInventroyItems'
	ORDER BY Legacy_ID__c
		 
END 


---exceptions

DROP TABLE GDB_Final_migration.dbo.XTR_Inventory


SELECT T1.*
FROM GDB_Final_migration.dbo.IMP_INVENTORY AS T1
LEFT JOIN GDB_Final_migration.dbo.XTR_Inventory AS T2 ON t2.[LEGACY_ID__C]=T1.Legacy_ID__c
WHERE T2.legacy_ID__c IS NULL


SELECT T1.*
FROM GDB_Final_migration.dbo.IMP_INVENTORY AS T1
WHERE T1.Source_Table__c='trefGDBInventroyItems'