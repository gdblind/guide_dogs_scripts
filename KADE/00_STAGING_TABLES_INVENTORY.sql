BEGIN	
		-- INVENTORY UPDATE tblEquipFood and CHART_EquipFood
				--tblEquipFood associated with stored procedure and can't run an update on the data. needed to create stg tbl. 

				DROP TABLE GDB_Final_migration.dbo.stg_tblEquipFood
			
			--create stg tbl.	
				SELECT *
				INTO GDB_Final_migration.dbo.stg_tblEquipFood
				FROM GDB_KADE_Final.dbo.tblEquipFood 

				SELECT  deq_EquipFood, deq_Type, deq_Size
                FROM GDB_Final_migration.dbo.stg_tblEquipFood
				WHERE deq_EquipFood IS NULL OR deq_Type IS NULL OR deq_Size IS NULL 
 
			--update stg tbl
				UPDATE GDB_Final_migration.dbo.stg_tblEquipFood 
				SET deq_EquipFood='NULL'
				WHERE deq_EquipFood IS NULL 

				UPDATE GDB_Final_migration.dbo.stg_tblEquipFood 
				SET deq_Type='NULL'
				WHERE deq_Type IS NULL 

				UPDATE GDB_Final_migration.dbo.stg_tblEquipFood 
				SET deq_Size='NULL'
				WHERE deq_Size IS NULL 

			--chart update 
				SELECT * FROM GDB_Final_migration.dbo.CHART_EquipFood
				
				DELETE GDB_Final_migration.dbo.CHART_EquipFood
				WHERE deq_EquipFood IS NULL AND deq_Type IS NULL AND [Convert] IS NULL 

				UPDATE GDB_Final_migration.dbo.CHART_EquipFood
				SET deq_EquipFood='NULL'
				WHERE deq_EquipFood ='' 

				UPDATE GDB_Final_migration.dbo.CHART_EquipFood 
				SET deq_Type='NULL'
				WHERE deq_Type ='' 

				UPDATE GDB_Final_migration.dbo.CHART_EquipFood 
				SET deq_Size='NULL'
				WHERE deq_Size ='' 
				
				--test
					SELECT DISTINCT T1.deq_EquipFood, T1.deq_Type, T1.deq_Size, T2.deq_EquipFood, T2.deq_Type, T2.deq_Size
									,T2.[Convert], T2.Item_Name, T2.Classifaction, T1.
					FROM GDB_Final_migration.dbo.stg_tblEquipFood AS T1
					LEFT JOIN GDB_Final_migration.dbo.CHART_EquipFood  AS T2 ON T1.deq_EquipFood =T2.deq_EquipFood 
																	 AND T1.deq_Type=T2.deq_Type 
																	 AND T1.deq_Size=T2.deq_Size
			 		WHERE T2.deq_EquipFood ='dry kibble'  
					ORDER BY  T2.[Convert], T1.deq_EquipFood, T1.deq_Type, T1.deq_Size

				  --INSERT INTO GDB_Final_migration.dbo.CHART_EquipFood (deq_EquipFood, deq_Type, deq_Size, [Convert], Item_Name, [Type])
				  --	VALUES ('Dry Kibble', 'Natural Balance Fat', 'Split-feed 2 cups', 'Yes', 'Dry Kibble - Natural Balance Fat', 'Food');
	END

	BEGIN -- UPDATE DEPARTMENT CODE

		
		UPDATE GDB_KADE_Final.DBO.trefGDBInvItemsDetail  
		SET iid_DepartmentCode=RTRIM(LTRIM(iid_DepartmentCode))

	END 

--Step 1 Inventory from trefGDBInventroyItems +trefGDBInvItemsDetail
	BEGIN 

			 SELECT DISTINCT GDB_Final_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 			--,'Ii-'+CAST(T1.ii_ItemID  AS NVARCHAR(30))+ IIF(t2.iid_Campus='0','SR', 'OR')
			,case when T2.[iid_ItemDetailID] is null then 'Ii-'+CAST(T1.ii_ItemID  AS NVARCHAR(30)) 
			      else 'Ii-'+CAST(T1.ii_ItemID  AS NVARCHAR(30)) +'-'+CAST(T2.[iid_ItemDetailID] as NVARCHAR(30)) end as  Legacy_ID__c	-- Concatenate 'Ii-'+[ii_ItemId] --FB-
	  
		--	,T1.ii_Description +' '+ CAST(YEAR(T1.ii_CreatedDate) AS NVARCHAR(4)) AS  Name__c	-- Concatenate [ii_Description]+' '+ ([ii_CreatedDate],YYYY)
			--,'Medicine'  AS  Type__c	-- Medicine
			,T1.ii_Description AS Name__c
			,T4.iicl_Classification  AS  Classification__c	-- migrate value from [iicl_Classification]
			,T5.iic_Category AS  Category__c	-- migrate value from [iic_Category]
			,T1.ii_ControlledItem  AS  Controlled_Item__c	
			,'No'  AS  Active__c	--FB-00483
	 		,'iv-' +CAST(T2A.iv_VendorID AS NVARCHAR(20))   AS  OrderFromAcctLink	
			,T6.[Description]  AS  Campus__c	-- migrate value from [Description]
		 	,T2.iid_DepartmentCode  AS  Department_Code__c	 
			,T2.iid_Status  AS  Status__c	
			,T2.iid_BuyRatio  AS  Buy_Ratio__c
		 	,T8.iuom_Type AS  Buy_UOM__c	-- migrate value from [iuom_Type]
			,T2.iid_SellRatio  AS  Sell_Ratio__c	
		 	,T9.iuom_Type AS  Sell_UOM__c	-- migrate value from [iuom_Type]
			,T2.iid_ReOrderPt  AS  ReOrder_Point__c	
			,T2.iid_ReOrderQty  AS  ReOrder_Qty__c	
			,T2.iid_OverstockPt  AS  Overstock_Point__c	
			,T2.iid_RequiredQty  AS  Required_Qty__c	
			,T2.iid_MaintainQOH  AS  Maintain_QOH__c
 			,T3.inv_QtyOnHand  AS  Initial_Inventory_Count__c	--	FB-02472	
			,T2.iid_OTC AS OVER_the_Counter__c
			,T1.ii_Active AS zrefActive
			,T2.iid_Campus AS zrefCampus
			,T1.ii_ItemID AS zrefItemID
			,T2.iid_ItemDetailID AS zrefItemDetailID
			,'trefGDBInventroyItems' AS zrefSrc
			--,row_number() over (order by T1.ii_Description) as SEQN

			INTO GDB_Final_migration.dbo.stg_trefInvItems --drop table GDB_Final_migration.dbo.stg_trefInvItems
			FROM GDB_KADE_Final.dbo.trefGDBInventroyItems AS T1
			LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvItemsDetail AS T2 ON T2.iid_ItemID = T1.ii_ItemID
			LEFT JOIN GDB_KADE_Final.dbo.tblGDBVendor AS T2A ON T2A.iv_VendorID=T2.iid_VendorID
			LEFT JOIN GDB_KADE_Final.dbo.tblGDBInventroy AS T3 ON T3.inv_ItemDetailID=T2.iid_ItemDetailID
			LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvClassification AS T4 ON T1.ii_ClassificationID=T4.iicl_ClassificationID
			LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvCategories AS T5 ON T1.ii_ClassCatID=T5.iic_CategoryID
			LEFT JOIN GDB_KADE_Final.dbo.trefFacility AS T6 ON T6.FacilityID=T2.iid_Campus
		 	LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvDepartments AS T7 ON T7.iidc_DeptCode=T2.iid_DepartmentCode
		 	LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvUOM AS T8 ON T8.iuom_ID=T2.iid_BuyUOM
		 	LEFT JOIN GDB_KADE_Final.dbo.trefGDBInvUOM AS T9 ON T9.iuom_ID=T2.iid_SellUOM
			--WHERE T2.iid_ItemDetailID ='2589'
		




/*
		--look at with Val
			SELECT *
		  	FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 

			SELECT *
		  	FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			WHERE zrefItemDetailID IN 
			(SELECT zrefItemDetailID FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			GROUP BY zrefItemDetailID HAVING count(*) >1)
			ORDER BY zrefItemDetailID 

	
				SELECT *
				INTO GDB_Final_migration.dbo.stg_InventoryTestDups
		  	FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			WHERE zrefItemID IN 
			(SELECT zrefItemID FROM GDB_Final_migration.dbo.stg_trefInvItems_test
			GROUP BY zrefItemID HAVING count(*) >1) --AND zrefItemID = '2777'
			ORDER BY zrefItemID
			

			SELECT COUNT(*)
			FROM GDB_KADE_Final.dbo.tblVetMedication AS T1
			INNER JOIN GDB_KADE_Final.dbo.trefGDBInventroyItems AS T2 ON T2.ii_ItemID=T1.vdm_MedCode
			INNER JOIN GDB_KADE_Final.dbo.tblVetEntry AS T3 ON T3.vdl_VetEntryID=T1.vdm_VetEntryID
			INNER JOIN GDB_KADE_Final.dbo.trefGDBInvItemsDetail AS T4 ON T4.iid_ItemID = T2.ii_ItemID
			INNER JOIN GDB_Final_migration.[dbo].[stg_trefInvItems_test] AS T5 ON T5.[zrefItemID] = T1.vdm_MedCode 
			WHERE T3.vdl_Facility = t4.iid_Campus

			                                                                                         
			                                                                                     

			--count tblVetMedication 777,923 with GDBInventroyItems 775,685 with stage inventory table 1,360,058

			*/


		--check duplicates

		
/*--Step 2	
		SELECT *
		--INTO GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems  -- Drop table GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems
		FROM GDB_Final_migration.dbo.stg_trefInvItems
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.stg_trefInvItems
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 

		
--Step 3
	SELECT DISTINCT t1.idt_ItemDetailID AS ItemDetailID
	INTO GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry --drop table GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry
	FROM GDB_KADE_Final.dbo.tblGDBInvDispenseTrans AS T1
	UNION
	SELECT DISTINCT T2.iot_ItemDetailID AS ItemDetailID
	FROM GDB_KADE_Final.dbo.tblGDBInvOrderTrans AS T2

	SELECT COUNT(*) FROM GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry  -- tc2: 1263

--Step 4
		SELECT 
				T1.[OwnerId]
			  ,T1.[Legacy_ID__c]
			  ,T1.[Name__c]
			  ,T1.[Classification__c]
			  ,T1.[Category__c]
			  ,T1.[Controlled_Item__c]
			  ,T1.[Active__c]
			  ,T1.[OrderFromAcctLink]
			  ,T1.[Campus__c]
			  ,T1.[Department_Code__c]
			  ,T1.[Status__c]
			  ,T1.[Buy_Ratio__c]
			  ,T1.[Buy_UOM__c]
			  ,T1.[Sell_Ratio__c]
			  ,T1.[Sell_UOM__c]
			  ,T1.[ReOrder_Point__c]
			  ,T1.[ReOrder_Qty__c]
			  ,T1.[Overstock_Point__c]
			  ,T1.[Required_Qty__c]
			  ,T1.[Maintain_QOH__c]
			  ,T1.[Initial_Inventory_Count__c]
			  ,T1.[zrefActive]
			  ,T1.[zrefCampus]
			  ,T1.[zrefItemID]
			  ,T1.[zrefItemDetailID]
			  ,T1.[zrefSrc]
		INTO GDB_Final_migration.dbo.stg_trefInvItems2 --drop table GDB_Final_migration.dbo.stg_trefInvItems2
		FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1 
		LEFT JOIN GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems AS T2 ON T1.Legacy_ID__c = T2.Legacy_ID__c
		WHERE T2.Legacy_ID__c IS NULL

		UNION
		SELECT
				T1.[OwnerId]
			  ,T1.[Legacy_ID__c]
			  ,T1.[Name__c]
			  ,T1.[Classification__c]
			  ,T1.[Category__c]
			  ,T1.[Controlled_Item__c]
			  ,T1.[Active__c]
			  ,T1.[OrderFromAcctLink]
			  ,T1.[Campus__c]
			  ,T1.[Department_Code__c]
			  ,T1.[Status__c]
			  ,T1.[Buy_Ratio__c]
			  ,T1.[Buy_UOM__c]
			  ,T1.[Sell_Ratio__c]
			  ,T1.[Sell_UOM__c]
			  ,T1.[ReOrder_Point__c]
			  ,T1.[ReOrder_Qty__c]
			  ,T1.[Overstock_Point__c]
			  ,T1.[Required_Qty__c]
			  ,T1.[Maintain_QOH__c]
			  ,T1.[Initial_Inventory_Count__c]
			  ,T1.[zrefActive]
			  ,T1.[zrefCampus]
			  ,T1.[zrefItemID]
			  ,T1.[zrefItemDetailID]
			  ,T1.[zrefSrc]
		FROM GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems AS T1
		WHERE T1.Department_Code__c='Veterinary'
 
	
-- Step 5
		        
		SELECT DISTINCT
				T1.[OwnerId]
			  ,T1.[Legacy_ID__c] +'_'+CAST(T2.ItemDetailID AS VARCHAR(20)) AS [Legacy_ID__C]
			  ,T1.[Name__c]
			  ,T1.[Classification__c]
			  ,T1.[Category__c]
			  ,T1.[Controlled_Item__c]
			  ,T1.[Active__c]
			  ,T1.[OrderFromAcctLink]
			  ,T1.[Campus__c]
			  ,T1.[Department_Code__c]
			  ,T1.[Status__c]
			  ,T1.[Buy_Ratio__c]
			  ,T1.[Buy_UOM__c]
			  ,T1.[Sell_Ratio__c]
			  ,T1.[Sell_UOM__c]
			  ,T1.[ReOrder_Point__c]
			  ,T1.[ReOrder_Qty__c]
			  ,T1.[Overstock_Point__c]
			  ,T1.[Required_Qty__c]
			  ,T1.[Maintain_QOH__c]
			  ,T1.[Initial_Inventory_Count__c]
			  ,'InActive' AS [zrefActive]
			  ,T1.[zrefCampus]
			  ,T1.[zrefItemID]
			  ,T1.[zrefItemDetailID]
			  ,T1.[zrefSrc]
		INTO GDB_Final_migration.dbo.stg_trefInvItems3 --drop table GDB_Final_migration.dbo.stg_trefInvItems3
		FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1
		INNER JOIN GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry AS T2 ON T1.zrefItemDetailID = T2.ItemDetailID
		WHERE T2.ItemDetailID NOT IN (SELECT DISTINCT T3.ItemDetailID FROM GDB_Final_migration.dbo.stg_Inventory_ItemDetail_From_Entry AS T3
										INNER JOIN GDB_Final_migration.dbo.stg_trefInvItems2 AS T4 ON T3.ItemDetailID=T4.zrefItemDetailID)
			--TC2: 33
*/

-- Audits
		SELECT T1.* 
		FROM GDB_Final_migration.dbo.stg_trefInvItems AS T1 
		LEFT JOIN GDB_Final_migration.dbo.stg_trefInvItems2 AS T2 ON T1.Legacy_ID__c = T2.Legacy_ID__c
		WHERE T2.Legacy_ID__c IS NULL


		
		SELECT COUNT(*) FROM GDB_Final_migration.dbo.stg_DUPLICATES_trefInvItems
		SELECT COUNT(*) FROM GDB_Final_migration.dbo.stg_trefInvItems
		SELECT COUNT(*) FROM GDB_Final_migration.dbo.stg_trefInvItems2


		SELECT * FROM GDB_Final_migration.dbo.stg_trefInvItems2
		WHERE zrefItemDetailID = '1509'

		SELECT * FROM GDB_Final_migration.dbo.stg_trefInvItems
		WHERE zrefItemDetailID = '1509'

		SELECT * FROM GDB_Final_migration.dbo.stg_trefInvItems
		WHERE zrefItemID = '1356'

		SELECT * FROM GDB_KADE_Final.DBO.trefGDBInvItemsDetail
		WHERE iid_ItemDetailID = '1104'

--dup checks
		SELECT *
		FROM GDB_Final_migration.dbo.stg_trefInvItems2
			WHERE zrefItemDetailID IN 
			(SELECT zrefItemDetailID FROM GDB_Final_migration.dbo.stg_trefInvItems2
			GROUP BY zrefItemDetailID HAVING count(*) >1)
			ORDER BY zrefItemDetailID 

		SELECT *
		FROM GDB_Final_migration.dbo.stg_trefInvItems2
			WHERE zrefItemID IN 
			(SELECT zrefItemID FROM GDB_Final_migration.dbo.stg_trefInvItems2
			GROUP BY zrefItemID HAVING count(*) >1)
			ORDER BY zrefItemID 

			SELECT *
		FROM GDB_Final_migration.dbo.stg_trefInvItems2
			WHERE Legacy_ID__c IN 
			(SELECT Legacy_ID__c FROM GDB_Final_migration.dbo.stg_trefInvItems2
			GROUP BY Legacy_ID__c HAVING count(*) >1)
			ORDER BY Legacy_ID__c 

SELECT * FROM GDB_Final_migration.dbo.stg_trefInvItems
