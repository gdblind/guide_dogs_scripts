/*OBJECTIVE****************************************************/
	--DATA MAP PROCESS TO:
		--1) gather LIST OF CUSTOM FIELDS 
		--2) Identify CHANGES BETWEEN maps. 
/*PROCESS******************************************************/
--In MS ACCESS
	--1.Use Conversion Toolkit to import the map to the Access database.
	--2.Remove exess tables (templates, index, reference, etc)	
--In MS SQL SERVER
	--03.Create new SQL SErver database called "CLIENTINITIALS_T1_maps". IF there are multiple conversion, use T2, FN. e.g. HILLEL_T1_maps, HILLEL_T2_maps, HILLEL_T3_maps.
	--04.Import all the maps from the conversion toolkit into the new database created above. 
	--05.Install stored prodecude called InsertTables
	--06.Create EMPTY table called "_client_maps".  Use One of the Imported maps to get the shema
	--07.Run code to insert all records from all maps into the __DATAMAP combined table
	--08.Check that all maps have been loaded 
	--09.Produce a list of all custom fields. 
	--10.Select results and Copy/Paste into REF_CustomFields worksheet. 
 --- NOTE : to DELTE ALL TABLES from a DATABASE, exceucte the following. 
	/*
		USE GDB_TC2_maps_Delivery
		GO
		EXEC sp_MSforeachtable @command1 = "DROP TABLE ?"
		GO
    */
	  /*5.Install: Store procedure to compile all maps into a new data table and produce a list of custom fields in the data map.*/

				USE [GDB_Final_MAPS]
		  		GO 
 
				CREATE PROCEDURE InsertTables  
				(
				@ExistingTableName nvarchar(128) 
				)
				AS
				BEGIN
				  SET NOCOUNT ON;
 				  DECLARE @Sql NVARCHAR(MAX);
				  DECLARE @TblName NVARCHAR(255);
	  
 					SET @Sql = N' INSERT INTO  dbo.[__DATAMAPS]
						   (Source_Table, OrderNo ,Source_Field  ,Sample_1  ,Sample_2 ,Sample_3 ,[#_of_Occurrences] ,[%_of_Records] ,[#_of_Unique_Values]
							  ,[%_Unique] ,Min_Length ,Max_Length, [Source_Data_Type] ,[Convert] ,Coded   
							  ,SF_Object ,SF_Field_API ,SF_Field_Label, Data_Type ,Custom ,Translation_Rules ,Chart_of_Values 
							  ,Coded_2 ,SF_Object_2 ,SF_Field_API_2, SF_Field_Label_2 , Data_Type_2 ,Custom_2 ,Translation_Rules_2 ,Chart_of_Values_2 ,Notes)  
						   SELECT ''' +@ExistingTableName+'''  , OrderNo, Source_Field ,Sample_1 ,Sample_2 ,Sample_3 ,[#_of_Occurrences] ,[%_of_Records],[#_of_Unique_Values] ,
							   [%_Unique] ,Min_Length ,Max_Length, [Source_Data_Type] ,[Convert] ,Coded 
							  ,SF_Object, SF_Field_API ,SF_Field_Label, Data_Type ,Custom ,Translation_Rules ,Chart_of_Values
							  ,Coded_2 ,SF_Object_2 ,SF_Field_API_2 ,SF_Field_Label_2 , Data_Type_2 ,Custom_2 ,Translation_Rules_2 ,Chart_of_Values_2 ,Notes
						   FROM ' + QUOTENAME(@ExistingTableName)  +
						 ' WHERE Source_field IS NOT NULL ' 
	 				EXECUTE sp_executesql  @Sql
				END;
				GO

	BEGIN	/*6.Create empty table that will have all combined map info ****/
					USE [GDB_Final_MAPS]
					DROP TABLE [dbo].[__DATAMAPS]       
                   
				    CREATE TABLE [dbo].[__DATAMAPS]
                        (
                          [Source_Table] [NVARCHAR](255) NULL ,
                          [OrderNo] [NVARCHAR](255) NULL,
						  [Source_Field] [NVARCHAR](255) NULL ,
                          [Sample_1] [NVARCHAR](255) NULL ,
                          [Sample_2] [NVARCHAR](255) NULL ,
                          [Sample_3] [NVARCHAR](255) NULL ,
                          [#_of_Occurrences] [FLOAT] NULL ,
                          [%_of_Records] [FLOAT] NULL ,
                          [#_of_Unique_Values] [FLOAT] NULL ,
                          [%_Unique] [FLOAT] NULL ,
                          [Min_Length] [FLOAT] NULL ,
                          [Max_Length] [FLOAT] NULL ,
						  [Source_Data_Type] [NVARCHAR](255) NULL ,
                          [Convert] [NVARCHAR](255) NULL ,
                          [Coded] [NVARCHAR](255) NULL ,
                          [SF_Object] [NVARCHAR](255) NULL ,
                          [SF_Field_Label] [NVARCHAR](255) NULL ,
                          [SF_Field_API] [NVARCHAR](255) NULL ,
                          [Data_Type] [NVARCHAR](255) NULL ,
                          [Custom] [NVARCHAR](255) NULL ,
                          [Translation_Rules] [NVARCHAR](Max) NULL ,
                          [Chart_of_Values] [NVARCHAR](255) NULL ,
                          [Coded_2] [NVARCHAR](255) NULL ,
                          [SF_Object_2] [NVARCHAR](255) NULL ,
                          [SF_Field_Label_2] [NVARCHAR](255) NULL ,
                          [SF_Field_API_2] [NVARCHAR](255) NULL ,
                          [Data_Type_2] [NVARCHAR](255) NULL ,
                          [Custom_2] [NVARCHAR](255) NULL ,
                          [Translation_Rules_2] [NVARCHAR](Max) NULL ,
                          [Chart_of_Values_2] [NVARCHAR](255) NULL ,
                          [Notes] [NVARCHAR](Max) NULL
                        )
                    ON  [PRIMARY];
	END 

			/*7.Run code to insert all records from all maps into the __DATAMAP combined table  */
				
				--7.1.Make sure the following query is pulling all the required tables 
					--remove in case it exist
					DROP TABLE #Tables
					GO 
					--create #tables tmp
					SELECT name
					INTO #Tables
					FROM sys.tables
					WHERE name !='__DATAMAPS' AND name NOT LIKE 'REF_%' AND name NOT LIKE 'CHART_%' AND name !='Index' AND name NOT LIKE '%$_ImportErrors'--exclude combined table.
					GO 
					--check #tables
					SELECT * FROM #Tables ORDER BY name
					GO 
				

				--7.2. Now use that temp table to pass table names to your procedure
				TRUNCATE TABLE dbo.__DATAMAPS
				SELECT * FROM dbo.__DATAMAPS ORDER BY Source_Table
                
			    BEGIN 
                    DECLARE @TableName NVARCHAR(128);

                    WHILE EXISTS ( SELECT   1
                                   FROM     #Tables )
                        BEGIN
                            SELECT TOP 1
                                    @TableName = name
                            FROM    #Tables
							ORDER BY NAME;

                            EXECUTE InsertTables @TableName;

                            DELETE  FROM #Tables
                            WHERE   name = @TableName;
                        END;
                END; 

		/*8.Check that all maps have been loaded */ 
		SELECT DISTINCT source_table FROM dbo.__DATAMAPS
		ORDER BY source_table
	
			SELECT * FROM dbo.__DATAMAPS
			ORDER BY source_table
	
		/*9.Produce a list of all custom fields. */ 
				DROP TABLE [GDB_Final_MAPS].DBO.__CUSTOM_FIELDS

				SELECT  Source_Table ,
						[OrderNo],
						[Source_Field] ,
						[Sample_1],
						[Sample_2],
						[Sample_3],
						[#_of_Unique_Values],
						[Min_Length],
						[Max_Length],
						[SF_Object] ,
						[SF_Field_Label] ,
						[SF_Field_API] ,
 						[Data_Type] AS Data_Type ,
						[Custom]
			  	INTO [GDB_Final_MAPS].DBO.__CUSTOM_FIELDS
				FROM    dbo.__DATAMAPS
				WHERE   [Source_Field] IS NOT NULL
						 AND Custom like '%Yes%'  
				UNION ALL
				SELECT  Source_Table ,
						[OrderNo],
						[Source_Field] ,
						[Sample_1],
						[Sample_2],
						[Sample_3],
						[#_of_Unique_Values],
						[Min_Length],
						[Max_Length],
						[SF_Object_2] AS [SF_Object] ,
						[SF_Field_Label_2] AS [SF_Field_Label],
						[SF_Field_API_2] AS [SF_Field_API] ,
						[Data_Type_2] AS [Data_Type],
						[Custom_2] AS [Custom]
				FROM    dbo.__DATAMAPS
				WHERE   [Source_Field] IS NOT NULL
						 AND [Custom_2] like '%Yes%' 
				ORDER BY [SF_Object] ,
						[SF_Field_Label];
 
				--list of all custom fields. 
				SELECT Source_Table, Source_Field, SF_Object, SF_Field_Label, SF_Field_API,	Data_Type
				FROM [GDB_Final_MAPS].DBO.__CUSTOM_FIELDS
				ORDER BY SF_Object, SF_Field_Label
				
				
				--picklist fields
				SELECT  Source_Table, OrderNo, SF_Object, SF_Field_API
				FROM [GDB_Final_MAPS].DBO.__CUSTOM_FIELDS
				--WHERE  Data_Type LIKE '%pick%'  
				ORDER BY Source_Table, OrderNo, SF_Object, SF_Field_API

	  		/*10.Produce a list of all  fields. */ 
		
				SELECT  Source_Table ,
						[Source_Field] ,
						[#_of_Unique_Values],
						[Min_Length],
						[Max_Length],
						[SF_Object] ,
						[SF_Field_Label] ,
						[SF_Field_API] ,
 						[Data_Type] AS Data_Type ,
						[Custom],
						[Convert],
						[Translation_Rules],
						[Notes]
						
			    INTO	[GDB_Final_MAPS].DBO.__T1_ALL_FIELDS  --DROP TABLE GDB_TC2_maps_Delivery.DBO.__T1_ALL_FIELDS
				FROM    [GDB_Final_MAPS].dbo.__DATAMAPS
				WHERE   [Source_Field] IS NOT NULL
						 
				UNION ALL
				SELECT  Source_Table ,
						[Source_Field] ,
						[#_of_Unique_Values],
						[Min_Length],
						[Max_Length],
						[SF_Object_2] AS [SF_Object] ,
						[SF_Field_Label_2] AS [SF_Field_Label],
						[SF_Field_API_2] AS [SF_Field_API] ,
						[Data_Type_2] AS [Data_Type],
						[Custom_2] AS [Custom],
						[Convert],
						[Translation_Rules_2] AS [Translation_Rules],
						[Notes]
				FROM    [GDB_Final_MAPS].dbo.__DATAMAPS
				WHERE   [Source_Field] IS NOT NULL AND [SF_Field_API_2] !=''
						 
				ORDER BY [SF_Object] ,
						[SF_Field_Label];
  	 
	 /*11.View a list of all  fields. */ 

		 SELECT DISTINCT SF_Object 
		 FROM [GDB_Final_MAPS].[dbo].[__T1_ALL_FIELDS] 
		 WHERE SF_Object IS NOT null
		 ORDER BY SF_Object

		  SELECT * 
		  FROM [GDB_Final_MAPS].[dbo].[__T1_ALL_FIELDS] 
		  WHERE SF_Object='OBERVATION'


		UPDATE  [GDB_Final_MAPS].[dbo].[__T1_ALL_FIELDS]
		SET SF_Object='VET RECORD'
		WHERE SF_Object='VET RECORDS'

		SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION,
			   COLUMN_DEFAULT, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH,
			   NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE,
			   DATETIME_PRECISION
		FROM GDB_TC2_kade.INFORMATION_SCHEMA.COLUMNS
 
		ORDER BY TABLE_NAME, ORDINAL_POSITION

		SELECT * FROM  [GDB_Final_MAPS].[dbo].[__T1_ALL_FIELDS] WHERE SF_Object IS NOT NULL AND Custom='Yes' AND Data_Type   LIKE '%mult%'
	 
	 SELECT DISTINCT SF_Object 
	 FROM  [GDB_Final_MAPS].dbo.__DATAMAPS
	 WHERE SF_Object LIKE '% %'
	 ORDER BY SF_Object 
	 
	 SELECT DISTINCT SF_Object_2 
	 FROM  [GDB_Final_MAPS].dbo.__DATAMAPS
	 WHERE SF_Object_2 LIKE '% %' 
	 ORDER BY SF_Object_2  NPSP__ADDRESS__C 
	 CAMPAIGN 
	  UPDATE GDB_TC2_maps.dbo.__DATAMAPS SET SF_Object_2=REPLACE(SF_Object_2,' ','_')  	 WHERE SF_Object_2 ='DOG NAME'
			