USE [GDB_FINAL_Migration]
GO

SELECT T7.[ttc_TestingID]
      ,T7.[2ndry Reinforcement] AS x2ndry_Reinforcement__c
      ,T7.[Attitude/Rapport] AS Attitude_Rapport__c
      ,T7.[Body Handling(Head/Mouth/Layover)] AS Body_Handling_Head_Mouth_Layover__c
      ,T7.[Body Handling(Head/Mouth/Stand/Rollover)] AS Body_Handling_Head_Mouth_Stand_Rollover__c
      ,T7.[Body Position] AS Body_Position__c
      ,IIF(T7.[Casual Greeting-Release] IS NOT NULL, T7.[Casual Greeting-Release], T7.[Casual Greeting - Release]) AS Casual_Greeting_Release__c
      ,T7.[Casual Greeting-Release]
      ,T7.[Clearances/Drop Offs] AS Clearances_Drop_Offs__c
      ,T7.[Clearances/Moving Obstacles] AS Clearances_Moving_Obstacles__c
      ,T7.[Clearances/Obstacles] AS Clearances_Obstacles__c
      ,T7.[Clearances/Stationary Obstacles] Clearances_Stationary_Obstacles__c
      ,T7.[Collar Response] AS Collar_Response__c
      ,T7.[Come Recall] AS Come_Recall__cc
      ,T7.[Command Response] AS Command_Response__c
      ,T7.[Crossings] AS Crossings__c
      ,T7.[Down Curbs] AS Down_Curbs__c
      ,T7.[Down Stay] AS Down_Stay__c
      ,T7.[Downs] AS Downs__c
      ,T7.[Elevator] AS Elevator__c
      ,T7.[Escalator] AS Escalator__c
      ,T7.[Focus] AS Focus__c
      ,T7.[Food Refusal] AS Food_Refusal__c
      ,T7.[Food Refusal-Food on Ground] AS Food_Refusal_Food_on_Ground__c
      ,T7.[Food Refusal-Offered by tester] AS Food_Refusal_Offered_by_Tester__c
      ,T7.[Greeting-Tester and dog] AS Greeting_tester_and_Dog__c
      ,T7.[Greeting-Tester only (no dog)] AS Greeting_Tester_only_no_dog__c
      ,T7.[Head Collar] AS Head_Collar__c
      ,T7.[Heel] AS Heel__c
      ,T7.[Heel Recall] AS Heel_Recall__c
      ,T7.[In Harness � Down and Sit] AS In_Harness_Down_Sit__c
      ,T7.[Informal Come] AS Informal_Come__c
      ,T7.[Initiative] AS Initiative__c
      ,T7.[Lead] AS Lead__c
      ,T7.[Lead in Block] AS Lead_in_Block__c
      ,T7.[Line] AS Line__c
      ,T7.[Line in Block] AS Line_in_Block__c
      ,T7.[Obedience on Route] AS Obedience_on_Route__c
      ,T7.[Obstacles] AS Obstacles__c
      ,T7.[Off Leash Down] AS Off_Leash_Down__c
      ,T7.[Off Leash Recall] AS Off_Leash_recall__c
      ,T7.[Off Leash Sit] AS Off_Leash_Sit__c
      ,T7.[Release] AS Release__c
      ,T7.[Reposition to Heel] AS Reposition_to_Heel__c
      ,T7.[Roll Over] AS Roll_Over__c
      ,T7.[Seating Positioning / Collar Response] AS Seating_Positioning_Collar_Response__c
      ,T7.[Sidewalkless] AS Sidewalkless__c
      ,T7.[Sit Stay] AS Sit_Stay__c
      ,T7.[Sits] AS Sits__c
      ,T7.[Stairs - Down] AS Stairs_Down__c
      ,T7.[Stairs - Up] AS Stairs_Up__c
      ,T7.[Stand] AS Stand__c
      ,T7.[Street Edge] AS Street_edge__c
      ,T7.[Surfaces] AS Surfaces__c
      ,T7.[Turns] AS Turns__c
      ,T7.[Turns - Moving] AS Turns_Moving__c
      ,T7.[Turns -Stationary] AS Turns_Stationary__c
      ,T7.[Up Curbs] AS Up_Curbs__c
  FROM [dbo].[stg_Training_Test_Cat1] AS t7
	WHERE t7.[Casual Greeting - Release] IS NOT NULL

GO


