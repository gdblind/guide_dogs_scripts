	SELECT DISTINCT
					CAST(zrefAccountId AS NVARCHAR(30)) AS [npsp__Household_Account__r:Legacy_Id__c] 
					,'add-' + CAST(ad_AddressID AS NVARCHAR(20)) AS Legacy_ID__c --+ '_' + CAST(ad_AddressDetailID AS NVARCHAR(20)) AS Legacy_Id__c
					,npsp__Default_Address__c
					,npsp__Address_Type__c	
					,add_Address AS npsp__MailingStreet__c	
					,add_Address2 AS npsp__MailingStreet2__c
					,add_City AS npsp__MailingCity__c	
					,add_State AS npsp__MailingState__c	
					,CAST(add_ZipCode AS NVARCHAR(30)) AS npsp__MailingPostalCode__c	
					,add_Country AS npsp__MailingCountry__c	
					,add_USCounty AS npsp__County_Name__c	
					,add_Directions AS Directions__c 
		
					,ad_AddressID AS zrefID
					,'tblAddress' zrefSource
					INTO GDB_TC2_migration.dbo.stg_Address_test -- drop table GDB_TC2_migration.dbo.stg_Address_test
				-- INTO GDB_TC2_migration.dbo.IMP_ADDRESS
				FROM  GDB_TC2_migration.dbo.stg_Address

					UNION 
			SELECT  DISTINCT	
					CAST(T2.zrefAccountId AS NVARCHAR(30)) AS [npsp__Household_Account__r:Legacy_Id__c] 
					,MAX('addH-'+CAST(T1.add_AddressHistID  AS NVARCHAR(20)))  AS  Legacy_ID__c		-- Concatenate with "addH-"&[add_AddressID]
  					,'FALSE' AS  npsp__Default_Address__c	-- FALSE
					,T3.[Description]  AS  npsp__Address_Type__c	
					,T1.add_Address  AS  npsp__MailingStreet__c	
					,T1.add_Address2  AS  npsp__MailingStreet2__c	
					,T1.add_City  AS  npsp__MailingCity__c	
					,CASE WHEN T1.add_State IS NULL THEN T1.add_CountryRegion ELSE T1.add_State END AS npsp__MailingState__c	-- If [add_State] is null migrate [add_CountryRegion] to State/Province
					,CAST(CASE WHEN T1.add_Zip IS NULL THEN T1.add_CountryCode ELSE T1.add_Zip END  AS NVARCHAR(30))  AS   npsp__MailingPostalCode__c	-- If [add_Zip] is null migrate [add_CountryCode] to Zip/Postal Code
				 	,T1.add_Country  AS  npsp__MailingCountry__c	
			 		,MAX(T1.add_USCounty)  AS  npsp__County_Name__c	
					,NULL AS Directions__c			--filler for other tbl.
					,MAX(T1.add_AddressHistID) AS zrefId
					,'tblAddressHistory' zrefSource 
			FROM GDB_TC2_kade.dbo.tblAddressHistory AS T1
			INNER JOIN GDB_TC2_kade.dbo.tblPerson AS T2 ON T1.add_PersonID=T2.psn_PersonID
			LEFT JOIN GDB_TC2_kade.DBO.trefAddressCode AS T3 ON T1.add_AddressCode=T3.AddressCode
			WHERE T1.add_AddressHistID NOT IN (SELECT T11.add_AddressHistID FROM GDB_TC2_migration.dbo.stg_Address AS T10 
												INNER JOIN GDB_TC2_Kade.dbo.tblAddressHistory AS T11 ON T11.add_Address = T10.add_Address 
												AND T11.add_City = T10.add_City AND T10.ad_PersonID = T11.add_PersonID 
												AND T10.zrefAddressCode1=T11.add_AddressCode)
			GROUP BY CAST(T2.zrefAccountId AS NVARCHAR(30)),T3.Description,T1.add_Address, T1.add_Address2, T1.add_City,
						CASE WHEN T1.add_State IS NULL THEN T1.add_CountryRegion ELSE T1.add_State END, 
						CAST(CASE WHEN T1.add_Zip IS NULL THEN T1.add_CountryCode ELSE T1.add_Zip END  AS NVARCHAR(30)),
						T1.add_Country
			--ORDER BY npsp__MailingStreet__c

				-- Duplicate check

				SELECT * FROM GDB_TC2_migration.dbo.stg_Address_test
			WHERE [Legacy_ID__c] IN (SELECT [Legacy_ID__c] FROM GDB_TC2_migration.dbo.stg_Address_test 
			GROUP BY [Legacy_ID__c] HAVING COUNT(*)>1)
			ORDER BY [npsp__Household_Account__r:Legacy_Id__c]
			AND ad_addressid ='1232'

			
				SELECT * FROM GDB_TC2_migration.dbo.stg_Address_test
			WHERE [npsp__Household_Account__r:Legacy_Id__c] IN (SELECT [npsp__Household_Account__r:Legacy_Id__c] 
			FROM GDB_TC2_migration.dbo.stg_Address_test 
			GROUP BY [npsp__Household_Account__r:Legacy_Id__c],npsp__MailingStreet__c HAVING COUNT(*)>1)
			ORDER BY [npsp__Household_Account__r:Legacy_Id__c]